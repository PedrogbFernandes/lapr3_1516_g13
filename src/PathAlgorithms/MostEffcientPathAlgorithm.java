/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PathAlgorithms;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import Graph.Edge;
import Graph.Graph;
import Graph.GraphAlgorithms;
import Graph.Vertex;
import java.util.Iterator;
import java.util.LinkedList;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Section;
import lapr3_1516_g13.model.Vehicle;

/**
 *
 * @author pedro
 */
public class MostEffcientPathAlgorithm implements PathAlgorithms {

    public MostEffcientPathAlgorithm() {

    }

    public double[] bestPath(Graph<Junction, List<Section>> graph, Junction start, Junction end, Vehicle v, Deque<Junction> path, List<Section> listSection) {
        Vertex<Junction, List<Section>> vOrig = graph.getVertex(start);
        Vertex<Junction, List<Section>> vDest = graph.getVertex(end);
        double[] results = new double[2];

        if (vOrig == null || vDest == null) {
            return null;
        }

        int nverts = graph.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];
        double[] times = new double[nverts];
        List<Section> sectionAux = new ArrayList<>();

        for (int i = 0; i < nverts; i++) {
            times[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }
        int aux = 0;
        if (vOrig.getKey() < vDest.getKey()) {
            shortestPathLength(graph, vOrig, visited, pathKeys, dist, times, v, sectionAux);
            aux = vDest.getKey();
        } else {
            shortestPathLength(graph, vDest, visited, pathKeys, dist, times, v, sectionAux);
            Junction x = start;
            start = end;
            end = x;
            aux = vOrig.getKey();

        }
       
        double lengthPath = times[aux];
       
        double energyPath = dist[aux];
      
        LinkedList<Vertex<Junction, List<Section>>> listV = new LinkedList<>();

        if (lengthPath != Double.MAX_VALUE) {
            getPathVertexs(graph, start, end, pathKeys, listV);
            getPath(graph, start, end, pathKeys, path, listSection, sectionAux, listV);
            results[0] = energyPath;
            results[1] = lengthPath;
            return results;
        }
        return null;
    }

    private static <V, E> void shortestPathLength(Graph<V, E> g, Vertex<V, E> vOrig,
            boolean[] visited, int[] pathKeys, double[] dist, double[] energy, Vehicle car, List<Section> sectionAux) {
        Section s1 = new Section();
        energy[vOrig.getKey()] = 0;

        while (vOrig != null) {
            visited[vOrig.getKey()] = true;
            Iterator<Edge<V, E>> outVerts = g.outgoingEdges(vOrig).iterator();
            Iterator<Edge<V, E>> inVerts = g.incomingEdges(vOrig).iterator();

            while (outVerts.hasNext()) {
                Edge<V, E> edge = outVerts.next();
                Vertex<V, E> v = edge.getVDest();
                List<Section> list = (List<Section>) edge.getElement();

                double time = 0;
                double energy_section = Double.MAX_VALUE;

                for (Section s : list) {

                    if (s.getDirection().equalsIgnoreCase("bidirectional") || s.getDirection().equalsIgnoreCase("direct")) {
                        double[] results = s.lowestComsumption(car, true);
                        
                        if (results[1] < energy_section) {
                            time = results[0];
                            s1 = s;
                            energy_section = results[1];

                        }

                    }
                }

                if (!visited[v.getKey()] && energy[v.getKey()] > energy[vOrig.getKey()] + energy_section) {
                    dist[v.getKey()] = dist[vOrig.getKey()] + time;
                    energy[v.getKey()] = energy[vOrig.getKey()] + energy_section;
                    pathKeys[v.getKey()] = vOrig.getKey();
                    sectionAux.add(s1);
                }
            }

            while (inVerts.hasNext()) {
                Edge<V, E> edge = inVerts.next();

                Vertex<V, E> v = edge.getVOrig();
                List<Section> list = (List<Section>) edge.getElement();

                double time = 0;
                double energy_section = Double.MAX_VALUE;

                for (Section s : list) {

                    if (s.getDirection().equalsIgnoreCase("bidirectional") || s.getDirection().equalsIgnoreCase("reverse")) {
                        double[] results = s.lowestComsumption(car, false);
                        
                        if (results[1] < energy_section) {
                            time = results[0];
                            s1 = s;
                            energy_section = results[1];

                        }

                    }
                }

                if (!visited[v.getKey()] && energy[v.getKey()] > energy[vOrig.getKey()] + energy_section) {
                    dist[v.getKey()] = dist[vOrig.getKey()] + time;
                    energy[v.getKey()] = energy[vOrig.getKey()] + energy_section;
                    pathKeys[v.getKey()] = vOrig.getKey();
                    sectionAux.add(s1);
                }

            }

            vOrig = null;
            double menor = Double.MAX_VALUE;
            for (Vertex<V, E> vdist : g.vertices()) {
                
                if (!visited[vdist.getKey()]) {
                    
                    if (energy[vdist.getKey()] < menor) {
                        menor = energy[vdist.getKey()];
                        vOrig = vdist;
                    }
                }
            }
        }
    }

    private static <V, E> void getPath(Graph<V, E> g, V voInf, V vdInf,
            int[] pathKeys, Deque<V> path, List<Section> listSection, List<Section> sectionAux, LinkedList<Vertex<V, E>> listV) {

        Vertex<V, E> vert = g.getVertex(vdInf);
        Vertex<V, E> vert2 = g.getVertex(voInf);

        if (vert != vert2) {
            path.push(vdInf);
            listV.removeFirst();

            for (Edge<V, E> e : g.incomingEdges(vert)) {
                if (e.getVOrig().equals(listV.getFirst())) {
                    List<Section> ls = (List<Section>) e.getElement();
                    for (Section s : sectionAux) {
                        if (ls.contains(s)) {
                            listSection.add(s);
                        }
                    }
                }
            }

            for (Edge<V, E> e : g.outgoingEdges(vert)) {
                List<Section> ls = (List<Section>) e.getElement();
                if (e.getVDest().equals(listV.getFirst())) {
                    for (Section s : sectionAux) {
                        if (ls.contains(s)) {
                            listSection.add(s);
                        }
                    }
                }
            }
            int vKey = vert.getKey();

            int prevVKey = pathKeys[vKey];
            vert = g.getVertex(prevVKey);
            vdInf = vert.getElement();

            getPath(g, voInf, vdInf, pathKeys, path, listSection, sectionAux, listV);
        } else {
            path.push(voInf);
        }
    }

    private static <V, E> void getPathVertexs(Graph<V, E> g, V voInf, V vdInf,
            int[] pathKeys, LinkedList<Vertex<V, E>> listV) {

        Vertex<V, E> vert = g.getVertex(vdInf);
        Vertex<V, E> vert2 = g.getVertex(voInf);
     
        if (vert != vert2) {

            listV.addLast(vert);

            int vKey = vert.getKey();

            int prevVKey = pathKeys[vKey];
     
            vert = g.getVertex(prevVKey);
            vdInf = vert.getElement();

            getPathVertexs(g, voInf, vdInf, pathKeys, listV);

        } else {

            listV.addLast(vert);
        }
    }

    @Override
    public String toString() {
        return "Most Efficient Path";
    }

}
