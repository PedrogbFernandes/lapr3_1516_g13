/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PathAlgorithms;


import java.util.Deque;
import java.util.List;
import Graph.Graph;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Section;
import lapr3_1516_g13.model.Vehicle;

/**
 *
 * @author pedro
 */
public interface PathAlgorithms {
    
    double[] bestPath(Graph <Junction,List<Section>> graph,Junction start,Junction end,Vehicle v,Deque<Junction> path,List<Section> sectionList);
    String toString();
}
