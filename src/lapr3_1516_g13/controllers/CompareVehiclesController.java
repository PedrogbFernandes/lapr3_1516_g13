package lapr3_1516_g13.controllers;

import PathAlgorithms.PathAlgorithms;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.model.AlgorithmList;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.ProjectRegister;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Vehicle;
import lapr3_1516_g13.model.VehicleList;

public class CompareVehiclesController {

    private Simulator simulator;
    private ProjectRegister projectRegister;
    private Project activeProject;
    private VehicleList listOfVehicles;
    private RoadNetwork roadNetwork;
    private StaticAnalysis staticAnalysis;
    private AlgorithmList algorithmList;
    private List<StaticAnalysis> staticAnalysisList;

    /**
     * CompareVehiclesController Constructor
     *
     * @param simulator the simulator
     */
    public CompareVehiclesController(Simulator simulator) {
        this.simulator = simulator;

    }

    /**
     * Initiates the controller data
     */
    public void newStaticAnalysis() {
        this.projectRegister = this.simulator.getProjectRegister();
        this.algorithmList = this.simulator.getAlgorithmList();
        this.activeProject = this.projectRegister.getActiveProject();
        this.listOfVehicles = this.activeProject.getVehicleList();
        this.roadNetwork = this.activeProject.getRoadNetwork();
        this.staticAnalysisList = new ArrayList<>();

    }

    /**
     * Returns the list of vehicles being compared by the analysis algorithm
     *
     * @param start the start node
     * @param end the end node
     * @param listVehicles the vehicles
     * @param algorithm the best path method
     * @return the list of vehicles being compared
     */
    public List<StaticAnalysis> compareValues(Junction start, Junction end, List<Vehicle> listVehicles, PathAlgorithms algorithm) {
        for (Vehicle v : listVehicles) {
            StaticAnalysis s = this.activeProject.newStaticAnalysis();
            s.setStart(start);
            s.setEnd(end);
            s.setVehicle(v);
            s.setAlgorithm(algorithm);
            if (s.validate()) {
                s.calculate(this.roadNetwork.getGraph());
                this.staticAnalysisList.add(s);

            }
        }
        return this.staticAnalysisList;

    }

    /**
     * Returns a list of vehicles
     *
     * @return a list of vehicles
     */
    public List<Vehicle> getListOfVehicles() {
        return listOfVehicles.getVehicleList();
    }

    /**
     * Returns the list of algorithms
     *
     * @return the list of algorithms
     */
    public List<PathAlgorithms> getAlgorithmList() {
        return algorithmList.getListOfAlgorithms();
    }

    /**
     * Returns the junction list
     *
     * @return the junction list
     */
    public List<Junction> getJunctionList() {
        return this.roadNetwork.getJunctions();
    }
}
