package lapr3_1516_g13.controllers;

import java.sql.SQLException;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CopyProjectController {

    private final Simulator simulator;
    private Project proj;
    private Project copy;
    private DataBase db;

    /**
     * CopyProjectController constructor, receives an instance of Simulator as
     * parameter
     *
     * @param simulator the simulator
     */
    public CopyProjectController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Verifies if there is a current active project. If there is one, a copy is
     * made.
     *
     * @return true if there is a current active project, otherwise returns
     * false.
     */
    public boolean getActiveProject() {
        this.proj = this.simulator.getProjectRegister().getActiveProject();

        if (this.proj == null) {
            return false;
        }

        this.copy = this.proj.cloneNoSimulations();
        return true;
    }

    /**
     * Returns the name and description of the project being copied
     *
     * @return the name and description of the project being copied
     */
    public String[] getNameDescription() {
        String[] data = new String[2];

        data[0] = this.proj.getName();
        data[1] = this.proj.getDescription();

        return data;
    }

    /**
     * Modifies the copied project name and description
     *
     * @param name the name
     * @param description the description
     */
    public void setNameDescription(String name, String description) {
        this.copy.setName(name);
        this.copy.setDescription(description);
    }

    /**
     * Validates the copied project
     *
     * @return true if the copied project is valid, otherwise returns false
     */
    public boolean validateProject() {
        return this.simulator.getProjectRegister().validateProject(copy);
    }

    /**
     * Registers the copied project in the project register
     *
     * @return true if the copied project is registered successfully, otherwise
     * returns false
     */
    public boolean registerProject() {
        return this.simulator.getProjectRegister().addProject(copy);
    }

    /**
     * Validates the copied project in the DB and in the project register
     *
     * @return true if project is valid, otherwise returns false
     */
    public boolean validateProjectDB() {
        try {
            if (db.validateProject(copy)) {
                return validateProject();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
        return false;
    }

    /**
     * Registers the copied project in the database and registers in the project
     * register
     *
     * @return true if saved and registered, otherwise returns false
     */
    public boolean registerProjectDB() {
        try {
            db.saveProject(copy);
            return registerProject();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

}
