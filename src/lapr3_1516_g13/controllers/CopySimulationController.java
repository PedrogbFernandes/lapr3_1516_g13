package lapr3_1516_g13.controllers;

import java.sql.SQLException;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CopySimulationController {

    private final Simulator simulator;
    private Project proj;
    private Simulation copy;
    private DataBase db;

    /**
     * CopySimulationController constructor, receives a Simulator object as
     * parameter
     *
     * @param simulator the simulator
     */
    public CopySimulationController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Creates a copy of the current active simulation if there is an active
     * project and an active simulation in that project
     *
     * @return true if there is an active project and active simulation in that
     * project, otherwise returns false
     */
    public boolean copyActiveSimulation() {
        this.proj = this.simulator.getProjectRegister().getActiveProject();

        if (this.proj == null) {
            return false;
        }

        if (this.proj.getActiveSimulation() == null) {
            return false;
        }

        this.copy = this.proj.getActiveSimulation().cloneNoResults();

        return true;
    }

    /**
     * Returns the name and description of the simulation being copied
     *
     * @return the name and description of the simulation being copied
     */
    public String[] getNameDescription() {
        String[] data = new String[2];

        data[0] = this.proj.getActiveSimulation().getName();
        data[1] = this.proj.getActiveSimulation().getDescription();

        return data;
    }

    /**
     * Modifies the name and description of the copied simulation
     *
     * @param name the name
     * @param description the description
     */
    public void setNameDescription(String name, String description) {
        this.copy.setName(name);
        this.copy.setDescription(description);
    }

    /**
     * Validates the copied simulation
     *
     * @return true if the copied simulation is valid, otherwise returns false
     */
    public boolean validateSimulation() {
        return this.proj.validateSimulation(copy);
    }

    /**
     * Registers the copied simulation in the current active project simulation
     * list
     *
     * @return true if the simulation is registered successfully, otherwise
     * returns false
     */
    public boolean registerSimulation() {
        return this.proj.addSimulation(copy);
    }

    /**
     * Validates the copied simulation in the database
     *
     * @return true if the simulation is valid, otherwise returns false
     */
    public boolean validateSimulationDB() {
        try {
            db.validateSimulation(proj, copy);
            return validateSimulation();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    /**
     * Registers the copied simulation in the simulation data base and
     * simulation list of the active project
     *
     * @return true if registered successfully, otherwise returns false
     */
    public boolean registerSimulationDB() {
        try {
            db.saveSimulation(proj, copy);
            return registerSimulation();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }
}
