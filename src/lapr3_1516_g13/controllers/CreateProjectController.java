package lapr3_1516_g13.controllers;

import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.datalayer.DataIO;
import java.io.File;
import java.sql.SQLException;
import java.util.List;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.*;

public class CreateProjectController {

    private final Simulator simulator;
    private Project project;
    private DataBase db;

    /**
     * Constructor that receives a Simulator as parameter
     *
     * @param simulator a simulator
     */
    public CreateProjectController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Modifies project in the controller as a new instance of Project
     */
    public void createNewProject() {
        project = this.simulator.getProjectRegister().createNewProject();
    }

    /**
     * Returns the file formats allowed by the project
     *
     * @return the file formats allowed by the project
     */
    public List<String> getFileFormats() {
        return project.getFileFormats();
    }

    /**
     * Modifies the name and description of the project
     *
     * @param name the name
     * @param description the description
     */
    public void insertNameDescription(String name, String description) {
        project.setName(name);
        project.setDescription(description);
    }

    /**
     * Modifies the roadnetwork graph from the project, with the one contained
     * in the XML file received as parameter
     *
     * @param file the XML file
     * @throws Exception
     */
    public void selectRoadNetworkFile(File file) throws Exception {
        DataIO dataio = new DataXML();
        RoadNetwork rn = project.getRoadNetwork();
        dataio.readFile(file, rn);
    }

    /**
     * Modifies the vehicle list from the project, with the one contained in the
     * XML file receive as parameter
     *
     * @param file the XML file
     * @throws Exception
     */
    public void selectVehicleFile(File file) throws Exception {
        DataIO dataio = new DataXML();
        VehicleList vl = project.getVehicleList();
        dataio.readFile(file, vl);
    }

    /**
     * Modifies the file format in the project
     *
     * @param format the file format
     */
    public void selectFileFormat(String format) {
        project.setFileFormat(format);
    }

    /**
     * Validates the project created in the controller
     *
     * @return true if the project is valid, otherwise returns false
     */
    public boolean validateProject() {
        return this.simulator.getProjectRegister().validateProject(project);
    }

    /**
     * Validates the project in the DB and in the project register
     *
     * @return true if project is valid, otherwise returns false
     */
    public boolean validateProjectDB() {
        try {
            if (db.validateProject(project)) {
                return validateProject();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
        return false;
    }

    /**
     * Registers the project in the simulator
     *
     * @return true if project is registered successfully, otherwise returns
     * false
     */
    public boolean registerProject() {
        return this.simulator.getProjectRegister().addProject(project);
    }

    /**
     * Saves the project in the database and registers in the project register
     *
     * @return true if saved and registered, otherwise returns false
     */
    public boolean registerProjectDB() {
        try {
            db.saveProject(project);
            return registerProject();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }
}
