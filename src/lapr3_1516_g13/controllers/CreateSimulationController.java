package lapr3_1516_g13.controllers;

import java.util.List;
import java.io.File;
import java.sql.SQLException;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CreateSimulationController {

    private Project project;
    private Simulation simulation;
    private final Simulator simulator;
    private DataBase db;

    /**
     * CreateSimulationController constructor, receives a instance of Simulator
     * as parameter
     *
     * @param simulator the simulator
     */
    public CreateSimulationController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Creates a new instance of Simulation from the current active project
     * simulation list
     *
     * @return true if there is an active project, otherwise returns false
     */
    public boolean createSimulation() {
        this.project = this.simulator.getProjectRegister().getActiveProject();

        if (this.project == null) {
            return false;
        }

        this.simulation = this.project.newSimulation();
        this.simulation.setAssociatedRoadNetwork(this.project.getRoadNetwork());
        return true;
    }

    /**
     * Modifies the name and description of the simulation
     *
     * @param name the name of the simulation
     * @param description the description of the simulation
     */
    public void setNameDescription(String name, String description) {
        this.simulation.setName(name);
        this.simulation.setDescription(description);
    }

    /**
     * Modifies the traffic list from the simulation, with the one contained in
     * the XML file received as parameter
     *
     * @param file the XML file
     * @throws Exception
     */
    public void selectTrafficFile(File file) throws Exception {
        DataIO dataio = new DataXML();
        List<Traffic> tList = this.simulation.getTrafficList();
        dataio.readFile(file, tList, project);
    }

    /**
     * Validates the new simulation
     *
     * @return true if the simulation is valid, otherwise returns false
     */
    public boolean validateSimulation() {
        return this.project.validateSimulation(this.simulation);
    }

    /**
     * Registers the new simulation in the simulation list of the active project
     *
     * @return true if registered successfully, otherwise returns false
     */
    public boolean registerSimulation() {
        return this.project.addSimulation(this.simulation);
    }

    /**
     * Validates the new simulation in the database
     *
     * @return true if the simulation is valid, otherwise returns false
     */
    public boolean validateSimulationDB() {
        try {
            db.validateSimulation(project, simulation);
            return validateSimulation();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    /**
     * Registers the new simulation in the simulation data base and simulation
     * list of the active project
     *
     * @return true if registered successfully, otherwise returns false
     */
    public boolean registerSimulationDB() {
        try {
            db.saveSimulation(project, simulation);
            return registerSimulation();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

}
