package lapr3_1516_g13.controllers;

import PathAlgorithms.PathAlgorithms;
import java.util.List;
import lapr3_1516_g13.datalayer.DataHTMLAnalysis;
import lapr3_1516_g13.model.AlgorithmList;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.ProjectRegister;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Vehicle;
import lapr3_1516_g13.model.VehicleList;

/**
 *
 * @author pedro
 */
public class CreateStaticAnalysisController {

    private Simulator simulator;
    private ProjectRegister projectRegister;
    private Project activeProject;
    private VehicleList listOfVehicles;
    private RoadNetwork roadNetwork;
    private StaticAnalysis staticAnalysis;
    private AlgorithmList algorithmList;

    public CreateStaticAnalysisController(Simulator simulator) {
        this.simulator = simulator;

    }

    public void newStaticAnalysis() {
        this.projectRegister = this.simulator.getProjectRegister();
        this.algorithmList = this.simulator.getAlgorithmList();
        this.activeProject = this.projectRegister.getActiveProject();
        this.listOfVehicles = this.activeProject.getVehicleList();
        this.roadNetwork = this.activeProject.getRoadNetwork();
        this.staticAnalysis = this.activeProject.newStaticAnalysis();

    }

    public StaticAnalysis insertValues(Junction start, Junction end, Vehicle car, PathAlgorithms algorithm) {
        this.staticAnalysis.setStart(start);
        this.staticAnalysis.setEnd(end);
        this.staticAnalysis.setVehicle(car);
        this.staticAnalysis.setAlgorithm(algorithm);
        if (this.staticAnalysis.validate()) {
            this.staticAnalysis.calculate(this.roadNetwork.getGraph());
            return this.staticAnalysis;
        }
        return null;

    }

    public void createHTMLFile() throws Exception {
        DataHTMLAnalysis data = new DataHTMLAnalysis();
        data.createFile();
    }

    public List<Vehicle> getListOfVehicles() {
        return listOfVehicles.getVehicleList();
    }

    public List<PathAlgorithms> getAlgorithmList() {
        return algorithmList.getListOfAlgorithms();
    }

    public List<Junction> getJunctionList() {
        return this.roadNetwork.getJunctions();
    }

}
