package lapr3_1516_g13.controllers;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.Simulation;
import lapr3_1516_g13.model.SimulationResult;
import lapr3_1516_g13.model.Simulator;

public class DeleteSimulationRunController {

    private Simulator simulator;
    private SimulationResult simulationResult;
    private Project activeProject;
    private Simulation activeSimulation;
    private DataBase db;

    /**
     * DeleteSimulatrionRunController Constructor
     *
     * @param simulator the simulator
     */
    public DeleteSimulationRunController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Initiates the controller by getting the current active project from the
     * simulator and the current active simulation from that project
     *
     * @return true if there is an active project and simulation
     */
    public boolean init() {
        this.activeProject = this.simulator.getProjectRegister().getActiveProject();
        if (this.activeProject == null) {
            return false;
        }
        this.activeSimulation = this.activeProject.getActiveSimulation();

        return this.activeSimulation != null;
    }

    /**
     * Modifies the simulation result
     *
     * @param result the new simulation result
     */
    public void selectedResult(SimulationResult result) {
        this.simulationResult = result;
    }

    /**
     * Removes the simulation result from the current active simulation
     *
     * @return true if removed successfully
     */
    public boolean remove() {
        return this.activeSimulation.remove(getSimulationResult().getName());
    }

    public List<SimulationResult> getSimulationResultListDB() {
        try {
            this.activeSimulation.setSimulationResultList(db.loadSimulationResultList(simulator, activeProject, activeSimulation));
            return this.activeSimulation.getSimulationResultList();
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    /**
     * Removes the simulation result from the current active simulation and from
     * the database
     *
     * @return true if removed successfully
     */
    public boolean removeDB() {
        try {
            db.deleteSimulationResult(activeProject, activeSimulation, simulationResult);
            return remove();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    /**
     * @return the simulationResult
     */
    public SimulationResult getSimulationResult() {
        return simulationResult;
    }

}
