package lapr3_1516_g13.controllers;

import Graph.Edge;
import Graph.Vertex;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;

/**
 *
 * @author Pedro Fernandes, Marcelo Oliveira
 */
public class EditProjectController {

    private Project presentProject;
    private VehicleList auxVehicleList;
    private Simulator simulator;
    private RoadNetwork input;
    private Project cloneProject;
    private DataBase db;

    /**
     * EditProject Controller constructor, receives a simulator as parameter
     *
     * @param s the simulator
     */
    public EditProjectController(Simulator s) {
        this.simulator = s;
        db = new DataBase();
    }

    /**
     * Initiates the EditProjectController by getting the current active project
     *
     * @return true if there is an active project, otherwise returns false
     */
    public boolean init() {
        this.presentProject = this.simulator.getProjectRegister().getActiveProject();

        if (this.presentProject == null) {
            return false;
        }

        this.cloneProject = this.presentProject.clone();
        this.auxVehicleList = new VehicleList();
        this.input = new RoadNetwork();
        return true;
    }

    /**
     * Modifies the edited project name
     *
     * @param name the edited project name
     */
    public void editName(String name) {
        this.cloneProject.setName(name);
    }

    /**
     * Modifies the edited project description
     *
     * @param description the edited project description
     */
    public void editDescription(String description) {
        this.cloneProject.setDescription(description);
    }

    /**
     * Creates a new vehicle list from a xml file. This list will be used to add
     * new vehicles to the edited project
     *
     * @param file the xml file
     * @throws Exception
     */
    public void selectVehicleFile(File file) throws Exception {
        DataIO dataio = new DataXML();
        dataio.readFile(file, this.auxVehicleList);

    }

    /**
     * Modifies the road network graph from the project, with the one contained
     * in the XML file received as parameter
     *
     * @param file the XML file
     * @throws Exception
     */
    public void selectRoadNetworkFile(File file) throws Exception {
        DataIO dataio = new DataXML();
        dataio.readFile(file, input);
    }

    /**
     * Registers the edited Project
     *
     * @return true if the project is valid
     */
    public boolean registerEditedProject() {
        if (removeProject()) {
            addVehicles();
            addRoads();
            if (this.simulator.getProjectRegister().addProject(cloneProject)) {
                return this.simulator.getProjectRegister().setActiveProject(cloneProject);
            }
        }
        return false;
    }

    /**
     * Registers the edited project in the database and registers in the project
     * register
     *
     * @return true if saved and registered, otherwise returns false
     */
    public boolean registerEditedProjectDB() {
        try {
            if (removeProject()) {
                addVehicles();
                addRoads();
                db.editProject(presentProject, cloneProject);
                if (this.simulator.getProjectRegister().addProject(cloneProject)) {
                    return this.simulator.getProjectRegister().setActiveProject(cloneProject);
                }
            }
            return false;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    private boolean removeProject() {
        return simulator.getProjectRegister().removeProject(this.presentProject);
    }

    private Vehicle addDuplicateVehicle(Vehicle a) {
        Vehicle auxCar = a.clone();
        String nomeOrig = a.getName();
        String nomeMod;
        int i = 1;
        while (cloneProject.getVehicleList().getVehicle(auxCar.getName()) != null) {
            nomeMod = nomeOrig + i;
            auxCar.setName(nomeMod);
            i++;
        }
        return auxCar;
    }

    /**
     * Appends all the new Junctions and Road on the roadnetwork belonging to
     * the active project It doesn't append duplicates
     */
    private void addRoads() {
        RoadNetwork rn = this.input;
        //Adds all junctions
        for (Junction j : rn.getJunctions()) {
            if (!existsJunction(j.getIndex())) {
                cloneProject.getRoadNetwork().getGraph().insertVertex(j);
            }
        }
        //Adds all Sections
        Iterable<Edge<Junction, List<Section>>> a = rn.getGraph().edges();
        Iterator iterator = a.iterator();
        while (iterator.hasNext()) {
            Edge<Junction, List<Section>> pres = (Edge<Junction, List<Section>>) iterator.next();
            Vertex<Junction, List<Section>> verOrig = cloneProject.getRoadNetwork().getGraph().getVertex(pres.getVOrig().getElement());
            Vertex<Junction, List<Section>> verDest = cloneProject.getRoadNetwork().getGraph().getVertex(pres.getVDest().getElement());
            Edge<Junction, List<Section>> pres2 = cloneProject.getRoadNetwork().getGraph().getEdge(verOrig, verDest);
            List<Section> aux1 = new ArrayList<>();
            List<Section> aux2 = new ArrayList<>();

            if (pres2 != null) {
                aux1.addAll(pres.getElement());
                aux2.addAll(pres2.getElement());
                pres2.setElement(morphSectionLists(aux2, aux1));

            } else {
                cloneProject.getRoadNetwork().getGraph().insertEdge(pres.getVOrig().getElement(), pres.getVDest().getElement(), pres.getElement(), pres.getWeight());
            }

        }
    }

    /**
     * Verifies if the Junction exists in the active project
     *
     * @param junctionName
     * @return
     */
    private boolean existsJunction(String junctionName) {
        for (Junction j : cloneProject.getRoadNetwork().getJunctions()) {
            if (j.getIndex().compareTo(junctionName) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifies if the Section exists in the active project
     *
     * @param roadName
     * @param bNode
     * @param eNode
     * @return
     */
    private boolean existsSection(String roadName, Junction bNode, Junction eNode) {
        Iterator it = cloneProject.getRoadNetwork().getGraph().edges().iterator();
        while (it.hasNext()) {
            Edge<Junction, List<Section>> pres = (Edge<Junction, List<Section>>) it.next();
            for (Section s : pres.getElement()) {
                if (s.getRoad().getName().compareTo(roadName) == 0 && s.getBeginNode() == bNode && s.getEndNode() == eNode) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Unifies two section lists
     *
     * @param a
     * @param b
     * @return the modified list
     */
    private List<Section> morphSectionLists(List<Section> a, List<Section> b) {
        if (a.isEmpty()) {
            return b;
        }
        if (b.isEmpty()) {
            return a;
        }
        for (Section s : b) {
            if (!existsSection(s.getRoad().getName(), s.getBeginNode(), s.getEndNode())) {
                a.add(s);
            }
        }
        return a;
    }

    private void addVehicles() {
        for (Vehicle actCar : auxVehicleList.getVehicleList()) {
            Vehicle auxCar = actCar.clone();
            if (cloneProject.getVehicleList().getVehicle(auxCar.getName()) == null) {
                cloneProject.getVehicleList().addVehicle(auxCar);
            } else {
                cloneProject.getVehicleList().addVehicle(addDuplicateVehicle(auxCar));
            }
        }

    }

}
