package lapr3_1516_g13.controllers;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.Simulation;
import lapr3_1516_g13.model.Simulator;

/**
 *
 * @author Marcelo Oliveira
 */
public class EditSimulationController {

    private final Simulator simulator;
    private Simulation simulation;
    private Simulation sClone;
    private Project project;
    private DataBase db;

    /**
     * EditSimulationController constructor, receives a Simulator instance as
     * parameter
     *
     * @param simulator the simulator
     */
    public EditSimulationController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Modifies the project and simulation variables with the current active
     * project and current active simulation from that project
     *
     * @return true if there is a current active simulation, otherwise returns
     * false
     */
    public boolean getActiveSimulation() {
        this.project = this.simulator.getProjectRegister().getActiveProject();
        if (this.project == null) {
            return false;
        }
        this.simulation = project.getActiveSimulation();
        if (this.simulation == null) {
            return false;
        }
        return true;
    }

    /**
     * Returns the name and description of the current active simulation
     *
     * @return the name and description of the current active simulation
     */
    public String[] getSimulationNameDescription() {
        this.sClone = this.simulation.clone();

        String[] nameDesc = new String[2];
        nameDesc[0] = this.sClone.getName();
        nameDesc[1] = this.sClone.getDescription();

        return nameDesc;
    }

    /**
     * Edits the name and description of the current active simulation and
     * checks if they are valid
     *
     * @param name the edited name of the simulation
     * @param description the edited description of the simulation
     * @return true if the name and description are valid, otherwise returns
     * false
     */
    public boolean editNameDescriptionDB(String name, String description) {
        this.sClone.setName(name);
        this.sClone.setDescription(description);
        if (!simulation.getName().equalsIgnoreCase(sClone.getName())) {
            try {
                db.validateSimulation(project, sClone);

            } catch (SQLException ex) {
                System.out.println(ex);
                return false;
            }
        }
        return this.project.editSimulation(this.simulation, this.sClone);

    }

    /**
     * Edits the name and description of the current active simulation and
     * checks if they are valid
     *
     * @param name the edited name of the simulation
     * @param description the edited description of the simulation
     * @return true if the name and description are valid, otherwise returns
     * false
     */
    public boolean editNameDescription(String name, String description) {
        this.sClone.setName(name);
        this.sClone.setDescription(description);

        return this.project.editSimulation(this.simulation, this.sClone);

    }

    /**
     * Registers the edited simulation
     *
     * @return true if registered successfully, otherwise returns false
     */
    public boolean registerEditedSimulation() {
        if (this.project.removeSimulation(this.simulation)) {
            if (this.project.addSimulation(this.sClone)) {
                if (this.project.setActiveSimulation(sClone)) {
                    return this.sClone.deleteSimulationResults();
                }
            }
        }
        return false;
    }

    /**
     * Registers the edited simulation
     *
     * @return true if registered successfully, otherwise returns false
     */
    public boolean registerEditedSimulationDB() {
        try {
            db.editSimulation(project, simulation, sClone);
            if (this.project.removeSimulation(this.simulation)) {
                if (this.project.addSimulation(this.sClone)) {
                    if (this.project.setActiveSimulation(sClone)) {
                        return this.sClone.deleteSimulationResults();
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
        return false;
    }
}
