package lapr3_1516_g13.controllers;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.*;

/**
 * Open Project Controller used to set a project as the active project of the
 * simulator
 *
 * @author Marcelo Oliveira
 */
public class OpenProjectController {

    private final Simulator simulator;
    private DataBase db;

    /**
     * Open Project constructor, receives a Simulator object
     *
     * @param simulator Simulator object
     */
    public OpenProjectController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Returns the project list from the simulator
     *
     * @return the project list
     */
    public List<Project> getProjectList() {
        return this.simulator.getProjectRegister().getProjectList();
    }

    /**
     * Sets the project received as parameter as the active project
     *
     * @param p the project
     * @return true if the project was successfully set as the active project,
     * otherwise returns false
     */
    public boolean selectProject(Project p) {
        return this.simulator.getProjectRegister().setActiveProject(p);
    }

    /**
     * Loads the project list from the database
     */
    public void loadProjectList() {
        try {
            LinkedList<Project> list = (LinkedList) db.loadProjectList();
            this.simulator.getProjectRegister().setProjectList(list);
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
}
