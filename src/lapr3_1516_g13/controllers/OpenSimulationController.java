package lapr3_1516_g13.controllers;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.*;

/**
 * Open Simulation Controller used to set a simulation as the active simulation
 * of the simulator
 *
 * @author Marcelo Oliveira
 */
public class OpenSimulationController {

    private final Simulator simulator;
    private Project project;
    private DataBase db;

    /**
     * Open Simulation constructor, receives a Simulator object
     *
     * @param simulator Simulator object
     */
    public OpenSimulationController(Simulator simulator) {
        this.simulator = simulator;
        db = new DataBase();
    }

    /**
     * Returns the simulation list from the simulator
     *
     * @return the simulation list
     */
    public List<Simulation> getSimulationList() {
        this.project = this.simulator.getProjectRegister().getActiveProject();

        if (this.project == null) {
            return null;
        }
        
        return this.project.getSimulationList().getListSimulation();
    }

    /**
     * Sets the simulation received as parameter as the active simulation
     *
     * @param s the simulation
     * @return true if the project was successfully set as the active
     * simulation, otherwise returns false
     */
    public boolean selectSimulation(Simulation s) {
        return this.project.setActiveSimulation(s);
    }

    /**
     * Loads the simulation list of the active project from the database
     */
    public void loadSimulationList() {
        try {
            LinkedList<Simulation> list = (LinkedList) db.loadSimulationList(project);
            this.project.getSimulationList().setListSimulation(list);
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
}
