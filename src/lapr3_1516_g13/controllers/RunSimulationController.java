package lapr3_1516_g13.controllers;

import PathAlgorithms.PathAlgorithms;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.model.*;

/**
 * RunSimulationController
 *
 * @author Pedro Fernandes, Gabriel Pereira, Marcelo Oliveira
 */
public class RunSimulationController {

    private Simulator simulator;
    private Simulation simulation;
    private Project activeProj;
    private SimulationResult result;
    private DataBase db;

    /**
     * RunSimulation Controller constructor, receives a simulator as parameter
     *
     * @param simulator the simulator
     */
    public RunSimulationController(Simulator simulator) {
        this.simulator = simulator;
        this.db = new DataBase();
    }

    /**
     * Initiates the controller by getting the current active project, the
     * current active simulation from that project and creating a new
     * SimulationResult instance
     *
     * @return true if there is an active project and if that project as an
     * active simulation, otherwise returns false
     */
    public boolean init() {
        activeProj = simulator.getProjectRegister().getActiveProject();

        if (activeProj == null) {
            return false;
        }

        simulation = activeProj.getActiveSimulation();

        if (simulation == null) {
            return false;
        }

        result = simulation.newSimulationResult();
        return true;
    }

    /**
     * Returns the best path algorithm list from the simulator
     *
     * @return the best path algorithm list from the simulator
     */
    public List<PathAlgorithms> getAlgorithmsList() {
        return this.simulator.getAlgorithmList().getListOfAlgorithms();
    }

    /**
     * Modifies the name, duration, time step and best path algorithm in the
     * simulation result. Validates this data and runs the simulation if valid.
     *
     * @param name the simulation result name
     * @param duration the simulation result duration
     * @param timeStep the simulation result time step
     * @param bestPath the simulation result best path method
     * @return true if the simulation result is valid, otherwise returns false
     */
    public boolean insertNameDurationTimeStepAlgorithm(String name, double duration, double timeStep, PathAlgorithms bestPath) {
        result.setName(name);
        result.setDuration(duration);
        result.setTimeStep(timeStep);
        result.setBestPathMethod(bestPath);

        if (simulation.validateResult(result)) {
            result.runSimulation(simulation.getTrafficList(), simulation.getAssociatedRoadNetwork());
            return true;
        }

        return false;
    }

    /**
     * Modifies the name, duration, time step and best path algorithm in the
     * simulation result. Validates this data in the database and runs the
     * simulation if valid.
     *
     * @param name the simulation result name
     * @param duration the simulation result duration
     * @param timeStep the simulation result time step
     * @param bestPath the simulation result best path method
     * @return true if the simulation result is valid, otherwise returns false
     */
    public boolean insertNameDurationTimeStepAlgorithmDB(String name, double duration, double timeStep, PathAlgorithms bestPath) {
        result.setName(name);
        result.setDuration(duration);
        result.setTimeStep(timeStep);
        result.setBestPathMethod(bestPath);

        try {
            if (simulation.validateResult(result) && db.validateSimulationResult(activeProj, simulation, result)) {
                result.runSimulation(simulation.getTrafficList(), simulation.getAssociatedRoadNetwork());
                return true;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }

        return false;
    }
    
    /**
     * Returns a string vector with the summary data from the simulation run
     *
     * @return a string vector with the summary data from the simulation run
     */
    public String[] getSummaryData() {
        int aux = 0;
        int aux2 = 0;
        String[] summary = new String[9];

        summary[0] = this.activeProj.getName();
        summary[1] = this.simulation.getName();
        summary[2] = this.result.getName();
        summary[3] = String.valueOf(this.result.getDuration());
        summary[4] = String.valueOf(this.result.getTimeStep());
        summary[5] = this.result.getBestPathMethod().toString();
        summary[6] = "Vehicles Created: " + this.result.getCreatedList().size();

        for (VehicleResult vr : this.result.getCreatedList()) {
            aux += vr.getSegmentsResults().size();
            if (vr.getEndTime() > 0) {
                aux2++;
            }
        }
        summary[7] = "Vehicle/Segment calculated: " + aux;
        summary[8] = "Vehicles Dropped on Arrival: " + aux2;

        return summary;
    }

    /**
     * Registers the simulation run (result) in the simulation result list of
     * the current active simulation
     *
     * @return true if the simulation result registered successfully, otherwise
     * returns false
     */
    public boolean registerSimulationRun() {
        return simulation.addSimulationResult(result);
    }

    public void setResult(SimulationResult result) {
        this.result = result;
    }

    /**
     * Registers the simulation run (result) in the simulation result list of
     * the current active simulation
     *
     * @return true if the simulation result registered successfully, otherwise
     * returns false
     */
    public boolean registerSimulationRunDB() {
        try {
            db.saveSimulationResult(activeProj, simulation, result);
            return this.registerSimulationRun();
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }
}