package lapr3_1516_g13.controllers;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import lapr3_1516_g13.datalayer.DataBase;
import lapr3_1516_g13.datalayer.DataCSV;
import lapr3_1516_g13.datalayer.DataHTMLSimulation;
import lapr3_1516_g13.model.*;

public class ShowResultSimulationController {

    private Simulator simulator;
    private SimulationResult simulationResult;
    private Simulation simulation;
    private Project project;
    private DataBase db;
    private int specificTraffic;

    public ShowResultSimulationController(Simulator simulator) {
        this.simulator = simulator;
        this.db = new DataBase();
    }

    public boolean init() {
        this.project = this.simulator.getProjectRegister().getActiveProject();

        if (this.project == null) {
            return false;
        }

        this.simulation = this.project.getActiveSimulation();

        return this.simulation != null;
    }

    public List<SimulationResult> getSimulationResultListDB() {
        try {
            return db.loadSimulationResultList(simulator, project, simulation);
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public void selectedResult(SimulationResult result) {
        this.simulationResult = simulator.getProjectRegister().getActiveProject().getActiveSimulation().getSelectedResult(result);
    }

    public SimulationResult selectedResultDB(SimulationResult result) {
        try {
            this.simulationResult = db.openSimulationResult(project, simulation, result);
            return this.simulationResult;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public void createHtmlFile(int option, Simulation simulation) {
        DataHTMLSimulation data = new DataHTMLSimulation();
        JFileChooser fileChooser = new JFileChooser(new File("C:\\"));
        definirFiltroExtensaoHTML(fileChooser);
        int resposta = fileChooser.showSaveDialog(null);
        if (resposta == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            data.setPath(file.getPath());
            data.setSimulationResult(simulationResult);
            data.setSimulation(simulation);
            data.setOption(option);
            data.setSpecificTraffic(specificTraffic);
            try {
                data.createFile();
                JOptionPane.showMessageDialog(null, "File successfully created");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error", "File could not save", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    public void createCsvFile(int option, Simulation simulation) {
        DataCSV data = new DataCSV();
        JFileChooser fileChooser = new JFileChooser(new File("C:\\"));
        definirFiltroExtensaoCSV(fileChooser);
        int resposta = fileChooser.showSaveDialog(null);
        if (resposta == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            data.setPath(file.getPath());
            data.setSimulationResult(simulationResult);
            data.setSimulation(simulation);
            data.setOption(option);
            data.setSpecificTraffic(specificTraffic);
            try {
                data.createFile();
                JOptionPane.showMessageDialog(null, "File successfully created");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error", "File could not save", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    

    public void setSpecificTraffic(int specificTraffic) {
        this.specificTraffic = specificTraffic;
    }

    private void definirFiltroExtensaoHTML(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("html");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.html";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    private void definirFiltroExtensaoCSV(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("csv");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.csv";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

}
