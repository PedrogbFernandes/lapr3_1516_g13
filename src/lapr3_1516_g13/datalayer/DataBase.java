package lapr3_1516_g13.datalayer;

import Graph.*;
import PathAlgorithms.PathAlgorithms;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import lapr3_1516_g13.model.*;
import oracle.jdbc.*;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class DataBase {

    private String url = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";
    private String user = "LAPR3_13";
    private String password = "comecaemumeacabaemseis";
//    private String url = "jdbc:oracle:thin://@localhost:1521/xe";
//    private String user = "mivogo";
//    private String password = "cenas";
    private Connection conn;
    private CallableStatement stmt;
    private ResultSet rSet;
    private ArrayDescriptor oracleVarchar2Collection;
    private ArrayDescriptor oracleNumberCollection;
    private ArrayDescriptor oracleIntegerCollection;

    /**
     * DataBase Constructor
     */
    public DataBase() {
    }

    /**
     * Starts the connection to the database
     *
     * @throws SQLException
     */
    private void openConnection() throws SQLException {
        DriverManager.registerDriver(new OracleDriver());
        conn = DriverManager.getConnection(url, user, password);

        conn.setAutoCommit(false);
    }

    /**
     * Loads the list of projects in the database
     *
     * @return a list of projects
     * @throws SQLException
     */
    public List<Project> loadProjectList() throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call openprojects(?,?,?,?,?,?,?,?,?,?,?,?) }");
        stmt.registerOutParameter(1, OracleTypes.CURSOR);
        stmt.registerOutParameter(2, OracleTypes.CURSOR);
        stmt.registerOutParameter(3, OracleTypes.CURSOR);
        stmt.registerOutParameter(4, OracleTypes.CURSOR);
        stmt.registerOutParameter(5, OracleTypes.CURSOR);
        stmt.registerOutParameter(6, OracleTypes.CURSOR);
        stmt.registerOutParameter(7, OracleTypes.CURSOR);
        stmt.registerOutParameter(8, OracleTypes.CURSOR);
        stmt.registerOutParameter(9, OracleTypes.CURSOR);
        stmt.registerOutParameter(10, OracleTypes.CURSOR);
        stmt.registerOutParameter(11, OracleTypes.CURSOR);
        stmt.registerOutParameter(12, OracleTypes.CURSOR);

        stmt.execute();

        List<Project> projList = new LinkedList<>();

        rSet = ((OracleCallableStatement) stmt).getCursor(1);

        ResultSet rSet2 = ((OracleCallableStatement) stmt).getCursor(3);
        CachedRowSet rAux2 = new CachedRowSetImpl();
        rAux2.populate(rSet2);

        ResultSet rSet3 = ((OracleCallableStatement) stmt).getCursor(2);
        CachedRowSet rAux3 = new CachedRowSetImpl();
        rAux3.populate(rSet3);

        ResultSet rSetSection = ((OracleCallableStatement) stmt).getCursor(4);
        CachedRowSet rAux4 = new CachedRowSetImpl();
        rAux4.populate(rSetSection);

        ResultSet rSetSJ = ((OracleCallableStatement) stmt).getCursor(5);
        CachedRowSet rAux5 = new CachedRowSetImpl();
        rAux5.populate(rSetSJ);

        ResultSet rSetRoad = ((OracleCallableStatement) stmt).getCursor(6);
        CachedRowSet rAux6 = new CachedRowSetImpl();
        rAux6.populate(rSetRoad);

        ResultSet rSetSegments = ((OracleCallableStatement) stmt).getCursor(7);
        CachedRowSet rAux7 = new CachedRowSetImpl();
        rAux7.populate(rSetSegments);

        ResultSet rSetVehicles = ((OracleCallableStatement) stmt).getCursor(8);
        CachedRowSet rAux8 = new CachedRowSetImpl();
        rAux8.populate(rSetVehicles);

        ResultSet rSetPV = ((OracleCallableStatement) stmt).getCursor(9);
        CachedRowSet rAux9 = new CachedRowSetImpl();
        rAux9.populate(rSetPV);

        ResultSet rSetRegime = ((OracleCallableStatement) stmt).getCursor(10);
        CachedRowSet rAux10 = new CachedRowSetImpl();
        rAux10.populate(rSetRegime);

        ResultSet rSetGear = ((OracleCallableStatement) stmt).getCursor(11);
        CachedRowSet rAux11 = new CachedRowSetImpl();
        rAux11.populate(rSetGear);

        ResultSet rSetLimit = ((OracleCallableStatement) stmt).getCursor(12);
        CachedRowSet rAux12 = new CachedRowSetImpl();
        rAux12.populate(rSetLimit);

        while (rSet.next()) {
            Project proj = new Project();
            proj.setName(rSet.getString("name"));
            proj.setDescription(rSet.getString("description"));
            proj.setFileFormat(rSet.getString("file_format"));

            int[] aux = new int[100];
            int aux_i = 0;

            rAux2.first();
            rAux2.previous();
            while (rAux2.next()) {
                if (rSet.getInt("project_id") == rAux2.getInt("project_id")) {
                    aux[aux_i] = rAux2.getInt("junction_id");
                    aux_i++;
                }
            }

            rAux3.first();
            rAux3.previous();
            while (rAux3.next()) {
                for (int i = 0; i <= aux_i; i++) {
                    if (rAux3.getInt("junction_id") == aux[i]) {
                        Junction j = new Junction();
                        j.setIndex(rAux3.getString("j_index"));

                        proj.getRoadNetwork().getGraph().insertVertex(j);
                    }
                }
            }

            rAux4.first();
            rAux4.previous();
            while (rAux4.next()) {

                int road_id;
                int[] junctions = new int[2];

                Section s = new Section();
                s.setToll(rAux4.getDouble("toll"));
                s.setType(rAux4.getString("type"));
                s.setWindDirection(rAux4.getDouble("wind_direction"));
                s.setWindSpeed(rAux4.getDouble("wind_speed"));
                s.setDirection(rAux4.getString("direction"));
                road_id = rAux4.getInt("road_id");

                rAux6.first();
                rAux6.previous();
                while (rAux6.next()) {
                    if (rAux6.getInt("road_id") == road_id) {
                        Road r = new Road();
                        r.setName(rAux6.getString("name"));
                        proj.getRoadNetwork().addRoad(r);
                        r = proj.getRoadNetwork().getRoad(r);
                        r.getSectionList().add(s);
                        s.setRoad(r);
                    }
                }

                rAux5.first();
                rAux5.previous();
                while (rAux5.next()) {
                    if (rAux5.getInt("section_id") == rAux4.getInt("section_id") && rAux5.getString("type").equals("begin")) {
                        junctions[0] = rAux5.getInt("junction_id");

                    }
                    if (rAux5.getInt("section_id") == rAux4.getInt("section_id") && rAux5.getString("type").equals("end")) {
                        junctions[1] = rAux5.getInt("junction_id");

                    }
                }

                rAux3.first();
                rAux3.previous();
                while (rAux3.next()) {
                    for (int i = 0; i < junctions.length; i++) {
                        if (rAux3.getInt("junction_id") == junctions[i]) {
                            Junction j = new Junction();
                            j.setIndex(rAux3.getString("j_index"));

                            if (i == 0) {
                                s.setBeginNode(j);
                            }

                            if (i == 1) {
                                s.setEndNode(j);
                            }
                        }
                    }
                }

                rAux7.first();
                rAux7.previous();
                while (rAux7.next()) {
                    if (rAux7.getInt("section_id") == rAux4.getInt("section_id")) {
                        Segment seg = new Segment();
                        seg.setId(String.valueOf(rAux7.getInt("segment_id")));
                        seg.setHeight(rAux7.getDouble("heigth"));
                        seg.setLength(rAux7.getDouble("length"));
                        seg.setSlope(rAux7.getDouble("slope"));
                        seg.setMaxVelocity(rAux7.getInt("max_velocity"));
                        seg.setMinVelocity(rAux7.getInt("min_velocity"));
                        seg.setNumberVehicles(rAux7.getInt("number_vehicles"));
                        s.getSegmentList().add(seg);
                    }

                }

                Graph<Junction, List<Section>> graph = proj.getRoadNetwork().getGraph();

                Vertex<Junction, List<Section>> begin = graph.getVertex(s.getBeginNode());
                Vertex<Junction, List<Section>> end = graph.getVertex(s.getEndNode());

                Edge<Junction, List<Section>> edge = graph.getEdge(begin, end);

                List<Section> list = new ArrayList<>();
                list.add(s);
                if (edge == null) {
                    graph.insertEdge(s.getBeginNode(), s.getEndNode(), list, list.size());
                } else {
                    edge.getElement().addAll(list);

                }

            }

            aux = new int[100];
            aux_i = 0;

            rAux9.first();
            rAux9.previous();
            while (rAux9.next()) {
                if (rSet.getInt("project_id") == rAux9.getInt("project_id")) {
                    aux[aux_i] = rAux9.getInt("vehicle_id");
                    aux_i++;
                }
            }

            rAux8.first();
            rAux8.previous();
            while (rAux8.next()) {
                for (int i = 0; i <= aux_i; i++) {
                    if (rAux8.getInt("vehicle_id") == aux[i]) {
                        Vehicle v = new Vehicle();

                        String fuel = rAux8.getString("fuel");
                        if (fuel.equalsIgnoreCase("electric")) {
                            EletricVehicle v2 = new EletricVehicle();
                            v = v2;
                        }

                        if (fuel.equalsIgnoreCase("gasoline")) {
                            GasolineVehicle v2 = new GasolineVehicle();
                            v = v2;
                        }

                        if (fuel.equalsIgnoreCase("diesel")) {
                            DieselVehicle v2 = new DieselVehicle();
                            v = v2;
                        }

                        v.setName(rAux8.getString("name"));
                        v.setDescription(rAux8.getString("description"));
                        v.setFuel(rAux8.getString("fuel"));
                        v.setMotorization(rAux8.getString("motorization"));
                        v.setType(rAux8.getString("type"));
                        v.setDragCoefficient(rAux8.getDouble("drag_coefficient"));
                        v.setMass(rAux8.getDouble("mass"));
                        v.setLoad(rAux8.getDouble("load"));
                        v.setRrc(rAux8.getDouble("rrc"));
                        v.setFrontal_area(rAux8.getDouble("frontal_area"));
                        v.setWheel_size(rAux8.getDouble("wheel_size"));
                        v.setMin_rpm(rAux8.getInt("min_rpm"));
                        v.setMax_rpm(rAux8.getInt("max_rpm"));
                        v.setFinal_drive_ratio(rAux8.getDouble("final_drive_ratio"));

                        if (v instanceof EletricVehicle) {
                            ((EletricVehicle) v).setEnergy_ratio(rAux8.getDouble("energy_regen_ratio"));
                        }

                        rAux10.first();
                        rAux10.previous();
                        while (rAux10.next()) {
                            if (rAux10.getInt("vehicle_id") == rAux8.getInt("vehicle_id")) {
                                Double throttle = rAux10.getDouble("throttle");

                                List<Regime> lReg = v.getThrottle_list().get(throttle);

                                Regime r = new Regime();
                                r.setRpm_high(rAux10.getInt("rpm_high"));
                                r.setRpm_low(rAux10.getInt("rpm_low"));
                                r.setSfc(rAux10.getDouble("sfc"));
                                r.setTorque(rAux10.getInt("torque"));

                                if (lReg == null) {
                                    lReg = new ArrayList<>();

                                }

                                lReg.add(r);
                                v.getThrottle_list().put(throttle, lReg);
                            }
                        }

                        rAux11.first();
                        rAux11.previous();
                        List<Double> gear_list = new ArrayList<>();
                        while (rAux11.next()) {
                            if (rAux11.getInt("vehicle_id") == rAux8.getInt("vehicle_id")) {
                                gear_list.add(rAux11.getDouble("ratio"));
                            }
                        }
                        v.setGear_ratio(gear_list);

                        rAux12.first();
                        rAux12.previous();
                        while (rAux12.next()) {
                            if (rAux12.getInt("vehicle_id") == rAux8.getInt("vehicle_id")) {
                                v.getVelocity_limit_list().put(rAux12.getString("type"), rAux12.getInt("limit"));
                            }
                        }

                        proj.getVehicleList().addVehicle(v);
                    }
                }
            }
            projList.add(proj);
        }

        rSet.close();
        rSet2.close();
        rAux2.close();

        rSet3.close();
        rAux3.close();

        rSetSection.close();
        rAux4.close();

        rSetSJ.close();
        rAux5.close();

        rSetRoad.close();
        rAux6.close();

        rSetSegments.close();
        rAux7.close();

        rSetVehicles.close();
        rAux8.close();

        rSetPV.close();
        rAux9.close();

        rSetRegime.close();
        rAux10.close();

        rSetGear.close();
        rAux11.close();

        rSetLimit.close();
        rAux12.close();

        conn.commit();
        conn.close();

        return projList;
    }

    /**
     * Validates a project in the database
     *
     * @param project the project
     * @return true if the project doesn't exist in the database
     * @throws SQLException
     */
    public boolean validateProject(Project project) throws SQLException {
        int invalid = 0;

        openConnection();

        stmt = conn.prepareCall("{ call validateproject(?,?) }");
        stmt.setString(1, project.getName());
        stmt.registerOutParameter(2, OracleTypes.INTEGER);

        stmt.execute();

        invalid = stmt.getInt(2);

        conn.commit();
        conn.close();

        return invalid == 1;
    }

    /**
     * Registers a project in the database
     *
     * @param project the project
     * @throws SQLException
     */
    public void saveProject(Project project) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call createProject(?, ?, ?, ?, ?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, project.getDescription());
        stmt.setString(3, project.getFileFormat());
        stmt.registerOutParameter(4, OracleTypes.INTEGER);
        stmt.registerOutParameter(5, OracleTypes.ARRAY, "INTEGER_T");

        stmt.execute();

        int project_id = stmt.getInt(4);

        createProjectData(project, project_id);

        conn.commit();
        conn.close();
    }

    /**
     * Edits a project in the database
     *
     * @param orig the original project
     * @param copy the edited project
     * @throws SQLException
     */
    public void editProject(Project orig, Project copy) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call editProject(?, ?, ?, ?) }");
        stmt.setString(1, orig.getName());
        stmt.setString(2, copy.getName());
        stmt.setString(3, copy.getDescription());
        stmt.registerOutParameter(4, OracleTypes.INTEGER);

        stmt.execute();

        int project_id = stmt.getInt(4);

        createProjectData(copy, project_id);

        conn.commit();
        conn.close();
    }

    /**
     * Loads the simulation list of the project in the database
     *
     * @param project the project
     * @return the project simulation list in the database
     * @throws SQLException
     */
    public List<Simulation> loadSimulationList(Project project) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call opensimulations(?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.registerOutParameter(2, OracleTypes.CURSOR);
        stmt.registerOutParameter(3, OracleTypes.CURSOR);

        stmt.execute();

        List<Simulation> simList = new LinkedList<>();

        rSet = ((OracleCallableStatement) stmt).getCursor(2);

        ResultSet rSetTraffic = ((OracleCallableStatement) stmt).getCursor(3);
        CachedRowSet rAux2 = new CachedRowSetImpl();
        rAux2.populate(rSetTraffic);

        while (rSet.next()) {
            Simulation s = new Simulation();
            s.setName(rSet.getString("name"));
            s.setDescription(rSet.getString("description"));
            s.setAssociatedRoadNetwork(project.getRoadNetwork());

            rAux2.first();
            rAux2.previous();
            List<Traffic> traffic_list = new ArrayList<>();
            while (rAux2.next()) {
                if (rAux2.getInt("simulation_id") == rSet.getInt("simulation_id")) {
                    Traffic tf = new Traffic();

                    Junction enter = new Junction();
                    enter.setIndex(rAux2.getString("enter_node"));

                    Junction exit = new Junction();
                    exit.setIndex(rAux2.getString("exit_node"));

                    tf.setEnterNode(enter);
                    tf.setExitNode(exit);

                    tf.setVehicle(project.getVehicleList().getVehicle(rAux2.getString("name")));
                    tf.setArrivalRate(rAux2.getInt("arrival_rate"));

                    traffic_list.add(tf);
                }

            }
            s.setTrafficList(traffic_list);
            simList.add(s);
        }

        conn.commit();
        conn.close();

        return simList;
    }

    /**
     * Validates the simulation in the database
     *
     * @param project the simulation project
     * @param simulation the simulation
     * @return true if the simulation doesn't exist in the database
     * @throws SQLException
     */
    public boolean validateSimulation(Project project, Simulation simulation) throws SQLException {
        int invalid = 0;

        openConnection();

        stmt = conn.prepareCall("{ call validatesimulation(?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.registerOutParameter(3, OracleTypes.INTEGER);

        stmt.execute();

        invalid = stmt.getInt(3);

        conn.commit();
        conn.close();

        return invalid == 1;
    }

    /**
     * Registers a simulation in the database
     *
     * @param project the simulation project
     * @param simulation the simulation
     * @throws SQLException
     */
    public void saveSimulation(Project project, Simulation simulation) throws SQLException {
        openConnection();

        oracleVarchar2Collection
                = ArrayDescriptor.createDescriptor("VARCHAR2_T", conn);
        oracleNumberCollection
                = ArrayDescriptor.createDescriptor("NUMBER_T", conn);
        oracleIntegerCollection
                = ArrayDescriptor.createDescriptor("INTEGER_T", conn);

        int traffic_size = simulation.getTrafficList().size();

        int[] traffic_id_array = new int[traffic_size];
        String[] traffic_enter_array = new String[traffic_size];
        String[] traffic_exit_array = new String[traffic_size];
        String[] traffic_vehiname_array = new String[traffic_size];
        int[] traffic_arrival_array = new int[traffic_size];

        List<Traffic> ltf = simulation.getTrafficList();
        for (int i = 0; i < traffic_size; i++) {
            Traffic tf = ltf.get(i);
            traffic_id_array[i] = i + 1;
            traffic_enter_array[i] = tf.getEnterNode().getIndex();
            traffic_exit_array[i] = tf.getExitNode().getIndex();
            traffic_vehiname_array[i] = tf.getVehicle().getName();
            traffic_arrival_array[i] = tf.getArrivalRate();
        }

        ARRAY ora_traffic_id = new ARRAY(oracleIntegerCollection, conn, traffic_id_array);
        ARRAY ora_traffic_enter = new ARRAY(oracleVarchar2Collection, conn, traffic_enter_array);
        ARRAY ora_traffic_exit = new ARRAY(oracleVarchar2Collection, conn, traffic_exit_array);
        ARRAY ora_traffic_vehiname = new ARRAY(oracleVarchar2Collection, conn, traffic_vehiname_array);
        ARRAY ora_traffic_arrival = new ARRAY(oracleIntegerCollection, conn, traffic_arrival_array);

        stmt = conn.prepareCall("{ call createsimulation(?, ?, ?, ?, ?, ?, ?, ?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.setString(3, simulation.getDescription());
        stmt.setObject(4, ora_traffic_id);
        stmt.setObject(5, ora_traffic_enter);
        stmt.setObject(6, ora_traffic_exit);
        stmt.setObject(7, ora_traffic_vehiname);
        stmt.setObject(8, ora_traffic_arrival);

        stmt.execute();

        conn.commit();
        conn.close();
    }

    /**
     * Validates if a simulation result exists in the database
     *
     * @param project the simulation project
     * @param simulation the simulation
     * @param result the simulation result
     * @return true if the simulation result does not exist
     * @throws SQLException
     */
    public boolean validateSimulationResult(Project project, Simulation simulation, SimulationResult result) throws SQLException {
        int invalid = 0;

        openConnection();

        stmt = conn.prepareCall("{ call validatesimulationresult(?,?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.setString(3, result.getName());
        stmt.registerOutParameter(4, OracleTypes.INTEGER);

        stmt.execute();

        invalid = stmt.getInt(4);

        conn.commit();
        conn.close();

        return invalid == 1;
    }

    /**
     * Register a simulation result in the simulation in the database
     *
     * @param project the simulation project
     * @param simulation the simulation
     * @param result the simulation result
     * @throws SQLException
     */
    public void saveSimulationResult(Project project, Simulation simulation, SimulationResult result) throws SQLException {
        openConnection();

        oracleVarchar2Collection
                = ArrayDescriptor.createDescriptor("VARCHAR2_T", conn);
        oracleNumberCollection
                = ArrayDescriptor.createDescriptor("NUMBER_T", conn);
        oracleIntegerCollection
                = ArrayDescriptor.createDescriptor("INTEGER_T", conn);

        int seg_size = 0;
        int results_size = result.getCreatedList().size();
        int aux = 1;
        int aux2 = 0;

        for (VehicleResult r : result.getCreatedList()) {
            seg_size += r.getSegmentsResults().size();
        }

        int[] result_trafficid = new int[results_size];
        int[] result_vehicle = new int[results_size];
        double[] result_endtime = new double[results_size];

        int[] resultseg_trafficid = new int[seg_size];
        int[] resultseg_vehicle = new int[seg_size];
        String[] result_seg_sectionbegin = new String[seg_size];
        String[] result_seg_sectionend = new String[seg_size];
        String[] result_seg_sectionroad = new String[seg_size];
        int[] resultseg_id = new int[seg_size];
        double[] resultseg_in = new double[seg_size];
        double[] resultseg_out = new double[seg_size];
        double[] resultseg_energy = new double[seg_size];

        List<VehicleResult> list = result.getCreatedList();
        Collections.sort(list);

        int traffic_aux = 0;

        for (int i = 0; i < list.size(); i++) {
            VehicleResult vr = list.get(i);

            if (traffic_aux == 0 || traffic_aux != vr.getTrafficId() + 1) {
                traffic_aux = vr.getTrafficId() + 1;
                aux = 1;
            }

            result_trafficid[i] = vr.getTrafficId() + 1;
            result_vehicle[i] = aux;
            result_endtime[i] = vr.getEndTime();

            for (Result r : vr.getSegmentsResults()) {
                resultseg_trafficid[aux2] = vr.getTrafficId() + 1;
                resultseg_vehicle[aux2] = aux;
                result_seg_sectionbegin[aux2] = r.getSection().getBeginNode().getIndex();
                result_seg_sectionend[aux2] = r.getSection().getEndNode().getIndex();
                result_seg_sectionroad[aux2] = r.getSection().getRoad().getName();
                resultseg_id[aux2] = Integer.parseInt(r.getSegment().getId());
                resultseg_in[aux2] = r.getIntantIn();
                resultseg_out[aux2] = r.getInstantOut();
                resultseg_energy[aux2] = r.getEnergySpent();

                aux2++;
            }
            aux++;
        }

        ARRAY ora_result_trafficid = new ARRAY(oracleIntegerCollection, conn, result_trafficid);
        ARRAY ora_result_vehicle = new ARRAY(oracleIntegerCollection, conn, result_vehicle);
        ARRAY ora_result_endtime = new ARRAY(oracleNumberCollection, conn, result_endtime);
        ARRAY ora_resultseg_trafficid = new ARRAY(oracleIntegerCollection, conn, resultseg_trafficid);
        ARRAY ora_resultseg_vehicle = new ARRAY(oracleIntegerCollection, conn, resultseg_vehicle);
        ARRAY ora_result_seg_sectionbegin = new ARRAY(oracleVarchar2Collection, conn, result_seg_sectionbegin);
        ARRAY ora_result_seg_sectionend = new ARRAY(oracleVarchar2Collection, conn, result_seg_sectionend);
        ARRAY ora_result_seg_sectionroad = new ARRAY(oracleVarchar2Collection, conn, result_seg_sectionroad);
        ARRAY ora_resultseg_id = new ARRAY(oracleIntegerCollection, conn, resultseg_id);
        ARRAY ora_resultseg_in = new ARRAY(oracleNumberCollection, conn, resultseg_in);
        ARRAY ora_resultseg_out = new ARRAY(oracleNumberCollection, conn, resultseg_out);
        ARRAY ora_resultseg_energy = new ARRAY(oracleNumberCollection, conn, resultseg_energy);

        stmt = conn.prepareCall("{ call createSimulationResult(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.setString(3, result.getName());
        stmt.setDouble(4, result.getDuration());
        stmt.setDouble(5, result.getTimeStep());
        stmt.setString(6, result.getBestPathMethod().toString());
        stmt.setObject(7, ora_result_trafficid);
        stmt.setObject(8, ora_result_vehicle);
        stmt.setObject(9, ora_result_endtime);
        stmt.setObject(10, ora_resultseg_trafficid);
        stmt.setObject(11, ora_resultseg_vehicle);
        stmt.setObject(12, ora_result_seg_sectionbegin);
        stmt.setObject(13, ora_result_seg_sectionend);
        stmt.setObject(14, ora_result_seg_sectionroad);
        stmt.setObject(15, ora_resultseg_id);
        stmt.setObject(16, ora_resultseg_in);
        stmt.setObject(17, ora_resultseg_out);
        stmt.setObject(18, ora_resultseg_energy);

        stmt.execute();

        conn.commit();
        conn.close();
    }

    /**
     * Returns the simulation result list from the simulation in the database
     *
     * @param simulator the simulator
     * @param project the project
     * @param simulation the simulation
     * @return the simulation result list
     * @throws SQLException
     */
    public List<SimulationResult> loadSimulationResultList(Simulator simulator, Project project, Simulation simulation) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call opensimulationresults(?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.registerOutParameter(3, OracleTypes.CURSOR);

        stmt.execute();

        List<SimulationResult> resultList = new LinkedList<>();

        rSet = ((OracleCallableStatement) stmt).getCursor(3);

        while (rSet.next()) {
            SimulationResult r = new SimulationResult();
            r.setName(rSet.getString("name"));
            r.setDuration(rSet.getDouble("duration"));
            r.setTimeStep(rSet.getDouble("time_step"));
            String method = rSet.getString("best_path_method");
            for (PathAlgorithms pa : simulator.getAlgorithmList().getListOfAlgorithms()) {
                if (pa.toString().equalsIgnoreCase(method)) {
                    r.setBestPathMethod(pa);
                }
            }

            resultList.add(r);
        }

        conn.commit();
        conn.close();

        return resultList;
    }

    /**
     * Returns a simulation result form the simulation in the database
     *
     * @param project the project
     * @param simulation the simulation
     * @param result the simulation result
     * @return a simulation result
     * @throws SQLException
     */
    public SimulationResult openSimulationResult(Project project, Simulation simulation, SimulationResult result) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call opensimulationresult(?,?,?,?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.setString(3, result.getName());
        stmt.registerOutParameter(4, OracleTypes.CURSOR);
        stmt.registerOutParameter(5, OracleTypes.CURSOR);
        stmt.registerOutParameter(6, OracleTypes.CURSOR);

        stmt.execute();

        rSet = ((OracleCallableStatement) stmt).getCursor(4);

        ResultSet rSetTraffic = ((OracleCallableStatement) stmt).getCursor(5);
        CachedRowSet rAux2 = new CachedRowSetImpl();
        rAux2.populate(rSetTraffic);

        ResultSet rSetTrafficSeg = ((OracleCallableStatement) stmt).getCursor(6);
        CachedRowSet rAux3 = new CachedRowSetImpl();
        rAux3.populate(rSetTrafficSeg);

        while (rSet.next()) {
            VehicleResult vr = new VehicleResult(simulation.getTrafficList().get(rSet.getInt("traffic_id") - 1));
            vr.setTrafficId(rSet.getInt("traffic_id"));
            vr.setEndTime(rSet.getDouble("end_time"));

            rAux2.first();
            rAux2.previous();
            rAux3.first();
            rAux3.previous();
            while (rAux2.next() && rAux3.next()) {
                if (rAux2.getInt("traffic_id") == rSet.getInt("traffic_id") && rAux2.getInt("result_traffic_id") == rSet.getInt("result_traffic_id")) {
                    Result res = new Result();

                    Junction enter = new Junction();
                    enter.setIndex(rAux2.getString("begin"));

                    Junction exit = new Junction();
                    exit.setIndex(rAux3.getString("end"));

                    Road r = new Road();
                    r.setName(rAux2.getString("road"));
                    r = simulation.getAssociatedRoadNetwork().getRoad(r);

                    for (Section s : r.getSectionList()) {
                        if (s.getBeginNode().equals(enter) && s.getEndNode().equals(exit)) {
                            res.setSection(s);
                            res.setSegment(s.getSegmentList().get(rAux2.getInt("segment_id") - 1));
                        }
                    }

                    res.setIntantIn(rAux2.getDouble("instant_in"));
                    res.setInstantOut(rAux2.getDouble("instant_out"));
                    res.setEnergySpent(rAux2.getDouble("energy_spent"));

                    vr.getSegmentsResults().addLast(res);
                }

            }
            result.getCreatedList().add(vr);
        }

        conn.commit();
        conn.close();

        return result;
    }

    /**
     * Edits a simulation in the database
     *
     * @param project the project
     * @param orig the original simulation
     * @param copy the edited simulation
     * @throws SQLException
     */
    public void editSimulation(Project project, Simulation orig, Simulation copy) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call editsimulation(?,?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, orig.getName());
        stmt.setString(3, copy.getName());
        stmt.setString(4, copy.getDescription());

        stmt.execute();

        stmt = conn.prepareCall("{ call deletesimulationresults(?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, copy.getName());

        stmt.execute();

        conn.commit();
        conn.close();

    }

    /**
     * Deletes a simulation result from the database
     *
     * @param project the project
     * @param simulation the simulation
     * @param result the simulation result
     * @throws SQLException
     */
    public void deleteSimulationResult(Project project, Simulation simulation, SimulationResult result) throws SQLException {
        openConnection();

        stmt = conn.prepareCall("{ call deletesimulationresult(?,?,?) }");
        stmt.setString(1, project.getName());
        stmt.setString(2, simulation.getName());
        stmt.setString(3, result.getName());

        stmt.execute();

        conn.commit();
        conn.close();

    }

    private void createProjectData(Project project, int project_id) throws SQLException {
        int i = 0;

        oracleVarchar2Collection
                = ArrayDescriptor.createDescriptor("VARCHAR2_T", conn);
        oracleNumberCollection
                = ArrayDescriptor.createDescriptor("NUMBER_T", conn);
        oracleIntegerCollection
                = ArrayDescriptor.createDescriptor("INTEGER_T", conn);

        Graph<Junction, List<Section>> graph = project.getRoadNetwork().getGraph();

        String[] j_index_array = new String[graph.numVertices()];

        Iterator<Vertex<Junction, List<Section>>> itJunction = graph.vertices().iterator();

        while (itJunction.hasNext()) {
            j_index_array[i] = itJunction.next().getElement().getIndex();
            i++;
        }

        ARRAY ora_j_index = new ARRAY(oracleVarchar2Collection, conn, j_index_array);

        stmt = conn.prepareCall("{ call createJunctions(?, ?, ?) }");
        stmt.setInt(1, project_id);
        stmt.setObject(2, ora_j_index);
        stmt.registerOutParameter(3, OracleTypes.ARRAY, "INTEGER_T");

        stmt.execute();

        Iterator<Edge<Junction, List<Section>>> itEdge = graph.edges().iterator();
        List<Section> lSection = new ArrayList<>();

        while (itEdge.hasNext()) {
            lSection.addAll(itEdge.next().getElement());
        }

        String[] begin_node_array = new String[lSection.size()];
        String[] exit_node_array = new String[lSection.size()];
        String[] road_array = new String[lSection.size()];
        String[] type_array = new String[lSection.size()];
        String[] direction_array = new String[lSection.size()];
        double[] toll_array = new double[lSection.size()];
        double[] wind_direction_array = new double[lSection.size()];
        double[] wind_speed_array = new double[lSection.size()];

        int k = 0, seg_size = 0;
        for (i = 0; i < lSection.size(); i++) {
            Section s = lSection.get(i);
            for (int j = 0; j < s.getSegmentList().size(); j++) {
                seg_size++;
            }
        }

        String[] seg_begin_node_array = new String[seg_size];
        String[] seg_exit_node_array = new String[seg_size];
        String[] seg_road_array = new String[seg_size];
        int[] seg_id_array = new int[seg_size];
        double[] seg_heigth_array = new double[seg_size];
        double[] seg_slope_array = new double[seg_size];
        double[] seg_length_array = new double[seg_size];
        int[] seg_maxv_array = new int[seg_size];
        int[] seg_minv_array = new int[seg_size];
        int[] seg_numberv_array = new int[seg_size];

        for (i = 0; i < lSection.size(); i++) {
            Section s = lSection.get(i);
            begin_node_array[i] = s.getBeginNode().getIndex();
            exit_node_array[i] = s.getEndNode().getIndex();
            road_array[i] = s.getRoad().getName();
            type_array[i] = s.getType();
            direction_array[i] = s.getDirection();
            toll_array[i] = s.getToll();
            wind_direction_array[i] = s.getWindDirection();
            wind_speed_array[i] = s.getWindSpeed();

            for (int j = 0; j < s.getSegmentList().size(); j++) {
                Segment seg = s.getSegmentList().get(j);
                seg_begin_node_array[k] = s.getBeginNode().getIndex();
                seg_exit_node_array[k] = s.getEndNode().getIndex();
                seg_road_array[k] = s.getRoad().getName();
                seg_id_array[k] = Integer.parseInt(seg.getId());
                seg_heigth_array[k] = seg.getHeight();
                seg_slope_array[k] = seg.getSlope();
                seg_length_array[k] = seg.getLength();
                seg_maxv_array[k] = seg.getMaxVelocity();
                seg_minv_array[k] = seg.getMinVelocity();
                seg_numberv_array[k] = seg.getNumberVehicles();
                k++;
            }
        }

        ARRAY ora_enter_node = new ARRAY(oracleVarchar2Collection, conn, begin_node_array);
        ARRAY ora_exit_node = new ARRAY(oracleVarchar2Collection, conn, exit_node_array);
        ARRAY ora_road = new ARRAY(oracleVarchar2Collection, conn, road_array);
        ARRAY ora_type = new ARRAY(oracleVarchar2Collection, conn, type_array);
        ARRAY ora_direction = new ARRAY(oracleVarchar2Collection, conn, direction_array);
        ARRAY ora_toll = new ARRAY(oracleNumberCollection, conn, toll_array);
        ARRAY ora_wind_direction = new ARRAY(oracleNumberCollection, conn, wind_direction_array);
        ARRAY ora_wind_speed = new ARRAY(oracleNumberCollection, conn, wind_speed_array);

        stmt = conn.prepareCall("{ call createSections(?, ?, ?, ?, ?, ?, ?, ?, ?) }");
        stmt.setInt(1, project_id);
        stmt.setObject(2, ora_enter_node);
        stmt.setObject(3, ora_exit_node);
        stmt.setObject(4, ora_road);
        stmt.setObject(5, ora_type);
        stmt.setObject(6, ora_direction);
        stmt.setObject(7, ora_toll);
        stmt.setObject(8, ora_wind_direction);
        stmt.setObject(9, ora_wind_speed);

        stmt.execute();

        ARRAY ora_seg_enter_node = new ARRAY(oracleVarchar2Collection, conn, seg_begin_node_array);
        ARRAY ora_seg_exit_node = new ARRAY(oracleVarchar2Collection, conn, seg_exit_node_array);
        ARRAY ora_seg_road = new ARRAY(oracleVarchar2Collection, conn, seg_road_array);
        ARRAY ora_seg_id = new ARRAY(oracleIntegerCollection, conn, seg_id_array);
        ARRAY ora_seg_heigth = new ARRAY(oracleNumberCollection, conn, seg_heigth_array);
        ARRAY ora_seg_slope = new ARRAY(oracleNumberCollection, conn, seg_slope_array);
        ARRAY ora_seg_length = new ARRAY(oracleNumberCollection, conn, seg_length_array);
        ARRAY ora_seg_maxv = new ARRAY(oracleIntegerCollection, conn, seg_maxv_array);
        ARRAY ora_seg_minv = new ARRAY(oracleIntegerCollection, conn, seg_minv_array);
        ARRAY ora_seg_numberv = new ARRAY(oracleIntegerCollection, conn, seg_numberv_array);

        stmt = conn.prepareCall("{ call createSegments(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
        stmt.setObject(1, ora_seg_enter_node);
        stmt.setObject(2, ora_seg_exit_node);
        stmt.setObject(3, ora_seg_road);
        stmt.setObject(4, ora_seg_id);
        stmt.setObject(5, ora_seg_heigth);
        stmt.setObject(6, ora_seg_slope);
        stmt.setObject(7, ora_seg_length);
        stmt.setObject(8, ora_seg_maxv);
        stmt.setObject(9, ora_seg_minv);
        stmt.setObject(10, ora_seg_numberv);
        stmt.registerOutParameter(11, OracleTypes.ARRAY, "INTEGER_T");

        stmt.execute();

        List<Vehicle> listVehicle = project.getVehicleList().getVehicleList();

        String[] vehi_name_array = new String[listVehicle.size()];
        String[] vehi_desc_array = new String[listVehicle.size()];
        String[] vehi_type_array = new String[listVehicle.size()];
        String[] vehi_motorization_array = new String[listVehicle.size()];
        String[] vehi_fuel_array = new String[listVehicle.size()];
        double[] vehi_mass_array = new double[listVehicle.size()];
        double[] vehi_load_array = new double[listVehicle.size()];
        double[] vehi_dragcoef_array = new double[listVehicle.size()];
        double[] vehi_rrc_array = new double[listVehicle.size()];
        double[] vehi_farea_array = new double[listVehicle.size()];
        double[] vehi_wsize_array = new double[listVehicle.size()];
        int[] vehi_minrpm_array = new int[listVehicle.size()];
        int[] vehi_maxrpm_array = new int[listVehicle.size()];
        double[] vehi_fdrive_array = new double[listVehicle.size()];
        double[] vehi_energyregen_array = new double[listVehicle.size()];

        int regime_size = 0;
        int gear_size = 0;
        int limit_size = 0;

        for (i = 0; i < listVehicle.size(); i++) {
            Vehicle v = listVehicle.get(i);

            gear_size += v.getGear_ratio().size();

            Iterator<Double> itDouble = v.getThrottle_list().keySet().iterator();

            while (itDouble.hasNext()) {
                Double t = itDouble.next();
                List<Regime> listR = v.getThrottle_list().get(t);
                for (int j = 0; j < listR.size(); j++) {
                    regime_size++;
                }
            }

            limit_size += v.getThrottle_list().size();
        }

        k = 0;
        int k2 = 0, k3 = 0;

        String[] regi_vehiname_array = new String[regime_size];
        int[] regi_torque_array = new int[regime_size];
        int[] regi_rpmlow_array = new int[regime_size];
        int[] regi_rpmhigh_array = new int[regime_size];
        double[] regi_sfc_array = new double[regime_size];
        double[] regi_throttle_array = new double[regime_size];

        String[] gear_vehiname_array = new String[gear_size];
        int[] gear_id_array = new int[gear_size];
        double[] gear_ratio_array = new double[gear_size];

        String[] limit_vehiname_array = new String[limit_size];
        String[] limit_type_array = new String[limit_size];
        int[] limit_id_array = new int[limit_size];
        int[] limit_limit_array = new int[limit_size];

        for (i = 0; i < listVehicle.size(); i++) {
            Vehicle v = listVehicle.get(i);
            vehi_name_array[i] = v.getName();
            vehi_desc_array[i] = v.getDescription();
            vehi_type_array[i] = v.getType();
            vehi_motorization_array[i] = v.getMotorization();
            vehi_fuel_array[i] = v.getFuel();
            vehi_mass_array[i] = v.getMass();
            vehi_load_array[i] = v.getLoad();
            vehi_dragcoef_array[i] = v.getDragCoefficient();
            vehi_rrc_array[i] = v.getRrc();
            vehi_farea_array[i] = v.getFrontal_area();
            vehi_wsize_array[i] = v.getWheel_size();
            vehi_minrpm_array[i] = v.getMin_rpm();
            vehi_maxrpm_array[i] = v.getMax_rpm();
            vehi_fdrive_array[i] = v.getFinal_drive_ratio();

            if (v instanceof EletricVehicle) {
                vehi_energyregen_array[i] = ((EletricVehicle) v).getEnergy_ratio();
            } else {
                vehi_energyregen_array[i] = 0;
            }

            for (int c = 0; c < v.getGear_ratio().size(); c++) {
                gear_vehiname_array[k2] = v.getName();
                gear_id_array[k2] = c + 1;
                gear_ratio_array[k2] = v.getGear_ratio().get(c);
                k2++;
            }

            Iterator<Double> itDouble = v.getThrottle_list().keySet().iterator();

            while (itDouble.hasNext()) {
                Double t = itDouble.next();
                List<Regime> listR = v.getThrottle_list().get(t);
                for (int j = 0; j < listR.size(); j++) {
                    regi_vehiname_array[k] = v.getName();
                    regi_throttle_array[k] = t;
                    regi_torque_array[k] = listR.get(j).getTorque();
                    regi_rpmlow_array[k] = listR.get(j).getRpm_low();
                    regi_rpmhigh_array[k] = listR.get(j).getRpm_high();
                    regi_sfc_array[k] = listR.get(j).getSfc();
                    k++;
                }
            }

            Iterator<String> itString = v.getVelocity_limit_list().keySet().iterator();
            int aux = 1;
            while (itString.hasNext()) {
                String sLimit = itString.next();
                int limit = v.getVelocity_limit_list().get(sLimit);

                limit_vehiname_array[k3] = v.getName();
                limit_id_array[k3] = aux;
                limit_type_array[k3] = sLimit;
                limit_limit_array[k3] = limit;

                k3++;
                aux++;
            }
        }

        ARRAY ora_vehi_name = new ARRAY(oracleVarchar2Collection, conn, vehi_name_array);
        ARRAY ora_vehi_desc = new ARRAY(oracleVarchar2Collection, conn, vehi_desc_array);
        ARRAY ora_vehi_type = new ARRAY(oracleVarchar2Collection, conn, vehi_type_array);
        ARRAY ora_vehi_motorization = new ARRAY(oracleVarchar2Collection, conn, vehi_motorization_array);
        ARRAY ora_vehi_fuel = new ARRAY(oracleVarchar2Collection, conn, vehi_fuel_array);
        ARRAY ora_vehi_mass = new ARRAY(oracleNumberCollection, conn, vehi_mass_array);
        ARRAY ora_vehi_load = new ARRAY(oracleNumberCollection, conn, vehi_load_array);
        ARRAY ora_vehi_dragcoef = new ARRAY(oracleNumberCollection, conn, vehi_dragcoef_array);
        ARRAY ora_vehi_rrc = new ARRAY(oracleNumberCollection, conn, vehi_rrc_array);
        ARRAY ora_vehi_farea = new ARRAY(oracleNumberCollection, conn, vehi_farea_array);
        ARRAY ora_vehi_wsize = new ARRAY(oracleNumberCollection, conn, vehi_wsize_array);
        ARRAY ora_vehi_minrpm = new ARRAY(oracleIntegerCollection, conn, vehi_minrpm_array);
        ARRAY ora_vehi_maxrpm = new ARRAY(oracleIntegerCollection, conn, vehi_maxrpm_array);
        ARRAY ora_vehi_fdrive = new ARRAY(oracleNumberCollection, conn, vehi_fdrive_array);
        ARRAY ora_vehi_energyregen = new ARRAY(oracleNumberCollection, conn, vehi_energyregen_array);
        ARRAY ora_regi_vehiname = new ARRAY(oracleVarchar2Collection, conn, regi_vehiname_array);
        ARRAY ora_regi_torque = new ARRAY(oracleIntegerCollection, conn, regi_torque_array);
        ARRAY ora_regi_rpmlow = new ARRAY(oracleIntegerCollection, conn, regi_rpmlow_array);
        ARRAY ora_regi_rpmhigh = new ARRAY(oracleIntegerCollection, conn, regi_rpmhigh_array);
        ARRAY ora_regi_sfc = new ARRAY(oracleNumberCollection, conn, regi_sfc_array);
        ARRAY ora_regi_throttle = new ARRAY(oracleNumberCollection, conn, regi_throttle_array);
        ARRAY ora_gear_vehiname = new ARRAY(oracleVarchar2Collection, conn, gear_vehiname_array);
        ARRAY ora_gear_id = new ARRAY(oracleIntegerCollection, conn, gear_id_array);
        ARRAY ora_gear_ratio = new ARRAY(oracleNumberCollection, conn, gear_ratio_array);
        ARRAY ora_limit_vehiname = new ARRAY(oracleVarchar2Collection, conn, limit_vehiname_array);
        ARRAY ora_limit_type = new ARRAY(oracleVarchar2Collection, conn, limit_type_array);
        ARRAY ora_limit_id = new ARRAY(oracleIntegerCollection, conn, limit_id_array);
        ARRAY ora_limit_limit = new ARRAY(oracleNumberCollection, conn, limit_limit_array);

        stmt = conn.prepareCall("{ call createVehicles(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
        stmt.setInt(1, project_id);
        stmt.setObject(2, ora_vehi_name);
        stmt.setObject(3, ora_vehi_desc);
        stmt.setObject(4, ora_vehi_type);
        stmt.setObject(5, ora_vehi_motorization);
        stmt.setObject(6, ora_vehi_fuel);
        stmt.setObject(7, ora_vehi_mass);
        stmt.setObject(8, ora_vehi_load);
        stmt.setObject(9, ora_vehi_dragcoef);
        stmt.setObject(10, ora_vehi_rrc);
        stmt.setObject(11, ora_vehi_farea);
        stmt.setObject(12, ora_vehi_wsize);
        stmt.setObject(13, ora_vehi_minrpm);
        stmt.setObject(14, ora_vehi_maxrpm);
        stmt.setObject(15, ora_vehi_fdrive);
        stmt.setObject(16, ora_vehi_energyregen);
        stmt.setObject(17, ora_regi_vehiname);
        stmt.setObject(18, ora_regi_torque);
        stmt.setObject(19, ora_regi_rpmlow);
        stmt.setObject(20, ora_regi_rpmhigh);
        stmt.setObject(21, ora_regi_sfc);
        stmt.setObject(22, ora_regi_throttle);
        stmt.setObject(23, ora_gear_vehiname);
        stmt.setObject(24, ora_gear_id);
        stmt.setObject(25, ora_gear_ratio);
        stmt.setObject(26, ora_limit_vehiname);
        stmt.setObject(27, ora_limit_type);
        stmt.setObject(28, ora_limit_id);
        stmt.setObject(29, ora_limit_limit);
        stmt.registerOutParameter(30, OracleTypes.ARRAY, "INTEGER_T");

        stmt.execute();
    }
}
