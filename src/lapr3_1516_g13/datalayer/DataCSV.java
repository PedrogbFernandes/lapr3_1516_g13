package lapr3_1516_g13.datalayer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Simulation;
import lapr3_1516_g13.model.SimulationResult;
import lapr3_1516_g13.model.Traffic;
import lapr3_1516_g13.model.VehicleList;

public class DataCSV implements DataIO {

    private Simulation simulation;
    private SimulationResult simulationResult;
    protected final String format = ".csv";
    private String path;
    private int option;
    private int specificTraffic;

    @Override
    public void createFile() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path + format));
        if (option == 1) {
            writer.write("Algorithm;Traffic Begin Node;Exit Node;Average Energy Consumption;\n");
            List<String> listResult = simulationResult.generateAvEnergyForEachTrafficPattern(simulation.getTrafficList());

            for (String str : listResult) {
                String[] s = str.split(";");
                writer.write(simulationResult.getBestPathMethod().toString() + ";");
                writer.write(s[2].replace(" Vehicle: ", "") + ";");
                writer.write(s[0].replace("Traffic begin node: ", "") + ";");
                writer.write(s[1].replace(" end node: ", "") + ";");
                writer.write(s[3].replace(" Average Energy Consumption: ", "") + ";");
                writer.write("\n");
            }

        }
        if (option == 2) {
            writer.write("Algorithm;Traffic Vehicle;Enter Node;Exit Node;Section;Segment;Average Energy Consumption;\n");
            List<String> listResult = simulationResult.generateAvEnergyForEachTrafficPattenAllSegments(simulation.getAssociatedRoadNetwork(),simulation.getTrafficList());
            for (String str : listResult) {
                String[] s = str.split(";");
                writer.write(simulationResult.getBestPathMethod().toString() + ";");
                writer.write(s[2].replace(" Vehicle: ", "") + ";");
                writer.write(s[0].replace("Traffic begin node: ", "") + ";");
                writer.write(s[1].replace(" end node: ", "") + ";");
                writer.write(s[3].replace(" Section: ", "") + ";");
                writer.write(s[4].replace(" Segment: ", "") + ";");
                writer.write(s[5].replace(" Average Energy Consumption: ", "") + ";");
                writer.write("\n");
            }
        }
        if (option == 3) {
            writer.write("Algorithm;Traffic Vehicle;Begin Node;Exit Node;Section;Segment;Average Energy Consumption;Average Time Spend;\n");
            List<String> listResult = simulationResult.generateAvEnergyForSpecificTrafficPatternAllSegments(simulation.getAssociatedRoadNetwork(),simulation.getTrafficList(), specificTraffic);
            for (String str : listResult) {
                String[] s = str.split(";");
                writer.write(simulationResult.getBestPathMethod().toString() + ";");
                writer.write(s[2].replace(" Vehicle: ", "") + ";");
                writer.write(s[0].replace("Traffic begin node: ", "") + ";");
                writer.write(s[1].replace(" end node: ", "") + ";");
                writer.write(s[3].replace(" Section: ", "") + ";");
                writer.write(s[4].replace(" Segment: ", "") + ";");
                writer.write(s[5].replace(" Average Energy Consumption: ", "") + ";");
                writer.write(calculateDuration(Double.parseDouble(s[6].replace(" Average Time Spent: ", "").replace(",", "."))) + ";");
                writer.write("\n");
            }
        }
        writer.close();
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setOption(int option) {
        this.option = option;
    }

    public void setSpecificTraffic(int specificTraffic) {
        this.specificTraffic = specificTraffic;
    }

    public void setSimulation(Simulation simulation) {
        this.simulation = simulation;
    }

    private String calculateDuration(double seg) {
        String str = "";
        if (seg >= 60) {
            int min = (int) (seg / 60);
            seg = seg % 60;
            if (min >= 60) {
                int h = min / 60;
                min = min % 60;
                str += h + " h ";
            }
            str += min + " min ";
        }
        str += (int) seg + " s";
        return str;
    }

    public SimulationResult getSimulationResult() {
        return simulationResult;
    }

    public void setSimulationResult(SimulationResult simulationResult) {
        this.simulationResult = simulationResult;
    }

    @Override
    public void readFile(File file) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void format(String file, String format) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void readFile(File file, RoadNetwork rn) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void readFile(File file, VehicleList vl) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void readFile(File file, List<Traffic> tl, Project proj) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
