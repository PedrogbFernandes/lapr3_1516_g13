package lapr3_1516_g13.datalayer;

import java.io.File;
import java.util.List;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.SimulationResult;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Traffic;
import lapr3_1516_g13.model.VehicleList;

public class DataHTML implements DataIO {

    private List<StaticAnalysis> listAnalysis;
    private SimulationResult simulationResult;
    protected final String format = ".html";

    @Override
    public void createFile() throws Exception{
        //Do nothing
    }

    @Override
    public void readFile(File file) throws Exception{
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void format(String file, String format) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<StaticAnalysis> getListAnalysis() {
        return listAnalysis;
    }

    public SimulationResult getSimulationResult() {
        return simulationResult;
    }

    public void setListAnalysis(List<StaticAnalysis> analysis) {
        this.listAnalysis = analysis;
    }

    public void setSimulationResult(SimulationResult simulationResult) {
        this.simulationResult = simulationResult;
    }

    @Override
    public void readFile(File file, RoadNetwork rn) throws Exception {
        //Do nothing
    }

    @Override
    public void readFile(File file, VehicleList vl) throws Exception {
        //Do nothing
    }

    @Override
    public void readFile(File file, List<Traffic> tl, Project proj) throws Exception {
        //Do nothing
    }
}
