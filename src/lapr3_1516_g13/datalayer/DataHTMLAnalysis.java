package lapr3_1516_g13.datalayer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import lapr3_1516_g13.model.StaticAnalysis;

public class DataHTMLAnalysis extends DataHTML {

    private String path;

    @Override
    public void createFile() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path + super.format));
        writer.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
                + "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
        writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n");
        writer.write("<head>\n");
        writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n");
        writer.write("<title>");
        writer.write("Analysis");
        writer.write("</title>\n");
        writer.write("</head>\n");

        writer.write("<body>\n");

        writer.write("<style>\n");
        writer.write("h1 { text-align: center; }\n");
        writer.write("h3 { font-weight: normal; }\n");
        writer.write("</style>\n");

        writer.write("<STYLE TYPE='text/css'> <!-- BODY { color:black; background-color:white; font: 18px Arial, sans-serif; } --> </STYLE>\n");

        writer.write("<center><img src='isep_logo.jpg' alt='logo'></center>\n");
        
        writer.write("<h1><span>");
        writer.write("Analysis");
        writer.write("</span></h1>\n");

        writer.write("<h3><span>");
        writer.write("Algorithm Used: \n");
        writer.write("<ul><li>");
        writer.write("" + super.getListAnalysis().get(0).getAlgorithm().toString());
        writer.write("</li></ul>\n");
        writer.write("</span></h3>\n");

        for (StaticAnalysis analysis : super.getListAnalysis()) {

            writer.write("<h3><span>");
            writer.write("Vehicle:");
            writer.write("\n<ul><li>");
            writer.write("" + analysis.getVehicle().getName());
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            writer.write("<h3><span>");
            writer.write("Start Junction:");
            writer.write("\n<ul><li>");
            writer.write("" + analysis.getStart());
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            writer.write("<h3><span>");
            writer.write("Finish Junction: ");
            writer.write("\n<ul><li>");
            writer.write("" + analysis.getEnd());
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            writer.write("<h3><span>");
            writer.write("Path: ");
            writer.write("\n<ul><li>");
            writer.write(analysis.getSectionList().get(0).toString());
            for (int i = 1; i < analysis.getSectionList().size(); i++) {
                writer.write(" -> " + analysis.getSectionList().get(i).toString());
            }
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            writer.write("<h3><span>");
            writer.write("Total cost by Tolls: ");
            writer.write("\n<ul><li>");
            writer.write(analysis.getTollcost() + " €");
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            writer.write("<h3><span>");
            writer.write("Time: ");
            writer.write("\n<ul><li>");
            writer.write(calculateTime(analysis));
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            writer.write("<h3><span>");
            writer.write("Fuel Comsumption: ");
            writer.write("\n<ul><li>");
            writer.write(analysis.getComsumption() + " g");
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            if (super.getListAnalysis().size() != 1) {
                writer.write("<hr>\n");
            }

        }

        writer.write("</body>\n");
        writer.write("</html>\n");

        writer.close();
    }

    public void setPath(String path) {
        this.path = path;
    }

    private String calculateTime(StaticAnalysis analysis) {
        String str = "";
        double seg = analysis.getTime();
        if (seg >= 60) {
            int min = (int) (seg / 60);
            seg = seg % 60;
            if (min >= 60) {
                int h = min / 60;
                min = min % 60;
                str += h + " h ";
            }
            str += min + " min ";
        }
        str += (int) seg + " s";
        return str;
    }
}
