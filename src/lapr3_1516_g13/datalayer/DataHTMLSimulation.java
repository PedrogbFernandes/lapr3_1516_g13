package lapr3_1516_g13.datalayer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;
import lapr3_1516_g13.model.Simulation;

public class DataHTMLSimulation extends DataHTML {

    private String path;
    private Simulation simulation;
    private int option;
    private int specificTraffic;

    @Override
    public void createFile() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path + super.format));
        writer.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
                + "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
        writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">");
        writer.write("\n<head>\n");
        writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n");
        writer.write("<title>");
        writer.write(super.getSimulationResult().toString());
        writer.write("</title>\n");
        writer.write("</head>\n");
        writer.write("<body>\n");

        writer.write("<style>\n");
        writer.write("h1 { text-align: center; }\n");
        writer.write("h3 { font-weight: normal; }\n");
        writer.write("</style>\n");

        writer.write("<STYLE TYPE='text/css'> <!-- BODY { color:black; background-color:white; font: 16px Arial, sans-serif; } --> </STYLE>\n");
        writer.write("<center><img src='isep_logo.jpg' alt='logo'></center>\n");

        writer.write("<h1><span>");
        writer.write("Simulation Results");
        writer.write("</span></h1>\n");
        if (option == 1) {
            List<String> listResult = super.getSimulationResult().generateAvEnergyForEachTrafficPattern(simulation.getTrafficList());
            writer.write("<h3><span>");
            writer.write("Algorithm Used: \n");
            writer.write("<ul><li>");
            writer.write(super.getSimulationResult().getBestPathMethod().toString());
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            for (String str : listResult) {
                String[] s = str.split(";");
                writer.write("<h3><span>");
                writer.write("Traffic:");
                writer.write("\n<ul><li>");
                writer.write(s[2].replace(" Vehicle: ", ""));
                writer.write("</li>\n");
                writer.write("\n<li>");
                writer.write("Enter Node: " + s[0].replace("Traffic begin node: ", ""));
                writer.write("</li>\n");
                writer.write("\n<li>");
                writer.write("Exit Node: " + s[1].replace(" end node: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Average Energy Consumption:");
                writer.write("\n<ul><li>");
                writer.write(s[3].replace(" Average Energy Consumption: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<hr>\n");

            }
        }
        if (option == 2) {
            List<String> listResult = super.getSimulationResult().generateAvEnergyForEachTrafficPattenAllSegments(simulation.getAssociatedRoadNetwork(), simulation.getTrafficList());

            writer.write("<h3><span>");
            writer.write("Algorithm Used: \n");
            writer.write("<ul><li>");
            writer.write(super.getSimulationResult().getBestPathMethod().toString());
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            for (String str : listResult) {
                String[] s = str.split(";");
                writer.write("<h3><span>");
                writer.write("Traffic:");
                writer.write("\n<ul><li>");
                writer.write(s[2].replace(" Vehicle: ", ""));
                writer.write("</li>\n");
                writer.write("\n<li>");
                writer.write("Enter Node: " + s[0].replace("Traffic begin node: ", ""));
                writer.write("</li>\n");
                writer.write("\n<li>");
                writer.write("Exit Node: " + s[1].replace(" end node: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Section:");
                writer.write("\n<ul><li>");
                writer.write(s[3].replace("Section: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Segment:");
                writer.write("\n<ul><li>");
                writer.write(s[4].replace(" Segment: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Average Energy Consumption:");
                writer.write("\n<ul><li>");
                writer.write(s[5].replace(" Average Energy Consumption: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<hr>\n");

            }

        }
        if (option == 3) {
            List<String> listResult = super.getSimulationResult().generateAvEnergyForSpecificTrafficPatternAllSegments(simulation.getAssociatedRoadNetwork(), simulation.getTrafficList(), specificTraffic);

            writer.write("<h3><span>");
            writer.write("Algorithm Used: \n");
            writer.write("<ul><li>");
            writer.write(super.getSimulationResult().getBestPathMethod().toString());
            writer.write("</li></ul>\n");
            writer.write("</span></h3>\n");

            for (String str : listResult) {
                String[] s = str.split(";");
                writer.write("<h3><span>");
                writer.write("Traffic:");
                writer.write("\n<ul><li>");
                writer.write(s[2].replace(" Vehicle: ", ""));
                writer.write("</li>\n");
                writer.write("\n<li>");
                writer.write("Enter Node: " + s[0].replace("Traffic begin node: ", ""));
                writer.write("</li>\n");
                writer.write("\n<li>");
                writer.write("Exit Node: " + s[1].replace(" end node: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Section:");
                writer.write("\n<ul><li>");
                writer.write(s[3].replace(" Section: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Segment:");
                writer.write("\n<ul><li>");
                writer.write(s[4].replace(" Segment: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Average Energy Consumption:");
                writer.write("\n<ul><li>");
                writer.write(s[5].replace(" Average Energy Consumption: ", ""));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<h3><span>");
                writer.write("Average Time: ");
                writer.write("\n<ul><li>");
                writer.write(calculateDuration(Double.parseDouble(s[6].replace(" Average Time Spent: ", "").replace(",", "."))));
                writer.write("</li></ul>\n");
                writer.write("</span></h3>\n");

                writer.write("<hr>\n");
            }
        }
        writer.write("</body>\n");
        writer.write("</html>\n");

        writer.close();
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setOption(int option) {
        this.option = option;
    }

    public void setSpecificTraffic(int specificTraffic) {
        this.specificTraffic = specificTraffic;
    }

    public void setSimulation(Simulation simulation) {
        this.simulation = simulation;
    }

    private String calculateDuration(double seg) {
        String str = "";
        if (seg >= 60) {
            int min = (int) (seg / 60);
            seg = seg % 60;
            if (min >= 60) {
                int h = min / 60;
                min = min % 60;
                str += h + " h ";
            }
            str += min + " min ";
        }
        str += (int) seg + " s";
        return str;
    }
}
