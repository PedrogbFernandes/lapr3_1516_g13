package lapr3_1516_g13.datalayer;

import java.io.File;
import java.util.List;
import lapr3_1516_g13.model.*;

public interface DataIO {

    public void createFile() throws Exception;

    public void readFile(File file) throws Exception;
    
    public void readFile(File file, RoadNetwork rn) throws Exception;
    
    public void readFile(File file, VehicleList vl) throws Exception;
    
    public void readFile(File file, List<Traffic> tl, Project proj) throws Exception;

    public void format(String file, String format);
}
