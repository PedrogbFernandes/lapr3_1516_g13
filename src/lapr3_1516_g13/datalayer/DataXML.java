package lapr3_1516_g13.datalayer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import Graph.Edge;
import java.lang.reflect.Constructor;
import lapr3_1516_g13.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DataXML implements DataIO {

    @Override
    public void createFile() {
        //Do nothing
    }

    @Override
    public void readFile(File file) throws Exception {
        //Do nothing
    }

    @Override
    public void format(String file, String format) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void readFile(File file, RoadNetwork rn) throws Exception {
        File fXmlFile = new File(file.getPath());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        doc.getDocumentElement().normalize();

        RoadNetwork roadNetwork = new RoadNetwork();

        NodeList nodeList = doc.getElementsByTagName("node_list");
        Node nodeNodes = nodeList.item(0);
        Element nodeElement = (Element) nodeNodes;
        NodeList nodes = nodeElement.getElementsByTagName("node");

        for (int i = 0; i < nodes.getLength(); i++) {
            Element eElement = (Element) nodes.item(i);
            Junction j = new Junction();
            j.setIndex(eElement.getAttribute("id"));
            roadNetwork.getGraph().insertVertex(j);
        }

        nodeList = doc.getElementsByTagName("section_list");
        Node nodeSections = nodeList.item(0);
        Element secElement = (Element) nodeSections;
        NodeList sections = secElement.getElementsByTagName("road_section");

        for (int i = 0; i < sections.getLength(); i++) {
            Element eElement = (Element) sections.item(i);
            Section section = new Section();

            Junction begin = new Junction();
            begin.setIndex(eElement.getAttribute("begin"));
            Junction end = new Junction();
            end.setIndex(eElement.getAttribute("end"));

            section.setBeginNode(roadNetwork.getGraph().getVertex(begin).getElement());
            section.setEndNode(roadNetwork.getGraph().getVertex(end).getElement());

            Road road = new Road();
            road.setName(eElement.getElementsByTagName("road").item(0).getTextContent());

            Road r = roadNetwork.getRoad(road);
            if (r == null) {
                roadNetwork.addRoad(road);
                r = roadNetwork.getRoad(road);
            }
            r.addSection(section);
            section.setRoad(road);

            section.setType(eElement.getElementsByTagName("typology").item(0).getTextContent());
            section.setDirection(eElement.getElementsByTagName("direction").item(0).getTextContent());
            section.setToll(Double.parseDouble(eElement.getElementsByTagName("toll").item(0).getTextContent()));
            section.setWindDirection(Double.parseDouble(eElement.getElementsByTagName("wind_direction").item(0).getTextContent()));
            section.setWindSpeed(Double.parseDouble(eElement.getElementsByTagName("wind_speed").item(0).getTextContent().toLowerCase().replace(" m/s", "")));

            NodeList nodeListSegments = eElement.getElementsByTagName("segment_list");
            Node nodeSegments = nodeListSegments.item(0);
            Element segElement = (Element) nodeSegments;
            NodeList segment = segElement.getElementsByTagName("segment");

            for (int j = 0; j < segment.getLength(); j++) {
                Element eElement2 = (Element) segment.item(j);

                Segment segm = new Segment();
                segm.setId(eElement2.getAttribute("id"));
                segm.setHeight(Double.parseDouble(eElement2.getElementsByTagName("height").item(0).getTextContent()));
                segm.setSlope(Double.parseDouble(eElement2.getElementsByTagName("slope").item(0).getTextContent().replace("%", "")));
                segm.setLength(Double.parseDouble(eElement2.getElementsByTagName("length").item(0).getTextContent().toLowerCase().replace(" km", "")));
                segm.setMaxVelocity(Integer.parseInt(eElement2.getElementsByTagName("max_velocity").item(0).getTextContent().toLowerCase().replace(" km/h", "")));
                segm.setMinVelocity(Integer.parseInt(eElement2.getElementsByTagName("min_velocity").item(0).getTextContent().toLowerCase().replace(" km/h", "")));
                segm.setNumberVehicles(Integer.parseInt(eElement2.getElementsByTagName("number_vehicles").item(0).getTextContent()));

                section.getSegmentList().add(segm);
            }

            List<Section> sectionList = new ArrayList<>();
            sectionList.add(section);
            for (Edge<Junction, List<Section>> edge : roadNetwork.getGraph().edges()) {
                if (edge.getVOrig().getElement().equals(begin) && edge.getVDest().getElement().equals(end)) {
                    edge.getElement().add(section);
                }
            }
            roadNetwork.getGraph().insertEdge(begin, end, sectionList, section.getSegmentList().size());
        }

        rn.setRoadNetwork(roadNetwork.getGraph());
    }

    @Override
    public void readFile(File file, VehicleList vl) throws Exception {
        File fXmlFile = new File(file.getPath());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getElementsByTagName("vehicle_list");
        Node nodeNodes = nodeList.item(0);
        Element nodeElement = (Element) nodeNodes;
        NodeList nodes = nodeElement.getElementsByTagName("vehicle");

        VehicleList list = new VehicleList();

        for (int i = 0; i < nodes.getLength(); i++) {
            Element eElement = (Element) nodes.item(i);

            Vehicle v = new Vehicle();

            String fuel = eElement.getElementsByTagName("fuel").item(0).getTextContent();

            if (fuel.equalsIgnoreCase("electric")) {
                EletricVehicle v2 = new EletricVehicle();
                v = v2;
            }

            if (fuel.equalsIgnoreCase("gasoline")) {
                GasolineVehicle v2 = new GasolineVehicle();
                v = v2;
            }

            if (fuel.equalsIgnoreCase("Diesel")) {
                DieselVehicle v2 = new DieselVehicle();
                v = v2;
            }

            v.setName(eElement.getAttribute("name"));
            v.setDescription(eElement.getAttribute("description"));

            v.setType(eElement.getElementsByTagName("type").item(0).getTextContent());
            v.setMotorization(eElement.getElementsByTagName("motorization").item(0).getTextContent());
            v.setFuel(eElement.getElementsByTagName("fuel").item(0).getTextContent());
            v.setMass(Double.parseDouble(eElement.getElementsByTagName("mass").item(0).getTextContent().toLowerCase().replace(" kg", "")));

            if (eElement.getElementsByTagName("load").item(0) != null) {
                v.setLoad(Double.parseDouble(eElement.getElementsByTagName("load").item(0).getTextContent().toLowerCase().replace(" kg", "")));
            } else {
                v.setLoad(0);
            }
            
            v.setDragCoefficient(Double.parseDouble(eElement.getElementsByTagName("drag").item(0).getTextContent()));
            v.setRrc(Double.parseDouble(eElement.getElementsByTagName("rrc").item(0).getTextContent()));
            v.setWheel_size(Double.parseDouble(eElement.getElementsByTagName("wheel_size").item(0).getTextContent()));
            v.setFrontal_area(Double.parseDouble(eElement.getElementsByTagName("frontal_area").item(0).getTextContent()));

            NodeList nodeListVelocity = eElement.getElementsByTagName("velocity_limit_list");
            Element nodeVelocityLimit = (Element) nodeListVelocity.item(0);
            if (nodeVelocityLimit != null) {
                NodeList velocityLimit = nodeVelocityLimit.getElementsByTagName("velocity_limit");
                for (int j = 0; j < velocityLimit.getLength(); j++) {
                    Element eElement2 = (Element) nodes.item(j);
                    v.getVelocity_limit_list().put(eElement2.getElementsByTagName("segment_type").item(0).getTextContent(), Integer.parseInt(eElement2.getElementsByTagName("limit").item(0).getTextContent()));
                }
            }

            NodeList nodeListEnergy = eElement.getElementsByTagName("energy");
            Element segElement = (Element) nodeListEnergy.item(0);
            v.setMin_rpm(Integer.parseInt(segElement.getElementsByTagName("min_rpm").item(0).getTextContent()));
            v.setMax_rpm(Integer.parseInt(segElement.getElementsByTagName("max_rpm").item(0).getTextContent()));
            v.setFinal_drive_ratio(Double.parseDouble(segElement.getElementsByTagName("final_drive_ratio").item(0).getTextContent()));
            if (v instanceof EletricVehicle) {
                ((EletricVehicle) v).setEnergy_ratio(Double.parseDouble(segElement.getElementsByTagName("energy_regeneration_ratio").item(0).getTextContent()));
            }

            NodeList nodeListGear = segElement.getElementsByTagName("gear_list");
            Element elementGear = (Element) nodeListGear.item(0);
            NodeList nodeGear = elementGear.getElementsByTagName("gear");

            for (int j = 0; j < nodeGear.getLength(); j++) {
                Element eElementGear = (Element) nodeGear.item(j);
                v.getGear_ratio().add(Double.parseDouble(eElementGear.getElementsByTagName("ratio").item(0).getTextContent()));
            }

            NodeList nodeListThrottle = segElement.getElementsByTagName("throttle_list");
            Element elementThrottle = (Element) nodeListThrottle.item(0);
            NodeList nodeThrottle = elementThrottle.getElementsByTagName("throttle");

            for (int j = 0; j < nodeThrottle.getLength(); j++) {
                Element eElementThrottle = (Element) nodeThrottle.item(j);
                double id = Double.parseDouble(eElementThrottle.getAttribute("id")) / 100;

                v.getThrottle_list().put(id, new ArrayList<>());

                NodeList nodeListRegime = eElementThrottle.getElementsByTagName("regime");

                for (int k = 0; k < nodeListRegime.getLength(); k++) {
                    Element eElementRegime = (Element) nodeListRegime.item(k);

                    Regime r = new Regime();
                    r.setTorque(Integer.parseInt(eElementRegime.getElementsByTagName("torque").item(0).getTextContent()));
                    r.setRpm_low(Integer.parseInt(eElementRegime.getElementsByTagName("rpm_low").item(0).getTextContent()));
                    r.setRpm_high(Integer.parseInt(eElementRegime.getElementsByTagName("rpm_high").item(0).getTextContent()));

                    if (v instanceof GasolineVehicle || v instanceof DieselVehicle) {
                        r.setSfc(Double.parseDouble(eElementRegime.getElementsByTagName("SFC").item(0).getTextContent()));
                    } else {
                        r.setSfc(0);
                    }

                    v.getThrottle_list().get(id).add(r);
                }
            }

            list.addVehicle(v);
        }

        vl.getVehicleList().addAll(list.getVehicleList());
    }

    @Override
    public void readFile(File file, List<Traffic> tl, Project proj) throws Exception {
        File fXmlFile = new File(file.getPath());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        doc.getDocumentElement().normalize();

        List<Traffic> list = new ArrayList<>();

        NodeList trafficList = doc.getElementsByTagName("traffic_list");
        Node trafficNodes = trafficList.item(0);
        Element trafficElement = (Element) trafficNodes;
        NodeList patterns = trafficElement.getElementsByTagName("traffic_pattern");

        for (int i = 0; i < patterns.getLength(); i++) {
            Element eElement = (Element) patterns.item(i);
            Traffic t = new Traffic();

            Junction begin = new Junction();
            Junction end = new Junction();

            begin.setIndex(eElement.getAttribute("begin"));
            end.setIndex(eElement.getAttribute("end"));

            t.setEnterNode(begin);
            t.setExitNode(end);

            t.setVehicle(proj.getVehicleList().getVehicle(eElement.getElementsByTagName("vehicle").item(0).getTextContent()).clone());
            String time = eElement.getElementsByTagName("arrival_rate").item(0).getTextContent().toLowerCase();
            String[] v_time = time.split(" ");
            t.setArrivalRate(carPerSecond(Integer.parseInt(v_time[0]), v_time[1]));

            list.add(t);
        }

        tl.addAll(list);
    }

    private int carPerSecond(int val, String unit) {
        if (unit.equals("1/m")) {
            return val * 60;
        }
        if (unit.equals("1/h")) {
            return val * 3600;
        }
        return val;
    }

}
