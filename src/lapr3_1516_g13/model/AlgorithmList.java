/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import PathAlgorithms.FastestPathAlgorithm;
import PathAlgorithms.MostEffcientPathAlgorithm;
import PathAlgorithms.PathAlgorithms;
import PathAlgorithms.TheoreticalLowestWorkPathAlgorithm;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedro
 */
public class AlgorithmList {
    /**
     * The list that contains the algorithms
     */
    List<PathAlgorithms> listOfAlgorithms;
    
    /**
     * Constructor
     */
    
    public AlgorithmList() {
        this.listOfAlgorithms = new ArrayList<>();
        this.listOfAlgorithms.add(new FastestPathAlgorithm());
        this.listOfAlgorithms.add(new MostEffcientPathAlgorithm());
        this.listOfAlgorithms.add(new TheoreticalLowestWorkPathAlgorithm());  
    }
    
    /**
     * Returns the list of algorithms
     * @return the list of algorithms
     */
    public List<PathAlgorithms> getListOfAlgorithms() {
        return listOfAlgorithms;
    }
    
    /**
     * Modifies the list of algorithms
     * @param listOfAlgorithms  the list of algorithms
     */
    public void setListOfAlgorithms(List<PathAlgorithms> listOfAlgorithms) {
        this.listOfAlgorithms = listOfAlgorithms;
    }
    
}
