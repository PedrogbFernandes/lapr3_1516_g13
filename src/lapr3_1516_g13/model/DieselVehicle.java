package lapr3_1516_g13.model;

import java.util.List;
import java.util.Map;
import static lapr3_1516_g13.model.Vehicle.KILO_WATT_HOUR;

public class DieselVehicle extends Vehicle implements CombustionEngine {

    private static final double DIESEL_ENERGY = 48000;

    /**
     * DieselVehicle Constructor
     */
    public DieselVehicle() {
    }

    /**
     * DieselVehicle Complete Constructor
     * 
     * @param name
     * @param description
     * @param type
     * @param motorization
     * @param fuel
     * @param mass
     * @param load
     * @param dragCoefficient
     * @param rrc
     * @param frontal_area
     * @param wheel_size
     * @param velocity_limit_list
     * @param min_rpm
     * @param max_rpm
     * @param final_drive_ratio
     * @param gear_ratio
     * @param throttle_list 
     */
    public DieselVehicle(String name, String description, String type, String motorization, String fuel, double mass, double load, double dragCoefficient, double rrc, double frontal_area, double wheel_size, Map<String, Integer> velocity_limit_list, int min_rpm, int max_rpm, double final_drive_ratio, List<Double> gear_ratio, Map<Double, List<Regime>> throttle_list) {
        super(name, description, type, motorization, fuel, mass, load, dragCoefficient, rrc, frontal_area, wheel_size, velocity_limit_list, min_rpm, max_rpm, final_drive_ratio, gear_ratio, throttle_list);
    }

    /**
     * Calculates the fuel on stand by
     * 
     * @param time the time
     * @return the fuel on stand by
     */
    @Override
    public double fuelOnStandBy(double time) {
        return calculateSpecificFuelComsumption(this.getMin_rpm(), Vehicle.MIN_TROTLE) * time *0.25;

    }

    /**
     * Calculates the energy consumption of the diesel vehicle
     * 
     * @param gear the gear
     * @param rpm the rpm
     * @param trotle the throttle
     * @param time the time
     * @param angle the segment angle
     * @param distance the segment distance
     * @param windSpeed the section wind speed
     * @param windDirection the section wind direction
     * @return the energy consumption
     */
    public double energyComsumption(int gear, int rpm, double trotle, double time, double angle, double distance, double windSpeed, double windDirection) {
        double energy = 0;

        if (angle >= 0 || angle < 90) {
            energy = calculateSpecificFuelComsumption(rpm, trotle) * time;
        } else {
            energy = fuelOnStandBy(time);
        }

        return energy;
    }

    /**
     * Calculates the energy consumed by the car
     *
     * @param gear the gear that the car is using
     * @param trotle should be 1 or 0.5 or 0.25
     * @return consumption
     */
    @Override
    public double calculateSpecificFuelComsumption(int rpm, double trotle) {
        double sfc = 0;
        List<Regime> list = getThrottle_list().get(trotle);
        for (Regime r : list) {
            if (rpm <= r.getRpm_high() && rpm >= r.getRpm_low()) {

                sfc = r.getSfc();
            }
        }

        double power = calculatePower(rpm, trotle); //energy in J
        double energy = sfc * (power / KILO_WATT_HOUR) * DIESEL_ENERGY;
        return energy;
    }
}
