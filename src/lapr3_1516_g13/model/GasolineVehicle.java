/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.util.List;
import java.util.Map;

/**
 *
 * @author pedro
 */
public class GasolineVehicle extends Vehicle implements CombustionEngine {

    private static final double GASOLINE_ENERGY = 44400;

    /**
     * GasolineVehicle Constructor
     */
    public GasolineVehicle() {
    }

    /**
     * GasolineVehicle Complete Constructor
     * 
     * @param name
     * @param description
     * @param type
     * @param motorization
     * @param fuel
     * @param mass
     * @param load
     * @param dragCoefficient
     * @param rrc
     * @param frontal_area
     * @param wheel_size
     * @param velocity_limit_list
     * @param min_rpm
     * @param max_rpm
     * @param final_drive_ratio
     * @param gear_ratio
     * @param throttle_list 
     */
    public GasolineVehicle(String name, String description, String type, String motorization, String fuel, double mass, double load, double dragCoefficient, double rrc, double frontal_area, double wheel_size, Map<String, Integer> velocity_limit_list, int min_rpm, int max_rpm, double final_drive_ratio, List<Double> gear_ratio, Map<Double, List<Regime>> throttle_list) {
        super(name, description, type, motorization, fuel, mass, load, dragCoefficient, rrc, frontal_area, wheel_size, velocity_limit_list, min_rpm, max_rpm, final_drive_ratio, gear_ratio, throttle_list);
    }

    /**
     * Calculates the vehicle fuel on stand by
     * 
     * @param time the time
     * @return the vehicle fuel on stand by
     */
    public double fuelOnStandBy(double time) {
        return calculateSpecificFuelComsumption(this.getMin_rpm(), Vehicle.MIN_TROTLE) * time*0.25;

    }

    /**
     * Calculates the energy consumption of the gasoline vehicle
     * 
     * @param gear the gear
     * @param rpm the rpm
     * @param trotle the throttle
     * @param time the time
     * @param angle the segment angle
     * @param distance the segment distance
     * @param windSpeed the section wind speed
     * @param windDirection the section wind direction
     * @return the energy consumption
     */
    public double energyComsumption(int gear, int rpm, double trotle, double time, double angle, double distance, double windSpeed, double windDirection) {
        double energy = 0;

        if (angle >= 0  || angle < 90) {
            energy = calculateSpecificFuelComsumption(rpm, trotle) * time ;
        }
        else {
            energy = fuelOnStandBy(time);
        }

        return energy;
    }

    /**
     * Calculates the energy consumed by the car
     *
     * @param gear the gear that the car is using
     * @param trotle should be 1 or 0.5 or 0.25
     * @return consumption
     */
    public double calculateSpecificFuelComsumption(int rpm, double trotle) {
        double sfc = 0;
        List<Regime> list = getThrottle_list().get(trotle);
        for (Regime r : list) {
            if (rpm <= r.getRpm_high() && rpm >= r.getRpm_low()) {

                sfc = r.getSfc();
            }
        }

        double power = calculatePower(rpm, trotle); //energy in J
        double energy = sfc * (power / KILO_WATT_HOUR) * GASOLINE_ENERGY;
        return energy;
    }

    /**
     * Creates a clone of the gasoline vehicle
     * 
     * @return a clone of the gasoline vehicle
     */
    public Vehicle clone() {
        return new GasolineVehicle(super.getName(), super.getDescription(), 
                super.getType(), super.getMotorization(), super.getFuel(), 
                super.getMass(), super.getLoad(), super.getDragCoefficient(), 
                super.getRrc(), super.getFrontal_area(), super.getWheel_size(),
                super.getVelocity_limit_list(), super.getMin_rpm(), 
                super.getMax_rpm(), super.getFinal_drive_ratio(), 
                super.getGear_ratio(), super.getThrottle_list());
    }

    /**
     * Creates a clone of the gasoline vehicle with another name
     * 
     * @return a clone of the gasoline vehicle with another name
     */
    public Vehicle clone2(double i, double initialTime) {
        Vehicle vehicle = new GasolineVehicle(super.getName(), super.getDescription(), 
                super.getType(), super.getMotorization(), super.getFuel(), 
                super.getMass(), super.getLoad(), super.getDragCoefficient(), 
                super.getRrc(), super.getFrontal_area(), super.getWheel_size(),
                super.getVelocity_limit_list(), super.getMin_rpm(), 
                super.getMax_rpm(), super.getFinal_drive_ratio(), 
                super.getGear_ratio(), super.getThrottle_list());
        vehicle.setName(super.getName() + "_" + i);
        vehicle.setTrafficId(super.getTrafficId());
        vehicle.setSection(0);
        vehicle.setSegment(0);
        vehicle.setTimeStep(i);
        vehicle.setInitialTime(initialTime);
        vehicle.setInitTimeSegment(initialTime);
        return vehicle;
    }

}
