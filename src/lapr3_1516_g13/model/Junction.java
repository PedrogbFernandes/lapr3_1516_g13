package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;

public class Junction {

    private String index;
    private List<Vehicle> locatedVehicles;

    /**
     * Junction Constructor
     */
    public Junction() {
        locatedVehicles = new ArrayList<>();
    }

    /**
     * Returns the junction index
     * 
     * @return the junction index
     */
    public String getIndex() {
        return index;
    }

    /**
     * Modifies the junction index
     * 
     * @param index the new junction index
     */
    public void setIndex(String index) {
        this.index = index;
    }

    /**
     * Returns the located vehicles list
     * 
     * @return the located vehicles list
     */
    public List<Vehicle> getLocatedVehicles() {
        return locatedVehicles;
    }

    /**
     * Modifies the located vehicles list
     * 
     * @param locatedVehicles the new located vehicles list
     */
    public void setLocatedVehicles(List<Vehicle> locatedVehicles) {
        this.locatedVehicles = locatedVehicles;
    }

    /**
     * Adds a vehicle to the located vehicles list
     * 
     * @param vinput the vehicle
     */
    public void addVehicle(Vehicle vinput) {
        locatedVehicles.add(vinput);
    }

    /**
     * Compares a junction with this
     * 
     * @param obj the other junction
     * @return true if the junctions index are the same, otherwise returns false 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Junction outro = (Junction) obj;

        return this.getIndex().equals(outro.getIndex());
    }

    /**
     * Returns the junction string
     * 
     * @return the junction string
     */
    @Override
    public String toString() {
        return String.format("Junction: %s", this.getIndex());
    }
}
