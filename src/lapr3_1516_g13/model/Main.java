package lapr3_1516_g13.model;

import lapr3_1516_g13.ui.MenuUI;

public class Main {

    public static void main(String[] args) {
        Simulator simulator = new Simulator();
        new MenuUI(simulator);
    }
}
