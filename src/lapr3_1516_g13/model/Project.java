package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.Iterator;

public class Project {

    private String name;
    private String description;
    private RoadNetwork roadNetwork;
    private VehicleList vehicleList;
    private SimulationRegister simulationList;
    private boolean active;
    private String fileFormat;
   

    private final ArrayList<String> fileFormats = new ArrayList<String>() {
        {
            add("HTML");
            add("CSV");
        }
    };

    /**
     * Project constructor
     */
    public Project() {
        this.roadNetwork = new RoadNetwork();
        this.vehicleList = new VehicleList();
        this.simulationList = new SimulationRegister();
        
    }

    /**
     * Project complete constructor
     *
     * @param name the project name
     * @param description the project description
     * @param roadNetwork the project network
     * @param vehicleList the project vehicle list
     * @param simulationList the project simulation list
     * @param active if the project is active or not
     * @param fileFormat the project file format
     */
    public Project(String name, String description, RoadNetwork roadNetwork, VehicleList vehicleList, SimulationRegister simulationList, boolean active, String fileFormat) {
        this.name = name;
        this.description = description;
        this.roadNetwork = roadNetwork;
        this.vehicleList = vehicleList;
        this.simulationList = simulationList;
        this.active = active;
        this.fileFormat = fileFormat;
        
    }

    /**
     * Returns the project name
     *
     * @return project name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the project description
     *
     * @return project description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the project roadnetwork
     *
     * @return project roadnetwork
     */
    public RoadNetwork getRoadNetwork() {
        return roadNetwork;
    }

    /**
     * Returns the project vehicle list
     *
     * @return project vehicle list
     */
    public VehicleList getVehicleList() {
        return vehicleList;
    }

    /**
     * Returns the simulation register
     *
     * @return the simulation register
     */
    public SimulationRegister getSimulationList() {
        return simulationList;
    }

    /**
     * Returns if the project is active or not
     *
     * @return true if project is active, otherwise returns false
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Returns the file format used in the project
     *
     * @return the file format
     */
    public String getFileFormat() {
        return fileFormat;
    }

    /**
     * Returns the file formats to export the simulation results of this project
     *
     * @return the file formats
     */
    public ArrayList<String> getFileFormats() {
        return fileFormats;
    }
   

    /**
     * Sets the project name
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the project description
     *
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the project roadnetwork
     *
     * @param roadNetwork roadnetwork
     */
    public void setRoadNetwork(RoadNetwork roadNetwork) {
        this.roadNetwork = roadNetwork;
    }

    /**
     * Sets the project vehicle list
     *
     * @param vehicleList vehicle list
     */
    public void setVehicleList(VehicleList vehicleList) {
        this.vehicleList = vehicleList;
    }

    /**
     * Modifies the project file format
     *
     * @param fileFormat file format
     */
    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }
   

    /**
     * Returns a new instance of Simulation
     *
     * @return a new instance of Simulation
     */
    public Simulation newSimulation() {
        return this.simulationList.newSimulation();
    }

    /**
     * Removes a simulation from the simulation register
     *
     * @param s the simulation
     * @return true if removed successfully, otherwise returns false
     */
    public boolean removeSimulation(Simulation s) {
        return this.simulationList.removeSimulation(s);
    }

    /**
     * Adds a simulation to the simulation register
     *
     * @param input the simulation
     * @return true if added successfully, otherwise returns false
     */
    public boolean addSimulation(Simulation input) {
        return this.simulationList.addSimulation(input);
    }

    /**
     * Sets the project as active or inactive
     *
     * @param active true(active) or false(inactive)
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Validates a simulation received as parameter in the simulation register
     *
     * @param s the simulation
     * @return true if the simulation is valid, otherwise returns false
     */
    public boolean validateSimulation(Simulation s) {
        return this.simulationList.validateSimulation(s);
    }

    /**
     * Modifies the simulation register
     *
     * @param simulationList the simulation register
     */
    public void setSimulationList(SimulationRegister simulationList) {
        this.simulationList = simulationList;
    }

    /**
     * Verifies if there's a car with the name it receives as a parameter
     *
     * @param vehicleName
     * @return true if the vehicles exists, otherwise returns false
     */
    public boolean vehicleExists(String vehicleName) {
        Iterator vehicles = this.getVehicleList().getVehicleList().iterator();
        while(vehicles.hasNext()) {
            Vehicle presentVehicle = (Vehicle) vehicles.next();
            if (presentVehicle.getName().compareTo(vehicleName) == 0) {
                return true;
            }
        }
        return false;
    }


    /**
     * Creates a clone of the current project
     *
     * @return a clone of the current project
     */
    public Project clone() {
        return new Project(name, description, roadNetwork, vehicleList, simulationList, active, fileFormat);
    }

    /**
     * Creates a clone with no simulations of the current project
     *
     * @return a clone with no simulations of the current project
     */
    public Project cloneNoSimulations() {
        Project copy = new Project();

        copy.setFileFormat(this.fileFormat);
        copy.setRoadNetwork(this.roadNetwork);
        copy.setVehicleList(this.vehicleList);

        return copy;
    }

    /**
     * Returns the current active simulation
     *
     * @return the current active simulation
     */
    public Simulation getActiveSimulation() {
        return this.simulationList.getActiveSimulation();
    }

    /**
     * Sets the simulation received as parameter as the current active
     * simulation
     *
     * @param s the simulation
     * @return true if set as active successfully, otherwise returns false
     */
    public boolean setActiveSimulation(Simulation s) {
        return this.simulationList.setActiveSimulation(s);
    }

    /**
     * Changes a present simulation in the register with the new edited
     * simulation
     *
     * @param s the current simulation
     * @param sClone the current simulation (edited)
     * @return true if changed successfully, otherwise returns false
     */
    public boolean editSimulation(Simulation s, Simulation sClone) {
        return this.simulationList.editSimulation(s, sClone);
    }

    /**
     * Validates the project data
     *
     * @return true if project is valid, otherwise returns false
     */
    public boolean validate() {
        if (this.name == null || this.description == null) {
            return false;
        }
        return !this.name.isEmpty()
                && !this.description.isEmpty()
                && this.roadNetwork.validate()
                && this.vehicleList.validate();
    }

    /**
     * Verifies if two Projects are equal
     *
     * @param obj another project
     * @return true if the project are equal, otherwise returns false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
        return this.getName().equalsIgnoreCase(other.getName());
    }
    
    public StaticAnalysis newStaticAnalysis(){
        return new StaticAnalysis();
    }

    /**
     * Returns a string with the project name and description
     *
     * @return a string with the project name and description
     */
    @Override
    public String toString() {
        return String.format("Project: %s, Description: %s", this.getName(), this.getDescription());
    }

}
