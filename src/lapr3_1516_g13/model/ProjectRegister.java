package lapr3_1516_g13.model;

import java.util.LinkedList;

public class ProjectRegister {

    private LinkedList<Project> listProject;

    /**
     * ProjectRegister constructor
     */
    public ProjectRegister() {
        this.listProject = new LinkedList<>();
    }

    /**
     * Returns the project list in ProjectRegister
     *
     * @return the project list
     */
    public LinkedList<Project> getProjectList() {
        return this.listProject;
    }

    /**
     * Modifies the project list in ProjectRegister
     *
     * @param pl the project list
     */
    public void setProjectList(LinkedList<Project> pl) {
        listProject = pl;
    }

    /**
     * Returns a new instance of Project
     *
     * @return a new instance of Project
     */
    public Project createNewProject() {
        return new Project();
    }

    /**
     * Validates a project receive as parameter
     *
     * @param p the project
     * @return true if project is valid, otherwise returns false
     */
    public boolean validateProject(Project p) {
        return validate(p) && p.validate();
    }

    /**
     * Adds a project to the list
     *
     * @param p the project
     * @return true if project added successfully, otherwise returns false
     */
    public boolean addProject(Project p) {
        return this.listProject.add(p);
    }

    /**
     * Returns the active project in the project list
     *
     * @return the active project
     */
    public Project getActiveProject() {
        if (this.listProject.isEmpty()) {
            return null;
        }

        Project p = this.listProject.getFirst();
        if (p.isActive()) {
            return p;
        }

        return null;
    }

    /**
     * Tries to set a project received as parameter as the active project
     *
     * @param p the project
     * @return true if the project was set as the active project, otherwise
     * returns false
     */
    public boolean setActiveProject(Project p) {
        Project first = this.getActiveProject();

        if (first == null) {
            return activateProject(p);
        }

        if (first.equals(p)) {
            return true;
        }

        if (activateProject(p)) {
            first.setActive(false);
            return true;
        }

        return false;
    }

    /**
     * Removes a project from the project list
     *
     * @param p the project
     * @return true if removed successfully, otherwise returns false
     */
    public boolean removeProject(Project p) {
        return this.listProject.remove(p);
    }

    
    private boolean activateProject(Project p) {
        if (this.listProject.contains(p)) {
            if (this.listProject.remove(p)) {
                p.setActive(true);
                this.listProject.addFirst(p);
                return true;
            }
        }
        return false;
    }

    private boolean validate(Project p) {
        return !this.listProject.contains(p);
    }
}
