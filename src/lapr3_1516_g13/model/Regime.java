package lapr3_1516_g13.model;

/**
 *
 * @author Marcelo Oliveira
 */
public class Regime {

    private int torque;
    private int rpm_low;
    private int rpm_high;
    private double sfc;

    /**
     * Regime Constructor
     */
    public Regime() {
    }

    /**
     * Returns the regime torque
     *
     * @return the regime torque
     */
    public int getTorque() {
        return torque;
    }

    /**
     * Returns the regime low rpm
     *
     * @return the regime low rpm
     */
    public int getRpm_low() {
        return rpm_low;
    }

    /**
     * Returns the regime high rpm
     *
     * @return the regime high rpm
     */
    public int getRpm_high() {
        return rpm_high;
    }

    /**
     * Returns the regime SFC
     *
     * @return the regime SFC
     */
    public double getSfc() {
        return sfc;
    }

    /**
     * Modifies the regime torque
     *
     * @param torque the new torque
     */
    public void setTorque(int torque) {
        this.torque = torque;
    }

    /**
     * Modifies the regime low rpm
     *
     * @param rpm_low the new low rpm
     */
    public void setRpm_low(int rpm_low) {
        this.rpm_low = rpm_low;
    }

    /**
     * Modifies the regime high rpm
     *
     * @param rpm_high the new high rpm
     */
    public void setRpm_high(int rpm_high) {
        this.rpm_high = rpm_high;
    }

    /**
     * Modifies the regime sfc
     *
     * @param sfc the new sfc
     */
    public void setSfc(double sfc) {
        this.sfc = sfc;
    }

}
