package lapr3_1516_g13.model;

public class Result {

    private Section section;
    private Segment segment;
    private double intantIn;
    private double instantOut;
    private double energySpent;

    /**
     * Default Result Constructor
     */
    public Result() {
    }

    /**
     * Complete Result Constructor
     *
     * @param section the result section
     * @param segment the result segment
     * @param intantIn the result instant in
     * @param instantOut the result instant out
     * @param energySpent the result energy spent
     */
    public Result(Section section, Segment segment, double intantIn, double instantOut, double energySpent) {
        this.section = section;
        this.segment = segment;
        this.intantIn = intantIn;
        this.instantOut = instantOut;
        this.energySpent = energySpent;
    }

    /**
     * Returns the result section
     *
     * @return the section
     */
    public Section getSection() {
        return section;
    }

    /**
     * Returns the result segment
     *
     * @return the segment
     */
    public Segment getSegment() {
        return segment;
    }

    /**
     * Returns the result instant in
     *
     * @return the intantIn
     */
    public double getIntantIn() {
        return intantIn;
    }

    /**
     * Returns the result instant out
     *
     * @return the instantOut
     */
    public double getInstantOut() {
        return instantOut;
    }

    /**
     * Modifies the result section
     *
     * @param section the section to set
     */
    public void setSection(Section section) {
        this.section = section;
    }

    /**
     * Modifies the result segment
     *
     * @param segment the segment to set
     */
    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    /**
     * Modifies the result instant in
     *
     * @param intantIn the intantIn to set
     */
    public void setIntantIn(double intantIn) {
        this.intantIn = intantIn;
    }

    /**
     * Modifies the result instant out
     *
     * @param instantOut the instantOut to set
     */
    public void setInstantOut(double instantOut) {
        this.instantOut = instantOut;
    }

    /**
     * Returns the result energy spent
     *
     * @return the result energy spent
     */
    public double getEnergySpent() {
        return energySpent;
    }

    /**
     * Modifies the result energy spent
     *
     * @param energySpent the result energy spent
     */
    public void setEnergySpent(double energySpent) {
        this.energySpent = energySpent;
    }

    /**
     * Checks if two results are equal
     *
     * @param obj the other result
     * @return true if equal, otherwise false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Result other = (Result) obj;

        return this.section.equals(other.section)
                && this.segment.getId().equalsIgnoreCase(other.segment.getId());
    }

    /**
     * Returns the result string
     *
     * @return the result string
     */
    @Override
    public String toString() {
        return "Section: " + section.getRoad().getName() + " Segment: " + segment.getId() + " Instant In: " + intantIn + " Instant Out: " + instantOut + " Energy Spent: " + energySpent;
    }
}
