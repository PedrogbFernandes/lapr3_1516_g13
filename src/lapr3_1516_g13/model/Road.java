package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;

public class Road {

    private String name;
    private List<Section> sectionList;

    /**
     * Road Constructor
     */
    public Road() {
        this.sectionList = new ArrayList<>();
    }

    /**
     * Returns the road name
     *
     * @return the road name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the road section list
     *
     * @return the road section list
     */
    public List<Section> getSectionList() {
        return sectionList;
    }

    /**
     * Modifies the road name
     *
     * @param name the new road name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Modifies the road section list
     *
     * @param sectionList the new section list
     */
    public void setSectionList(List<Section> sectionList) {
        this.sectionList = sectionList;
    }

    /**
     * Adds a section to the road section list
     *
     * @param section the new section
     * @return true if added successfully, otherwise returns false
     */
    public boolean addSection(Section section) {
        if (!this.sectionList.contains(section)) {
            return this.sectionList.add(section);
        }
        return false;
    }

    /**
     * Compares this road with another received as parameter
     *
     * @param obj the other road
     * @return true if the names are equal, otherwise returns false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Road other = (Road) obj;
        return this.getName().equalsIgnoreCase(other.getName());
    }

    /**
     * Returns the road string
     *
     * @return the road string
     */
    @Override
    public String toString() {
        return this.getName();
    }

}
