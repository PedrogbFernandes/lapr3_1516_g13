package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;
import Graph.Graph;
import Graph.Vertex;

/**
 *
 * @author Marcelo Oliveira
 */
public class RoadNetwork {

    private Graph<Junction, List<Section>> roadNetwork;
    private List<Road> roadList;

    /**
     * RoadNetwork Constructor
     */
    public RoadNetwork() {
        this.roadNetwork = new Graph(true);
        this.roadList = new ArrayList<>();
    }

    /**
     * Returns the road network graph
     *
     * @return the road network graph
     */
    public Graph<Junction, List<Section>> getGraph() {
        return this.roadNetwork;
    }

    /**
     * Returns the road network road list
     *
     * @return the road network road list
     */
    public List<Road> getRoadList() {
        return roadList;
    }

    /**
     * Modifies the road network graph
     *
     * @param roadNetwork the new graph
     */
    public void setRoadNetwork(Graph<Junction, List<Section>> roadNetwork) {
        this.roadNetwork = roadNetwork;
    }

    /**
     * Modifies the road network road list
     *
     * @param roadList the new road list
     */
    public void setRoadList(List<Road> roadList) {
        this.roadList = roadList;
    }

    /**
     * Adds a road to the road list
     *
     * @param road the road
     * @return true if added successfully, otherwise returns false
     */
    public boolean addRoad(Road road) {
        if (!roadList.contains(road)) {
            return roadList.add(road);
        }

        return false;
    }

    /**
     * Returns a road from the road list equal to the road received as parameter
     *
     * @param road the road
     * @return the road if exists in the road list
     */
    public Road getRoad(Road road) {
        for (Road r : roadList) {
            if (r.equals(road)) {
                return r;
            }
        }
        return null;
    }

    /**
     * Returns the junctions from the graph in the road network
     *
     * @return the junctions from the graph in the road network
     */
    public List<Junction> getJunctions() {
        ArrayList<Junction> result = new ArrayList<>();
        for (Vertex<Junction, List<Section>> a : this.roadNetwork.vertices()) {
            result.add(a.getElement());
        }
        return result;
    }

    /**
     * Validates the road network graph
     *
     * @return true if the graph is valid, otherwise returns false
     */
    public boolean validate() {
        return this.roadNetwork.numVertices() > 1
                && this.roadNetwork.numEdges() > 0;
    }

}
