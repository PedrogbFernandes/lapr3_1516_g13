package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Section {

    private Junction beginNode;
    private Junction endNode;
    private Road road;
    private String type;
    private String direction;
    private double toll;
    private double windDirection;
    private double windSpeed;
    private List<Segment> segmentList;

    /**
     * Section Constructor
     */
    public Section() {
        this.segmentList = new ArrayList<>();
    }

    /**
     * Returns the section begin node
     *
     * @return the section begin node
     */
    public Junction getBeginNode() {
        return beginNode;
    }

    /**
     * Returns the section end node
     *
     * @return the section end node
     */
    public Junction getEndNode() {
        return endNode;
    }

    /**
     * Returns the section road
     *
     * @return the section road
     */
    public Road getRoad() {
        return road;
    }

    /**
     * Returns the section type
     *
     * @return the section type
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the section direction
     *
     * @return the section direction
     */
    public String getDirection() {
        return direction;
    }

    /**
     * Returns the section toll
     *
     * @return the section toll
     */
    public double getToll() {
        return toll;
    }

    /**
     * Returns the section wind direction
     *
     * @return the section wind direction
     */
    public Double getWindDirection() {
        return windDirection;
    }

    /**
     * Returns the section wind speed
     *
     * @return the section wind speed
     */
    public double getWindSpeed() {
        return windSpeed;
    }

    /**
     * Returns the section segment list
     *
     * @return the section segment list
     */
    public List<Segment> getSegmentList() {
        return segmentList;
    }

    /**
     * Modifies the section begin node
     *
     * @param beginNode the new begin node
     */
    public void setBeginNode(Junction beginNode) {
        this.beginNode = beginNode;
    }

    /**
     * Modifies the section end node
     *
     * @param endNode the new end node
     */
    public void setEndNode(Junction endNode) {
        this.endNode = endNode;
    }

    /**
     * Modifies the section road
     *
     * @param road the new road
     */
    public void setRoad(Road road) {
        this.road = road;
    }

    /**
     * Modifies the section type
     *
     * @param type the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Modifies the section direction
     *
     * @param direction the new direction
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     * Modifies the section toll
     *
     * @param toll the new toll
     */
    public void setToll(double toll) {
        this.toll = toll;
    }

    /**
     * Modifies the section wind direction
     *
     * @param windDirection the new wind direction
     */
    public void setWindDirection(Double windDirection) {
        this.windDirection = windDirection;
    }

    /**
     * Modifies the section wind speed
     *
     * @param windSpeed the new wind speed
     */
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * Modifies the section segment list
     *
     * @param segmentList the new segment list
     */
    public void setSegmentList(List<Segment> segmentList) {
        this.segmentList = segmentList;
    }

    /**
     * Calculates the fastest time that a given car can drive trough this
     * section
     *
     * @param car the car that is being driven
     * @return the time that it takes
     */
    public double[] fastestTime(Vehicle car, boolean direct) {
        // d = v*t;
        double[] results = new double[2];
        double time = 0;
        double comsumption = 0;
        double final_time = 0;
        double final_comsumption = 0;

        double slope = 0;
        Map<String, Integer> limits = car.getVelocity_limit_list();
        for (Segment s : segmentList) {
            if (s.getSlope() == 0) {
                if (limits.containsKey(this.type.toLowerCase())) {
                    double carMaxVelocity = (limits.get(this.type.toLowerCase()) / 3.6);
                    double time_segment = (s.getLength() * 1000) / (carMaxVelocity / 3.6);
                    final_time += time_segment;
                    int size = car.getGear_ratio().size();

                    final_comsumption += car.energyComsumption(size, car.getMax_rpm(), Vehicle.MAX_TROTLE, time_segment, 0, s.getLength() * 1000, windSpeed, windDirection);

                } else {
                    double time_segment = (s.getLength() * 1000) / (s.getMaxVelocity() / 3.6);
                    final_time += time_segment;
                    int size = car.getGear_ratio().size();
                    final_comsumption += car.energyComsumption(size, car.getMax_rpm(), Vehicle.MAX_TROTLE, time_segment, 0, s.getLength() * 1000, windSpeed, windDirection);

                }

            } else {

                slope = s.getSlope() / 100;
                slope = Math.atan(Math.toRadians(slope));
                if (!direct) {
                    slope = slope + 180;
                }
                double comsumption_segment = 0;
                double time_segment = Double.MAX_VALUE;
                double velocity = 0;
                double work = 0;
                for (int i = car.getGear_ratio().size(); i > 0; i--) {
                    if (limits.containsKey(this.type.toLowerCase())) {
                        for (int r = car.getMax_rpm(); r > car.getMin_rpm(); r--) {

                            velocity = car.velocityByGearAndRpm(i, r);
                            time = (s.getLength() * 1000) / velocity;
                            work = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, windSpeed, windDirection);
                            comsumption = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);

                            if (work > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity < limits.get(this.type.toLowerCase()) / 3.6 && velocity > s.getMinVelocity() / 3.6 && time < time_segment) {
                                comsumption_segment = comsumption;
                                time_segment = time;

                            }

                        }

                    } else {
                        for (int r = car.getMax_rpm(); r > car.getMin_rpm(); r--) {
                            velocity = car.velocityByGearAndRpm(i, r);
                            time = (s.getLength() * 1000) / velocity;
                            work = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, windSpeed, windDirection);
                            comsumption = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);

                            if (work > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity > s.getMinVelocity() / 3.6 && time < time_segment) {
                                comsumption_segment = comsumption;
                                time_segment = time;

                            }
                        }

                    }

                }
                final_comsumption = final_comsumption + comsumption_segment;
                final_time = final_time + time_segment;
            }

        }
        results[0] = final_time;
        results[1] = final_comsumption;
        return results;
    }

    /**
     * Calculates the fastest time that a given car can drive trough this
     * section with the lowest consumption
     *
     * @param car the car that is being driven
     * @return the time and consumption that it takes
     */
    public double[] lowestComsumption(Vehicle car, boolean direct) {
        // d = v*t;

        double[] results = new double[2];
        double final_time = 0;
        double comsumption = 0;

        double slope = 0;

        Map<String, Integer> limits = car.getVelocity_limit_list();
        for (Segment s : segmentList) {
            double comsumption_segment = Double.MAX_VALUE;
            double time_segment = 0;
            double workmin = 0;
            double workmed = 0;
            double workmax = 0;

            double comsumption_min = 0;
            double comsumption_med = 0;
            double comsumption_max = 0;

            double velocity = 0;

            double time = 0;

            slope = s.getSlope() / 100;
            slope = Math.atan(Math.toRadians(slope));
            if (!direct) {
                slope = slope + 180;
            }
            for (int i = car.getGear_ratio().size(); i > 0; i--) {
                if (limits.containsKey(this.type.toLowerCase())) {
                    for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                        if (r != 0) {
                            velocity = car.velocityByGearAndRpm(i, r);
                            time = (s.getLength() * 1000) / velocity;

                            workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, windSpeed, windDirection);
                            workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, windSpeed, windDirection);
                            workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, windSpeed, windDirection);

                            comsumption_min = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);
                            comsumption_med = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);
                            comsumption_max = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);

                            if (workmin > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity < limits.get(this.type.toLowerCase()) / 3.6 && velocity > s.getMinVelocity() / 3.6 && comsumption_min < comsumption_segment) {
                                comsumption_segment = comsumption_min;
                                time_segment = time;

                            }
                            if (workmed > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity < limits.get(this.type.toLowerCase()) / 3.6 && velocity > s.getMinVelocity() / 3.6 && comsumption_med < comsumption_segment) {
                                comsumption_segment = comsumption_med;
                                time_segment = time;

                            }
                            if (workmax > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity < limits.get(this.type.toLowerCase()) / 3.6 && velocity > s.getMinVelocity() / 3.6 && comsumption_max < comsumption_segment) {
                                comsumption_segment = comsumption_max;
                                time_segment = time;

                            }
                        }
                    }

                } else {
                    for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                        if (r != 0) {
                            velocity = car.velocityByGearAndRpm(i, r);
                            time = (s.getLength() * 1000) / velocity;

                            workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, windSpeed, windDirection);
                            workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, windSpeed, windDirection);
                            workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, windSpeed, windDirection);

                            comsumption_min = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);
                            comsumption_med = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);
                            comsumption_max = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, s.getLength() * 1000, windSpeed, windDirection);

                            if (workmin > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity > s.getMinVelocity() / 3.6 && comsumption_min < comsumption_segment) {
                                comsumption_segment = comsumption_min;
                                time_segment = time;

                            }
                            if (workmed > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity > s.getMinVelocity() / 3.6 && comsumption_med < comsumption_segment) {
                                comsumption_segment = comsumption_med;
                                time_segment = time;

                            }
                            if (workmax > 0 && (velocity < s.getMaxVelocity() / 3.6) && velocity > s.getMinVelocity() / 3.6 && comsumption_max < comsumption_segment) {
                                comsumption_segment = comsumption_max;
                                time_segment = time;

                            }
                        }

                    }

                }

            }

            comsumption = comsumption + comsumption_segment;
            final_time = final_time + time_segment;

        }

        results[0] = final_time;
        results[1] = comsumption;

        return results;

    }

    /**
     * Calculates the fastest time that a given car can drive trough this
     * section with the lowest work
     *
     * @param car the car that is being driven
     * @return the time, consumption and work that it takes
     */
    public double[] lowestWork(Vehicle car, boolean direct) {
        // d = v*t;
        double[] results = new double[3];
        double work = 0;
        double time = 0;
        double comsumption = 0;

        double slope = 0;

        Map<String, Integer> limits = car.getVelocity_limit_list();
        for (Segment s : segmentList) {
            double work_segment = Double.MAX_VALUE;
            double time_segment = 0;
            double comsumption_segment = 0;
            double workmin = 0;
            double workmed = 0;
            double workmax = 0;
            slope = s.getSlope() / 100;
            slope = Math.atan(Math.toRadians(slope));
            if (!direct) {
                slope = slope + 180;
            }
            for (int i = car.getGear_ratio().size(); i > 0; i--) {
                if (limits.containsKey(this.type.toLowerCase())) {
                    for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                        if (r != 0) {
                            workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, windSpeed, windDirection);
                            workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, windSpeed, windDirection);
                            workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, windSpeed, windDirection);
                            if (workmin > 0 && (car.velocityByGearAndRpm(i, r) < s.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) < limits.get(this.type.toLowerCase()) / 3.6 && car.velocityByGearAndRpm(i, r) > s.getMinVelocity() / 3.6 && workmin < work_segment) {
                                work_segment = workmin;
                                time_segment = (s.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                                comsumption_segment = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time_segment, slope, s.getLength() * 1000, windSpeed, windDirection);

                            }
                            if (workmed > 0 && (car.velocityByGearAndRpm(i, r) < s.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) < limits.get(this.type.toLowerCase()) / 3.6 && car.velocityByGearAndRpm(i, r) > s.getMinVelocity() / 3.6 && workmed < work_segment) {
                                work_segment = workmed;
                                time_segment = (s.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                                comsumption_segment = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time_segment, slope, s.getLength() * 1000, windSpeed, windDirection);
                            }
                            if (workmax > 0 && (car.velocityByGearAndRpm(i, r) < s.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) < limits.get(this.type.toLowerCase()) / 3.6 && car.velocityByGearAndRpm(i, r) > s.getMinVelocity() / 3.6 && workmax < work_segment) {
                                work_segment = workmax;
                                time_segment = (s.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                                comsumption_segment = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time_segment, slope, s.getLength() * 1000, windSpeed, windDirection);
                            }
                        }
                    }

                } else {
                    for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                        if (r != 0) {
                            workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, windSpeed, windDirection);
                            workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, windSpeed, windDirection);
                            workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, windSpeed, windDirection);

                            if (workmin > 0 && (car.velocityByGearAndRpm(i, r) < s.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) > s.getMinVelocity() / 3.6 && workmin < work_segment) {
                                work_segment = workmin;
                                time_segment = (s.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                                comsumption_segment = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time_segment, slope, s.getLength() * 1000, windSpeed, windDirection);
                            }
                            if (workmed > 0 && (car.velocityByGearAndRpm(i, r) < s.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) > s.getMinVelocity() / 3.6 && workmed < work_segment) {
                                work_segment = workmed;
                                time_segment = (s.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                                comsumption_segment = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time_segment, slope, s.getLength() * 1000, windSpeed, windDirection);
                            }
                            if (workmax > 0 && (car.velocityByGearAndRpm(i, r) < s.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) > s.getMinVelocity() / 3.6 && workmax < work_segment) {
                                work_segment = workmax;
                                time_segment = (s.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                                comsumption_segment = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time_segment, slope, s.getLength() * 1000, windSpeed, windDirection);
                            }
                        }

                    }

                }

            }
            work = work + (work_segment * (s.getLength() * 1000));
            time = time + time_segment;
            comsumption = comsumption + comsumption_segment;
        }
        results[0] = time;
        results[1] = comsumption;
        results[2] = work;
        return results;
    }

    /**
     * Compares two Sections
     *
     * @param obj the other section
     * @return true if the sections are equal, otherwise returns false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Section other = (Section) obj;
        
        return this.getBeginNode().equals(other.getBeginNode())
                && this.getEndNode().equals(other.getEndNode())
                && this.getRoad().equals(other.getRoad());
    }

    /**
     * Returns the section string
     *
     * @return the section string
     */
    @Override
    public String toString() {
        return String.format("Section - Begin: %s, End: %s, Road: %s", this.getBeginNode().getIndex(), this.getEndNode().getIndex(), this.getRoad().getName());
    }

}
