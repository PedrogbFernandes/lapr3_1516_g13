package lapr3_1516_g13.model;

import PathAlgorithms.PathAlgorithms;
import java.util.LinkedList;
import java.util.Map;

public class Segment {

    private String id;
    private double height;
    private double slope;
    private double length;
    private int maxVelocity;
    private int minVelocity;
    private int numberVehicles;

    private LinkedList<Vehicle> vehicleList;
    private LinkedList<Vehicle> vehicleWaitingList;

    /**
     * Segment Constructor
     */
    public Segment() {
        this.vehicleList = new LinkedList<>();
        this.vehicleWaitingList = new LinkedList<>();
    }

    /**
     * Returns the segment id
     *
     * @return the segment id
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the segment height
     *
     * @return the segment height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Returns the segment slope angle
     *
     * @return the segment slope angle
     */
    public double getSlope() {
        return slope;
    }

    /**
     * Returns the segment length
     *
     * @return the segment length
     */
    public double getLength() {
        return length;
    }

    /**
     * Returns the segment max velocity
     *
     * @return the segment max velocity
     */
    public int getMaxVelocity() {
        return maxVelocity;
    }

    /**
     * Returns the segment min velocity
     *
     * @return the segment min velocity
     */
    public int getMinVelocity() {
        return minVelocity;
    }

    /**
     * Returns the segment max number of vehicles
     *
     * @return the segment max number of vehicles
     */
    public int getNumberVehicles() {
        return numberVehicles;
    }

    /**
     * Returns the segment list of vehicles
     *
     * @return the segment list of vehicles
     */
    public LinkedList<Vehicle> getVehicleList() {
        return vehicleList;
    }

    /**
     * Returns the segment vehicle waiting list
     *
     * @return the segment vehicle waiting list
     */
    public LinkedList<Vehicle> getVehicleWaitingList() {
        return vehicleWaitingList;
    }

    /**
     * Modifies the segment id
     *
     * @param id the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Modifies the segment height
     *
     * @param height the new height
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Modifies the segment slope angle
     *
     * @param slope the new slop angle
     */
    public void setSlope(double slope) {
        this.slope = slope;
    }

    /**
     * Modifies the segment length
     *
     * @param length the new length
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * Modifies the segment max velocity
     *
     * @param maxVelocity the new max velocity
     */
    public void setMaxVelocity(int maxVelocity) {
        this.maxVelocity = maxVelocity;
    }

    /**
     * Modifies the segment min velocity
     *
     * @param minVelocity the new min velocity
     */
    public void setMinVelocity(int minVelocity) {
        this.minVelocity = minVelocity;
    }

    /**
     * Modifies the segment max number of vehicles
     *
     * @param numberVehicles the new max number of vehicles
     */
    public void setNumberVehicles(int numberVehicles) {
        this.numberVehicles = numberVehicles;
        this.vehicleList = new LinkedList<>();
    }

    /**
     * Modifies the segment vehicle list
     *
     * @param vehicleList the new vehicle list
     */
    public void setVehicleList(LinkedList<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    /**
     * Modifies the segment vehicle waiting list
     *
     * @param vehicleWaitingList the new vehicle waiting list
     */
    public void setVehicleWaitingList(LinkedList<Vehicle> vehicleWaitingList) {
        this.vehicleWaitingList = vehicleWaitingList;
    }

    /**
     * Adds a vehicle to the vehicle list, if the segment is full adds to the
     * waiting list
     *
     * @param vehicle the vehicle
     * @return true if added to the vehicle list, false if added to the waiting
     * list
     */
    public boolean addVehicle(Vehicle vehicle, Double waiting) {
        if (this.vehicleList.size() < numberVehicles) {
            this.vehicleList.addLast(vehicle);
            return true;
        } else {
            vehicle.setInitWaiting(waiting);
            this.vehicleWaitingList.addLast(vehicle);
            return false;
        }
    }

    /**
     * Removes a vehicle from the segment vehicle list
     *
     * @param vehicle the vehicle
     * @return true if removed successfully, otherwise returns false
     */
    public boolean removeVehicle(Vehicle vehicle) {
        return this.vehicleList.remove(vehicle);
    }

    /**
     * Checks if the segment vehicle list is empty
     *
     * @return true if empty, otherwise returns false
     */
    public boolean isEmpty() {
        return this.vehicleList.isEmpty();
    }

    /**
     * Calculates the fastest time a vehicle takes to complete the segment
     *
     * @param car the vehicle
     * @param s the section
     * @param direct the direction
     * @return the time and consumption
     */
    public double[] timeSegment(Vehicle car, Section s, boolean direct) {
        double[] results = new double[2];
        double time = 0;
        double comsumption = 0;
        double final_time = 0;
        double final_comsumption = 0;

        double slope = 0;
        Map<String, Integer> limits = car.getVelocity_limit_list();
        if (this.getSlope() == 0) {
            if (limits.containsKey(s.getType().toLowerCase())) {
                double carMaxVelocity = (limits.get(s.getType().toLowerCase()) / 3.6);
                double time_segment = (this.getLength() * 1000) / (carMaxVelocity / 3.6);
                final_time += time_segment;
                int size = car.getGear_ratio().size();

                final_comsumption += car.energyComsumption(size, car.getMax_rpm(), Vehicle.MAX_TROTLE, time_segment, 0, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

            } else {
                double time_segment = (this.getLength() * 1000) / (this.getMaxVelocity() / 3.6);
                final_time += time_segment;
                int size = car.getGear_ratio().size();
                final_comsumption += car.energyComsumption(size, car.getMax_rpm(), Vehicle.MAX_TROTLE, time_segment, 0, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

            }

        } else {

            slope = this.getSlope() / 100;
            slope = Math.atan(Math.toRadians(slope));
            if (!direct) {
                slope = slope + 180;
            }
            double comsumption_segment = 0;
            double time_segment = Double.MAX_VALUE;
            double velocity = 0;
            double work = 0;
            for (int i = car.getGear_ratio().size(); i > 0; i--) {
                if (limits.containsKey(s.getType().toLowerCase())) {
                    for (int r = car.getMax_rpm(); r > car.getMin_rpm(); r--) {

                        velocity = car.velocityByGearAndRpm(i, r);
                        time = (this.getLength() * 1000) / velocity;
                        work = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        comsumption = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

                        if (work > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity < limits.get(s.getType().toLowerCase()) / 3.6 && velocity > this.getMinVelocity() / 3.6 && time < time_segment) {
                            comsumption_segment = comsumption;
                            time_segment = time;

                        }

                    }

                } else {
                    for (int r = car.getMax_rpm(); r > car.getMin_rpm(); r--) {
                        velocity = car.velocityByGearAndRpm(i, r);
                        time = (this.getLength() * 1000) / velocity;
                        work = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        comsumption = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

                        if (work > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity > this.getMinVelocity() / 3.6 && time < time_segment) {
                            comsumption_segment = comsumption;
                            time_segment = time;

                        }
                    }

                }

            }
            final_comsumption = final_comsumption + comsumption_segment;
            final_time = final_time + time_segment;
        }

        results[0] = final_time;
        results[1] = final_comsumption;
        return results;
    }

    /**
     * Calculates the fastest time a vehicle takes to complete the segment based
     * on consumption
     *
     * @param car the vehicle
     * @param s the section
     * @param direct the direction
     * @return the time and consumption
     */
    public double[] lowestConsuption(Vehicle car, Section s, boolean direct) {
        double[] results = new double[2];
        double final_time = 0;
        double comsumption = 0;

        double slope = 0;

        Map<String, Integer> limits = car.getVelocity_limit_list();
        double comsumption_segment = Double.MAX_VALUE;
        double time_segment = 0;
        double workmin = 0;
        double workmed = 0;
        double workmax = 0;

        double comsumption_min = 0;
        double comsumption_med = 0;
        double comsumption_max = 0;

        double velocity = 0;

        double time = 0;

        slope = this.getSlope() / 100;
        slope = Math.atan(Math.toRadians(slope));
        if (!direct) {
            slope = slope + 180;
        }
        for (int i = car.getGear_ratio().size(); i > 0; i--) {
            if (limits.containsKey(s.getType().toLowerCase())) {
                for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                    if (r != 0) {
                        velocity = car.velocityByGearAndRpm(i, r);
                        time = (this.getLength() * 1000) / velocity;

                        workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, s.getWindSpeed(), s.getWindDirection());

                        comsumption_min = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        comsumption_med = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        comsumption_max = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

                        if (workmin > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity < limits.get(s.getType().toLowerCase()) / 3.6 && velocity > this.getMinVelocity() / 3.6 && comsumption_min < comsumption_segment) {
                            comsumption_segment = comsumption_min;
                            time_segment = time;

                        }
                        if (workmed > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity < limits.get(s.getType().toLowerCase()) / 3.6 && velocity > this.getMinVelocity() / 3.6 && comsumption_med < comsumption_segment) {
                            comsumption_segment = comsumption_med;
                            time_segment = time;

                        }
                        if (workmax > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity < limits.get(s.getType().toLowerCase()) / 3.6 && velocity > this.getMinVelocity() / 3.6 && comsumption_max < comsumption_segment) {
                            comsumption_segment = comsumption_max;
                            time_segment = time;

                        }
                    }
                }

            } else {
                for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                    if (r != 0) {
                        velocity = car.velocityByGearAndRpm(i, r);
                        time = (this.getLength() * 1000) / velocity;

                        workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, s.getWindSpeed(), s.getWindDirection());

                        comsumption_min = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        comsumption_med = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        comsumption_max = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

                        if (workmin > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity > this.getMinVelocity() / 3.6 && comsumption_min < comsumption_segment) {
                            comsumption_segment = comsumption_min;
                            time_segment = time;

                        }
                        if (workmed > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity > this.getMinVelocity() / 3.6 && comsumption_med < comsumption_segment) {
                            comsumption_segment = comsumption_med;
                            time_segment = time;

                        }
                        if (workmax > 0 && (velocity < this.getMaxVelocity() / 3.6) && velocity > this.getMinVelocity() / 3.6 && comsumption_max < comsumption_segment) {
                            comsumption_segment = comsumption_max;
                            time_segment = time;

                        }
                    }

                }

            }

        }

        comsumption = comsumption + comsumption_segment;
        final_time = final_time + time_segment;

        results[0] = final_time;
        results[1] = comsumption;
        return results;
    }

    /**
     * Calculates the fastest time a vehicle takes to complete the segment based
     * on work
     *
     * @param car the vehicle
     * @param s the section
     * @param direct the direction
     * @return the time, consumption and work
     */
    public double[] lowestWork(Vehicle car, Section s, boolean direct) {
        // d = v*t;
        double[] results = new double[3];
        double work = 0;
        double time = 0;
        double comsumption = 0;

        double slope = 0;

        Map<String, Integer> limits = car.getVelocity_limit_list();
        double work_segment = Double.MAX_VALUE;
        double time_segment = 0;
        double comsumption_segment = 0;
        double workmin = 0;
        double workmed = 0;
        double workmax = 0;
        slope = this.getSlope() / 100;
        slope = Math.atan(Math.toRadians(slope));
        if (!direct) {
            slope = slope + 180;
        }
        for (int i = car.getGear_ratio().size(); i > 0; i--) {
            if (limits.containsKey(s.getType().toLowerCase())) {
                for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                    if (r != 0) {
                        workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        if (workmin > 0 && (car.velocityByGearAndRpm(i, r) < this.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) < limits.get(s.getType().toLowerCase()) / 3.6 && car.velocityByGearAndRpm(i, r) > this.getMinVelocity() / 3.6 && workmin < work_segment) {
                            work_segment = workmin;
                            time_segment = (this.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                            comsumption_segment = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time_segment, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());

                        }
                        if (workmed > 0 && (car.velocityByGearAndRpm(i, r) < this.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) < limits.get(s.getType().toLowerCase()) / 3.6 && car.velocityByGearAndRpm(i, r) > this.getMinVelocity() / 3.6 && workmed < work_segment) {
                            work_segment = workmed;
                            time_segment = (this.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                            comsumption_segment = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time_segment, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        }
                        if (workmax > 0 && (car.velocityByGearAndRpm(i, r) < this.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) < limits.get(s.getType().toLowerCase()) / 3.6 && car.velocityByGearAndRpm(i, r) > this.getMinVelocity() / 3.6 && workmax < work_segment) {
                            work_segment = workmax;
                            time_segment = (this.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                            comsumption_segment = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time_segment, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        }
                    }
                }

            } else {
                for (int r = car.getMin_rpm(); r <= car.getMax_rpm(); r++) {
                    if (r != 0) {
                        workmin = car.calculateTotalForceApplied(i, slope, Vehicle.MIN_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmed = car.calculateTotalForceApplied(i, slope, Vehicle.MED_TROTLE, r, s.getWindSpeed(), s.getWindDirection());
                        workmax = car.calculateTotalForceApplied(i, slope, Vehicle.MAX_TROTLE, r, s.getWindSpeed(), s.getWindDirection());

                        if (workmin > 0 && (car.velocityByGearAndRpm(i, r) < this.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) > this.getMinVelocity() / 3.6 && workmin < work_segment) {
                            work_segment = workmin;
                            time_segment = (this.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                            comsumption_segment = car.energyComsumption(i, r, Vehicle.MIN_TROTLE, time_segment, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        }
                        if (workmed > 0 && (car.velocityByGearAndRpm(i, r) < this.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) > this.getMinVelocity() / 3.6 && workmed < work_segment) {
                            work_segment = workmed;
                            time_segment = (this.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                            comsumption_segment = car.energyComsumption(i, r, Vehicle.MED_TROTLE, time_segment, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        }
                        if (workmax > 0 && (car.velocityByGearAndRpm(i, r) < this.getMaxVelocity() / 3.6) && car.velocityByGearAndRpm(i, r) > this.getMinVelocity() / 3.6 && workmax < work_segment) {
                            work_segment = workmax;
                            time_segment = (this.getLength() * 1000) / car.velocityByGearAndRpm(i, r);
                            comsumption_segment = car.energyComsumption(i, r, Vehicle.MAX_TROTLE, time_segment, slope, this.getLength() * 1000, s.getWindSpeed(), s.getWindDirection());
                        }
                    }

                }

            }

        }
        work = work + (work_segment * (this.getLength() * 1000));
        time = time + time_segment;
        comsumption = comsumption + comsumption_segment;

        results[0] = time;
        results[1] = comsumption;
        results[2] = work;
        return results;
    }

    /**
     * Determines the best path algorithm calculation to use
     *
     * @param bestPathMethod the algorithm
     * @param vehicle the vehicle
     * @param section the section
     * @return the calculation result
     */
    public double[] algorithmUse(PathAlgorithms bestPathMethod, Vehicle vehicle, Section section) {
        if (bestPathMethod.toString().equals("Fastest Path")) {
            return this.timeSegment(vehicle, section, true);
        }
        if (bestPathMethod.toString().equals("Most Efficient Path")) {
            return this.lowestConsuption(vehicle, section, true);
        }
        return this.lowestWork(vehicle, section, true);
    }

    /**
     * Returns the Segment string
     *
     * @return the Segment string
     */
    @Override
    public String toString() {
        return this.getId();
    }

}
