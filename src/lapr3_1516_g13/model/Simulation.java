package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;

public class Simulation {

    private String name;
    private String description;
    private RoadNetwork associatedRoadNetwork;
    private List<SimulationResult> simulationResultList;
    private List<Traffic> trafficList;
    private boolean active;

    /**
     * Default Simulation Constructor
     */
    public Simulation() {
        this.associatedRoadNetwork = new RoadNetwork();
        this.simulationResultList = new ArrayList();
        this.trafficList = new ArrayList();
    }

    /**
     * Complete Simulation Constructor
     *
     * @param name the simulation name
     * @param description the simulation description
     * @param associatedRoadNetwork the simulation network
     * @param simulationResultList the simulation result list
     * @param trafficList the simulation traffic list
     * @param active the simulation active state
     */
    public Simulation(String name, String description, RoadNetwork associatedRoadNetwork, List<SimulationResult> simulationResultList, List<Traffic> trafficList, boolean active) {
        this.name = name;
        this.description = description;
        this.associatedRoadNetwork = associatedRoadNetwork;
        this.simulationResultList = simulationResultList;
        this.trafficList = trafficList;
        this.active = active;
    }

    /**
     * Returns the simulation name
     *
     * @return the simulation name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the simulation description
     *
     * @return the simulation description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the simulation network
     *
     * @return the simulation network
     */
    public RoadNetwork getAssociatedRoadNetwork() {
        return associatedRoadNetwork;
    }

    /**
     * Returns the simulation result list
     *
     * @return the simulation result list
     */
    public List<SimulationResult> getSimulationResultList() {
        return simulationResultList;
    }

    /**
     * Returns the simulation traffic list
     *
     * @return the simulation traffic list
     */
    public List<Traffic> getTrafficList() {
        return trafficList;
    }

    /**
     * Returns the simulation active state
     *
     * @return the simulation active state
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Modifies the simulation name
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Modifies the simulation description
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Modifies the simulation active state (true or false)
     *
     * @param active the new active state
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Modifies the simulation network
     *
     * @param associatedRoadNetwork the new network
     */
    public void setAssociatedRoadNetwork(RoadNetwork associatedRoadNetwork) {
        this.associatedRoadNetwork = associatedRoadNetwork;
    }

    /**
     * Modifies the simulation result list
     *
     * @param simulationResultList the new result list
     */
    public void setSimulationResultList(List<SimulationResult> simulationResultList) {
        this.simulationResultList = simulationResultList;
    }

    /**
     * Modifies the simulation traffic list
     *
     * @param trafficList the new traffic list
     */
    public void setTrafficList(List<Traffic> trafficList) {
        this.trafficList = trafficList;
    }

    /**
     * Deletes the simulation result list
     *
     * @return the deleted simulation result list
     */
    public boolean deleteSimulationResults() {
        this.simulationResultList.clear();

        return this.simulationResultList.isEmpty();
    }

    /**
     * Creates a new instance of SimulationResult
     *
     * @return a new instance of SimulationResult
     */
    public SimulationResult newSimulationResult() {
        return new SimulationResult();
    }

    /**
     * Registers a new Simulation result in the simulation result list
     *
     * @param result the new result
     * @return true if registered successfully, otherwise returns false
     */
    public boolean addSimulationResult(SimulationResult result) {
        if (!this.simulationResultList.contains(result)) {
            return this.simulationResultList.add(result);
        }
        return false;
    }

    /**
     * Validates the data of the simulation
     *
     * @return true if the simulation data is valid, otherwise returns false
     */
    public boolean validate() {
        if (this.name == null || this.description == null) {
            return false;
        }

        return !this.name.trim().isEmpty()
                && !this.description.trim().isEmpty()
                && this.associatedRoadNetwork.validate()
                && !this.trafficList.isEmpty();
    }

    /**
     * Validates a Simulation Result parameters
     * 
     * @param result the simulation result
     * @return true if valid, otherwise returns false
     */
    public boolean validateResult(SimulationResult result) {
        return !this.simulationResultList.contains(result) && result.validate();
    }

    /**
     * Creates and retuns a clone of the simulation without the results
     *
     * @return the cloned simulation without
     */
    public Simulation cloneNoResults() {
        Simulation copy = new Simulation();
        copy.setAssociatedRoadNetwork(this.associatedRoadNetwork);
        copy.setTrafficList(this.trafficList);
        return copy;
    }

    /**
     * Creates and returns a clone of the simulation
     *
     * @return a clone of the simulation
     */
    public Simulation clone() {
        return new Simulation(this.name, this.description, this.associatedRoadNetwork, this.simulationResultList, this.trafficList, this.active);
    }

    /**
     * Verifies if the simulation is equal to another simulation received as
     * parameter
     *
     * @param obj the other simulation
     * @return true if the simulations are equal, otherwise returns false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Simulation other = (Simulation) obj;
        return this.getName().equalsIgnoreCase(other.getName());
    }

    /**
     * Returns a string with the name and description of the simulation
     *
     * @return a string with the name and description of the simulation
     */
    @Override
    public String toString() {
        return String.format("Simulation %s - %s", this.name, this.description);
    }

    /**
     * Returns a SimulationResult with the name that equals the string
     *
     * @param result
     * @return a simulation with the name that equals the string str
     */
    public SimulationResult getSelectedResult(SimulationResult result) {
        for (SimulationResult simulationResult : this.getSimulationResultList()) {
            if (simulationResult.getName().equals(result.getName())) {
                return simulationResult;
            }
        }
        return null;
    }

    /**
     * Removes a SimulationResult from the simulationResultList that has the
     * same as the string that it receives
     *
     * @param name the simulation name
     * @return true if it successfully removed the SimulatioResult desired and
     * returns false otherwise
     */
    public boolean remove(String name) {
        for (SimulationResult simulationResult : simulationResultList) {
            if (simulationResult.getName().equals(name)) {
                return simulationResultList.remove(simulationResult);
            }
        }
        return false;
    }

}
