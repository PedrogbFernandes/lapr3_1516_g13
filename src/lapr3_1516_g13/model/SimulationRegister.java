package lapr3_1516_g13.model;

import java.util.LinkedList;
import java.util.List;

public class SimulationRegister {

    private List<Simulation> listSimulation;

    /**
     * SimulationRegister controller
     */
    public SimulationRegister() {
        this.listSimulation = new LinkedList<>();
    }

    /**
     * Returns a simulation with the same name as the string received as
     * parameter
     *
     * @param str the string with the name
     * @return a simulation
     */
    public Simulation getSelectedSimulation(String str) {
        for (Simulation simulation : listSimulation) {
            if (simulation.getName().equals(str)) {
                return simulation;
            }
        }
        return null;
    }

    public Simulation newSimulation() {
        return new Simulation();
    }

    /**
     * Removes a simulation from the simulation list
     *
     * @param s the simulation
     * @return true if removed successfully, otherwise returns false
     */
    public boolean removeSimulation(Simulation s) {
        return this.listSimulation.remove(s);
    }

    /**
     * Adds a simulation to the simulation list
     *
     * @param s the simulation
     * @return true if the simulation is added successfully, otherwise returns
     * false
     */
    public boolean addSimulation(Simulation s) {
        LinkedList<Simulation> list;
        list = (LinkedList) this.listSimulation;
        list.addLast(s);

        return listSimulation.contains(s);
    }

    /**
     * Returns the simulation list
     *
     * @return the simulation list
     */
    public List<Simulation> getListSimulation() {
        return listSimulation;
    }

    /**
     * Modifies the simulation list
     *
     * @param listSimulation a simulation list
     */
    public void setListSimulation(List<Simulation> listSimulation) {
        this.listSimulation = listSimulation;
    }

    /**
     * Validates a simulation received as parameter
     * 
     * @param s the simulation
     * @return true if simulation is valid, otherwise returns false
     */
    public boolean validateSimulation(Simulation s) {
        return s.validate() && validate(s);
    }

    /**
     * Changes a present simulation in the list with the new edited simulation
     *
     * @param s the current simulation
     * @param sClone the current simulation (edited)
     * @return true if changed successfully, otherwise returns false
     */
    public boolean editSimulation(Simulation s, Simulation sClone) {
        List<Simulation> lstSim = new LinkedList<>(this.listSimulation);
        lstSim.remove(s);
        if (sClone.validate() && !lstSim.contains(sClone)) {
            return lstSim.add(sClone);
        }
        return false;
    }

    /**
     * Returns the current active simulation
     *
     * @return the current active simulation
     */
    public Simulation getActiveSimulation() {
        if (this.listSimulation.isEmpty()) {
            return null;
        }

        LinkedList<Simulation> list = (LinkedList) this.listSimulation;

        Simulation s = list.getFirst();
        if (s.isActive()) {
            return s;
        }

        return null;
    }

    /**
     * Tries to set a simulation received as parameter as the active simulation
     *
     * @param s the simulation
     * @return true if the simulation was set as the active simulation ,
     * otherwise returns false
     */
    public boolean setActiveSimulation(Simulation s) {
        Simulation first = this.getActiveSimulation();

        if (first == null) {
            return activateSimulation(s);
        }

        if (first.equals(s)) {
            return true;
        }

        if (activateSimulation(s)) {
            first.setActive(false);
            return true;
        }

        return false;
    }

    private boolean activateSimulation(Simulation s) {
        if (this.listSimulation.contains(s)) {
            if (this.listSimulation.remove(s)) {
                s.setActive(true);
                LinkedList<Simulation> list = (LinkedList) this.listSimulation;
                list.addFirst(s);
                return true;
            }
        }
        return false;
    }

    private boolean validate(Simulation s) {
        return !this.listSimulation.contains(s);
    }
}
