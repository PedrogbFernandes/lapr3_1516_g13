package lapr3_1516_g13.model;

import Graph.Edge;
import Graph.Graph;
import PathAlgorithms.PathAlgorithms;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SimulationResult {

    private String name;
    private double duration;
    private double timeStep;
    private PathAlgorithms bestPathMethod;
    //Para mostrar
    private List<VehicleResult> createdList;

    /**
     * Default SimulationResult Constructor
     */
    public SimulationResult() {
        this.createdList = new ArrayList<>();
    }

    /**
     * Complete SimulationResult controller
     *
     * @param name the result name
     * @param duration the result duration
     * @param timeStep the result time step
     * @param bestPathMethod the result best path algorithm
     */
    public SimulationResult(String name, double duration, double timeStep, PathAlgorithms bestPathMethod) {
        this.name = name;
        this.duration = duration;
        this.timeStep = timeStep;
        this.bestPathMethod = bestPathMethod;
        this.createdList = new ArrayList<>();
    }

    /**
     * Returns the simulation result name
     *
     * @return the simulation result name
     */
    public String getName() {
        return name;
    }

    public List<VehicleResult> getCreatedList() {
        return createdList;
    }

    public void setCreatedList(List<VehicleResult> createdList) {
        this.createdList = createdList;
    }

    /**
     * Returns the simulation result duration
     *
     * @return the simulation result duration
     */
    public double getDuration() {
        return duration;
    }

    /**
     * Returns the simulation result time step
     *
     * @return the simulation result time step
     */
    public double getTimeStep() {
        return timeStep;
    }

    /**
     * Returns the simulation result best path algorithm
     *
     * @return the simulation result best path algorithm
     */
    public PathAlgorithms getBestPathMethod() {
        return bestPathMethod;
    }

    /**
     * Modifies the simulation result name
     *
     * @param name the new simulation result name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Modifies the simulation result duration
     *
     * @param duration the new simulation result duration
     */
    public void setDuration(double duration) {
        this.duration = duration;
    }

    /**
     * Modifies the simulation result time step
     *
     * @param timeStep the new simulation result time step
     */
    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    /**
     * Modifies the simulation result best path method
     *
     * @param bestPathMethod the new simulation result best path method
     */
    public void setBestPathMethod(PathAlgorithms bestPathMethod) {
        this.bestPathMethod = bestPathMethod;
    }

    /**
     * Validates the simulation result
     *
     * @return true if valid, otherwise returns false
     */
    public boolean validate() {
        if (this.name == null || this.bestPathMethod == null) {
            return false;
        }

        return !this.name.trim().isEmpty()
                && this.duration > 0
                && this.timeStep > 0 && this.timeStep < this.duration;
    }

    /**
     * Verifies if the simulation result is equal to another simulation result
     * received as parameter
     *
     * @param obj the other simulation result
     * @return true if the simulations results are equal, otherwise returns
     * false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimulationResult other = (SimulationResult) obj;
        return this.getName().equalsIgnoreCase(other.getName());
    }

    /**
     * Runs the simulation traffic analysis based on the path algorithm chosen
     * for the simulation run
     *
     * @param analise the static analysis
     * @param trafico the traffic list
     * @param roadNetwork the network graph
     */
    public void runTrafficAnalysis(StaticAnalysis analise, List<Traffic> trafico, Graph<Junction, List<Section>> roadNetwork) {
        int i = 0;
        for (Traffic t : trafico) {
            Junction enterNode = t.getEnterNode();
            Junction exitNode = t.getExitNode();
            Vehicle veiculo = t.getVehicle();
            t.getVehicle().setTrafficId("t" + i);
            analise.setStart(enterNode);
            analise.setEnd(exitNode);
            analise.setVehicle(veiculo);
            analise.setSectionList(new ArrayList<>());

            analise.calculate(roadNetwork);
            t.setListPathTraffic(analise.getSectionList());
            i++;
        }
    }

    /**
     * Runs a simulation
     *
     * @param traffic the simulation traffic list
     * @param roadNetwork the simulation road network
     */
    public void runSimulation(List<Traffic> traffic, RoadNetwork roadNetwork) {

        StaticAnalysis analise = new StaticAnalysis();
        analise.setAlgorithm(this.bestPathMethod);

        Graph<Junction, List<Section>> graph = roadNetwork.getGraph();

        runTrafficAnalysis(analise, traffic, graph);

        for (int i = 0; i <= duration; i += timeStep) {
            int trafficId = 0;
            for (Traffic t : traffic) {
                if (i * 60 >= t.getNextTimeEnter()) {
                    //Insere veículos a partir do gerador random de tempos e entrada
                    Vehicle b = new Vehicle();
                    b = t.getVehicle().clone2(i, t.getNextTimeEnter());
                    t.getListPathTraffic().get(0).getSegmentList().get(0).addVehicle(b, 0.0);
                    t.setNextTimeEnter(t.getNextTimeEnter() + t.timeInterval());

                    VehicleResult a = new VehicleResult(t);
                    a.setVehicleName(b.getName());
                    a.setTrafficId(trafficId);
                    createdList.add(a);
                }

                for (Section section : t.getListPathTraffic()) {
                    for (int j = section.getSegmentList().size() - 1; j >= 0; j--) {
                        Segment segment = section.getSegmentList().get(j);
                        if (!segment.isEmpty()) {
                            LinkedList<Vehicle> auxList = new LinkedList<>();
                            auxList.addAll(segment.getVehicleList());
                            LinkedList<Vehicle> auxWaitingList = new LinkedList<>();
                            auxWaitingList.addAll(segment.getVehicleWaitingList());
                            for (Vehicle vehicle : segment.getVehicleList()) {
                                if (t.getVehicle().getTrafficId().equals(vehicle.getTrafficId()) && vehicle.getTimeStep() == i) {
                                    double[] aux = segment.algorithmUse(bestPathMethod, vehicle, section);
                                    double time = aux[0];
                                    double consumption = aux[1];
                                    if (i * 60 >= time + vehicle.getInitTimeSegment()) {
                                        auxList.remove(vehicle);
                                        if (!auxWaitingList.isEmpty()) {
                                            //Se a waitingList tiver algum carro após remover o veículo insere o que está na fila de espera
                                            auxList.addLast(auxWaitingList.getFirst());
                                            auxList.getLast().setTimeStep(i + timeStep);
                                            auxList.getLast().setTotalTimeWaiting(auxList.getLast().getTotalTimeWaiting() + auxList.getLast().getInitWaiting() - vehicle.getEndTimeSegment());
                                            auxWaitingList.getFirst().setInitTimeSegment(vehicle.getEndTimeSegment());
                                            auxWaitingList.removeFirst();

                                        }
                                        if (section.getSegmentList().size() - 1 == vehicle.getSegment()) {
                                            if (t.getListPathTraffic().size() - 1 == vehicle.getSection()) {
                                                //Se chegar ao fim do seu guarda o tempo final
                                                vehicle.setEndTimeSegment(vehicle.getInitTimeSegment() + time);
                                                vehicle.setEndTime(vehicle.getEndTimeSegment());
                                                //Guarda resultados na lista de resultados de segmentos
                                                t.getInstantsDrops().add(vehicle.getEndTime());

                                                Result c = new Result(section, segment, vehicle.getInitTimeSegment(), vehicle.getEndTimeSegment(), (consumption + vehicle.fuelOnStandBy(vehicle.getTotalTimeWaiting())));
                                                for (VehicleResult d : createdList) {
                                                    if (d.getVehicleName() == vehicle.getName() && d.getTrafficId() == trafficId) {
                                                        d.setEndTime(vehicle.getEndTime());
                                                        d.addResult(c);
                                                    }
                                                }

                                            } else {
                                                //Se chegar ao fim da secção muda para a próxima e guarda dados do fim do segmento anterior
                                                vehicle.setEndTimeSegment(vehicle.getInitTimeSegment() + time);
                                                //Guarda resultados na lista de resultados de segmentos
                                                Result c = new Result(section, segment, vehicle.getInitTimeSegment(), vehicle.getEndTimeSegment(), consumption);
                                                for (VehicleResult d : createdList) {
                                                    if (d.getVehicleName() == vehicle.getName() && d.getTrafficId() == trafficId) {
                                                        d.addResult(c);
                                                    }
                                                }

                                                vehicle.setSection(vehicle.getSection() + 1);
                                                vehicle.setSegment(0);
                                                if (t.getListPathTraffic().get(vehicle.getSection()).getSegmentList().get(vehicle.getSegment()).addVehicle(vehicle, vehicle.getEndTimeSegment())) {
                                                    vehicle.setInitTimeSegment(vehicle.getEndTimeSegment());
                                                }

                                            }
                                        } else {
                                            //Se chegar ao fim do segmento e tem outro segmento a seguir guarda os dados do segmento
                                            vehicle.setEndTimeSegment(vehicle.getInitTimeSegment() + time);
                                            //Guarda dados da viagem do veiculo no segmento
                                            Result c = new Result(section, segment, vehicle.getInitTimeSegment(), vehicle.getEndTimeSegment(), (consumption));
                                            for (VehicleResult d : createdList) {
                                                if (d.getVehicleName() == vehicle.getName() && d.getTrafficId() == trafficId) {
                                                    d.addResult(c);
                                                }
                                            }

                                            vehicle.setSegment(vehicle.getSegment() + 1);
                                            if (t.getListPathTraffic().get(vehicle.getSection()).getSegmentList().get(vehicle.getSegment()).addVehicle(vehicle, vehicle.getEndTimeSegment())) {
                                                vehicle.setInitTimeSegment(vehicle.getEndTimeSegment());
                                            }
                                        }

                                    }
                                    vehicle.setTimeStep(vehicle.getTimeStep() + timeStep);
                                }
                                segment.setVehicleWaitingList(auxWaitingList);
                                segment.setVehicleList(auxList);
                            }
                        }
                    }
                }
                trafficId++;
            }
        }

        for (Traffic t : traffic) {
            for (Section section : t.getListPathTraffic()) {
                for (Segment seg : section.getSegmentList()) {
                    seg.getVehicleList().clear();
                    seg.getVehicleWaitingList().clear();
                }
            }
            t.getListPathTraffic().clear();
            t.setNextTimeEnter(0);
        }
    }

    /**
     * Returns the simulation result string
     *
     * @return the simulation result string
     */
    @Override
    public String toString() {
        return String.format("Result: %s - Duration: %.2f, Step: %.2f, Method: %s",
                this.getName(), this.getDuration(), this.getTimeStep(), this.getBestPathMethod().toString());
    }

    public List<String> generateAvEnergyForEachTrafficPattern(List<Traffic> traffic) {
        DecimalFormat df = new DecimalFormat("0.##E0");
        int numTraffic = traffic.size();
        List<String> result = new ArrayList<>();

        for (int i = 0; i < numTraffic; i++) {
            double sum = 0;
            double count = 0;
            for (VehicleResult b : createdList) {
                if (b.getTrafficId() == i) {
                    for (Result a : b.getSegmentsResults()) {
                        sum += a.getEnergySpent();
                        count++;
                    }
                }
            }

            if (count != 0) {
                result.add("Traffic begin node: " + traffic.get(i).getEnterNode().getIndex() + "; end node: " + traffic.get(i).getExitNode().getIndex() + "; Vehicle: " + traffic.get(i).getVehicle().getName() + "; Average Energy Consumption: " + df.format(sum / count));
            }
        }
        return result;
    }

    public List<String> generateAvEnergyForEachTrafficPattenAllSegments(RoadNetwork road, List<Traffic> traffic) {
        DecimalFormat df = new DecimalFormat("0.##E0");
        int numTraffic = traffic.size();
        ArrayList<Section> allSections = new ArrayList<Section>();
        for (Edge<Junction, List<Section>> a : road.getGraph().edges()) {
            for (Section b : a.getElement()) {
                if (!allSections.contains(b)) {
                    allSections.add(b);
                }
            }
        }
        List<String> result = new ArrayList<>();
        for (int i = 0; i < numTraffic; i++) {
            for (Section sec : allSections) {
                for (Segment seg : sec.getSegmentList()) {
                    double sum = 0;
                    double count = 0;
                    int entrou = 0;
                    for (VehicleResult b : createdList) {
                        if (b.getTrafficId() == i) {
                            for (Result a : b.getSegmentsResults()) {
                                if (sec.getRoad().getName().equals(a.getSection().getRoad().getName()) && seg.getId() == a.getSegment().getId()) {
                                    entrou = 1;
                                    sum += a.getEnergySpent();
                                    count++;
                                }
                            }
                        }
                    }
                    if (entrou == 1) {
                        result.add("Traffic begin node: " + traffic.get(i).getEnterNode().getIndex() + ", end node: " + traffic.get(i).getExitNode().getIndex() + ", Vehicle: " + traffic.get(i).getVehicle().getName() + ", Section: " + sec.getRoad().getName() + ", Segment: " + seg.getId() + ", Average Energy Consumption: " + df.format(sum / count));
                    }
                }
            }
        }
        return result;
    }

    public List<String> generateAvEnergyForSpecificTrafficPatternAllSegments(RoadNetwork road, List<Traffic> traffic, int numberTraffic) {
        DecimalFormat df = new DecimalFormat("0.##E0");
        DecimalFormat df2 = new DecimalFormat("0.00");
        List<String> result = new ArrayList<>();
        ArrayList<Section> allSections = new ArrayList<Section>();
        for (Edge<Junction, List<Section>> a : road.getGraph().edges()) {
            for (Section b : a.getElement()) {
                if (!allSections.contains(b)) {
                    allSections.add(b);
                }
            }
        }
        for (Section sec : allSections) {
            for (Segment seg : sec.getSegmentList()) {
                double sum = 0;
                double sumTime = 0;
                double count = 0;
                int entrou = 0;
                for (VehicleResult b : createdList) {
                    if (b.getTrafficId() == numberTraffic) {
                        for (Result a : b.getSegmentsResults()) {
                            if (sec.getRoad().getName().equals(a.getSection().getRoad().getName()) && seg.getId() == a.getSegment().getId()) {
                                entrou = 1;
                                sumTime += a.getInstantOut() - a.getIntantIn();
                                sum += a.getEnergySpent();
                                count++;
                            }
                        }
                    }
                }
                if (entrou == 1) {
                    result.add("Traffic begin node: " + traffic.get(numberTraffic).getEnterNode().getIndex() + ", end node: " + traffic.get(numberTraffic).getExitNode().getIndex() + ", Vehicle: " + traffic.get(numberTraffic).getVehicle().getName() + ", Section: " + sec.getRoad().getName() + ", Segment: " + seg.getId() + ", Average Energy Consumption: " + df.format(sum / count) + ", Average Time Spent: " + df.format(sumTime / count));
                }
            }
        }
        return result;
    }
}
