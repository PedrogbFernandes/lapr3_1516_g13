package lapr3_1516_g13.model;

public class Simulator {

    private ProjectRegister projectRegister;
    private AlgorithmList algorithmList;

    /**
     * Simulator Constructor
     */
    public Simulator() {
        this.projectRegister = new ProjectRegister();
        this.algorithmList = new AlgorithmList();
    }

    /**
     * Returns the Simulator project register
     *
     * @return the Simulator project register
     */
    public ProjectRegister getProjectRegister() {
        return this.projectRegister;
    }

    /**
     * Returns the Simulator algorithm list
     *
     * @return the Simulator algorithm list
     */
    public AlgorithmList getAlgorithmList() {
        return algorithmList;
    }

}
