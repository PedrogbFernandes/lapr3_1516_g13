/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import PathAlgorithms.PathAlgorithms;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import Graph.Graph;
import java.util.Collections;

/**
 *
 * @author pedro
 */
public class StaticAnalysis {

    /**
     * The vehicle that user will be using
     */
    private Vehicle vehicle;

    /**
     * The starting point
     */
    private Junction start;

    /**
     * The ending point
     */
    private Junction end;

    /**
     * The algorithm that this "simulation" will be using
     */
    private PathAlgorithms algorithm;

    /**
     * The cost of the toll
     */
    private double tollcost;

    /**
     * The time to complete the course
     */
    private double time;
    /**
     * The fuel used to complete the course
     */
    private double comsumption;

    /**
     * The list of sections on which the car is driven
     */
    private List<Section> sectionList;

    /**
     * Constructor
     */
    public StaticAnalysis() {
        sectionList = new ArrayList<>();
    }

    /**
     * Returns the vehicle that the user will be using;
     *
     * @return the vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Returns the starting point
     *
     * @return starting point
     */
    public Junction getStart() {
        return start;
    }

    /**
     * Returns the ending point
     *
     * @return ending point
     */
    public Junction getEnd() {
        return end;
    }

    /**
     * Returns the analysis path algorithm
     *
     * @return the analysis path algorithm
     */
    public PathAlgorithms getAlgorithm() {
        return algorithm;
    }

    /**
     * Returns the analysis toll cost
     *
     * @return the analysis toll cost
     */
    public double getTollcost() {
        return tollcost;
    }

    /**
     * Returns the analysis time
     *
     * @return the analysis time
     */
    public double getTime() {
        return time;
    }

    /**
     * Returns the analysis section list
     *
     * @return the analysis section list
     */
    public List<Section> getSectionList() {
        return sectionList;
    }

    /**
     * Returns the analysis consumption
     *
     * @return the analysis consumption
     */
    public double getComsumption() {
        return this.comsumption;
    }

    /**
     * Modifies the current vehicle
     *
     * @param vehicle the vehicle to be added to this analysis
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * Modifies the current starting point
     *
     * @param start the new starting point
     */
    public void setStart(Junction start) {
        this.start = start;
    }

    /**
     * Modifies the current ending point
     *
     * @param end the new ending point
     */
    public void setEnd(Junction end) {
        this.end = end;
    }

    /**
     * Modifies the analysis algorithm
     *
     * @param algorithm the new algorithm
     */
    public void setAlgorithm(PathAlgorithms algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * Modifies the analysis toll cost
     *
     * @param tollcost the new toll cost
     */
    public void setTollcost(double tollcost) {
        this.tollcost = tollcost;
    }

    /**
     * Modifies the analysis time
     *
     * @param time the new time
     */
    public void setTime(double time) {
        this.time = time;
    }

    public void setSectionList(List<Section> sectionList) {
        this.sectionList = sectionList;
    }

    /**
     * Modifies the analysis consumption
     *
     * @param comsumption the new consumption
     */
    public void setComsumption(double comsumption) {
        this.comsumption = comsumption;
    }

    /**
     * Calculates all the information for the given parameters
     */
    public void calculate(Graph<Junction, List<Section>> graph) {

        LinkedList<Junction> path = new LinkedList<>();
        double[] results = this.algorithm.bestPath(graph, start, end, vehicle, path, this.sectionList);
        Collections.reverse(sectionList);
        this.time = results[0];
        this.comsumption = results[1];

        for (Section s : sectionList) {
            this.tollcost += s.getToll();
        }

    }

    /**
     * Method validate. checks if the beggining is equals to the end
     *
     * @return boolean
     */
    public boolean validate() {
        String startString = this.start.getIndex();
        String endString = this.end.getIndex();
        return !startString.equals(endString);

    }

    /**
     * Returns the analysis string
     *
     * @return the analysis string
     */
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Static Analysis Information: \n");
        str.append(algorithm.toString());
        str.append("\n");
        str.append("From: ");
        str.append(start.toString());
        str.append("\n");
        str.append("To: ");
        str.append(end.toString());
        str.append("\n\n");
        str.append("Vehicle: ");
        str.append(vehicle.getName());
        str.append("-");
        str.append(vehicle.getFuel());
        str.append("\n\n");
        str.append("Path: \n");
        for (Section s : sectionList) {
            str.append(s.toString());
            str.append("\n");
        }

        str.append("\nToll Costs: ");
        str.append(this.tollcost);
        str.append(" $");
        str.append("\nDriving Time: ");
        str.append(String.format("%.1f", this.time));
        str.append(" s");
        str.append("\nEnergy Comsumption: ");
        str.append(String.format("%.1f", this.comsumption));
        str.append(" W");
        return str.toString();
    }

}
