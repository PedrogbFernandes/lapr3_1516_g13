package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcelo Oliveira
 */
public class Traffic {

    private Junction enterNode;
    private Junction exitNode;
    private Vehicle vehicle;
    private int arrivalRate;

    private double nextTimeEnter;

    private List<Section> listPathTraffic;
    private List<Double> instantsDrops;

    /**
     * Traffic Constructor
     */
    public Traffic() {
        this.listPathTraffic = new ArrayList<>();
        this.instantsDrops = new ArrayList<>();
    }

    /**
     * Returns the traffic enter node
     *
     * @return the traffic enter node
     */
    public Junction getEnterNode() {
        return enterNode;
    }

    /**
     * Returns the traffic exit node
     *
     * @return the traffic exit node
     */
    public Junction getExitNode() {
        return exitNode;
    }

    /**
     * Returns the traffic vehicle
     *
     * @return the traffic vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Returns the traffic arrival rate
     *
     * @return the traffic arrival rate
     */
    public int getArrivalRate() {
        return arrivalRate;
    }

    /**
     * Returns the traffic path traffic list
     *
     * @return the traffic path traffic list
     */
    public List<Section> getListPathTraffic() {
        return listPathTraffic;
    }

    /**
     * Returns the next time of enter
     *
     * @return the next time of enter
     */
    public double getNextTimeEnter() {
        return nextTimeEnter;
    }

    /**
     * Returns the list of instants drop
     *
     * @return the list of instants drop
     */
    public List<Double> getInstantsDrops() {
        return instantsDrops;
    }

    /**
     * Modifies the traffic enter node
     *
     * @param enterNode the traffic enter node
     */
    public void setEnterNode(Junction enterNode) {
        this.enterNode = enterNode;
    }

    /**
     * Modifies the traffic exit node
     *
     * @param exitNode the traffic exit node
     */
    public void setExitNode(Junction exitNode) {
        this.exitNode = exitNode;
    }

    /**
     * Modifies the traffic vehicle
     *
     * @param vehicle the new vehicle
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * Modifies the traffic arrival rate
     *
     * @param arrivalRate the new arrival rate
     */
    public void setArrivalRate(int arrivalRate) {
        this.arrivalRate = arrivalRate;
    }

    /**
     * Modifies the traffic path list
     *
     * @param listPathTraffic the new path list
     */
    public void setListPathTraffic(List<Section> listPathTraffic) {
        this.listPathTraffic = listPathTraffic;
    }

    /**
     * Modifies the traffic next time enter
     *
     * @param nextTimeEnter the new next time enter
     */
    public void setNextTimeEnter(double nextTimeEnter) {
        this.nextTimeEnter = nextTimeEnter;
    }

    /**
     * Calculates the time interval based on the arrival rate
     *
     * @return the time interval
     */
    public double timeInterval() {
        double timeInterval = 0;
        double lambda = 1.0 / this.arrivalRate;
        double distribution = Math.random();
        timeInterval = (-1 * Math.log(1 - distribution)) / lambda;
        return timeInterval;
    }

}
