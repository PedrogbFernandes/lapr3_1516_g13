package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Vehicle {

    private String name;
    private String description;
    private String type;
    private String motorization;
    private String fuel;

    private int segment;
    private int section;
    private double timeStep;
    private double initialTime;
    private double endTime;
    private double initTimeSegment;
    private double endTimeSegment;
    private double initWaiting;
    private double totalTimeWaiting;

    private String trafficId;

    private double mass;
    private double load;
    private double dragCoefficient;
    private double rrc;
    private double frontal_area;
    private double wheel_size;

    private Map<String, Integer> velocity_limit_list;

    private int min_rpm;
    private int max_rpm;

    private double final_drive_ratio;

    private List<Double> gear_ratio;

    private Map<Double, List<Regime>> throttle_list;

    public static final double GRAVITY = 9.81;
    public static final double AIR_DENSITY = 1.225;
    public static final int KILO_WATT_HOUR = 3600000;

    public static final double MAX_TROTLE = 1;
    public static final double MED_TROTLE = 0.5;
    public static final double MIN_TROTLE = 0.25;

    public static final int windMax = 30;

    /**
     * Vehicle Constructor
     */
    public Vehicle() {
        this.velocity_limit_list = new HashMap();
        this.gear_ratio = new ArrayList<>();
        this.throttle_list = new TreeMap();
    }

    public Vehicle(String name, String description, String type, String motorization, String fuel, double mass, double load, double dragCoefficient, double rrc, double frontal_area, double wheel_size, Map<String, Integer> velocity_limit_list, int min_rpm, int max_rpm, double final_drive_ratio, List<Double> gear_ratio, Map<Double, List<Regime>> throttle_list) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.motorization = motorization;
        this.fuel = fuel;
        this.mass = mass;
        this.load = load;
        this.dragCoefficient = dragCoefficient;
        this.rrc = rrc;
        this.frontal_area = frontal_area;
        this.wheel_size = wheel_size;
        this.velocity_limit_list = velocity_limit_list;
        this.min_rpm = min_rpm;
        this.max_rpm = max_rpm;
        this.final_drive_ratio = final_drive_ratio;
        this.gear_ratio = gear_ratio;
        this.throttle_list = throttle_list;
    }

    /**
     * Returns the vehicle initial time in the segment
     *
     * @return the vehicle initial time in the segment
     */
    public double getInitTimeSegment() {
        return initTimeSegment;
    }

    public double getTotalTimeWaiting() {
        return totalTimeWaiting;
    }

    public void setTotalTimeWaiting(double totalTimeWaiting) {
        this.totalTimeWaiting = totalTimeWaiting;
    }

    /**
     * Modifies the vehicle initial time in the segment
     *
     * @param initTimeSegment the new initial time
     */
    public void setInitTimeSegment(double initTimeSegment) {
        this.initTimeSegment = initTimeSegment;
    }

    public double getInitWaiting() {
        return initWaiting;
    }

    public void setInitWaiting(double initWaiting) {
        this.initWaiting = initWaiting;
    }

    /**
     * Returns the vehicle end time in the segment
     *
     * @return the vehicle end time in the segment
     */
    public double getEndTimeSegment() {
        return endTimeSegment;
    }

    /**
     * Modifies the vehicle end time in the segment
     *
     * @param endTimeSegment the new end time
     */
    public void setEndTimeSegment(double endTimeSegment) {
        this.endTimeSegment = endTimeSegment;
    }

    /**
     * Returns the vehicle time step
     *
     * @return the vehicle time step
     */
    public double getTimeStep() {
        return timeStep;
    }

    /**
     * Modifies the vehicle time step
     *
     * @param timeStep the new time step
     */
    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    /**
     * Returns the vehicle section
     *
     * @return the vehicle section
     */
    public int getSection() {
        return section;
    }

    /**
     * Modifies the vehicle section
     *
     * @param section the new section
     */
    public void setSection(int section) {
        this.section = section;
    }

    /**
     * Returns the vehicle segment
     *
     * @return the vehicle segment
     */
    public int getSegment() {
        return segment;
    }

    /**
     * Modifies the vehicle segment
     *
     * @param segment the new segment
     */
    public void setSegment(int segment) {
        this.segment = segment;
    }

    /**
     * Returns the vehicle name
     *
     * @return the vehicle name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the vehicle description
     *
     * @return the vehicle description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the vehicle type
     *
     * @return the vehicle type
     */
    public String getType() {
        return type;
    }

    /**
     * Modifies the vehicle type
     *
     * @param type the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the vehicle motorization
     *
     * @return the vehicle motorization
     */
    public String getMotorization() {
        return motorization;
    }

    /**
     * Returns the vehicle fuel
     *
     * @return the vehicle fuel
     */
    public String getFuel() {
        return fuel;
    }

    /**
     * Returns the vehicle mass
     *
     * @return the vehicle mass
     */
    public double getMass() {
        return mass;
    }

    /**
     * Returns the vehicle load
     *
     * @return the vehicle load
     */
    public double getLoad() {
        return load;
    }

    /**
     * Returns the vehicle drag coefficient
     *
     * @return the vehicle drag coefficient
     */
    public double getDragCoefficient() {
        return dragCoefficient;
    }

    /**
     * Returns the vehicle rrc
     *
     * @return the vehicle rrc
     */
    public double getRrc() {
        return rrc;
    }

    /**
     * Returns the vehicle frontal area
     *
     * @return the vehicle frontal area
     */
    public double getFrontal_area() {
        return frontal_area;
    }

    /**
     * Returns the vehicle wheel size
     *
     * @return the vehicle wheel size
     */
    public double getWheel_size() {
        return wheel_size;
    }

    /**
     * Returns the vehicle velocity limit list
     *
     * @return the vehicle velocity limit list
     */
    public Map<String, Integer> getVelocity_limit_list() {
        return velocity_limit_list;
    }

    /**
     * Returns the vehicle min rpm
     *
     * @return the vehicle min rpm
     */
    public int getMin_rpm() {
        return min_rpm;
    }

    /**
     * Returns the vehicle max rpm
     *
     * @return the vehicle max rpm
     */
    public int getMax_rpm() {
        return max_rpm;
    }

    /**
     * Returns the vehicle final drive ratio
     *
     * @return the vehicle final drive ratio
     */
    public double getFinal_drive_ratio() {
        return final_drive_ratio;
    }

    /**
     * Returns the vehicle gear ratio list
     *
     * @return the vehicle gear ratio list
     */
    public List<Double> getGear_ratio() {
        return gear_ratio;
    }

    /**
     * Returns the vehicle throttle list
     *
     * @return the vehicle throttle list
     */
    public Map<Double, List<Regime>> getThrottle_list() {
        return throttle_list;
    }

    /**
     * Returns the vehicle initial time
     *
     * @return the vehicle initial time
     */
    public double getInitialTime() {
        return initialTime;
    }

    /**
     * Returns the vehicle end time
     *
     * @return the vehicle end time
     */
    public double getEndTime() {
        return endTime;
    }

    /**
     * Modifies the vehicle name
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Modifies the vehicle description
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Modifies the vehicle motorization
     *
     * @param motorization the new motorization
     */
    public void setMotorization(String motorization) {
        this.motorization = motorization;
    }

    /**
     * Modifies the vehicle fuel
     *
     * @param fuel the new fuel
     */
    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    /**
     * Modifies the vehicle mass
     *
     * @param mass the new mass
     */
    public void setMass(double mass) {
        this.mass = mass;
    }

    /**
     * Modifies the vehicle load
     *
     * @param load the new load
     */
    public void setLoad(double load) {
        this.load = load;
    }

    /**
     * Modifies the vehicle drag coefficient
     *
     * @param dragCoefficient the new drag coefficient
     */
    public void setDragCoefficient(double dragCoefficient) {
        this.dragCoefficient = dragCoefficient;
    }

    /**
     * Modifies the vehicle rrc
     *
     * @param rrc the new rrc
     */
    public void setRrc(double rrc) {
        this.rrc = rrc;
    }

    /**
     * Modifies the vehicle frontal area
     *
     * @param frontal_area the new frontal area
     */
    public void setFrontal_area(double frontal_area) {
        this.frontal_area = frontal_area;
    }

    /**
     * Modifies the vehicle wheel size
     *
     * @param wheel_size the new wheel size
     */
    public void setWheel_size(double wheel_size) {
        this.wheel_size = wheel_size;
    }

    /**
     * Modifies the vehicle velocity limit list
     *
     * @param velocity_limit_list the new velocity limit list
     */
    public void setVelocity_limit_list(Map<String, Integer> velocity_limit_list) {
        this.velocity_limit_list = velocity_limit_list;
    }

    /**
     * Modifies the vehicle min rpm
     *
     * @param min_rpm the new min rpm
     */
    public void setMin_rpm(int min_rpm) {
        this.min_rpm = min_rpm;
    }

    /**
     * Modifies the vehicle max rpm
     *
     * @param max_rpm the new max rpm
     */
    public void setMax_rpm(int max_rpm) {
        this.max_rpm = max_rpm;
    }

    /**
     * Modifies the vehicle final drive ratio
     *
     * @param final_drive_ratio the new final drive ratio
     */
    public void setFinal_drive_ratio(double final_drive_ratio) {
        this.final_drive_ratio = final_drive_ratio;
    }

    /**
     * Modifies the vehicle gear ratio list
     *
     * @param gear_ratio the new gear ratio list
     */
    public void setGear_ratio(List<Double> gear_ratio) {
        this.gear_ratio = gear_ratio;
    }

    /**
     * Modifies the vehicle throttle list
     *
     * @param throttle_list the new throttle list
     */
    public void setThrottle_list(Map<Double, List<Regime>> throttle_list) {
        this.throttle_list = throttle_list;
    }

    /**
     * Modifies the vehicle initial time
     *
     * @param initialTime the new initial time
     */
    public void setInitialTime(double initialTime) {
        this.initialTime = initialTime;
    }

    /**
     * Modifies the vehicle end time
     *
     * @param endTime the new end time
     */
    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    /**
     * Returns the force that the car exercs on itself to go foward
     *
     * @param gear the gear that the engine is using
     * @param rpm the rotations per minute of the engine
     * @param trotle the amount of trotle that is being used on the accelarator
     * @return the force
     */
    public double calculateCarFowardForce(int gear, int rpm, double trotle) {
        int torque = getTorqueByRpmAndTrotle(rpm, trotle);
        double force = (torque * final_drive_ratio * this.gear_ratio.get(gear - 1)) / (this.wheel_size / 2);
        return force;
    }

    /**
     * Returns the Dry friction that the wheels exerce on the ground
     *
     * @return the force
     */
    public double calculateCarRollingForce(double angle) {
        return (this.mass + this.load) * GRAVITY * this.rrc * Math.cos(Math.toRadians(angle));
    }

    /**
     * Returns the air drag that the air exercs on the car
     *
     * @param gear the gear that the engine is using
     * @return the force
     */
    public double calculateCarAirDragForce(int gear, int rpm, double windSpeed, double windDirection) {
        double airDragForce = 0;
        double windSpeedx = 0;
        double carVelocity = 0;
        if (windDirection > 90 || windDirection < -90) {
            windSpeedx = windSpeed * Math.cos(Math.toRadians(windDirection));
            windSpeed = Math.abs(windSpeedx);
            carVelocity = velocityByGearAndRpm(gear, rpm);
            carVelocity = carVelocity - windSpeed;
            if (carVelocity >= windMax) {
                carVelocity = windMax;

            }
        } else {
            windSpeedx = windSpeed * Math.cos(Math.toRadians(windDirection));
            windSpeed = Math.abs(windSpeedx);
            carVelocity = velocityByGearAndRpm(gear, rpm);
            carVelocity = carVelocity + windSpeed;
        }

        airDragForce = 0.5 * frontal_area * dragCoefficient * AIR_DENSITY * carVelocity * carVelocity;

        return airDragForce;
    }

    /**
     * Returns the maximum velocity that the car can achieve using a determined
     * gear
     *
     * @param gear the gear that the car is using
     * @return the maximum velocity
     */
    public double velocityByGearAndRpm(int gear, int rpm) {
        double rps = (double) rpm / 60;
        return (2 * Math.PI * (this.wheel_size / 2) * rps) / (this.final_drive_ratio * this.gear_ratio.get(gear - 1));

    }

    /**
     * Returns the force applied to the car when its using an inclinated road
     *
     * @param angle the angle that the segment has
     * @return the force
     */
    public double calculateInclinationAppliedForce(double angle) {
        double force = (this.mass + this.load) * GRAVITY * Math.sin(Math.toRadians(angle));;
        return force;
    }

    /**
     * Returns all the forces applied to a car on a determined road at a
     * determined gear
     *
     * @param gear the gear that the car is using
     * @param angle the angle that the segment has
     * @return all the force applieds
     */
    public double calculateTotalForceApplied(int gear, double angle, double trotle, int rpm, double windSpeed, double windDirection) {
        return calculateCarFowardForce(gear, rpm, trotle) - (calculateCarRollingForce(angle) + calculateInclinationAppliedForce(angle) + calculateCarAirDragForce(gear, rpm, windSpeed, windDirection));
    }

    /**
     * Calculate the vehicle energy consumption
     *
     * @param gear the vehicle gear
     * @param rpm the vehicle rpm
     * @param trotle the vehicle throttle
     * @param time the time
     * @param angle the segment angle
     * @param distance the segment distance
     * @param windSpeed the section wind speed
     * @param windDirection the section wind direction
     * @return the consumption
     */
    public double energyComsumption(int gear, int rpm, double trotle, double time, double angle, double distance, double windSpeed, double windDirection) {
        return 0;
    }

    /**
     * Returns the torque based on the rpm an throttle
     *
     * @param rpm the rpm
     * @param trotle the throttle
     * @return the torque
     */
    public int getTorqueByRpmAndTrotle(int rpm, double trotle) {
        int torque = 0;
        List<Regime> list = this.throttle_list.get(trotle);
        for (Regime r : list) {
            if (rpm <= r.getRpm_high() && rpm >= r.getRpm_low()) {
                torque = r.getTorque();
                break;
            }
        }

        return torque;
    }

    /**
     * Creates a clone of the vehicle
     *
     * @return the clone
     */
    public Vehicle clone() {
        return new Vehicle(name, description, type, motorization, fuel, mass, load, dragCoefficient, rrc, frontal_area, wheel_size, velocity_limit_list, min_rpm, max_rpm, final_drive_ratio, gear_ratio, throttle_list);
    }

    /**
     * Creates a clone of the vehicle with a new name
     *
     * @param i the name index
     * @param initialTime the initial time
     * @return the clone
     */
    public Vehicle clone2(double i, double initialTime) {
        Vehicle vehicle = new Vehicle(name, description, type, motorization, fuel, mass, load, dragCoefficient, rrc, frontal_area, wheel_size, velocity_limit_list, min_rpm, max_rpm, final_drive_ratio, gear_ratio, throttle_list);
        vehicle.setName(name + "_" + i);
        vehicle.setTrafficId(trafficId);
        vehicle.setSection(0);
        vehicle.setSegment(0);
        vehicle.setTimeStep(i);
        vehicle.setInitialTime(initialTime);
        vehicle.setInitTimeSegment(initialTime);
        return vehicle;
    }

    /**
     * Calculates the vehicle power
     *
     * @param rpm the rpm
     * @param trotle the throttle
     * @return the power
     */
    public double calculatePower(int rpm, double trotle) {
        int torque = 0;
        double rps = 0;
        List<Regime> list = this.throttle_list.get(trotle);
        for (Regime r : list) {
            if (rpm <= r.getRpm_high() && rpm >= r.getRpm_low()) {
                torque = r.getTorque();
                rps = (double) rpm / 60;

            }
        }

        double power = 2 * Math.PI * torque * rps;
        return power;
    }

    /**
     * Calculates the vehicle dissipative forces
     *
     * @param angle the segment angle
     * @param gear the vehicle gear
     * @param rpm the vehicle rpm
     * @param windSpeed the section wind speed
     * @param windDirection the section wind direction
     * @return the force
     */
    public double calculateDissipativeForces(double angle, int gear, int rpm, double windSpeed, double windDirection) {
        return calculateCarAirDragForce(gear, rpm, windSpeed, windDirection) + calculateCarRollingForce(angle) + calculateInclinationAppliedForce(angle);
    }

    /**
     * Returns the fuel on stand by
     *
     * @param time the time
     * @return the fuel
     */
    public double fuelOnStandBy(double time) {
        return 0;
    }

    /**
     * Returns the vehicle traffic id
     *
     * @return the vehicle traffic id
     */
    public String getTrafficId() {
        return trafficId;
    }

    /**
     * Modifies the vehicle traffic id
     *
     * @param trafficId the new traffic id
     */
    public void setTrafficId(String trafficId) {
        this.trafficId = trafficId;
    }

    /**
     * Compares two vehicles by name and traffic id
     *
     * @param obj the other vehicle
     * @return true if the vehicle are equal, otherwise returns false
     */
    @Override
    public boolean equals(Object obj) {
        Vehicle vehicle = (Vehicle) obj;
        if (this.getTrafficId() == null) {
            return this.getName().equals(vehicle.getName());
        }
        return this.getName().equals(vehicle.getName()) && this.getTrafficId().equals(vehicle.getTrafficId());
    }

    /**
     * Returns the vehicle string
     *
     * @return the vehicle string
     */
    @Override
    public String toString() {
        return getName() + " - " + getDescription();
    }

}
