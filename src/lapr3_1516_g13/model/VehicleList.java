package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;

public class VehicleList {

    private List<Vehicle> vehicleList;

    /**
     * VehicleList Constructor
     */
    public VehicleList() {
        this.vehicleList = new ArrayList();
    }

    /**
     * Returns the vehicle list
     *
     * @return the vehicle list
     */
    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }

    /**
     * Modifies the vehicle list
     *
     * @param vehicleList the vehicle list
     */
    public void setVehicleList(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    /**
     * Adds a vehicle to the vehicle list
     *
     * @param a the vehicle
     */
    public void addVehicle(Vehicle a) {
        this.vehicleList.add(a);
    }

    /**
     * Returns a vehicle from the vehicle list based on the name
     *
     * @param name the name of the vehicle
     * @return the vehicle
     */
    public Vehicle getVehicle(String name) {
        for (Vehicle v : this.vehicleList) {
            if (v.getName().equalsIgnoreCase(name)) {
                return v;
            }
        }
        return null;
    }

    /**
     * Validates the vehicle list
     *
     * @return true if valid (not empty), otherwise returns false
     */
    public boolean validate() {
        return !this.vehicleList.isEmpty();
    }

}
