package lapr3_1516_g13.model;

import java.util.LinkedList;

public class VehicleResult implements Comparable {

    private Traffic traffic;
    private int traffic_id;
    private double endTime;
    private LinkedList<Result> segmentsResults;

    private String vehicle_name;

    /**
     * VehicleResult Constructor, receives a Traffic object as parameter
     *
     * @param traffic the traffic object
     */
    public VehicleResult(Traffic traffic) {
        this.traffic = traffic;
        this.segmentsResults = new LinkedList<>();
    }

    /**
     * Returns the vehicle result traffic pattern
     *
     * @return the vehicle result traffic pattern
     */
    public Traffic getTraffic() {
        return traffic;
    }

    /**
     * Returns the vehicle result end time
     *
     * @return the vehicle result end time
     */
    public double getEndTime() {
        return endTime;
    }

    /**
     * Returns the vehicle result traffic id
     *
     * @return the vehicle result traffic id
     */
    public int getTrafficId() {
        return traffic_id;
    }

    /**
     * Returns the segments result list from the vehicle result
     *
     * @return the segments result list
     */
    public LinkedList<Result> getSegmentsResults() {
        return segmentsResults;
    }

    /**
     * Returns the vehicle name of the vehicle result
     *
     * @return the vehicle name of the vehicle result
     */
    public String getVehicleName() {
        return vehicle_name;
    }

    /**
     * Modifies the traffic patterns
     *
     * @param traffic the new traffic pattern
     */
    public void setTraffic(Traffic traffic) {
        this.traffic = traffic;
    }

    /**
     * Modifies the traffic id
     *
     * @param traffic_id the new traffic id
     */
    public void setTrafficId(int traffic_id) {
        this.traffic_id = traffic_id;
    }

    /**
     * Modifies the end time
     *
     * @param endTime the new end time
     */
    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    /**
     * Modifies the segment result list
     *
     * @param segmentsResults the new segment result list
     */
    public void setSegmentsResults(LinkedList<Result> segmentsResults) {
        this.segmentsResults = segmentsResults;
    }

    /**
     * Modifies the vehicle name
     *
     * @param vehicle_name the new vehicle name
     */
    public void setVehicleName(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    /**
     * Adds a result to the segment result list
     *
     * @param a the result
     */
    public void addResult(Result a) {
        this.segmentsResults.addLast(a);
    }

    /**
     * Compares two VehicleResults by traffic id
     *
     * @param o the other VehicleResult
     * @return 1 if the traffic id is greater, -1 if lesser, otherwise 0
     */
    @Override
    public int compareTo(Object o) {
        VehicleResult other = (VehicleResult) o;

        if (other.traffic_id > this.traffic_id) {
            return -1;
        }

        if (other.traffic_id < this.traffic_id) {
            return 1;
        }

        return 0;
    }

    /**
     * Returns the vehicle result string
     *
     * @return the vehicle result string
     */
    @Override
    public String toString() {
        return "Vehicle name " + this.traffic.getVehicle().getName() + " Enter node: " + this.traffic.getEnterNode().getIndex() + " Exit node: " + this.traffic.getExitNode().getIndex() + " Instant dropped out: " + this.endTime;
    }

}
