package lapr3_1516_g13.ui;

import PathAlgorithms.PathAlgorithms;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.DefaultCaret;
import lapr3_1516_g13.controllers.CompareVehiclesController;
import lapr3_1516_g13.datalayer.DataHTMLAnalysis;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Vehicle;

/**
 *
 * @author pedro
 */
public class CompareVehiclesUI extends JDialog {

    private Simulator simulator;

    private CompareVehiclesController controller;

    private JTextArea txtAddedCars;

    private JComboBox comboCar, comboSource, comboDestiny, comboAlgorithm;

    private JLabel labelCar, labelSource, labelDestiny, labelAlgorithm, labelAddedCars;

    private Junction start, end;

    private PathAlgorithms algorithm;

    private Vehicle car;

    private JButton buttonCreate, buttonCancel;

    private List<StaticAnalysis> listStaticAnalysis;

    private List<Vehicle> listVehicles;

    private static final Dimension LABEL_SIZE
            = new JLabel("XXXXXXXXXXXXXX").getPreferredSize();

    private static final Dimension JCOMBO_SIZE
            = new JLabel("XXXXXXXXXXXXXXXXXXXXXXXXXXX").getPreferredSize();

    private static final Dimension DIM_TXTAREA = new Dimension(400, 130);

    public CompareVehiclesUI(JFrame framepai, Simulator s) {
        super(framepai, "Compare Vehicles", true);
        if (s.getProjectRegister().getActiveProject() == null) {
            JOptionPane.showMessageDialog(CompareVehiclesUI.this,
                    "Error:\n No active project",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } else {
            this.listVehicles = new ArrayList<>();
            this.simulator = s;
            this.controller = new CompareVehiclesController(simulator);
            this.controller.newStaticAnalysis();
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowOpened(WindowEvent e) {

                    for (Vehicle v : controller.getListOfVehicles()) {
                        comboCar.addItem(v);
                    }

                    for (PathAlgorithms p : controller.getAlgorithmList()) {
                        comboAlgorithm.addItem(p);
                    }

                    for (Junction j : controller.getJunctionList()) {
                        comboSource.addItem(j);
                        comboDestiny.addItem(j);
                    }

                }
            });
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            add(panelPrincipal());
            setResizable(false);
            pack();
            setLocationRelativeTo(null);
            setVisible(true);
        }
    }

    private JPanel panelPrincipal() {
        JPanel painel = new JPanel(new BorderLayout());
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;

        painel.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        painel.add(panelChoose(), BorderLayout.NORTH);
        painel.add(panelAddedCars(), BorderLayout.CENTER);
        painel.add(panelButtons(), BorderLayout.SOUTH);
        return painel;

    }

    private JPanel panelChoose() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        p.add(panelStart());
        p.add(panelEnd());
        p.add(panelVehicle());
        p.add(panelAlgorithm());
        return p;
    }

    private JPanel panelStart() {

        JPanel panel = new JPanel(new FlowLayout());
        labelSource = new JLabel("Source point");
        labelSource.setPreferredSize(LABEL_SIZE);
        comboSource = new JComboBox();
        comboSource.setPreferredSize(JCOMBO_SIZE);

        comboSource.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboSource.getSelectedItem();
                start = (Junction) o;

            }

        });

        panel.add(labelSource);
        panel.add(comboSource);

        return panel;
    }

    private JPanel panelEnd() {

        JPanel panel = new JPanel(new FlowLayout());
        labelDestiny = new JLabel("Destiny point");
        labelDestiny.setPreferredSize(LABEL_SIZE);
        comboDestiny = new JComboBox();
        comboDestiny.setPreferredSize(JCOMBO_SIZE);

        comboDestiny.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboDestiny.getSelectedItem();
                end = (Junction) o;

            }

        });

        panel.add(labelDestiny);
        panel.add(comboDestiny);

        return panel;
    }

    private JPanel panelVehicle() {

        JPanel panel = new JPanel(new FlowLayout());
        labelCar = new JLabel("Vehicle");
        labelCar.setPreferredSize(LABEL_SIZE);
        comboCar = new JComboBox();
        comboCar.setPreferredSize(JCOMBO_SIZE);

        comboCar.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboCar.getSelectedItem();
                car = (Vehicle) o;

            }

        });

        panel.add(labelCar);
        panel.add(comboCar);

        return panel;
    }

    private JPanel panelAlgorithm() {

        JPanel panel = new JPanel(new FlowLayout());
        labelAlgorithm = new JLabel("Algorithm");
        labelAlgorithm.setPreferredSize(LABEL_SIZE);
        comboAlgorithm = new JComboBox();
        comboAlgorithm.setPreferredSize(JCOMBO_SIZE);

        comboAlgorithm.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboAlgorithm.getSelectedItem();
                algorithm = (PathAlgorithms) o;

            }

        });

        panel.add(labelAlgorithm);
        panel.add(comboAlgorithm);

        return panel;
    }

    private JPanel panelAddedCars() {
        JPanel painel = new JPanel(new FlowLayout());
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        painel.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        painel.add(painelAddedVehicles());
        return painel;
    }

    private JPanel painelAddedVehicles() {
        JPanel painel = new JPanel(new GridLayout(2, 1));
        labelAddedCars = new JLabel("Added Vehicles", JLabel.CENTER);
        txtAddedCars = new JTextArea();

        txtAddedCars.setLineWrap(true);
        txtAddedCars.setEditable(false);
        txtAddedCars.setFont(new Font("Calibri", Font.BOLD, 13));

        JScrollPane s = new JScrollPane(txtAddedCars, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        s.setPreferredSize(new Dimension(DIM_TXTAREA));
        DefaultCaret caret = (DefaultCaret) txtAddedCars.getCaret();
        caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);

        painel.add(labelAddedCars);
        painel.add(s);
        return painel;
    }

    private JPanel panelButtons() {
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(buttonCreate());
        panel.add(buttonCancel());
        panel.add(buttonAdd());
        return panel;
    }

    private JButton buttonCreate() {
        JButton b = new JButton("Create");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (start == null || end == null || algorithm == null || listVehicles == null || listVehicles.size() < 2) {
                    JOptionPane.showMessageDialog(CompareVehiclesUI.this,
                            "Error:\n You need to choose two or more vehicles,two nodes and a algorithm",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    listStaticAnalysis = controller.compareValues(start, end, listVehicles, algorithm);
                    if (listStaticAnalysis.isEmpty()) {
                        JOptionPane.showMessageDialog(CompareVehiclesUI.this,
                                "Error:\n You can't choose the same source and destiny",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        StringBuilder str = new StringBuilder();
                        for (StaticAnalysis s : listStaticAnalysis) {
                            str.append(s.toString());
                            str.append("\n\n\n");
                        }
                        int result = JOptionPane.OK_CANCEL_OPTION;
                        String[] option = {"Export to Html", "Cancel"};
                        result = JOptionPane.showOptionDialog(CompareVehiclesUI.this,
                                str.toString(), "Info", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, option, null);
                        if (result == 0) {
                            DataHTMLAnalysis html = new DataHTMLAnalysis();
                            JFileChooser fileChooser = new JFileChooser(new File("C:\\"));
                            definirFiltroExtensaoHTML(fileChooser);
                            int resposta = fileChooser.showSaveDialog(CompareVehiclesUI.this);
                            if (resposta == JFileChooser.APPROVE_OPTION) {
                                File file = fileChooser.getSelectedFile();
                                html.setPath(file.getPath());
                                html.setListAnalysis(listStaticAnalysis);
                                try {
                                    html.createFile();
                                } catch (Exception ex) {
                                    JOptionPane.showMessageDialog(
                                            CompareVehiclesUI.this, "Error", "File could not save", JOptionPane.ERROR_MESSAGE);
                                }
                            }
                        } else if (result == 1) {
                            dispose();
                        }
                        dispose();

                    }

                }

            }

        });
        return b;

    }

    private JButton buttonCancel() {
        JButton b = new JButton("Cancel");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }

        });
        return b;

    }

    private JButton buttonAdd() {
        JButton b = new JButton("Add vehicle");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Vehicle car = (Vehicle) comboCar.getSelectedItem();
                if (!listVehicles.contains(car)) {
                    listVehicles.add(car);
                    JOptionPane.showMessageDialog(CompareVehiclesUI.this,
                            car.getName() + " has been added!", "New Car Added", JOptionPane.INFORMATION_MESSAGE);
                    StringBuilder str = new StringBuilder();
                    for (Vehicle v : listVehicles) {
                        str.append(v.toString());
                        str.append("\n");
                    }
                    txtAddedCars.setText(str.toString());
                } else {
                    JOptionPane.showMessageDialog(CompareVehiclesUI.this,
                            "Error:\n That car is already in the list",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }

            }

        });
        return b;

    }

    private void definirFiltroExtensaoHTML(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("html");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.html";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }
}
