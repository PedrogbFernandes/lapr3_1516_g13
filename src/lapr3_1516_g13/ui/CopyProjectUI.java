package lapr3_1516_g13.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import lapr3_1516_g13.controllers.CopyProjectController;
import lapr3_1516_g13.model.Simulator;

/**
 *
 * @author Marcelo Oliveira
 */
public class CopyProjectUI extends JDialog {

    private final Simulator simulator;
    private final CopyProjectController controller;

    private JButton btnCopy, btnCancel;
    private JLabel lblTitle, lblTitle2, lblName, lblDescription;
    private JTextField txtName, txtDescription;
    private JTextArea areaJuctions, areaSections, areaTraffic;
    
    public CopyProjectUI(JFrame owner, Simulator simulator) {
        super(owner, "Copy Project");

        this.simulator = simulator;
        this.controller = new CopyProjectController(this.simulator);

        if (!controller.getActiveProject()) {
            JOptionPane.showMessageDialog(CopyProjectUI.this,
                    "No current active project",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);
            dispose();
        } else {

            setPreferredSize(new Dimension(600, 225));
            setResizable(false);

            add(createPanelCopySimulation(), BorderLayout.NORTH);

            pack();
            setLocationRelativeTo(owner);

            setVisible(true);
        }
    }

    private JPanel createPanelCopySimulation() {
        final int UP_MARGIN = 10, DOWN_MARGIN = 10;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelData(), BorderLayout.CENTER);
        painel.add(createPanelButtons(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelData() {
        final int UP_MARGIN = 5, DOWN_MARGIN = 20;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelNameDescription(), BorderLayout.NORTH);
        return painel;
    }

    private JPanel createPanelNameDescription() {
        JPanel p = new JPanel(new GridLayout(4, 2, 0, 5));

        String[] data = controller.getNameDescription();

        lblTitle = new JLabel("Copying Project", JLabel.LEFT);
        lblTitle2 = new JLabel("Project - " + data[0] + ", " + data[1]);

        lblName = new JLabel("Name: ", JLabel.LEFT);
        txtName = new JTextField();

        lblDescription = new JLabel("Description: ", JLabel.LEFT);
        txtDescription = new JTextField();

        p.add(lblTitle);
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(lblTitle2);
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(lblName);
        p.add(txtName);
        p.add(lblDescription);
        p.add(txtDescription);

        return p;
    }

    private JPanel createPanelButtons() {
        final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 10, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS,
                NUMERO_COLUNAS,
                INTERVALO_HORIZONTAL,
                INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        btnCopy = createButtonCopy();
        btnCancel = createButtonCancel();

        p.add(btnCopy);
        p.add(btnCancel);

        return p;
    }

    private JButton createButtonCopy() {
        JButton b = new JButton("Copy");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                controller.setNameDescription(txtName.getText(), txtDescription.getText());
                
                Object[] options = {"Yes", "No"};
                int n = JOptionPane.showOptionDialog(CopyProjectUI.this, "Create the current project copy?", "Copy Project", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (n == 0) {
                    boolean validated = controller.validateProjectDB();
                    if (validated) {
                        boolean registered = controller.registerProjectDB();
                        if (registered) {
                            JOptionPane.showMessageDialog(CopyProjectUI.this,
                                    "Project successfully copied");
                            dispose();
                        } else {
                            JOptionPane.showMessageDialog(CopyProjectUI.this,
                                    "Unable to register project:\n Another project already has that name",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(CopyProjectUI.this,
                                "Invalid name and/or description",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCancel() {
        JButton b = new JButton("Cancel");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b;
    }
}
