package lapr3_1516_g13.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import lapr3_1516_g13.controllers.CreateProjectController;
import lapr3_1516_g13.model.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CreateProjectUI extends JDialog {

    private final Simulator simulator;
    private final CreateProjectController controller;

    private JButton btnCreate, btnCancel, btnRoad, btnVehicles;
    private JLabel lblPName, lblPDescription, lblPRoadNetwork, lblPVehicles, lblPFormat;
    private JTextField txtName, txtDescription;
    private JComboBox comboFormat;
    private JFileChooser fileChooser;

    public CreateProjectUI(JFrame owner, Simulator simulator) {
        super(owner, "Create Project");

        this.simulator = simulator;
        this.controller = new CreateProjectController(this.simulator);

        this.controller.createNewProject();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        setPreferredSize(new Dimension(600, 290));
        setResizable(false);

        add(createPanelCreateProject(), BorderLayout.NORTH);

        pack();
        setLocationRelativeTo(owner);

        setVisible(true);
    }

    private JPanel createPanelCreateProject() {
        final int UP_MARGIN = 10, DOWN_MARGIN = 10;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelData(), BorderLayout.CENTER);
        painel.add(createPanelButtons(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelData() {
        final int UP_MARGIN = 5, DOWN_MARGIN = 20;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelProjectData(), BorderLayout.CENTER);
        painel.add(createPanelProjectFiles(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelProjectData() {
        JPanel p = new JPanel(new GridLayout(2, 2, 0, 5));

        lblPName = new JLabel("Name: ", JLabel.LEFT);
        txtName = new JTextField();

        lblPDescription = new JLabel("Description: ", JLabel.LEFT);
        txtDescription = new JTextField();

        p.add(lblPName);
        p.add(txtName);
        p.add(lblPDescription);
        p.add(txtDescription);

        return p;
    }

    private JPanel createPanelProjectFiles() {
        JPanel p = new JPanel(new GridLayout(4, 2, 0, 5));

        lblPRoadNetwork = new JLabel("RoadNetwork File (XML): ", JLabel.LEFT);
        lblPVehicles = new JLabel("Vehicles File (XML): ", JLabel.LEFT);
        lblPFormat = new JLabel("Simulation Results Export Format: ", JLabel.LEFT);

        comboFormat = new JComboBox(this.controller.getFileFormats().toArray());

        btnRoad = createRoadButton();
        btnVehicles = createVehiclesButton();

        p.add(new JLabel(""));
        p.add(new JLabel(""));
        p.add(lblPRoadNetwork);
        p.add(btnRoad);
        p.add(lblPVehicles);
        p.add(btnVehicles);
        p.add(lblPFormat);
        p.add(comboFormat);

        return p;
    }

    private JPanel createPanelButtons() {
        final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 10, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS,
                NUMERO_COLUNAS,
                INTERVALO_HORIZONTAL,
                INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        btnCreate = createButtonCreate();
        btnCancel = createButtonCancel();

        p.add(btnCreate);
        p.add(btnCancel);

        return p;
    }

    private JButton createRoadButton() {
        JButton b = new JButton("Choose File...");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                defineXMLFilter(fileChooser);

                int choose = fileChooser.showOpenDialog(CreateProjectUI.this);

                if (choose == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    try {
                        controller.selectRoadNetworkFile(file);
                        btnRoad.setText(file.getName());
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(CreateProjectUI.this,
                                "Invalid file!",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createVehiclesButton() {
        JButton b = new JButton("Choose File...");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                defineXMLFilter(fileChooser);

                int choose = fileChooser.showOpenDialog(CreateProjectUI.this);

                if (choose == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    try {
                        controller.selectVehicleFile(file);
                        btnVehicles.setText(file.getName());
                    } catch (Exception ex) {
                        System.out.println(ex);
                        JOptionPane.showMessageDialog(CreateProjectUI.this,
                                "Invalid file!",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCreate() {
        JButton b = new JButton("Create");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                controller.insertNameDescription(txtName.getText(), txtDescription.getText());
                controller.selectFileFormat((String) comboFormat.getSelectedItem());

                Object[] options = {"Yes", "No"};
                int n = JOptionPane.showOptionDialog(CreateProjectUI.this, "Create project?", "Create Project", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (n == 0) {
                    boolean validated = controller.validateProjectDB();
                    if (validated) {
                        boolean registered = controller.registerProjectDB();
                        if (registered) {
                            JOptionPane.showMessageDialog(CreateProjectUI.this,
                                    "Project successfully created");
                            dispose();
                        } else {
                            JOptionPane.showMessageDialog(CreateProjectUI.this,
                                    "Error creating the project",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(CreateProjectUI.this,
                                "Invalid Project",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCancel() {
        JButton b = new JButton("Cancel");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b;
    }

    private void defineXMLFilter(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("xml");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.xml";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

}
