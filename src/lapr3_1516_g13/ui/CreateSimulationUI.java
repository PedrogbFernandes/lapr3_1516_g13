package lapr3_1516_g13.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import lapr3_1516_g13.controllers.CreateSimulationController;
import lapr3_1516_g13.model.Simulator;

/**
 *
 * @author Marcelo Oliveira
 */
public class CreateSimulationUI extends JDialog {

    private final Simulator simulator;
    private final CreateSimulationController controller;

    private JButton btnCreate, btnCancel, btnTraffic;
    private JLabel lblName, lblDescription, lblTraffic;
    private JTextField txtName, txtDescription;
    private JFileChooser fileChooser;

    public CreateSimulationUI(JFrame owner, Simulator simulator) {
        super(owner, "Create Simulation");
        this.simulator = simulator;
        this.controller = new CreateSimulationController(this.simulator);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        if (!controller.createSimulation()) {
            JOptionPane.showMessageDialog(CreateSimulationUI.this,
                    "No current active project",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);
            dispose();
        } else {

            setPreferredSize(new Dimension(550, 230));
            setResizable(false);

            add(createPanelCreateSimulation(), BorderLayout.NORTH);

            pack();
            setLocationRelativeTo(owner);

            setVisible(true);
        }
    }

    private JPanel createPanelCreateSimulation() {
        final int UP_MARGIN = 10, DOWN_MARGIN = 10;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelData(), BorderLayout.CENTER);
        painel.add(createPanelButtons(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelData() {
        final int UP_MARGIN = 5, DOWN_MARGIN = 20;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelSimulationData(), BorderLayout.CENTER);
        painel.add(createPanelSimulationFiles(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelSimulationData() {
        JPanel p = new JPanel(new GridLayout(2, 2, 0, 5));

        lblName = new JLabel("Name: ", JLabel.LEFT);
        txtName = new JTextField();

        lblDescription = new JLabel("Description: ", JLabel.LEFT);
        txtDescription = new JTextField();

        p.add(lblName);
        p.add(txtName);
        p.add(lblDescription);
        p.add(txtDescription);

        return p;
    }

    private JPanel createPanelSimulationFiles() {
        JPanel p = new JPanel(new GridLayout(2, 2, 0, 5));

        lblTraffic = new JLabel("Traffic File (XML): ", JLabel.LEFT);

        btnTraffic = createTrafficButton();

        p.add(new JLabel(""));
        p.add(new JLabel(""));
        p.add(lblTraffic);
        p.add(btnTraffic);

        return p;
    }

    private JPanel createPanelButtons() {
        final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 10, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS,
                NUMERO_COLUNAS,
                INTERVALO_HORIZONTAL,
                INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        btnCreate = createButtonCreate();
        btnCancel = createButtonCancel();

        p.add(btnCreate);
        p.add(btnCancel);

        return p;
    }

    private JButton createTrafficButton() {
        JButton b = new JButton("Choose File...");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                defineXMLFilter(fileChooser);

                int choose = fileChooser.showOpenDialog(CreateSimulationUI.this);

                if (choose == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    try {
                        controller.selectTrafficFile(file);
                        btnTraffic.setText(file.getName());
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(CreateSimulationUI.this,
                                "Invalid file!",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }


    private JButton createButtonCreate() {
        JButton b = new JButton("Create");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                controller.setNameDescription(txtName.getText(), txtDescription.getText());

                Object[] options = {"Yes", "No"};
                int n = JOptionPane.showOptionDialog(CreateSimulationUI.this, "Create simulation?", "Create Simulation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (n == 0) {
                    boolean validated = controller.validateSimulationDB();
                    if (validated) {
                        boolean registered = controller.registerSimulationDB();
                        if (registered) {
                            JOptionPane.showMessageDialog(CreateSimulationUI.this,
                                    "Simulation successfully created");
                            dispose();
                        } else {
                            JOptionPane.showMessageDialog(CreateSimulationUI.this,
                                    "Error creating the simulation",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(CreateSimulationUI.this,
                                "Invalid Simulation - Missing data",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCancel() {
        JButton b = new JButton("Cancel");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b;
    }

    private void defineXMLFilter(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("xml");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.xml";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

}
