package lapr3_1516_g13.ui;

import PathAlgorithms.PathAlgorithms;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import lapr3_1516_g13.controllers.CreateStaticAnalysisController;
import lapr3_1516_g13.datalayer.DataHTMLAnalysis;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Vehicle;

/**
 *
 * @author pedro
 */
public class CreateStaticAnalysisUI extends JDialog {

    private Simulator simulator;

    private CreateStaticAnalysisController controller;

    private JComboBox comboCar, comboSource, comboDestiny, comboAlgorithm;

    private JLabel labelCar, labelSource, labelDestiny, labelAlgorithm;

    private Junction start, end;

    private PathAlgorithms algorithm;

    private Vehicle car;

    private JButton buttonCreate, buttonCancel;

    private static final Dimension LABEL_SIZE
            = new JLabel("XXXXXXXXXXXXXX").getPreferredSize();

    private static final Dimension JCOMBO_SIZE
            = new JLabel("XXXXXXXXXXXXXXXXXXXXXXXXXXX").getPreferredSize();

    public CreateStaticAnalysisUI(JFrame framepai, Simulator s) {
        super(framepai, "Create new Static Analysis", true);
        if (s.getProjectRegister().getActiveProject() == null) {
            JOptionPane.showMessageDialog(CreateStaticAnalysisUI.this,
                    "Error:\n No active project",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } else {

            this.simulator = s;
            this.controller = new CreateStaticAnalysisController(simulator);
            this.controller.newStaticAnalysis();
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowOpened(WindowEvent e) {

                    for (Vehicle v : controller.getListOfVehicles()) {
                        comboCar.addItem(v);
                    }

                    for (PathAlgorithms p : controller.getAlgorithmList()) {
                        comboAlgorithm.addItem(p);
                    }

                    for (Junction j : controller.getJunctionList()) {
                        comboSource.addItem(j);
                        comboDestiny.addItem(j);
                    }

                }
            });
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            add(panelPrincipal());
            setResizable(false);
            pack();
            setLocationRelativeTo(null);
            setVisible(true);
        }
    }

    private JPanel panelPrincipal() {
        JPanel painel = new JPanel(new GridLayout(5, 1));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;

        painel.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        painel.add(panelStart());
        painel.add(panelEnd());
        painel.add(panelVehicle());
        painel.add(panelAlgorithm());
        painel.add(panelButtons());
        return painel;

    }

    private JPanel panelStart() {

        JPanel panel = new JPanel(new FlowLayout());
        labelSource = new JLabel("Source point");
        labelSource.setPreferredSize(LABEL_SIZE);
        comboSource = new JComboBox();
        comboSource.setPreferredSize(JCOMBO_SIZE);

        comboSource.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboSource.getSelectedItem();
                start = (Junction) o;

            }

        });

        panel.add(labelSource);
        panel.add(comboSource);

        return panel;
    }

    private JPanel panelEnd() {

        JPanel panel = new JPanel(new FlowLayout());
        labelDestiny = new JLabel("Destiny point");
        labelDestiny.setPreferredSize(LABEL_SIZE);
        comboDestiny = new JComboBox();
        comboDestiny.setPreferredSize(JCOMBO_SIZE);

        comboDestiny.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboDestiny.getSelectedItem();
                end = (Junction) o;

            }

        });

        panel.add(labelDestiny);
        panel.add(comboDestiny);

        return panel;
    }

    private JPanel panelVehicle() {

        JPanel panel = new JPanel(new FlowLayout());
        labelCar = new JLabel("Vehicle");
        labelCar.setPreferredSize(LABEL_SIZE);
        comboCar = new JComboBox();
        comboCar.setPreferredSize(JCOMBO_SIZE);

        comboCar.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboCar.getSelectedItem();
                car = (Vehicle) o;

            }

        });

        panel.add(labelCar);
        panel.add(comboCar);

        return panel;
    }

    private JPanel panelAlgorithm() {

        JPanel panel = new JPanel(new FlowLayout());
        labelAlgorithm = new JLabel("Algorithm");
        labelAlgorithm.setPreferredSize(LABEL_SIZE);
        comboAlgorithm = new JComboBox();
        comboAlgorithm.setPreferredSize(JCOMBO_SIZE);

        comboAlgorithm.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                Object o = comboAlgorithm.getSelectedItem();
                algorithm = (PathAlgorithms) o;

            }

        });

        panel.add(labelAlgorithm);
        panel.add(comboAlgorithm);

        return panel;
    }

    private JPanel panelButtons() {
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(buttonCreate());
        panel.add(buttonCancel());
        return panel;
    }

    private JButton buttonCreate() {
        JButton b = new JButton("Create");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (start == null || end == null || algorithm == null || car == null) {
                    JOptionPane.showMessageDialog(CreateStaticAnalysisUI.this,
                            "Error:\n You need to choose one vehicle,two nodes and a algorithm",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    StaticAnalysis s = controller.insertValues(start, end, car, algorithm);
                    if (s == null) {
                        JOptionPane.showMessageDialog(CreateStaticAnalysisUI.this,
                                "Error:\n You can't choose the same source and destiny",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    } else {

                        int result = JOptionPane.OK_CANCEL_OPTION;
                        String[] option = {"Export to Html", "Cancel"};
                        result = JOptionPane.showOptionDialog(CreateStaticAnalysisUI.this,
                                s.toString(), "Info", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, option, null);
                        if (result == 0) {
                            DataHTMLAnalysis html = new DataHTMLAnalysis();
                            JFileChooser fileChooser = new JFileChooser(new File("C:\\"));
                            definirFiltroExtensaoHTML(fileChooser);
                            int resposta = fileChooser.showSaveDialog(CreateStaticAnalysisUI.this);
                            if (resposta == JFileChooser.APPROVE_OPTION) {
                                File file = fileChooser.getSelectedFile();
                                html.setPath(file.getPath());
                                ArrayList<StaticAnalysis> list = new ArrayList<>();
                                list.add(s);
                                html.setListAnalysis(list);
                                try {
                                    html.createFile();
                                } catch (Exception ex) {
                                    JOptionPane.showMessageDialog(
                                            CreateStaticAnalysisUI.this, "Error", "File could not save", JOptionPane.ERROR_MESSAGE);
                                }
                            }
                        } else if (result == 1) {
                            dispose();
                        }
                        dispose();

                    }

                }

            }

        });
        return b;

    }

    private JButton buttonCancel() {
        JButton b = new JButton("Cancel");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }

        });
        return b;

    }

    private void definirFiltroExtensaoHTML(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("html");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.html";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }
}
