package lapr3_1516_g13.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lapr3_1516_g13.controllers.EditSimulationController;
import lapr3_1516_g13.model.Simulator;

/**
 *
 * @author Marcelo Oliveira
 */
public class EditSimulationUI extends JDialog {

    private final Simulator simulator;
    private final EditSimulationController controller;
    private final MenuUI owner;

    private JButton btnEdit, btnCancel;
    private JLabel lblTitle, lblName, lblDescription;
    private JTextField txtName, txtDescription;

    public EditSimulationUI(JFrame owner, Simulator simulator) {
        super(owner, "Edit Simulation");
        this.owner = (MenuUI) owner;
        this.simulator = simulator;
        this.controller = new EditSimulationController(simulator);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        if (!controller.getActiveSimulation()) {
            JOptionPane.showMessageDialog(EditSimulationUI.this,
                    "No current active project/simulation",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);
            dispose();
        } else {

            setPreferredSize(new Dimension(600, 225));
            setResizable(false);

            add(createPanelEditSimulation(), BorderLayout.NORTH);

            pack();
            setLocationRelativeTo(owner);

            setVisible(true);
        }
    }

    private JPanel createPanelEditSimulation() {
        final int UP_MARGIN = 10, DOWN_MARGIN = 10;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelData(), BorderLayout.CENTER);
        painel.add(createPanelButtons(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelData() {
        final int UP_MARGIN = 5, DOWN_MARGIN = 20;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelNameDescription(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelNameDescription() {
        JPanel p = new JPanel(new GridLayout(4, 2, 0, 5));

        String[] data = controller.getSimulationNameDescription();

        lblTitle = new JLabel("Simulation", JLabel.LEFT);

        lblName = new JLabel("Name: ", JLabel.LEFT);
        txtName = new JTextField();
        txtName.setText(data[0]);

        lblDescription = new JLabel("Description: ", JLabel.LEFT);
        txtDescription = new JTextField();
        txtDescription.setText(data[1]);

        p.add(lblTitle);
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(lblName);
        p.add(txtName);
        p.add(lblDescription);
        p.add(txtDescription);

        return p;
    }

    private JPanel createPanelButtons() {
         final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 10, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS,
                NUMERO_COLUNAS,
                INTERVALO_HORIZONTAL,
                INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        btnEdit = createButtonEdit();
        btnCancel = createButtonCancel();

        p.add(btnEdit);
        p.add(btnCancel);

        return p;
    }

    private JButton createButtonEdit() {
        JButton b = new JButton("Edit");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Object[] options = {"Yes", "No"};
                int n = JOptionPane.showOptionDialog(EditSimulationUI.this, "Edit the name and description of the simulation?\n (All previous results will be deleted)", "Edit Simulation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (n == 0) {
                    boolean validated = controller.editNameDescriptionDB(txtName.getText(), txtDescription.getText());
                    if (validated) {
                        boolean registered = controller.registerEditedSimulationDB();
                        if (registered) {
                            JOptionPane.showMessageDialog(EditSimulationUI.this,
                                    "Simulation successfully edited");
                            owner.modifyLabelSimulation();
                            dispose();
                        } else {
                            JOptionPane.showMessageDialog(EditSimulationUI.this,
                                    "Unable to register simulation:\n Another simulation already has that name",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(EditSimulationUI.this,
                                "Invalid name and/or description",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCancel() {
        JButton b = new JButton("Cancel");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b;
    }

}
