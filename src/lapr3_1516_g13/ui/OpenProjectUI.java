package lapr3_1516_g13.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import lapr3_1516_g13.controllers.OpenProjectController;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.Simulator;

/**
 *
 * @author Marcelo Oliveira
 */
public class OpenProjectUI extends JDialog {

    private final MenuUI owner;
    private final Simulator simulator;
    private final OpenProjectController controller;

    private JLabel labelProjects, lblPTitle, lblPName, lblPDescription;
    private JLabel lblPName2, lblPDescription2;
    private JComboBox listProjects;
    private JButton btnSelect, btnCancel;

    private static final Dimension FIELDSIZE
            = new Dimension(462, 25);

    public OpenProjectUI(JFrame owner, Simulator simulator) {
        super(owner, "Open Project");
        this.owner = (MenuUI) owner;

        this.simulator = simulator;
        this.controller = new OpenProjectController(this.simulator);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        controller.loadProjectList();
        if (controller.getProjectList() == null || controller.getProjectList().isEmpty()) {
            JOptionPane.showMessageDialog(OpenProjectUI.this,
                    "No projects to show",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);
            dispose();

        } else {
            setPreferredSize(new Dimension(550, 255));
            setResizable(false);

            add(createPanelOpenProject(), BorderLayout.NORTH);

            pack();
            setLocationRelativeTo(owner);

            setVisible(true);
        }

    }

    private JPanel createPanelOpenProject() {
        final int UP_MARGIN = 10, DOWN_MARGIN = 10;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelData(), BorderLayout.CENTER);
        painel.add(createPanelButtons(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelData() {
        final int UP_MARGIN = 5, DOWN_MARGIN = 20;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelProjectList(), BorderLayout.CENTER);
        painel.add(createPanelProjectData(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelProjectList() {
        JPanel p = new JPanel();
        final int UP_MARGIN = 0, DOWN_MARGIN = 30;
        final int LEFT_MARGIN = 0, RIGHT_MARGIN = 0;

        p.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        labelProjects = new JLabel("Available Projects", JLabel.LEFT);
        listProjects = new JComboBox(controller.getProjectList().toArray());
        listProjects.setPreferredSize(FIELDSIZE);

        listProjects.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                Project proj = (Project) listProjects.getSelectedItem();

                lblPName2.setText(proj.getName());
                lblPDescription2.setText(proj.getDescription());

            }
        });

        p.add(labelProjects);
        p.add(listProjects);

        return p;
    }

    private JPanel createPanelProjectData() {
        JPanel p = new JPanel(new GridLayout(4, 2, 0, 0));
        Project proj = (Project) listProjects.getSelectedItem();

        lblPTitle = new JLabel("Project:", JLabel.LEFT);

        lblPName = new JLabel("Name: ", JLabel.LEFT);
        lblPName2 = new JLabel(proj.getName(), JLabel.LEFT);

        lblPDescription = new JLabel("Description: ", JLabel.LEFT);
        lblPDescription2 = new JLabel(proj.getDescription(), JLabel.LEFT);

        p.add(lblPTitle);
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(lblPName);
        p.add(lblPName2);
        p.add(lblPDescription);
        p.add(lblPDescription2);

        return p;
    }

    private JPanel createPanelButtons() {
        final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 10, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS,
                NUMERO_COLUNAS,
                INTERVALO_HORIZONTAL,
                INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        btnSelect = createButtonSelect();
        btnCancel = createButtonCancel();

        p.add(btnSelect);
        p.add(btnCancel);

        return p;
    }

    private JButton createButtonSelect() {
        JButton b = new JButton("Select");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Project proj = (Project) listProjects.getSelectedItem();

                Object[] options = {"Yes", "No"};
                int n = JOptionPane.showOptionDialog(OpenProjectUI.this, "Open the selected project?", "Open Project", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (n == 0) {
                    boolean validated = controller.selectProject(proj);
                    if (validated) {
                        owner.modifyLabel();
                        JOptionPane.showMessageDialog(OpenProjectUI.this,
                                "Project opened");
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(OpenProjectUI.this,
                                "Couldn't open the project",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCancel() {
        JButton b = new JButton("Cancel");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                owner.modifyLabel();
                dispose();
            }
        });
        return b;
    }
}
