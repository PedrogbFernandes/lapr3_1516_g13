package lapr3_1516_g13.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import lapr3_1516_g13.controllers.OpenSimulationController;
import lapr3_1516_g13.model.Simulation;
import lapr3_1516_g13.model.Simulator;

/**
 *
 * @author Marcelo Oliveira
 */
public class OpenSimulationUI extends JDialog {

    private final Simulator simulator;
    private final OpenSimulationController controller;
    private final MenuUI owner;

    private JLabel labelSimulations, lblPTitle, lblPName, lblPDescription;
    private JLabel lblPName2, lblPDescription2;
    private JComboBox listSimulations;
    private JButton btnSelect, btnCancel;

    private static final Dimension FIELDSIZE
            = new Dimension(462, 25);

    public OpenSimulationUI(JFrame owner, Simulator simulator) {
        super(owner, "Open Simulation");
        this.owner = (MenuUI) owner;
        this.simulator = simulator;
        this.controller = new OpenSimulationController(this.simulator);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
        if (controller.getSimulationList() == null) {
            JOptionPane.showMessageDialog(OpenSimulationUI.this,
                    "Current active project not available",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);
            dispose();
        } else {
            controller.loadSimulationList();
            if (controller.getSimulationList() == null || controller.getSimulationList().isEmpty()) {
                JOptionPane.showMessageDialog(OpenSimulationUI.this,
                        "No simulations to show",
                        "Warning",
                        JOptionPane.WARNING_MESSAGE);
                dispose();
            } else {
                setPreferredSize(new Dimension(550, 255));
                setResizable(false);

                add(createPanelOpenSimulation(), BorderLayout.NORTH);

                pack();
                setLocationRelativeTo(owner);

                setVisible(true);
            }
        }
    }

    private JPanel createPanelOpenSimulation() {
        final int UP_MARGIN = 10, DOWN_MARGIN = 10;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelData(), BorderLayout.CENTER);
        painel.add(createPanelButtons(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelData() {
        final int UP_MARGIN = 5, DOWN_MARGIN = 20;
        final int LEFT_MARGIN = 20, RIGHT_MARGIN = 20;

        JPanel painel = new JPanel(new BorderLayout());
        painel.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        painel.add(createPanelSimulationList(), BorderLayout.CENTER);
        painel.add(createPanelSimulationData(), BorderLayout.SOUTH);
        return painel;
    }

    private JPanel createPanelSimulationList() {
        JPanel p = new JPanel();
        final int UP_MARGIN = 0, DOWN_MARGIN = 30;
        final int LEFT_MARGIN = 0, RIGHT_MARGIN = 0;

        p.setBorder(BorderFactory.createEmptyBorder(UP_MARGIN, LEFT_MARGIN, DOWN_MARGIN, RIGHT_MARGIN));

        labelSimulations = new JLabel("Available Simulations", JLabel.LEFT);
        listSimulations = new JComboBox(controller.getSimulationList().toArray());
        listSimulations.setPreferredSize(FIELDSIZE);

        listSimulations.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                Simulation simulation = (Simulation) listSimulations.getSelectedItem();

                lblPName2.setText(simulation.getName());
                lblPDescription2.setText(simulation.getDescription());

            }
        });

        p.add(labelSimulations);
        p.add(listSimulations);

        return p;
    }

    private JPanel createPanelSimulationData() {
        JPanel p = new JPanel(new GridLayout(4, 2, 0, 0));
        Simulation simulation = (Simulation) listSimulations.getSelectedItem();

        lblPTitle = new JLabel("Project:", JLabel.LEFT);

        lblPName = new JLabel("Name: ", JLabel.LEFT);
        lblPName2 = new JLabel(simulation.getName(), JLabel.LEFT);

        lblPDescription = new JLabel("Description: ", JLabel.LEFT);
        lblPDescription2 = new JLabel(simulation.getDescription(), JLabel.LEFT);

        p.add(lblPTitle);
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(new JLabel(" ", JLabel.LEFT));
        p.add(lblPName);
        p.add(lblPName2);
        p.add(lblPDescription);
        p.add(lblPDescription2);

        return p;
    }

    private JPanel createPanelButtons() {
        final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 10, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS,
                NUMERO_COLUNAS,
                INTERVALO_HORIZONTAL,
                INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        btnSelect = createButtonSelect();
        btnCancel = createButtonCancel();

        p.add(btnSelect);
        p.add(btnCancel);

        return p;
    }

    private JButton createButtonSelect() {
        JButton b = new JButton("Select");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Simulation simulation = (Simulation) listSimulations.getSelectedItem();

                Object[] options = {"Yes", "No"};
                int n = JOptionPane.showOptionDialog(OpenSimulationUI.this, "Open the selected simulation?", "Open Simulation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (n == 0) {
                    boolean validated = controller.selectSimulation(simulation);
                    if (validated) {
                        JOptionPane.showMessageDialog(OpenSimulationUI.this,
                                "Simulation opened");
                        owner.modifyLabelSimulation();
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(OpenSimulationUI.this,
                                "Couldn't open the simulation",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        return b;
    }

    private JButton createButtonCancel() {
        JButton b = new JButton("Cancel");
        b.setFocusPainted(false);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                owner.modifyLabelSimulation();
                dispose();
            }
        });
        return b;
    }

}
