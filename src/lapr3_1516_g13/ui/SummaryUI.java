package lapr3_1516_g13.ui;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import lapr3_1516_g13.controllers.RunSimulationController;

/**
 *
 * @author Marcelo Oliveira e Pedro Aguiar
 */
public class SummaryUI extends javax.swing.JDialog {

    private String[] summary;
    private RunSimulationController controller;

    /**
     * Creates new form SummaryUI
     *
     * @param parent the parent window
     * @param summary the summary string vector
     */
    public SummaryUI(JDialog parent, RunSimulationController controller) {

        super(parent, "Run Simulation Summary");
        this.controller = controller;
        this.summary = this.controller.getSummaryData();

        initComponents();

        this.lblProject.setText(summary[0]);
        this.lblSimulation.setText(summary[1]);
        this.lblRunName.setText(summary[2]);
        this.lblDuration.setText(summary[3]);
        this.lblTimeStep.setText(summary[4]);
        this.lblMethod.setText(summary[5]);
        this.lblRecords.setText(summary[6]);
        this.lblRecords1.setText(summary[8]);
        this.lblRecords2.setText(summary[7]);

        pack();
        this.setResizable(false);
        this.setLocationRelativeTo(parent);
        this.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblProjectTitle = new javax.swing.JLabel();
        lblProject = new javax.swing.JLabel();
        lblSimulationTitle = new javax.swing.JLabel();
        lblSimulation = new javax.swing.JLabel();
        lblDurationTitle = new javax.swing.JLabel();
        lblDuration = new javax.swing.JLabel();
        lblTimeStepTitle = new javax.swing.JLabel();
        lblTimeStep = new javax.swing.JLabel();
        lblRunTitle = new javax.swing.JLabel();
        lblRunName = new javax.swing.JLabel();
        lblMethodTitle = new javax.swing.JLabel();
        lblMethod = new javax.swing.JLabel();
        lblRecordsTitle = new javax.swing.JLabel();
        lblRecords = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        lblRecords1 = new javax.swing.JLabel();
        lblRecords2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblProjectTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblProjectTitle.setText("Project's name:");

        lblProject.setText("jLabel3");

        lblSimulationTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblSimulationTitle.setText("Simulation's name:");

        lblSimulation.setText("jLabel5");

        lblDurationTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblDurationTitle.setText("Simulation run's duration (min):");

        lblDuration.setText("jLabel7");

        lblTimeStepTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTimeStepTitle.setText("Simulation run's time step (min):");

        lblTimeStep.setText("jLabel9");

        lblRunTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblRunTitle.setText("Simulation run's name:");

        lblRunName.setText("jLabel11");

        lblMethodTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMethodTitle.setText("Simulation run's best path method:");

        lblMethod.setText("jLabel11");

        lblRecordsTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblRecordsTitle.setText("Records Stored:");

        lblRecords.setText("created");

        jButton1.setText("Save Result");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Close");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        lblRecords1.setText("created");

        lblRecords2.setText("dropped");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(159, 159, 159)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(129, 129, 129)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addGap(135, 135, 135)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblRunName)
                            .addComponent(lblRunTitle)
                            .addComponent(lblTimeStep)
                            .addComponent(lblTimeStepTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDuration)
                            .addComponent(lblDurationTitle)
                            .addComponent(lblProject, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblProjectTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblRecordsTitle)
                            .addComponent(lblMethod)
                            .addComponent(lblMethodTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                            .addComponent(lblRecords1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblRecords2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblSimulation)
                            .addComponent(lblSimulationTitle)
                            .addComponent(lblRecords, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblProjectTitle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblProject))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblSimulationTitle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblSimulation)))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblRunTitle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblRunName))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblMethodTitle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblMethod)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(lblDurationTitle))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblRecordsTitle)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblDuration)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblTimeStepTitle))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblRecords, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblRecords2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTimeStep))
                    .addComponent(lblRecords1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        boolean registered = controller.registerSimulationRunDB();
        if (registered) {
            JOptionPane.showMessageDialog(SummaryUI.this,
                    "Simulation Result successfully saved");
            dispose();
        } else {
            JOptionPane.showMessageDialog(SummaryUI.this,
                    "Error saving the simulation result",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblDuration;
    private javax.swing.JLabel lblDurationTitle;
    private javax.swing.JLabel lblMethod;
    private javax.swing.JLabel lblMethodTitle;
    private javax.swing.JLabel lblProject;
    private javax.swing.JLabel lblProjectTitle;
    private javax.swing.JLabel lblRecords;
    private javax.swing.JLabel lblRecords1;
    private javax.swing.JLabel lblRecords2;
    private javax.swing.JLabel lblRecordsTitle;
    private javax.swing.JLabel lblRunName;
    private javax.swing.JLabel lblRunTitle;
    private javax.swing.JLabel lblSimulation;
    private javax.swing.JLabel lblSimulationTitle;
    private javax.swing.JLabel lblTimeStep;
    private javax.swing.JLabel lblTimeStepTitle;
    // End of variables declaration//GEN-END:variables
}
