/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import PathAlgorithms.FastestPathAlgorithm;
import PathAlgorithms.MostEffcientPathAlgorithm;
import PathAlgorithms.PathAlgorithms;
import PathAlgorithms.TheoreticalLowestWorkPathAlgorithm;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Vehicle;
import lapr3_1516_g13.model.VehicleList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class CompareVehiclesControllerTest {

    Simulator s = new Simulator();
    CompareVehiclesController controller = new CompareVehiclesController(s);
    DataXML data = new DataXML();
    RoadNetwork net = new RoadNetwork();
    Vehicle car1 = new Vehicle();
    Vehicle car2 = new Vehicle();
    VehicleList v = new VehicleList();
    FastestPathAlgorithm fpa = new FastestPathAlgorithm();
    MostEffcientPathAlgorithm mea = new MostEffcientPathAlgorithm();
    TheoreticalLowestWorkPathAlgorithm lwa = new TheoreticalLowestWorkPathAlgorithm();
    Junction j1 = new Junction();
    Junction j2 = new Junction();
    Junction j3 = new Junction();
    List<StaticAnalysis> staticAnalysisList = new ArrayList<StaticAnalysis>();
    Project p = new Project();
    File file = new File("Test_Network.xml");
    File file2 = new File("TestSet03_VehiclesSFCGrande.xml"); //é o ficheiro testset3 mas com os valores sfc novos

    public CompareVehiclesControllerTest() throws Exception {
        data.readFile(file, net);
        data.readFile(file2, v);
        j1 = net.getGraph().getVertex(0).getElement();
        j2 = net.getGraph().getVertex(2).getElement();
        j3 = net.getGraph().getVertex(1).getElement();
        car1 = v.getVehicleList().get(0);
        car2 = v.getVehicleList().get(1);
        p.setActive(true);
        s.getProjectRegister().addProject(p);
        p.setVehicleList(v);
        p.setRoadNetwork(net);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of newStaticAnalysis method, of class CompareVehiclesController.
     */
    @Test
    public void testNewStaticAnalysis() {
        controller.newStaticAnalysis();
        assertNotNull(controller.getAlgorithmList());
        assertNotNull(controller.getListOfVehicles());
        assertNotNull(controller.getJunctionList());
        assertEquals(controller.getAlgorithmList().isEmpty(), false);
        assertEquals(controller.getJunctionList().isEmpty(), false);
        assertEquals(controller.getListOfVehicles().isEmpty(), false);

    }

    /**
     * Test of compareValues method, of class CompareVehiclesController. one
     * test for fastest path algorithm
     */
    @Test
    public void testCompareValuesForFastestPathAlgorithm() throws Exception {
        List<Vehicle> listCar = new ArrayList<Vehicle>();
        listCar.add(car1);
        listCar.add(car2);

        System.out.println("calculate Fastest Path Algoritm");
        controller.newStaticAnalysis();
        staticAnalysisList = controller.compareValues(j1, j3, listCar, fpa);
        double expResultTime1 = 750.0;
        double expResultTollCost1 = 12.0;
        double expResultEnergy1 = 3.74632737E8;
        String expResultPath1 = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double expResultTime2 = 750.0;
        double expResultTollCost2 = 12.0;
        double expResultEnergy2 = 3.35639905E8;
        String expResultPath2 = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime1 = staticAnalysisList.get(0).getTime();
        double resultTollCost1 = staticAnalysisList.get(0).getTollcost();
        double resultEnergy1 = staticAnalysisList.get(0).getComsumption();
        String resultPath1 = staticAnalysisList.get(0).getSectionList().toString();

        double resultTime2 = staticAnalysisList.get(1).getTime();
        double resultTollCost2 = staticAnalysisList.get(1).getTollcost();
        double resultEnergy2 = staticAnalysisList.get(1).getComsumption();
        String resultPath2 = staticAnalysisList.get(1).getSectionList().toString();

        assertEquals(expResultTime1, resultTime1, 0.1);
        assertEquals(expResultTollCost1, resultTollCost1, 0.0);
        assertEquals(expResultEnergy1, resultEnergy1, 1);
        assertEquals(expResultPath1, resultPath1);

        assertEquals(expResultTime2, resultTime2, 0.1);
        assertEquals(expResultTollCost2, resultTollCost2, 0.0);
        assertEquals(expResultEnergy2, resultEnergy2, 1);
        assertEquals(expResultPath2, resultPath2);

        assertNotNull(staticAnalysisList);

    }

    /**
     * Test of compareValues method, of class CompareVehiclesController. one
     * test for most efficient path algorithm
     */
    @Test
    public void testCompareValuesForMostEfficientPathAlgorithm() throws Exception {
        List<Vehicle> listCar = new ArrayList<Vehicle>();
        listCar.add(car1);
        listCar.add(car2);

        System.out.println("calculate Most Efficient Path Algorithm");
        controller.newStaticAnalysis();
        staticAnalysisList = controller.compareValues(j1, j3, listCar, mea);

        double expResultTime1 = 1410.3;
        double expResultTollCost1 = 12.0;
        double expResultEnergy1 = 9.8998045E7;
        String expResultPath1 = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double expResultTime2 = 1546.2;
        double expResultTollCost2 = 12.0;
        double expResultEnergy2 = 9.1868594E7;
        String expResultPath2 = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime1 = staticAnalysisList.get(0).getTime();
        double resultTollCost1 = staticAnalysisList.get(0).getTollcost();
        double resultEnergy1 = staticAnalysisList.get(0).getComsumption();
        String resultPath1 = staticAnalysisList.get(0).getSectionList().toString();

        double resultTime2 = staticAnalysisList.get(1).getTime();
        double resultTollCost2 = staticAnalysisList.get(1).getTollcost();
        double resultEnergy2 = staticAnalysisList.get(1).getComsumption();
        String resultPath2 = staticAnalysisList.get(1).getSectionList().toString();

        assertEquals(expResultTime1, resultTime1, 0.1);
        assertEquals(expResultTollCost1, resultTollCost1, 0.0);
        assertEquals(expResultEnergy1, resultEnergy1, 1);
        assertEquals(expResultPath1, resultPath1);

        assertEquals(expResultTime2, resultTime2, 0.1);
        assertEquals(expResultTollCost2, resultTollCost2, 0.0);
        assertEquals(expResultEnergy2, resultEnergy2, 1);
        assertEquals(expResultPath2, resultPath2);

        assertNotNull(staticAnalysisList);

    }

    /**
     * Test of compareValues method, of class CompareVehiclesController. one
     * test for lowest twork
     */
    @Test
    public void testCompareValuesForLowesttWork() throws Exception {
        List<Vehicle> listCar = new ArrayList<Vehicle>();
        listCar.add(car1);
        listCar.add(car2);

        System.out.println("calculate lowest work algorithm");
        controller.newStaticAnalysis();
        staticAnalysisList = controller.compareValues(j1, j3, listCar, lwa);

        double expResultTime1 =  970.0;
        double expResultTollCost1 = 12.0;
        double expResultEnergy1 = 1.6744572E8;
        String expResultPath1 = "[Section - Begin: n0, End: n1, Road: \"A01\"]";
                                       
        double expResultTime2 = 750.0;
        double expResultTollCost2 = 12.0;
        double expResultEnergy2 = 9.1868595E7;
        String expResultPath2 = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime1 = staticAnalysisList.get(0).getTime();
        double resultTollCost1 = staticAnalysisList.get(0).getTollcost();
        double resultEnergy1 = staticAnalysisList.get(0).getComsumption();
        String resultPath1 = staticAnalysisList.get(0).getSectionList().toString();

        double resultTime2 = staticAnalysisList.get(1).getTime();
        double resultTollCost2 = staticAnalysisList.get(1).getTollcost();
        double resultEnergy2 = staticAnalysisList.get(1).getComsumption();
        String resultPath2 = staticAnalysisList.get(1).getSectionList().toString();

        System.out.println(resultTime1);
        System.out.println(resultTollCost1);
        System.out.println(resultEnergy1);
        System.out.println(resultPath1);
        System.out.println(resultTime2);
        System.out.println(resultTollCost2);
        System.out.println(resultEnergy2);
        System.out.println(resultPath2);

        assertEquals(expResultTime1, resultTime1, 0.1);
        assertEquals(expResultTollCost1, resultTollCost1, 0.0);
        assertEquals(expResultEnergy1, resultEnergy1, 1);
        assertEquals(expResultPath1, resultPath1);

        assertEquals(expResultTime2, resultTime2, 0.1);
        assertEquals(expResultTollCost2, resultTollCost2, 0.0);
        assertEquals(expResultEnergy2, resultEnergy2, 1);
        assertEquals(expResultPath2, resultPath2);

        assertNotNull(staticAnalysisList);

    }

    /**
     * Test of insertValues method, of class CreateStaticAnalysisController. One
     * test for failure (same source and destiny)
     */
    @Test
    public void testInsertValuesNull() throws Exception {
        System.out.println("null test validate");
        List<Vehicle> listCar = new ArrayList<Vehicle>();
        listCar.add(car1);
        listCar.add(car2);
        controller.newStaticAnalysis();
        staticAnalysisList = controller.compareValues(j1, j1, listCar, fpa);
        assertEquals(staticAnalysisList.isEmpty(),true);

    }

}
