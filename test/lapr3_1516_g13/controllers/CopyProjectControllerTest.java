/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class CopyProjectControllerTest {

    Simulator s = new Simulator();
    Project p1 = new Project();
    Project p2 = new Project();
    Project p3 = new Project();
    LinkedList<Project> plist = new LinkedList();

    public CopyProjectControllerTest() {

        p1.setName("Test1");
        p2.setName("Test2");
        p3.setName("Test3");

        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet02_Vehicles.xml"), p2.getVehicleList());
            data.readFile(new File("Test_Network.xml"), p2.getRoadNetwork());
        } catch (Exception ex) {
            System.out.println(ex);
        }
        plist.addLast(p1);
        plist.addLast(p2);
        plist.addLast(p3);

        s.getProjectRegister().setProjectList(plist);
        s.getProjectRegister().setActiveProject(p2);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getActiveProject method, of class CopyProjectController.
     */
    @Test
    public void testGetActiveProject() {
        System.out.println("getActiveProject");
        CopyProjectController instance = new CopyProjectController(s);

        boolean expResult = true;
        boolean result = instance.getActiveProject();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNameDescription method, of class CopyProjectController.
     */
    @Test
    public void testSetNameDescription() {
        System.out.println("setNameDescription, validateProject and registerProject");
        String name = "Test4";
        String description = "Desc";
        CopyProjectController instance = new CopyProjectController(s);
        instance.getActiveProject();
        instance.setNameDescription(name, description);

        //Project list size should be 3 before register
        assertTrue("size should be 3", s.getProjectRegister().getProjectList().size() == 3);

        assertTrue("should be true", instance.validateProject() == true);
        assertTrue("should be true", instance.registerProject() == true);

        //Project list size should be 4 before register
        assertTrue("size should be 4", s.getProjectRegister().getProjectList().size() == 4);

        assertTrue("graph of copied project should be equal to current active",
                p2.getRoadNetwork().getGraph().equals(s.getProjectRegister().getProjectList().getLast().getRoadNetwork().getGraph()));

        assertTrue("simulation list should be empty",
                s.getProjectRegister().getProjectList().getLast().getSimulationList().getListSimulation().isEmpty() == true);

        assertTrue("name should be Test4",
                s.getProjectRegister().getProjectList().getLast().getName().equals("Test4"));
    }

}
