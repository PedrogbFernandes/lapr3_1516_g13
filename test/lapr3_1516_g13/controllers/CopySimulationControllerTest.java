package lapr3_1516_g13.controllers;

import java.io.File;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CopySimulationControllerTest {

    private Simulator simulator = new Simulator();
    private Project proj = new Project();
    private File file = new File("Test_Simulation.xml");
    private Simulation simulation = new Simulation();

    public CopySimulationControllerTest() {

        proj.setName("Test");
        proj.setDescription("Description");
        proj.setActive(true);

        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet02_Vehicles.xml"), proj.getVehicleList());
            data.readFile(new File("Test_Network.xml"), proj.getRoadNetwork());
            data.readFile(file, simulation.getTrafficList(), proj);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        simulation.setName("Test1");
        simulation.setDescription("Desc");
        simulation.setAssociatedRoadNetwork(proj.getRoadNetwork());
        simulation.setActive(true);
        
        simulation.getSimulationResultList().add(new SimulationResult());

        proj.addSimulation(simulation);

        simulator.getProjectRegister().getProjectList().add(proj);
        simulator.getProjectRegister().setActiveProject(proj);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of copyActiveSimulation method, of class CopySimulationController.
     */
    @Test
    public void testCopyActiveSimulation() {
        System.out.println("copyActiveSimulation");
        CopySimulationController instance = new CopySimulationController(simulator);

        boolean expResult = true;
        boolean result = instance.copyActiveSimulation();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getNameDescription method, of class CopySimulationController.
     */
    @Test
    public void testGetNameDescription() {
        System.out.println("getNameDescription");
        CopySimulationController instance = new CopySimulationController(simulator);
        instance.copyActiveSimulation();

        String[] data = instance.getNameDescription();

        assertTrue("should be Test1", data[0].equals("Test1"));
        assertTrue("should be Desc", data[1].equals("Desc"));
    }

    /**
     * Test of setNameDescription method, of class CopySimulationController.
     */
    @Test
    public void testSetNameDescription() {
        System.out.println("setNameDescription, validateSimulation and registerSimulation");
        String name = "Test2";
        String description = "Desc2";
        CopySimulationController instance = new CopySimulationController(simulator);
        instance.copyActiveSimulation();
        instance.setNameDescription(name, description);

        
        //Before register size should be 1
        
        assertTrue("simulation list size should be 1",
                proj.getSimulationList().getListSimulation().size() == 1);
        
        assertTrue("should be true", instance.validateSimulation() == true);

        assertTrue("should be true", instance.registerSimulation() == true);

        //After register size should be 2
         
        assertTrue("simulation list size should be 2",
                proj.getSimulationList().getListSimulation().size() == 2);

        assertTrue("simulation 2 name should be Test2",
                proj.getSimulationList().getListSimulation().get(1).getName().equals("Test2"));

        assertTrue("simulation 2 description should be Desc2",
                proj.getSimulationList().getListSimulation().get(1).getDescription().equals("Desc2"));
        
        assertTrue("result list should be empty",
                proj.getSimulationList().getListSimulation().get(1).getSimulationResultList().isEmpty()==true);
    }

}
