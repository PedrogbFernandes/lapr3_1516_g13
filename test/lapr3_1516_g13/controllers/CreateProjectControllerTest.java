/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import Graph.Edge;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CreateProjectControllerTest {

    private Simulator simulator;

    public CreateProjectControllerTest() {
        simulator = new Simulator();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFileFormats method, of class CreateProjectController.
     */
    @Test
    public void testGetFileFormats() {
        System.out.println("#### getFileFormats");
        CreateProjectController instance = new CreateProjectController(simulator);
        instance.createNewProject();

        List<String> result = instance.getFileFormats();

        assertTrue("size should be 2", result.size() == 2);

        assertTrue("should be HTML", result.get(0).equalsIgnoreCase("HTML"));
        assertTrue("should be CSV", result.get(1).equalsIgnoreCase("CSV"));
    }

    /**
     * Test of insertNameDescription method, of class CreateProjectController.
     */
    @Test
    public void testInsertNameDescription() {
        System.out.println("#### insertNameDescription");
        String name = "Test";
        String description = "DescriptionTest";
        CreateProjectController instance = new CreateProjectController(simulator);
        instance.createNewProject();
        instance.insertNameDescription(name, description);
        instance.registerProject();

        Project p = simulator.getProjectRegister().getProjectList().getFirst();

        assertTrue("should be Test", p.getName().equals("Test"));
        assertTrue("should be DescriptionTest", p.getDescription().equals("DescriptionTest"));
    }

    /**
     * Test of selectRoadNetworkFile method, of class CreateProjectController.
     */
    @Test
    public void testSelectRoadNetworkFile() throws Exception {
        System.out.println("#### selectRoadNetworkFile");
        File file = new File("Test_Network.xml");

        CreateProjectController instance = new CreateProjectController(simulator);
        instance.createNewProject();
        instance.insertNameDescription("Teste1", "Description1");

        instance.selectRoadNetworkFile(file);
        instance.registerProject();

        Project p = simulator.getProjectRegister().getProjectList().getFirst();

        LinkedList<Edge<Junction, List<Section>>> temp = new LinkedList<>();

        Iterator<Edge<Junction, List<Section>>> edgesIt = p.getRoadNetwork().getGraph().edges().iterator();
        while (edgesIt.hasNext()) {
            temp.add(edgesIt.next());
        }

        Collections.sort(temp);

        edgesIt = temp.iterator();
        Edge<Junction, List<Section>> edge = edgesIt.next();
        
        assertTrue("Should be n0", edge.getVOrig().getElement().getIndex().equalsIgnoreCase("n0"));
        assertTrue("Should be n1", edge.getVDest().getElement().getIndex().equalsIgnoreCase("n1"));
        assertTrue("Sections should be 1", edge.getElement().size() == 1);
        assertTrue("Segments should be 1", edge.getElement().get(0).getSegmentList().size() == 1);

        edge = edgesIt.next();

        assertTrue("Should be n1", edge.getVOrig().getElement().getIndex().equalsIgnoreCase("n1"));
        assertTrue("Should be n3", edge.getVDest().getElement().getIndex().equalsIgnoreCase("n3"));
        assertTrue("Sections should be 1", edge.getElement().size() == 1);
        assertTrue("Segments should be 1", edge.getElement().get(0).getSegmentList().size() == 1);

        edge = edgesIt.next();

        assertTrue("Should be n0", edge.getVOrig().getElement().getIndex().equalsIgnoreCase("n0"));
        assertTrue("Should be n2", edge.getVDest().getElement().getIndex().equalsIgnoreCase("n2"));
        assertTrue("Sections should be 1", edge.getElement().size() == 1);
        assertTrue("Segments should be 2", edge.getElement().get(0).getSegmentList().size() == 2);

        for (Edge<Junction, List<Section>> e : temp) {
            System.out.println("\nOrigin: " + e.getVOrig().getElement().getIndex());
            System.out.println("Destiny: " + e.getVDest().getElement().getIndex());
            System.out.println("Sections: " + e.getElement());
        }

    }

    /**
     * Test of selectVehicleFile method, of class CreateProjectController.
     */
    @Test
    public void testSelectVehicleFile() throws Exception {
        System.out.println("#### selectVehicleFile");
        File file = new File("Test_Vehicles.xml");
        CreateProjectController instance = new CreateProjectController(simulator);
        instance.createNewProject();
        instance.insertNameDescription("Teste1", "Description1");

        instance.registerProject();

        instance.selectVehicleFile(file);
        Project p = simulator.getProjectRegister().getProjectList().getFirst();
        VehicleList vl = p.getVehicleList();
        Vehicle v = vl.getVehicleList().get(0);
        
        
        assertTrue("Frontal area should be 1.8",v.getFrontal_area()==1.8);

        assertTrue("Highway speed equals 100", v.getVelocity_limit_list().get("Highway") == 100);
        assertTrue("1st Gear ratio equals 3.5", v.getGear_ratio().get(0) == 3.5);
        assertTrue("2st Gear ratio equals 2.5", v.getGear_ratio().get(1) == 2.5);
        assertTrue("3st Gear ratio equals 1.25", v.getGear_ratio().get(2) == 1.25);
        assertTrue("4st Gear ratio equals 0.9", v.getGear_ratio().get(3) == 0.9);

        assertTrue("Throttle list size equals 3", v.getThrottle_list().size() == 3);

        Iterator<Double> throttle = v.getThrottle_list().keySet().iterator();
        assertTrue("Should be 0.25", throttle.next() == 0.25);
        assertTrue("Should be 0.5", throttle.next() == 0.5);
        assertTrue("Should be 1", throttle.next() == 1);

        Iterator<Regime> regimes = v.getThrottle_list().get(0.25).iterator();
        Regime reg = regimes.next();
        assertTrue("Throttle 25 - Torque should be 85", reg.getTorque() == 85);
        assertTrue("Throttle 25 - Rpm low should be 1000", reg.getRpm_low() == 1000);
        assertTrue("Throttle 25 - Rpm high should be 2499", reg.getRpm_high() == 2499);
        assertTrue("Throttle 25 - SFC should be 8.2", reg.getSfc() == 8.2);

        reg = regimes.next();
        assertTrue("Throttle 25 - Torque should be 95", reg.getTorque() == 95);
        assertTrue("Throttle 25 - Rpm low should be 2500", reg.getRpm_low() == 2500);
        assertTrue("Throttle 25 - Rpm high should be 3999", reg.getRpm_high() == 3999);
        assertTrue("Throttle 25 - SFC should be 6.2", reg.getSfc() == 6.2);

        regimes = v.getThrottle_list().get(0.5).iterator();
        reg = regimes.next();
        assertTrue("Throttle 25 - Torque should be 135", reg.getTorque() == 135);
        assertTrue("Throttle 25 - Rpm low should be 1000", reg.getRpm_low() == 1000);
        assertTrue("Throttle 25 - Rpm high should be 2499", reg.getRpm_high() == 2499);
        assertTrue("Throttle 25 - SFC should be 5.2", reg.getSfc() == 5.2);

    }

    /**
     * Test of selectFileFormat method, of class CreateProjectController.
     */
    @Test
    public void testSelectFileFormat() {
        System.out.println("#### selectFileFormat");
        String format = "HTML";
        CreateProjectController instance = new CreateProjectController(simulator);
        instance.createNewProject();
        instance.selectFileFormat(format);
        instance.registerProject();

        Project p = simulator.getProjectRegister().getProjectList().getFirst();

        assertTrue("should be HTML", p.getFileFormat().equals("HTML"));
    }

    /**
     * Test of validateProject method, of class CreateProjectController.
     */
    @Test
    public void testValidateProject() throws Exception {
        System.out.println("#### validateProject and registerProject");
        CreateProjectController instance = new CreateProjectController(simulator);
        instance.createNewProject();
        instance.insertNameDescription("Test", "DescriptionTest");
        instance.selectRoadNetworkFile(new File("Test_Network.xml"));
        instance.selectVehicleFile(new File("Test_Vehicles.xml"));
        instance.selectFileFormat("HTML");
        boolean result = instance.validateProject();

        assertEquals(true, result);

        instance.registerProject();

        Project p = simulator.getProjectRegister().getProjectList().getFirst();
        assertNotNull(p);

    }

}
