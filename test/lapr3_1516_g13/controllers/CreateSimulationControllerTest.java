package lapr3_1516_g13.controllers;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class CreateSimulationControllerTest {

    private Simulator simulator = new Simulator();
    private Project proj = new Project();
    private File file = new File("Test_Simulation.xml");

    public CreateSimulationControllerTest() {

        proj.setName("Test");
        proj.setDescription("Description");
        proj.setActive(true);
        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet02_Vehicles.xml"), proj.getVehicleList());
            data.readFile(new File("Test_Network.xml"), proj.getRoadNetwork());
        } catch (Exception ex) {
            System.out.println(ex);
        }

        simulator.getProjectRegister().getProjectList().add(proj);
        simulator.getProjectRegister().setActiveProject(proj);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setNameDescription method, of class CreateSimulationController.
     */
    @Test
    public void testSetNameDescription() {

    }

    /**
     * Test of selectTrafficFile method, of class CreateSimulationController.
     */
    @Test
    public void testSelectTrafficFile() {
        System.out.println("selectTrafficFile");
        CreateSimulationController instance = new CreateSimulationController(simulator);
        instance.createSimulation();
        try {
            instance.selectTrafficFile(file);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * Test of validateSimulation method, of class CreateSimulationController.
     */
    @Test
    public void testValidateSimulation() {
        System.out.println("validateSimulation");
        System.out.println("validateSimulation, setNameDescription, createSimulation and registerSimulation");

        String name = "Test1";
        String description = "Desc1";
        CreateSimulationController instance = new CreateSimulationController(new Simulator());

        //No project/active project
        assertTrue("should be false", instance.createSimulation() == false);

        //With an active project
        instance = new CreateSimulationController(simulator);
        instance.createSimulation();
        assertTrue("should be true", instance.createSimulation() == true);

        instance.setNameDescription(name, description);
        try {
            instance.selectTrafficFile(file);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        assertTrue("should be true", instance.validateSimulation() == true);
        
        instance.registerSimulation();
        
        assertTrue("name should be Test1", proj.getSimulationList().getListSimulation().get(0).getName().equals("Test1"));
        assertTrue("description should be Desc1", proj.getSimulationList().getListSimulation().get(0).getDescription().equals("Desc1"));
    }


}
