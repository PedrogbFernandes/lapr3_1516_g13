/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import PathAlgorithms.FastestPathAlgorithm;
import PathAlgorithms.MostEffcientPathAlgorithm;
import PathAlgorithms.PathAlgorithms;
import PathAlgorithms.TheoreticalLowestWorkPathAlgorithm;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.StaticAnalysis;
import lapr3_1516_g13.model.Vehicle;
import lapr3_1516_g13.model.VehicleList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class CreateStaticAnalysisControllerTest {

    Simulator s = new Simulator();
    CreateStaticAnalysisController controller = new CreateStaticAnalysisController(s);
    DataXML data = new DataXML();
    RoadNetwork net = new RoadNetwork();
    Vehicle car = new Vehicle();
    VehicleList v = new VehicleList();
    FastestPathAlgorithm fpa = new FastestPathAlgorithm();
    MostEffcientPathAlgorithm mea = new MostEffcientPathAlgorithm();
    TheoreticalLowestWorkPathAlgorithm lwa = new TheoreticalLowestWorkPathAlgorithm();
    Junction j1 = new Junction();
    Junction j2 = new Junction();
    Junction j3 = new Junction();
    StaticAnalysis staticAnalysis = new StaticAnalysis();
    Project p = new Project();
    File file = new File("Test_Network.xml");
    File file2 = new File("TestSet03_VehiclesSFCGrande.xml"); //é o ficheiro testset3 mas com os valores sfc novos
   
    
    public CreateStaticAnalysisControllerTest() throws Exception {
        data.readFile(file, net);
        data.readFile(file2, v);
        j1 = net.getGraph().getVertex(0).getElement();
        j2 = net.getGraph().getVertex(2).getElement();
        j3 = net.getGraph().getVertex(1).getElement();
        car = v.getVehicleList().get(0);
        p.setActive(true);
        s.getProjectRegister().addProject(p);
        p.setVehicleList(v);
        p.setRoadNetwork(net);
        
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of newStaticAnalysis method, of class
     * CreateStaticAnalysisController.
     * @throws java.lang.Exception
     */
    @Test
    public void testNewStaticAnalysis() throws Exception {
        
        controller.newStaticAnalysis();
        assertNotNull(controller.getAlgorithmList());
        assertNotNull(controller.getListOfVehicles());
        assertNotNull(controller.getJunctionList());
        assertEquals(controller.getAlgorithmList().isEmpty(),false);
        assertEquals(controller.getJunctionList().isEmpty(),false);
        assertEquals(controller.getListOfVehicles().isEmpty(),false);

    }

    /**
     * Test of insertValues method, of class CreateStaticAnalysisController.
     * One test for fastest path algorithm
     */
    @Test
    public void testInsertValuesForFastestPathAlgorithm() throws Exception {
        System.out.println("calculate Fastest Path Algoritm");
        controller.newStaticAnalysis();
        staticAnalysis=controller.insertValues(j1, j3, car, fpa);
        double expResultTime = 750.0;
        double expResultTollCost = 12.0;
        double expResultEnergy = 3.74632737E8;
        String expResultPath = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime = staticAnalysis.getTime();
        double resultTollCost = staticAnalysis.getTollcost();
        double resultEnergy = staticAnalysis.getComsumption();
        String resultPath = staticAnalysis.getSectionList().toString();

        assertEquals(expResultTime, resultTime, 0.1);
        assertEquals(expResultTollCost, resultTollCost, 0.0);
        assertEquals(expResultEnergy, resultEnergy, 1);
        assertEquals(expResultPath, resultPath);
        assertNotNull(staticAnalysis);

    }
    
    /**
     * Test of insertValues method, of class CreateStaticAnalysisController.
     * One test for most efficient  path algorithm
     */
    @Test
    public void testInsertValuesForMostEfficientPathAlgorithm() throws Exception {
        System.out.println("calculate most efficient path");
        controller.newStaticAnalysis();
        staticAnalysis=controller.insertValues(j1, j3, car, mea);
         double expResultTime = 1410.3;
        double expResultTollCost = 12.0;
        double expResultEnergy = 9.8998045E7;
        String expResultPath = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime = staticAnalysis.getTime();
        double resultTollCost = staticAnalysis.getTollcost();
        double resultEnergy = staticAnalysis.getComsumption();
        String resultPath = staticAnalysis.getSectionList().toString();

        assertEquals(expResultTime, resultTime, 0.1);
        assertEquals(expResultTollCost, resultTollCost, 0.0);
        assertEquals(expResultEnergy, resultEnergy, 1);
        assertEquals(expResultPath, resultPath);
        assertNotNull(staticAnalysis);

    }
    
    /**
     * Test of insertValues method, of class CreateStaticAnalysisController.
     * One test for lowestwork algorithm
     */
    @Test
    public void testInsertValuesForTheoreticalLowestWork() throws Exception {
        System.out.println("calculate lowest work");
        controller.newStaticAnalysis();
        staticAnalysis=controller.insertValues(j1, j3, car, lwa);
        double expResultTime = 970.0;
        double expResultTollCost = 12.0;
        double expResultEnergy = 1.6744572E8;
        String expResultPath = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime = staticAnalysis.getTime();
        double resultTollCost = staticAnalysis.getTollcost();
        double resultEnergy = staticAnalysis.getComsumption();
        String resultPath = staticAnalysis.getSectionList().toString();

        assertEquals(expResultTime, resultTime, 0.1);
        assertEquals(expResultTollCost, resultTollCost, 0.0);
        assertEquals(expResultEnergy, resultEnergy, 1);
        assertEquals(expResultPath, resultPath);
        assertNotNull(staticAnalysis);

    }
    
    /**
     * Test of insertValues method, of class CreateStaticAnalysisController.
     * One test for failure (same source and destiny)
     */
    @Test
    public void testInsertValuesNull() throws Exception {
        System.out.println("null test validate");
        controller.newStaticAnalysis();
        staticAnalysis=controller.insertValues(j1, j1, car, fpa);
        assertNull(staticAnalysis);

    }


}
