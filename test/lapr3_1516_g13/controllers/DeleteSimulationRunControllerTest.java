/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import java.io.File;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.Simulation;
import lapr3_1516_g13.model.SimulationResult;
import lapr3_1516_g13.model.Simulator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gabriel
 */
public class DeleteSimulationRunControllerTest {

    private Simulator simulator;

    public DeleteSimulationRunControllerTest() {
        simulator = new Simulator();
        Project proj = new Project();
        File file = new File("Test_Simulation.xml");
        Simulation simulation = new Simulation();

        proj.setName("Test");
        proj.setDescription("Description");
        proj.setActive(true);

        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet02_Vehicles.xml"), proj.getVehicleList());
            data.readFile(new File("Test_Network.xml"), proj.getRoadNetwork());
            data.readFile(file, simulation.getTrafficList(), proj);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        simulation.setName("Test1");
        simulation.setDescription("Desc");
        simulation.setAssociatedRoadNetwork(proj.getRoadNetwork());
        simulation.setActive(true);

        SimulationResult simResult = new SimulationResult();
        simResult.setName("Result 1");
        simResult.setDuration(30);
        simResult.setTimeStep(15);

        simulation.getSimulationResultList().add(simResult);

        proj.addSimulation(simulation);

        simulator.getProjectRegister().getProjectList().add(proj);
        simulator.getProjectRegister().setActiveProject(proj);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of selectedResult method, of class DeleteSimulationRunController.
     */
    @Test
    public void testSelectedResult() {
        System.out.println("selectedResult");
        String str = "Result 1";
        SimulationResult result = new SimulationResult();
        result.setName("Result 1");
        result.setDuration(30);
        result.setTimeStep(15);
        DeleteSimulationRunController instance = new DeleteSimulationRunController(simulator);
        instance.selectedResult(result);
        assertEquals(instance.getSimulationResult().getName(), result.getName());
        assertTrue("Should have the same duration", instance.getSimulationResult().getDuration() == result.getDuration());
        assertTrue("Should have the same timestep", instance.getSimulationResult().getTimeStep() == result.getTimeStep());
    }

    /**
     * Test of remove method, of class DeleteSimulationRunController.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        SimulationResult simulationResult = new SimulationResult();
        simulationResult.setName("Result 1");
        simulationResult.setDuration(30);
        simulationResult.setTimeStep(15);
        DeleteSimulationRunController instance = new DeleteSimulationRunController(simulator);
        instance.selectedResult(simulationResult);
        boolean expResult = true;
        boolean result = instance.remove();
        assertEquals(expResult, result);
    }

}
