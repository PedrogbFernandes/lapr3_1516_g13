/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import Graph.Edge;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.Junction;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Section;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.Vehicle;
import lapr3_1516_g13.model.VehicleList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 11410
 */
public class EditProjectControllerTest {

    Simulator s = new Simulator();
    Project p1 = new Project();
    EditProjectController instance;

    public EditProjectControllerTest() throws Exception {
        s = new Simulator();
        File file = new File("Test_Network.xml");
        p1.setName("Teste1");
        p1.setDescription("Descrição1");

        DataIO dataio = new DataXML();
        RoadNetwork rn = p1.getRoadNetwork();
        dataio.readFile(file, rn);

        file = new File("Test_Vehicles.xml");
        VehicleList vl = p1.getVehicleList();
        dataio.readFile(file, vl);

        s.getProjectRegister().addProject(p1);
        s.getProjectRegister().setActiveProject(p1);

        instance = new EditProjectController(s);
        instance.init();

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of editName method, of class EditProjectController.
     */
    @Test
    public void testEditName() {
        System.out.println("editName");
        instance.editName("Teste2");
        instance.registerEditedProject();
        assertEquals("Teste2", s.getProjectRegister().getActiveProject().getName());
    }

    /**
     * Test of editDescription method, of class EditProjectController.
     */
    @Test
    public void testEditDescription() {
        System.out.println("editDescription");
        instance.editName("Descrição2");
        instance.registerEditedProject();
        assertEquals("Descrição2", s.getProjectRegister().getActiveProject().getName());
    }

    /**
     * Test of selectVehicleFile method, of class EditProjectController.
     */
    @Test
    public void testSelectVehicleFile() throws Exception {
        System.out.println("selectVehicleFile");
        File file = new File("Test_Vehicles.xml");
        instance.selectVehicleFile(file);
        instance.registerEditedProject();
        assertEquals(2, s.getProjectRegister().getActiveProject().getVehicleList().getVehicleList().size());
        ArrayList<String> nomes = new ArrayList<>();
        for (Vehicle a : s.getProjectRegister().getActiveProject().getVehicleList().getVehicleList()) {
           nomes.add(a.getName());
        }
        assertEquals(true, nomes.contains("Dummy01"));
        assertEquals(true, nomes.contains("Dummy011"));
    }

    /**
     * Test of selectRoadNetworkFile method, of class EditProjectController.
     */
    @Test
    public void testSelectRoadNetworkFile() throws Exception {
        File file = new File("Test_Network2.xml");
        System.out.println("selectRoadNetworkFile");
        assertEquals(6, s.getProjectRegister().getActiveProject().getRoadNetwork().getGraph().numEdges());
        assertEquals(5, s.getProjectRegister().getActiveProject().getRoadNetwork().getGraph().numVertices());
        instance.selectRoadNetworkFile(file);
        instance.registerEditedProject();
        assertEquals(7, s.getProjectRegister().getActiveProject().getRoadNetwork().getGraph().numEdges());
        assertEquals(6, s.getProjectRegister().getActiveProject().getRoadNetwork().getGraph().numVertices());
    }

    /**
     * Test of registerEditedProject method, of class EditProjectController.
     */
    @Test
    public void testRegisterEditedProject() {
        System.out.println("registerEditedProject");
        instance.editName("Teste4");
        instance.registerEditedProject();
        assertEquals("Teste4", s.getProjectRegister().getActiveProject().getName());
    }

}
