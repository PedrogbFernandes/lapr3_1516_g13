package lapr3_1516_g13.controllers;

import java.io.File;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class EditSimulationControllerTest {

    Simulator s = new Simulator();
    Project p1 = new Project();
    LinkedList<Project> plist = new LinkedList();

    RoadNetwork roadN = new RoadNetwork();

    Simulation s1 = new Simulation();
    Simulation s2 = new Simulation();
    Simulation s3 = new Simulation();
    Simulation s4 = new Simulation();

    public EditSimulationControllerTest() {

        DataXML dataXML = new DataXML();
        try {
            dataXML.readFile(new File("Test_Network.xml"), roadN);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        p1.setActive(true);
        plist.addFirst(p1);

        s.getProjectRegister().setProjectList(plist);

        s1.setName("Test1");
        s2.setName("Test2");
        s3.setName("Test3");
        s4.setName("Test4");

        s1.setDescription("Description1");
        s2.setDescription("Description2");
        s3.setDescription("Description3");
        s4.setDescription("Description4");

        s1.getTrafficList().add(new Traffic());
        s2.getTrafficList().add(new Traffic());
        s3.getTrafficList().add(new Traffic());
        s4.getTrafficList().add(new Traffic());

        s1.setAssociatedRoadNetwork(roadN);
        s2.setAssociatedRoadNetwork(roadN);
        s3.setAssociatedRoadNetwork(roadN);
        s4.setAssociatedRoadNetwork(roadN);

        p1.addSimulation(s1);
        p1.addSimulation(s2);
        p1.addSimulation(s3);
        p1.addSimulation(s4);

        p1.setActiveSimulation(s2);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getSimulationNameDescription method, of class
     * EditSimulationController.
     */
    @Test
    public void testGetSimulationNameDescription() {
        System.out.println("getSimulationNameDescription and getActiveSimulation");
        EditSimulationController instance = new EditSimulationController(s);
        instance.getActiveSimulation();

        String[] expResult = {"Test2", "Description2"};
        String[] result = instance.getSimulationNameDescription();
        assertArrayEquals(expResult, result);

        //No active project
        p1.setActive(false);
        assertTrue("should be false", instance.getActiveSimulation() == false);

        //No active simulation
        p1.setActive(true);
        s2.setActive(false);
        assertTrue("should be false", instance.getActiveSimulation() == false);
    }

    /**
     * Test of editNameDescription method, of class EditSimulationController.
     */
    @Test
    public void testEditNameDescription() {
        System.out.println("editNameDescription");
        String name = "EditTest2";
        String description = "EditDescription2";

        EditSimulationController instance = new EditSimulationController(s);
        instance.getActiveSimulation();
        instance.getSimulationNameDescription();

        boolean expResult = true;
        boolean result = instance.editNameDescription(name, description);
        assertEquals(expResult, result);

        //Invalid name
        name = " ";

        expResult = false;
        result = instance.editNameDescription(name, description);
        assertEquals(expResult, result);

        name = null;

        expResult = false;
        result = instance.editNameDescription(name, description);
        assertEquals(expResult, result);

        //Invalid description
        name = "EditTest2";
        description = " ";

        expResult = false;
        result = instance.editNameDescription(name, description);
        assertEquals(expResult, result);

        description = null;

        expResult = false;
        result = instance.editNameDescription(name, description);
        assertEquals(expResult, result);

        //Name already exists
        name = "Test4";
        description = "Description2";

        expResult = false;
        result = instance.editNameDescription(name, description);
        assertEquals(expResult, result);
    }

    /**
     * Test of registerEditedSimulation method, of class
     * EditSimulationController.
     */
    @Test
    public void testRegisterEditedSimulation() {
        System.out.println("registerEditedSimulation");
        EditSimulationController instance = new EditSimulationController(s);
        instance.getActiveSimulation();
        instance.getSimulationNameDescription();
        instance.editNameDescription("EditTest2", "EditDescription2");

        boolean expResult = true;
        boolean result = instance.registerEditedSimulation();
        assertEquals(expResult, result);

    }

}
