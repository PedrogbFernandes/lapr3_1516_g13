package lapr3_1516_g13.controllers;

import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class OpenProjectControllerTest {

    Simulator s = new Simulator();
    Project p1 = new Project();
    Project p2 = new Project();
    Project p3 = new Project();
    LinkedList<Project> plist = new LinkedList();

    public OpenProjectControllerTest() {
        p1.setName("Project Test 1");
        p1.setDescription("Testing Project 1");
        p1.setActive(false);

        p2.setName("Project Test 2");
        p2.setDescription("Testing Project 2");
        p2.setActive(false);

        p3.setName("Project Test 3");
        p3.setDescription("Testing Project 3");
        p3.setActive(false);

        plist.addLast(p1);
        plist.addLast(p2);
        plist.addLast(p3);

        s.getProjectRegister().setProjectList(plist);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getProjectList method, of class OpenProjectController.
     */
    @Test
    public void testGetProjectList() {
        System.out.println("getProjectList");
        OpenProjectController instance = new OpenProjectController(s);
        List<Project> expResult = plist;
        List<Project> result = instance.getProjectList();
        assertEquals(expResult, result);

    }

    /**
     * Test of selectProject method, of class OpenProjectController.
     */
    @Test
    public void testSelectProject() {
        System.out.println("selectProject");
        Project p = p2;
        OpenProjectController instance = new OpenProjectController(s);
        boolean expResult = true;
        boolean result = instance.selectProject(p);
        assertEquals(expResult, result);
    }

}
