package lapr3_1516_g13.controllers;

import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.model.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class OpenSimulationControllerTest {

    private Simulation s1, s2, s3;
    private final Simulator simulator;
    private final Project project;

    public OpenSimulationControllerTest() {

        simulator = new Simulator();

        project = new Project();
        project.setActive(true);

        s1 = new Simulation();
        s1.setName("Test1");
        s1.setDescription("Test1Description");

        s2 = new Simulation();
        s2.setName("Test2");
        s2.setDescription("Test2Description");

        s3 = new Simulation();
        s3.setName("Test3");
        s3.setDescription("Test3Description");

        project.getSimulationList().addSimulation(s1);
        project.getSimulationList().addSimulation(s2);
        project.getSimulationList().addSimulation(s3);

        simulator.getProjectRegister().addProject(project);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getSimulationList method, of class OpenSimulationController.
     */
    @Test
    public void testGetSimulationList() {
        System.out.println("getSimulationList");
        OpenSimulationController instance = new OpenSimulationController(simulator);

        List<Simulation> expResult = new LinkedList<>();
        expResult.add(s1);
        expResult.add(s2);
        expResult.add(s3);

        List<Simulation> result = instance.getSimulationList();

        assertEquals(expResult, result);
    }

    /**
     * Test of selectSimulation method, of class OpenSimulationController.
     */
    @Test
    public void testSelectSimulation() {
        System.out.println("selectSimulation");
        Simulation s = s2;
        OpenSimulationController instance = new OpenSimulationController(simulator);
        instance.getSimulationList();

        boolean expResult = true;
        boolean result = instance.selectSimulation(s);
        
        assertEquals(expResult, result);
    }

}
