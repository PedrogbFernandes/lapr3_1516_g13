/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import PathAlgorithms.*;
import PathAlgorithms.TheoreticalLowestWorkPathAlgorithm;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.*;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pedro
 */
public class RunSimulationControllerTest {

    private RunSimulationController controller;
    private Simulator simulator;
    private Simulation simulacao;
    private ProjectRegister preg;
    private Project proj = new Project();
    private List<Traffic> traffic;
    private List<SimulationResult> results = new ArrayList();
    private SimulationRegister simulationList = new SimulationRegister();
    private RoadNetwork g = new RoadNetwork();

    public RunSimulationControllerTest() {
        simulator = new Simulator();

        traffic = new ArrayList<>();
        proj.setName("P1");
        proj.setDescription("X");
        proj.setActive(true);

        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet04_Vehicles.xml"), proj.getVehicleList());
            data.readFile(new File("TestSet04_Network.xml"), proj.getRoadNetwork());
            data.readFile(new File("TestSet04_Simulation.xml"), traffic, proj);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        preg = simulator.getProjectRegister();
        simulacao = new Simulation("S1", "1", proj.getRoadNetwork(), results, traffic, true);

        
        simulationList.addSimulation(simulacao);
        proj.setSimulationList(simulationList);
        proj.setActiveSimulation(simulacao);
        proj.addSimulation(simulacao);
        preg.addProject(proj);
        preg.setActiveProject(proj);

        this.controller = new RunSimulationController(simulator);
        
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of runSimulation method, of class RunSimulationController.
     */
    @Test
    public void testRunSimulationMostEfficientPath() {
        System.out.println("\n-----------------------------------------------------------------------------------------------\n");
        System.out.println("RunSimulationMostEfficientPath");
        RunSimulationController instance = controller;
        String name = "S1R";
        double duration = 30;
        double timeStep = 1;
        FastestPathAlgorithm fpa = new FastestPathAlgorithm();

        instance.init();
        instance.insertNameDurationTimeStepAlgorithm(name, duration, timeStep, fpa);
        
    }

    /**
     * Test of init method, of class RunSimulationController.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        RunSimulationController instance = controller;
        boolean expResult = true;
        boolean result = instance.init();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAlgorithmsList method, of class RunSimulationController.
     */
    @Test
    public void testGetAlgorithmsList() {
        System.out.println("getAlgorithmsList"); 
        List<PathAlgorithms> list = controller.getAlgorithmsList();
        RunSimulationController instance = controller;
       
        List<PathAlgorithms> expResult = list;
        
        List<PathAlgorithms> result = instance.getAlgorithmsList();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of insertNameDurationTimeStepAlgorithm method, of class RunSimulationController.
     */
    @Test
    public void testInsertNameDurationTimeStepAlgorithm() {
        System.out.println("insertNameDurationTimeStepAlgorithm");
        String name = "S1R";
        double duration = 10.0;
        double timeStep = 1.0;
        PathAlgorithms bestPath = new FastestPathAlgorithm();
        SimulationResult results= new SimulationResult();
        RunSimulationController instance = controller;
        instance.init();
        instance.setResult(results);
        boolean expResult = true;
        boolean result = instance.insertNameDurationTimeStepAlgorithm(name, duration, timeStep, bestPath);
        assertEquals(expResult, result);

    }

    /**
     * Test of getSummaryData method, of class RunSimulationController.
     */
    @Test
    public void testGetSummaryData() {
        System.out.println("getSummaryData");
        RunSimulationController instance = controller;
        controller.init();
        instance.init();
        SimulationResult r = new SimulationResult();
        r.setName("S1R");
        PathAlgorithms bestPath = new FastestPathAlgorithm();
        r.setBestPathMethod(bestPath);
        instance.setResult(r);
        String[] expResult = controller.getSummaryData();
        String[] result = instance.getSummaryData();
        for (int i = 0; i < result.length; i++) {
            System.out.println(expResult[i]);
        }
        
        assertArrayEquals(expResult, result);
        
    }

    /**
     * Test of registerSimulationRun method, of class RunSimulationController.
     */
    @Test
    public void testRegisterSimulationRun() {
        System.out.println("registerSimulationRun");
        RunSimulationController instance = controller;

        instance.init();
        boolean expResult = true;
        boolean result = instance.registerSimulationRun();
        assertEquals(expResult, result);

    }

    /**
     * Test of setResult method, of class RunSimulationController.
     */
    @Test
    public void testSetResult() {
        System.out.println("setResult");
        SimulationResult result_2 = new SimulationResult();
        RunSimulationController instance = controller;
        instance.setResult(result_2);
        

    }



}
