/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.controllers;

import PathAlgorithms.FastestPathAlgorithm;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import lapr3_1516_g13.model.Project;
import lapr3_1516_g13.model.ProjectRegister;
import lapr3_1516_g13.model.Result;
import lapr3_1516_g13.model.RoadNetwork;
import lapr3_1516_g13.model.Simulation;
import lapr3_1516_g13.model.SimulationRegister;
import lapr3_1516_g13.model.SimulationResult;
import lapr3_1516_g13.model.Simulator;
import lapr3_1516_g13.model.Traffic;
import lapr3_1516_g13.model.VehicleResult;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class ShowResultSimulationControllerTest {

    private RunSimulationController controller;
    private Simulator simulator;
    private Simulation simulacao;
    private ProjectRegister preg;
    private Project proj = new Project();
    private List<Traffic> traffic;
    private RoadNetwork g = new RoadNetwork();
    RunSimulationController simuc;

    public ShowResultSimulationControllerTest() {
        simulator = new Simulator();

        traffic = new ArrayList<>();
        proj.setName("P1");
        proj.setDescription("X");
        proj.setActive(true);

        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet04_Vehicles.xml"), proj.getVehicleList());
            data.readFile(new File("TestSet04_Network.xml"), proj.getRoadNetwork());
            data.readFile(new File("TestSet04_Simulation.xml"), traffic, proj);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        preg = simulator.getProjectRegister();
        List<SimulationResult> resultado = new ArrayList();
        simulacao = new Simulation("S1", "1", proj.getRoadNetwork(), resultado, traffic, true);

        SimulationRegister listaSimu = new SimulationRegister();
        listaSimu.addSimulation(simulacao);
        proj.setSimulationList(listaSimu);
        proj.setActiveSimulation(simulacao);
        proj.addSimulation(simulacao);
        preg.addProject(proj);
        preg.setActiveProject(proj);
        this.controller = new RunSimulationController(simulator);
        simuc = controller;
        String name = "S1R";
        double duration = 30;
        double timeStep = 1;
        FastestPathAlgorithm fpa = new FastestPathAlgorithm();
        simuc.init();
        simuc.insertNameDurationTimeStepAlgorithm(name, duration, timeStep, fpa);
        simuc.registerSimulationRun();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of selectedResult method, of class ShowResultSimulationController.
     */
    @Test
    public void testSelectedResult() {
//        System.out.println("selectedResult");
//        String str = "";
//        ShowResultSimulationController instance = null;
//        instance.selectedResult(str);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of createHtmlFile method, of class ShowResultSimulationController.
     */
    @Test
    public void testCreateHtmlFile() {
//        System.out.println("createHtmlFile");
//        String name = "";
//        ShowResultSimulationController instance = null;
//        instance.createHtmlFile(name);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

}
