/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import PathAlgorithms.PathAlgorithms;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 11410
 */
public class AlgorithmListTest {
    
    public AlgorithmListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListOfAlgorithms method, of class AlgorithmList.
     */
    @Test
    public void testSetandGetListOfAlgorithms() {
        System.out.println("getListOfAlgorithms");
        AlgorithmList instance = new AlgorithmList();
        List<PathAlgorithms> expResult = new ArrayList<>();
        instance.setListOfAlgorithms(expResult);
        assertEquals(expResult, instance.getListOfAlgorithms());
    }

    
}
