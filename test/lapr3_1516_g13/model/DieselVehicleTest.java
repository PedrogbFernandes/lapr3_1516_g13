/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.io.File;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class DieselVehicleTest {
   
    VehicleList v = new VehicleList();
    DataXML data = new DataXML();
    File f = new File("TestDiesel.xml");
    DieselVehicle car = new DieselVehicle();

   
    
    public DieselVehicleTest() throws Exception {
        data.readFile(f, v);
        car = (DieselVehicle) v.getVehicleList().get(0);
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of fuelOnStandBy method, of class DieselVehicle.
     */
    @Test
    public void testFuelOnStandBy() {
        System.out.println("fuelOnStandBy");
        double time = 1;   
        double expResult = 973.2;
        double result = car.fuelOnStandBy(time);
        assertEquals(expResult, result,0.1);
        
        
    }

    /**
     * Test of energyComsumption method, of class DieselVehicle.
     */
    @Test
    public void testEnergyComsumption() {
        System.out.println("energyComsumption");
        int gear = 1;
        int rpm = 2500;
        double trotle = Vehicle.MAX_TROTLE;
        double time = 10;
        double angle = 15;
        double distance = 1000;
        double windSpeed = 10;
        double windDirection = 20;
        double expResult = 10053.00;
        double result = car.energyComsumption(gear, rpm, trotle, time, angle, distance, windSpeed, windDirection);
        assertEquals(expResult, result, 0.1);
        
    }

    /**
     * Test of calculateSpecificFuelComsumption method, of class DieselVehicle.
     */
    @Test
    public void testCalculateSpecificFuelComsumption() {
        System.out.println("calculateSpecificFuelComsumption");
        int rpm = 2500;
        double trotle = Vehicle.MAX_TROTLE;
        double expResult = 1005.3;
        double result = car.calculateSpecificFuelComsumption(rpm, trotle);
        assertEquals(expResult, result, 0.1);
        
    }
    
}
