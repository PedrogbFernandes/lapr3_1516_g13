/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.io.File;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class EletricVehicleTest {

    VehicleList v = new VehicleList();
    DataXML data = new DataXML();
    File f = new File("TestSet03_VehiclesSFCGrande.xml");
    EletricVehicle car = new EletricVehicle();

    public EletricVehicleTest() throws Exception {

        data.readFile(f, v);
        car = (EletricVehicle) v.getVehicleList().get(2);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testEnergy_ratio() {
        System.out.println("Energy_ratio");
        double expResult = 420;
        car.setEnergy_ratio(420);
        double result = car.getEnergy_ratio();
        assertEquals(expResult, result, 0.0);
        
    }

    

    /**
     * Test of energyComsumption method, of class EletricVehicle.
     */
    @Test
    public void testEnergyComsumption() {
        System.out.println("energyComsumption");
        int gear = 1;
        int rpm = 2500;
        double trotle = Vehicle.MAX_TROTLE;
        double time = 10;
        double angle = 15;
        double distance = 1000;
        double windSpeed = 10;
        double windDirection = 20;
        double expResult = 654498.5;
        double result = car.energyComsumption(gear, rpm, trotle, time, angle, distance, windSpeed, windDirection);
        assertEquals(expResult, result, 0.1);
    }

    /**
     * Test of regenerateEnergy method, of class EletricVehicle.
     */
    @Test
    public void testRegenerateEnergy() {
        System.out.println("regenerateEnergy");
        int gear = 1;
        int rpm = 2500;
        double angle = -15;
        double distance = 1000;
        double windSpeed = 10;
        double windDirection = 20;

        double expResult = 2080000.0;
        double result = car.regenerateEnergy(gear, rpm, angle, distance, windSpeed, windDirection);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of fuelOnStandBy method, of class EletricVehicle.
     */
    @Test
    public void testFuelOnStandBy() {
        System.out.println("fuelOnStandBy");
        double expResult = 0.0;
        double result = car.fuelOnStandBy();
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of clone method, of class EletricVehicle.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        EletricVehicle instance = new EletricVehicle();
        Vehicle expResult = null;
        Vehicle result = instance.clone();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clone2 method, of class EletricVehicle.
     */
    @Test
    public void testClone2() {
        System.out.println("clone2");
        double i = 0.0;
        double initialTime = 0.0;
        EletricVehicle instance = new EletricVehicle();
        Vehicle expResult = null;
        Vehicle result = instance.clone2(i, initialTime);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
