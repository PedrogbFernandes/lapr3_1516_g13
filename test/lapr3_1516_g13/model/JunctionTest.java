/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 11410
 */
public class JunctionTest {
    public JunctionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addVehicle method, of class Junction.
     */
    @Test
    public void testAddVehicle() {
        System.out.println("addVehicle");
        Vehicle vinput = new Vehicle();
        vinput.setName("CarroTeste");
        Junction instance = new Junction();
        instance.addVehicle(vinput);
        assertEquals(true, instance.getLocatedVehicles().contains(vinput));
    }

    /**
     * Test of getIndex method, of class Junction.
     */
    @Test
    public void testSetandGetIndex() {
        System.out.println("getIndex");
        Junction instance = new Junction();
        instance.setIndex("TesteIndex");
        String expResult = "TesteIndex";
        String result = instance.getIndex();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocatedVehicles method, of class Junction.
     */
    @Test
    public void testSetandGetLocatedVehicles() {
        System.out.println("getLocatedVehicles");
        Junction instance = new Junction();
        Vehicle vInput = new Vehicle();
        vInput.setName("CarroTeste");
        Vehicle vInput2 = new Vehicle();
        vInput2.setName("CarroTeste2");
        ArrayList<Vehicle> lVehicles = new ArrayList<>();
        lVehicles.add(vInput);
        lVehicles.add(vInput2);
        instance.setLocatedVehicles(lVehicles);
        int expResult = lVehicles.size();
        int result = instance.getLocatedVehicles().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Junction.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Junction instance = new Junction();
        instance.setIndex("Teste");
        Junction testInstance = new Junction();
        testInstance.setIndex("Teste");
        Junction testInstance2 = new Junction();
        testInstance2.setIndex("Teste2");
        boolean result = instance.equals(testInstance);
        assertEquals(true, result);
        result = instance.equals(testInstance2);
        assertEquals(false, result);
    }

    /**
     * Test of toString method, of class Junction.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Junction instance = new Junction();
        instance.setIndex("Teste");
        String expResult = "Junction: Teste";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
