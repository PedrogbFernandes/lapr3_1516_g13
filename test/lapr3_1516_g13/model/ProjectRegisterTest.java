package lapr3_1516_g13.model;

import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProjectRegisterTest {

    Project p1 = new Project();
    Project p2 = new Project();
    Project p3 = new Project();
    LinkedList<Project> plist = new LinkedList();

    public ProjectRegisterTest() {

        p1.setName("Project Test 1");
        p1.setDescription("Testing Project 1");
        p1.setActive(false);

        p2.setName("Project Test 2");
        p2.setDescription("Testing Project 2");
        p2.setActive(false);

        p3.setName("Project Test 3");
        p3.setDescription("Testing Project 3");
        p3.setActive(false);

        plist.addLast(p1);
        plist.addLast(p2);
        plist.addLast(p3);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setProjectList method, of class ProjectRegister.
     */
    @Test
    public void testSetProjectList() {
        System.out.println("Test setProjectList and getProjectList");
        LinkedList<Project> pl = new LinkedList();
        pl.addLast(p1);
        pl.addLast(p2);
        pl.addLast(p3);

        ProjectRegister instance = new ProjectRegister();
        instance.setProjectList(pl);

        assertEquals(pl, instance.getProjectList());

    }


    /**
     * Test of getProjectList method, of class ProjectRegister.
     */
    @Test
    public void testGetProjectList() {
        System.out.println("getProjectList");
        ProjectRegister instance = new ProjectRegister();
        LinkedList<Project> expResult = null;
        LinkedList<Project> result = instance.getProjectList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createNewProject method, of class ProjectRegister.
     */
    @Test
    public void testCreateNewandRemoveProject() {
        System.out.println("createNewandRemvoeProject");
        ProjectRegister instance = new ProjectRegister();
        Project testProject = instance.createNewProject();
        testProject.setName("Project 1");
        testProject.setDescription("Description 1");
        boolean expResult = true;
        assertEquals(true, instance.getProjectList().size()==1);
        instance.removeProject(testProject);
        assertEquals(true, instance.getProjectList().size()==0);
    }

    /**
     * Test of validateProject method, of class ProjectRegister.
     */
    @Test
    public void testValidateProject() {
        System.out.println("validateProject");
        Project p = null;
        ProjectRegister instance = new ProjectRegister();
        boolean expResult = false;
        boolean result = instance.validateProject(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addProject method, of class ProjectRegister.
     */
    @Test
    public void testAddProject() {
        System.out.println("addProject");
        Project p = new Project();
        p.setName("Project 1");
        p.setDescription("Description 1");
        ProjectRegister instance = new ProjectRegister();
        boolean expResult = true;
        boolean result = instance.addProject(p);
        assertEquals(expResult, result);
        assertEquals(expResult, instance.getProjectList().contains(p));
    }

    /**
     * Test of getActiveProject method, of class ProjectRegister.
     */
    @Test
    public void testSetandGetActiveProject() {
        System.out.println("getActiveProject");
        Project p = new Project();
        p.setName("Project 1");
        p.setDescription("Description 1");
        Project p2 = new Project();
        p2.setName("Project 2");
        p2.setDescription("Description 2");
        ProjectRegister instance = new ProjectRegister();
        instance.addProject(p);
        instance.addProject(p2);
        instance.setActiveProject(p);
        assertEquals(p, instance.getActiveProject());
        instance.setActiveProject(p2);
        assertEquals(p2, instance.getActiveProject());
    }


}
