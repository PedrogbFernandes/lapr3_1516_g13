/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.io.File;
import java.util.ArrayList;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 11410
 */
public class ProjectTest {

    public ProjectTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Project.
     */
    @Test
    public void testSetandGetName() {
        System.out.println("setandgetName");
        Project instance = new Project();
        instance.setName("ProjectTest");
        String expResult = "ProjectTest";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescription method, of class Project.
     */
    @Test
    public void testSetandGetDescription() {
        System.out.println("setandgetDescription");
        Project instance = new Project();
        instance.setDescription("DescriptionTest");
        String expResult = "DescriptionTest";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRoadNetwork method, of class Project.
     */
    @Test
    public void testSetandGetRoadNetwork() throws Exception {
        System.out.println("setandgetRoadNetwork");
        Project instance = new Project();
        File testFile = new File("Test_Network.xml");
        DataIO dataio = new DataXML();
        RoadNetwork testRoadNetwork = instance.getRoadNetwork();
        dataio.readFile(testFile, testRoadNetwork);
        instance.setRoadNetwork(testRoadNetwork);
        RoadNetwork result = instance.getRoadNetwork();
        assertEquals(testRoadNetwork,instance.getRoadNetwork());
    }

    /**
     * Test of getVehicleList method, of class Project.
     */
    @Test
    public void testSetandGetVehicleList() throws Exception {
        System.out.println("setandgetVehicleList");
        Project instance = new Project();
        File testFile = new File("Test_Vehicles.xml");
        DataIO dataio = new DataXML();
        VehicleList testVehicles = instance.getVehicleList();
        dataio.readFile(testFile, testVehicles);
        instance.setVehicleList(testVehicles);
        assertEquals(testVehicles, instance.getVehicleList());
    }

    /**
     * Test of isActive method, of class Project.
     */
    @Test
    public void testGetandIsActive() throws Exception {
        System.out.println("isActive");
        Project instance = new Project();
        instance.setActive(true);
        assertEquals(true, instance.isActive());
    }

    /**
     * Test of getFileFormat method, of class Project.
     */
    @Test
    public void testSetandGetFileFormat() {
        System.out.println("setandgetFileFormat");
        Project instance = new Project();
        instance.setFileFormat("HTML");
        String expResult = "HTML";
        String result = instance.getFileFormat();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFileFormats method, of class Project.
     */
    @Test
    public void testGetFileFormats() {
        System.out.println("getFileFormats");
        Project instance = new Project();
        ArrayList<String> result = instance.getFileFormats();
        Boolean Verification1 = result.contains("CSV");
        Boolean Verification2 = result.contains("HTML");
        assertEquals(true, Verification1 && Verification2);
    }


    /**
     * Test of newSimulation method, of class Project.
     */
    @Test
    public void testAddandNewandRemoveSimulation() {
        System.out.println("newSimulation");
        Project instance = new Project();
        Simulation testSimulation = instance.newSimulation();
        testSimulation.setName("Test Simulation");
        testSimulation.setDescription("Test Description");
        instance.addSimulation(testSimulation);
        assertEquals(1, instance.getSimulationList().getListSimulation().size());
        instance.removeSimulation(testSimulation);
        assertEquals(0, instance.getSimulationList().getListSimulation().size());
    }


    /**
     * Test of validateSimulation method, of class Project.
     */
    @Test
    public void testValidateSimulation() throws Exception {
        System.out.println("validateSimulation");
        Project instance = new Project();
        Simulation simulationTest = new Simulation();
        simulationTest.setName("Simulation Test");
        simulationTest.setDescription("Simulation Test Description");
        File testFile = new File("Test_Network.xml");
        DataIO dataio = new DataXML();
        RoadNetwork testRoadNetwork = instance.getRoadNetwork();
        dataio.readFile(testFile, testRoadNetwork);
        instance.setRoadNetwork(testRoadNetwork);
        simulationTest.setAssociatedRoadNetwork(testRoadNetwork);
        ArrayList<Traffic> testListTraffic = new ArrayList<>();
        Traffic testTraffic = new Traffic();
        testTraffic.setArrivalRate(20);
        testListTraffic.add(testTraffic);
        simulationTest.setTrafficList(testListTraffic);
        boolean expResult = true;
        boolean result = instance.validateSimulation(simulationTest);
        assertEquals(expResult, result);
    }

    /**
     * Test of setSimulationList method, of class Project.
     */
    @Test
    public void testSetandGetSimulationList() {
        System.out.println("setSimulationList");
        SimulationRegister simulationList = new SimulationRegister();
        Simulation testSimulation = new Simulation("Test name 1","Test Description 1", null, null, null, true);
        simulationList.addSimulation(testSimulation);
        Project instance = new Project();
        instance.setSimulationList(simulationList);
        assertEquals(true, instance.getSimulationList().getActiveSimulation().getName().compareTo("Test name 1")==0);
    }

    /**
     * Test of vehicleExists method, of class Project.
     */
    @Test
    public void testVehicleExists() {
        System.out.println("vehicleExists");
        String vehicleName = "Smart";
        Vehicle testVehicle = new Vehicle();
        testVehicle.setName("Smart");
        Project instance = new Project();
        VehicleList testVehicleList = new VehicleList();
        testVehicleList.addVehicle(testVehicle);
        instance.setVehicleList(testVehicleList);
        boolean result = instance.vehicleExists(vehicleName);
        assertEquals(true, result);
    }

    /**
     * Test of clone method, of class Project.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Project instance = new Project();
        instance.setName("Test Project");
        instance.setDescription("Test Project Description");
        Project expResult = instance;
        Project result = instance.clone();
        assertEquals(expResult, result);
    }

    /**
     * Test of cloneNoSimulations method, of class Project.
     */
    @Test
    public void testCloneNoSimulations() {
       System.out.println("clone");
        String str = "Test Simulation";
        Project instance = new Project();
        instance.setName("Test Project");
        instance.setDescription("Test Project Description");
        Simulation testSimulation = new Simulation();
        testSimulation.setName(str);
        instance.addSimulation(testSimulation);
        Project result = instance.cloneNoSimulations();
        assertEquals(0,result.getSimulationList().getListSimulation().size());
    }

    /**
     * Test of setActiveSimulation method, of class Project.
     */
    @Test
    public void testSetandGetActiveSimulation() {
        System.out.println("setActiveSimulation");
        Simulation simulationTest = new Simulation("Simulation Test 1", "Description 1", null, null, null, false);
        Simulation simulationTest2 = new Simulation("Simulation Test 2", "Description 2", null, null, null, false);
        Project instance = new Project();
        instance.addSimulation(simulationTest);
        instance.addSimulation(simulationTest2);
        instance.setActiveSimulation(simulationTest);
        assertEquals(simulationTest, instance.getActiveSimulation());
    }

    /**
     * Test of editSimulation method, of class Project.
     */
    @Test
    public void testEditSimulation() throws Exception {
        System.out.println("editSimulation");
        Simulation s = new Simulation();
        s.setName("Simulation 1");
        s.setDescription("Description 1");
        Simulation sClone = new Simulation();
        sClone.setName("Simulation 1");
        sClone.setDescription("Description 2");
        Project instance = new Project();
        File testFile = new File("Test_Network.xml");
        DataIO dataio = new DataXML();
        RoadNetwork testRoadNetwork = instance.getRoadNetwork();
        dataio.readFile(testFile, testRoadNetwork);
        sClone.setAssociatedRoadNetwork(testRoadNetwork);
        ArrayList<Traffic> testListTraffic = new ArrayList<>();
        Traffic testTraffic = new Traffic();
        testTraffic.setArrivalRate(20);
        testListTraffic.add(testTraffic);
        sClone.setTrafficList(testListTraffic);
        instance.addSimulation(s);
        assertEquals(true, instance.editSimulation(s, sClone));
    }

    /**
     * Test of validate method, of class Project.
     */
    @Test
    public void testValidate() throws Exception {
        System.out.println("validate");
        Project instance = new Project();
        instance.setName("Project 1");
        instance.setDescription("Description 1");
        File testFile = new File("Test_Network.xml");
        DataIO dataio = new DataXML();
        RoadNetwork testRoadNetwork = instance.getRoadNetwork();
        dataio.readFile(testFile, testRoadNetwork);
        testFile = new File("Test_Vehicles.xml");
        dataio = new DataXML();
        VehicleList testVehicles = instance.getVehicleList();
        dataio.readFile(testFile, testVehicles);
        instance.setVehicleList(testVehicles);
        assertEquals(true, instance.validate());
    }

    /**
     * Test of equals method, of class Project.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Project instance = new Project();
        instance.setName("Project 1");
        Project testProject = new Project();
        testProject.setName("Project 1");
        assertEquals(true, instance.equals(testProject));
    }

    /**
     * Test of toString method, of class Project.
     */
    @Test
    public void testToString() {
        //Project: %s, Description: %s
        System.out.println("toString");
        Project instance = new Project();
        instance.setName("Project 1");
        instance.setDescription("Description 1");
        String expResult = "Project: Project 1, Description: Description 1";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
