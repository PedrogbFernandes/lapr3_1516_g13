/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class RegimeTest {
    
    public RegimeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTorque method, of class Regime.
     */
    @Test
    public void testGetSetTorque() {
        System.out.println("getSetTorque");
        Regime instance = new Regime();
        
        instance.setTorque(10);
        int expResult = 10;
        
        int result = instance.getTorque();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRpm_low method, of class Regime.
     */
    @Test
    public void testGetSetRpm_low() {
        System.out.println("getSetRpm_low");
        Regime instance = new Regime();
        instance.setRpm_low(10);
        int expResult = 10;
        int result = instance.getRpm_low();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRpm_high method, of class Regime.
     */
    @Test
    public void testGetSetRpm_high() {
        System.out.println("getRpm_high");
        Regime instance = new Regime();
        instance.setRpm_high(10);
        int expResult = 10;
        int result = instance.getRpm_high();
        assertEquals(expResult, result);

    }

    /**
     * Test of getSfc method, of class Regime.
     */
    @Test
    public void testGetSetSfc() {
        System.out.println("getSetSfc");
        Regime instance = new Regime();
        instance.setSfc(10.0);
        double expResult = 10.0;
        double result = instance.getSfc();
        assertEquals(expResult, result, 0.0);

    }

   
}
