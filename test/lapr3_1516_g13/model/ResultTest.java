/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import Graph.Graph;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class ResultTest {
        File f = new File("TestSet03_VehiclesSFCGrande.xml");
    File file = new File("TestSet02_Network.xml");
    VehicleList vl = new VehicleList();
    DataXML data = new DataXML();
    RoadNetwork rn = new RoadNetwork();
    Junction n01 = new Junction();
    Junction n02 = new Junction();
    Junction n03 = new Junction();
    Segment s01 = new Segment();
    Segment s02 = new Segment();
    Segment s03 = new Segment();
    Segment s04 = new Segment();
    Section sa = new Section();
    Section sa1 = new Section();
    Traffic t = new Traffic();
    
    public ResultTest() {
           //Junctions 
        n01.setIndex("n01");
        n02.setIndex("n02");
        n03.setIndex("n03");

        //Segment 01
        s01.setHeight(100);
        s01.setId("01");
        s01.setLength(3.2);
        s01.setMaxVelocity(90);
        s01.setMinVelocity(0);
        s01.setNumberVehicles(20);
        s01.setSlope(15);

        //Segment 02
        s02.setHeight(148);
        s02.setId("02");
        s02.setLength(3.2);
        s02.setMaxVelocity(90);
        s02.setMinVelocity(0);
        s02.setNumberVehicles(20);
        s02.setSlope(15);

        //Segment 03
        s03.setHeight(148);
        s03.setId("03");
        s03.setLength(3.2);
        s03.setMaxVelocity(90);
        s03.setMinVelocity(0);
        s03.setNumberVehicles(20);
        s03.setSlope(15);

        //Segment 04
        s04.setHeight(148);
        s04.setId("04");
        s04.setLength(3.2);
        s04.setMaxVelocity(90);
        s04.setMinVelocity(0);
        s04.setNumberVehicles(20);
        s04.setSlope(15);

        ArrayList<Segment> segmentList = new ArrayList<>();
        segmentList.add(s01);
        segmentList.add(s02);
        ArrayList<Segment> segmentList2 = new ArrayList<>();
        segmentList2.add(s03);
        segmentList2.add(s04);

        //Section E01
        sa.setBeginNode(n01);
        sa.setEndNode(n02);
        sa.setDirection("bidirectional");
        Road e01 = new Road();
        e01.setName("E01");
        sa.setRoad(e01);
        sa.setSegmentList(segmentList);
        sa.setToll(0);
        sa.setType("regular road");
        sa.setWindDirection(20.0);
        sa.setWindSpeed(3);

        //Section E02
        sa1.setBeginNode(n02);
        sa1.setEndNode(n03);
        sa1.setDirection("bidirectional");
        Road e02 = new Road();
        e02.setName("E02");
        sa1.setRoad(e02);
        sa1.setSegmentList(segmentList2);
        sa1.setToll(0);
        sa1.setType("regular road");
        sa1.setWindDirection(20.0);
        sa1.setWindSpeed(3);

        // Road Network
        Graph<Junction, List<Section>> a = rn.getGraph();
        a.insertVertex(n01);
        a.insertVertex(n02);
        a.insertVertex(n03);
        List<Section> sectionList = new ArrayList<>();
        sectionList.add(sa);
        sectionList.add(sa1);
        a.insertEdge(n01, n02, sectionList, 10);
        a.insertEdge(n02, n03, sectionList, 10);
        rn.setRoadNetwork(a);
        try {
            data.readFile(f, vl);
            data.readFile(file, rn);
        } catch (Exception ex) {
            System.out.println(ex);
        }
      
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSection method, of class Result.
     */
    @Test
    public void testGetSetSection() {
        System.out.println("getSetSection");
        Result instance = new Result();
        instance.setSection(sa);
        Section expResult = sa;
        Section result = instance.getSection();
        assertEquals(expResult, result);

    }

    /**
     * Test of getSegment method, of class Result.
     */
    @Test
    public void testGetSetSegment() {
        System.out.println("getSetSegment");
        Result instance = new Result();
        instance.setSegment(s01);
        Segment expResult = s01;
        Segment result = instance.getSegment();
        assertEquals(expResult, result);

    }

    /**
     * Test of getIntantIn method, of class Result.
     */
    @Test
    public void testGetSetIntantIn() {
        System.out.println("getSetIntantIn");
        Result instance = new Result();
        instance.setIntantIn(10.0);
        double expResult = 10.0;
        double result = instance.getIntantIn();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getInstantOut method, of class Result.
     */
    @Test
    public void testGetSetInstantOut() {
        System.out.println("getSetInstantOut");
        Result instance = new Result();
        instance.setInstantOut(10.0);
        double expResult = 10.0;
        double result = instance.getInstantOut();
        assertEquals(expResult, result, 0.0);

    }

    

    /**
     * Test of getEnergySpent method, of class Result.
     */
    @Test
    public void testGetSetEnergySpent() {
        System.out.println("getSetEnergySpent");
        Result instance = new Result();
        instance.setEnergySpent(10.0);
        double expResult = 10.0;
        double result = instance.getEnergySpent();
        assertEquals(expResult, result, 0.0);

    }

 

    /**
     * Test of equals method, of class Result.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Result obj = new Result();
        obj.setSection(sa);
        obj.setSegment(s01);
        Result instance = new Result();
        instance.setSection(sa);
        instance.setSegment(s01);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Result.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Result instance = new Result();
        instance.setEnergySpent(1);
        instance.setInstantOut(10);
        instance.setIntantIn(5);
        instance.setSection(sa);
        instance.setSegment(s01);
        String expResult = "Section: E01 Segment: 01 Instant In: 5.0 Instant Out: 10.0 Energy Spent: 1.0";
        String result = instance.toString();
        System.out.println(result);
        assertEquals(expResult, result);

    }
    
}
