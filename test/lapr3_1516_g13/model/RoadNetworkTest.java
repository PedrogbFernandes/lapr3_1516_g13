/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import Graph.Graph;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class RoadNetworkTest {
    
    public RoadNetworkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getGraph method, of class RoadNetwork.
     */
    @Test
    public void testGetSetGraph() {
        System.out.println("getSetGraph");
        RoadNetwork instance = new RoadNetwork();
        Graph<Junction, List<Section>> roadNetwork = new Graph(true);
        instance.setRoadNetwork(roadNetwork);
        Graph<Junction, List<Section>> expResult = roadNetwork;
        Graph<Junction, List<Section>> result = instance.getGraph();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRoadList method, of class RoadNetwork.
     */
    @Test
    public void testGetSetRoadList() {
        System.out.println("getSetRoadList");
        RoadNetwork instance = new RoadNetwork();
        List<Road> expResult = new ArrayList<>();
        Road r = new Road();
        expResult.add(r);
        instance.setRoadList(expResult);
        List<Road> result = instance.getRoadList();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRoadNetwork method, of class RoadNetwork.
     */
    @Test
    public void testSetRoadNetwork() {
        System.out.println("setRoadNetwork");
        Graph<Junction, List<Section>> roadNetwork = null;
        RoadNetwork instance = new RoadNetwork();
        instance.setRoadNetwork(roadNetwork);

    }

    /**
     * Test of addRoad method, of class RoadNetwork.
     */
    @Test
    public void testAddRoad() {
        System.out.println("addRoad");
        Road road = new Road();
        RoadNetwork instance = new RoadNetwork();
        boolean expResult = true;
        boolean result = instance.addRoad(road);
        assertEquals(expResult, result);
 
    }

    /**
     * Test of getRoad method, of class RoadNetwork.
     */
    @Test
    public void testGetRoad() {
        System.out.println("getRoad");
        Road road = new Road();
        road.setName("road");
        List<Road> r = new ArrayList();
        r.add(road);
        
        RoadNetwork instance = new RoadNetwork();
        instance.setRoadList(r);
        Road expResult = road;
        Road result = instance.getRoad(road);
        assertEquals(expResult, result);

    }



    /**
     * Test of validate method, of class RoadNetwork.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        DataXML data = new DataXML();
        File file = new File("TestSet01_Network.xml");
        RoadNetwork instance = new RoadNetwork();
         try{
            data.readFile(file,instance);
        }catch(Exception ex){
            System.out.println(ex);
        }
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);

    }
    
}
