/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class RoadTest {
    
    public RoadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Road.
     */
    @Test
    public void testGetSetName() {
        System.out.println("getSetName");
        Road instance = new Road();
        instance.setName("Road");
        String expResult = "Road";
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getSectionList method, of class Road.
     */
    @Test
    public void testGetSetSectionList() {
        System.out.println("getSectionList");
        Road instance = new Road();
        Section section = new Section();
        List<Section> sl = new ArrayList();
        sl.add(section); 
        
        instance.setSectionList(sl);
        List<Section> expResult = sl;
        List<Section> result = instance.getSectionList();
        assertEquals(expResult, result);

    }



    /**
     * Test of addSection method, of class Road.
     */
    @Test
    public void testAddSection() {
        System.out.println("addSection");
        Section section = new Section();
        Road instance = new Road();
        boolean expResult = true;
        boolean result = instance.addSection(section);
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Road.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Road road = new Road();
        road.setName("Road");
        Road instance = new Road();
        instance.setName("Road");
        boolean expResult = true;
        boolean result = instance.equals(road);
        assertEquals(expResult, result);

    }
    
}
