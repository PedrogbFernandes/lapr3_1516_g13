/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import Graph.Edge;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import Graph.Graph;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class SectionTest {

    RoadNetwork r1 = new RoadNetwork();
    Junction n01 = new Junction();
    Junction n02 = new Junction();
    Segment s01 = new Segment();
    Segment s02 = new Segment();
    Section sa = new Section();
    Vehicle car = new Vehicle();
    DataXML data = new DataXML();
    File f = new File("TestSet03_VehiclesSFCGrande.xml");
    File fnetwork = new File("Test_Network.xml");
    VehicleList v = new VehicleList();

    public SectionTest() throws Exception {

        data.readFile(fnetwork, r1);
        data.readFile(f, v);
        car = v.getVehicleList().get(0);
        Iterator<Edge<Junction, List<Section>>> it = r1.getGraph().edges().iterator();
        List<Edge<Junction, List<Section>>> list = new ArrayList<>();
        while (it.hasNext()) {
            list.add(it.next());
        }
        Collections.sort(list);
        Edge<Junction, List<Section>> edge = list.get(0);
        List<Section> listsection = edge.getElement();
        sa = listsection.get(0);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of BeginNode method, of class Section.
     */
    @Test
    public void testGetBeginNode() {
        System.out.println("BeginNode");
        Junction expResult = new Junction();
        expResult.setIndex("1");
        sa.setBeginNode(expResult);
        Junction result = sa.getBeginNode();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEndNode method, of class Section.
     */
    @Test
    public void testEndNode() {
        System.out.println("EndNode");
        Junction expResult = new Junction();
        expResult.setIndex("2");
        sa.setEndNode(expResult);
        Junction result = sa.getEndNode();
        assertEquals(expResult, result);
    }

    /**
     * Test of Road method, of class Section.
     */
    @Test
    public void testRoad() {
        System.out.println("Road");
        Road r = new Road();
        r.setName("Rua do gervásio");
        sa.setRoad(r);
        Road expResult = r;
        Road result = sa.getRoad();
        assertEquals(expResult, result);
    }

    /**
     * Test of Type method, of class Section.
     */
    @Test
    public void testType() {
        System.out.println("Type");
        String expResult = "Highway to Hell";
        sa.setType(expResult);
        String result = sa.getType();
        assertEquals(expResult, result);

    }

    /**
     * Test of Direction method, of class Section.
     */
    @Test
    public void testDirection() {
        System.out.println("Direction");
        String expResult = "One way or Another";
        sa.setDirection(expResult);
        String result = sa.getDirection();
        assertEquals(expResult, result);
    }

    /**
     * Test of Toll method, of class Section.
     */
    @Test
    public void testToll() {
        System.out.println("Toll");
        double expResult = 420;
        sa.setToll(expResult);
        double result = sa.getToll();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of WindDirection method, of class Section.
     */
    @Test
    public void testWindDirection() {
        System.out.println("WindDirection");
        double expResult = 420;
        sa.setWindDirection(expResult);
        double result = sa.getWindDirection();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of WindSpeed method, of class Section.
     */
    @Test
    public void testGetWindSpeed() {
        System.out.println("WindSpeed");
        double expResult = 420;
        sa.setWindSpeed(expResult);
        double result = sa.getWindSpeed();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of SegmentList method, of class Section.
     */
    @Test
    public void testGetSegmentList() {
        System.out.println("SegmentList");

        List<Segment> expResult = new ArrayList<Segment>();
        Segment s1 = new Segment();
        Segment s2 = new Segment();

        s1.setHeight(100);
        s1.setId("100");
        s1.setLength(100);
        s1.setSlope(1);
        s1.setMaxVelocity(100);
        s1.setMinVelocity(10);
        
        s2.setHeight(200);
        s2.setId("200");
        s2.setLength(200);
        s2.setSlope(2);
        s2.setMaxVelocity(120);
        s2.setMinVelocity(20);
       
        expResult.add(s1);
        expResult.add(s2);
        sa.setSegmentList(expResult);
        List<Segment> result = sa.getSegmentList();
        assertEquals(expResult, result);

    }


    /**
     * Test of fastestTime method, of class Section.
     */
    @Test
    public void testFastestTime() {
        System.out.println("fastestTime");
        boolean direct = true;  
        double[] result = sa.fastestTime(car, direct);
        double expResultTime = 750.0;
        double expResultEnergy = 3.74632737E8;
        double resultTime = result[0];
        double resultEnergy = result[1];
        assertEquals(expResultTime, resultTime,0.1);
        assertEquals(expResultEnergy, resultEnergy,1);
        
    }

    /**
     * Test of lowestComsumption method, of class Section.
     */
    @Test
    public void testLowestComsumption() {
        System.out.println("lowestComsumption");
        boolean direct = true;  
        double[] result = sa.lowestComsumption(car, direct);
        double expResultTime = 1410.3;
        double expResultEnergy = 9.8998045E7;
        double resultTime = result[0];
        double resultEnergy = result[1];
        assertEquals(expResultTime, resultTime,0.1);
        assertEquals(expResultEnergy, resultEnergy,1);
    }

    /**
     * Test of lowestWork method, of class Section.
     */
    @Test
    public void testLowestWork() {
        System.out.println("lowestWork");
        boolean direct = true;  
        double[] result = sa.lowestWork(car, direct);
        double expResultTime = 970.0;
        double expResultEnergy = 1.6744572E8;
        double resultTime = result[0];
        double resultEnergy = result[1];
        assertEquals(expResultTime, resultTime,0.1);
        assertEquals(expResultEnergy, resultEnergy,1);
   }
    /**
     * Test of toString method, of class Section.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Section - Begin: n0, End: n1, Road: \"A01\"";
        String result = sa.toString();
        assertEquals(expResult, result);
        System.out.println(result);
        
    }

}
