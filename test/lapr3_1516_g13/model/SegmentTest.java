/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import Graph.Graph;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class SegmentTest {

    File f = new File("TestSet02_Vehicles.xml");
    File file = new File("TestSet02_Network.xml");
    VehicleList vl = new VehicleList();
    DataXML data = new DataXML();
    RoadNetwork rn = new RoadNetwork();
    Junction n01 = new Junction();
    Junction n02 = new Junction();
    Junction n03 = new Junction();
    Segment s01 = new Segment();
    Segment s02 = new Segment();
    Segment s03 = new Segment();
    Segment s04 = new Segment();
    Section sa = new Section();
    Section sa1 = new Section();

    public SegmentTest() {
        //Junctions 
        n01.setIndex("n01");
        n02.setIndex("n02");
        n03.setIndex("n03");

        //Segment 01
        s01.setHeight(100);
        s01.setId("01");
        s01.setLength(3.2);
        s01.setMaxVelocity(90);
        s01.setMinVelocity(0);
        s01.setNumberVehicles(20);
        s01.setSlope(15);

        //Segment 02
        s02.setHeight(148);
        s02.setId("02");
        s02.setLength(3.2);
        s02.setMaxVelocity(90);
        s02.setMinVelocity(0);
        s02.setNumberVehicles(20);
        s02.setSlope(15);

        //Segment 03
        s03.setHeight(148);
        s03.setId("03");
        s03.setLength(3.2);
        s03.setMaxVelocity(90);
        s03.setMinVelocity(0);
        s03.setNumberVehicles(20);
        s03.setSlope(15);

        //Segment 04
        s04.setHeight(148);
        s04.setId("04");
        s04.setLength(3.2);
        s04.setMaxVelocity(90);
        s04.setMinVelocity(0);
        s04.setNumberVehicles(20);
        s04.setSlope(15);

        ArrayList<Segment> segmentList = new ArrayList<>();
        segmentList.add(s01);
        segmentList.add(s02);
        ArrayList<Segment> segmentList2 = new ArrayList<>();
        segmentList2.add(s03);
        segmentList2.add(s04);

        //Section E01
        sa.setBeginNode(n01);
        sa.setEndNode(n02);
        sa.setDirection("bidirectional");
        Road e01 = new Road();
        e01.setName("E01");
        sa.setRoad(e01);
        sa.setSegmentList(segmentList);
        sa.setToll(0);
        sa.setType("regular road");
        sa.setWindDirection(20.0);
        sa.setWindSpeed(3);

        //Section E02
        sa1.setBeginNode(n02);
        sa1.setEndNode(n03);
        sa1.setDirection("bidirectional");
        Road e02 = new Road();
        e02.setName("E02");
        sa1.setRoad(e02);
        sa1.setSegmentList(segmentList2);
        sa1.setToll(0);
        sa1.setType("regular road");
        sa1.setWindDirection(20.0);
        sa1.setWindSpeed(3);

        // Road Network
        Graph<Junction, List<Section>> a = rn.getGraph();
        a.insertVertex(n01);
        a.insertVertex(n02);
        a.insertVertex(n03);
        List<Section> sectionList = new ArrayList<>();
        sectionList.add(sa);
        sectionList.add(sa1);
        a.insertEdge(n01, n02, sectionList, 10);
        a.insertEdge(n02, n03, sectionList, 10);
        rn.setRoadNetwork(a);
        try {
            data.readFile(f, vl);
            data.readFile(file, rn);
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Segment.
     */
    @Test
    public void testGetSetId() {
        System.out.println("getSetId");
        Segment instance = new Segment();
        instance.setId("1");
        String expResult = "1";
        String result = instance.getId();
        assertEquals(expResult, result);

    }

    /**
     * Test of getHeight method, of class Segment.
     */
    @Test
    public void testGetSetHeight() {
        System.out.println("getSetHeight");
        Segment instance = new Segment();
        instance.setHeight(1.0);
        double expResult = 1.0;
        double result = instance.getHeight();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getSlope method, of class Segment.
     */
    @Test
    public void testGetSetSlope() {
        System.out.println("getSetSlope");
        Segment instance = new Segment();
        instance.setSlope(1.0);
        double expResult = 1.0;
        double result = instance.getSlope();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getLength method, of class Segment.
     */
    @Test
    public void testGetSetLength() {
        System.out.println("getSetLength");
        Segment instance = new Segment();
        instance.setLength(1.0);
        double expResult = 1.0;
        double result = instance.getLength();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getMaxVelocity method, of class Segment.
     */
    @Test
    public void testGetSetMaxVelocity() {
        System.out.println("getSetMaxVelocity");
        Segment instance = new Segment();
        instance.setMaxVelocity(10);
        int expResult = 10;
        int result = instance.getMaxVelocity();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMinVelocity method, of class Segment.
     */
    @Test
    public void testGetSetMinVelocity() {
        System.out.println("getSetMinVelocity");
        Segment instance = new Segment();
        instance.setMinVelocity(8);
        int expResult = 8;
        int result = instance.getMinVelocity();
        assertEquals(expResult, result);

    }

    /**
     * Test of getNumberVehicles method, of class Segment.
     */
    @Test
    public void testGetSetNumberVehicles() {
        System.out.println("getSetNumberVehicles");
        Segment instance = new Segment();
        instance.setNumberVehicles(10);
        int expResult = 10;
        int result = instance.getNumberVehicles();
        assertEquals(expResult, result);

    }

    /**
     * Test of getVehicleList method, of class Segment.
     */
    @Test
    public void testGetSetVehicleList() {
        System.out.println("getSetVehicleList");
        Segment instance = new Segment();
        VehicleList v = new VehicleList();
        List<Vehicle> vl = v.getVehicleList();
        LinkedList<Vehicle> expResult = new LinkedList();
        for (Vehicle veh : vl) {
            expResult.add(veh);
        }
        instance.setVehicleList(expResult);
        LinkedList<Vehicle> result = instance.getVehicleList();
        assertEquals(expResult, result);

    }

    /**
     * Test of getVehicleWaitingList method, of class Segment.
     */
    @Test
    public void testGetSetVehicleWaitingList() {
        System.out.println("getVehicleWaitingList");
        Segment instance = new Segment();
        VehicleList v = new VehicleList();
        List<Vehicle> vl = v.getVehicleList();
        LinkedList<Vehicle> expResult = new LinkedList<>();
        for (Vehicle veh : vl) {
            expResult.add(veh);
        }
        instance.setVehicleWaitingList(expResult);
        LinkedList<Vehicle> result = instance.getVehicleWaitingList();
        assertEquals(expResult, result);

    }

    /**
     * Test of addVehicle method, of class Segment.
     */
    @Test
    public void testAddVehicle() {
        System.out.println("addVehicle");
        Vehicle vehicle = new Vehicle();
        LinkedList<Vehicle> v = new LinkedList<>();
        for (Vehicle a : vl.getVehicleList()) {
            v.add(a);
        }
        Segment instance = new Segment();
        instance.setNumberVehicles(10);
        instance.setVehicleList(v);
        boolean expResult = true;
        boolean result = instance.addVehicle(vehicle,0.0);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeVehicle method, of class Segment.
     */
    @Test
    public void testRemoveVehicle() {
        System.out.println("removeVehicle");
        Vehicle vehicle = vl.getVehicleList().get(0);
        Segment instance = s01;

        LinkedList<Vehicle> vs = new LinkedList<>();
        for (Vehicle a : vl.getVehicleList()) {
            vs.add(a);
        }
        vehicle.setTrafficId("1");
        instance.addVehicle(vehicle,0.0);

        instance.setVehicleList(vs);
        for (int i = 0; i < vs.size(); i++) {
            System.out.println(vs.get(i));
        }

        boolean expResult = true;
        boolean result = instance.removeVehicle(vehicle);
        assertEquals(expResult, result);

    }

    /**
     * Test of isEmpty method, of class Segment.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        Segment instance = new Segment();
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);

    }

    /**
     * Test of timeSegment method, of class Segment.
     */
    @Test
    public void testTimeSegment() {
//        System.out.println("timeSegment");
//        List<Road> rl = rn.getRoadList();
//
//        Vehicle car = vl.getVehicleList().get(0);
//
//        boolean direct = false;
//
//        Segment instance = s.getSegmentList().get(0);
//        double expResult = 0.0;
//        double result = instance.timeSegment(car, s, direct);
//        System.out.println(result);
//        assertEquals(expResult, result, 0.0);
//
//        Segment instance = sa1.getSegmentList().get(0);
//        double[] expResult = new double[2];
//        expResult[0] = 144.45481380268026;
//        expResult[1] = 183.08538181818184;
//        double[] result = instance.timeSegment(car, sa1, direct);
//        assertEquals(expResult[0], result[0], 0.0);
//        assertEquals(expResult[1], result[1], 0.0);

    }

}
