package lapr3_1516_g13.model;

import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcelo Oliveira
 */
public class SimulationRegisterTest {

    Simulation s1, s2, s3;
    LinkedList<Simulation> slist = new LinkedList<>();

    public SimulationRegisterTest() {

        s1 = new Simulation();
        s1.setName("Test1");
        s1.setDescription("Test1Description");
        s1.setActive(false);

        s2 = new Simulation();
        s2.setName("Test2");
        s2.setDescription("Test2Description");
        s2.setActive(false);

        s3 = new Simulation();
        s3.setName("Test3");
        s3.setDescription("Test3Description");
        s3.setActive(false);

        slist.addFirst(s3);
        slist.addFirst(s2);
        slist.addFirst(s1);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setActiveSimulation method, of class SimulationRegister.
     */
    @Test
    public void testSetActiveSimulation() {
        System.out.println("setActiveSimulation and getActiveSimulation");
        Simulation s = s2;
        SimulationRegister instance = new SimulationRegister();

        //Empty list of simulations
        instance.setActiveSimulation(s);
        assertTrue("it should be null", instance.getActiveSimulation() == null);

        //List with an active simulation
        s = s2;
        s1.setActive(true);
        instance.setListSimulation(slist);
        instance.setActiveSimulation(s);
        assertTrue("it should be p2", instance.getActiveSimulation()== s
                && s1.isActive() == false && s3.isActive() == false);

        //List with the same active simulation
        slist.clear();
        slist.addLast(s2);
        slist.addLast(s1);
        slist.addLast(s3);
        s = s2;
        instance.setListSimulation(slist);
        instance.setActiveSimulation(s);
        assertTrue("it should be p2", instance.getActiveSimulation() == s
                && s1.isActive() == false && s3.isActive() == false);

        //List without active simulation
        instance.setListSimulation(slist);
        instance.setActiveSimulation(s);
        assertTrue("it should be p2", instance.getActiveSimulation() == s
                && s1.isActive() == false && s3.isActive() == false);
    }

}
