/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import Graph.Graph;
import PathAlgorithms.FastestPathAlgorithm;
import PathAlgorithms.PathAlgorithms;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lapr3_1516_g13.controllers.RunSimulationController;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gabriel
 */
public class SimulationResultTest {

    private Simulator simulator;
    private Simulation simulation;
    private SimulationResult simulationResult;
    private ProjectRegister preg;
    private Project proj = new Project();
    private List<Traffic> traffic;

    public SimulationResultTest() {
        simulator = new Simulator();

        traffic = new ArrayList<>();
        proj.setName("P1");
        proj.setDescription("X");
        proj.setActive(true);

        DataIO data = new DataXML();
        try {
            data.readFile(new File("TestSet03_Vehicles.xml"), proj.getVehicleList());
            data.readFile(new File("TestSet03_Network.xml"), proj.getRoadNetwork());
            data.readFile(new File("TestSet03_Simulation.xml"), traffic, proj);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        preg = simulator.getProjectRegister();
        List<SimulationResult> resultado = new ArrayList();
        simulation = new Simulation("S1", "1", proj.getRoadNetwork(), resultado, traffic, true);

        simulationResult = new SimulationResult();
        simulationResult.setBestPathMethod(new FastestPathAlgorithm());
        simulationResult.setName("Run");
        simulationResult.setDuration(30);
        simulationResult.setTimeStep(1);
        simulationResult.runSimulation(traffic, proj.getRoadNetwork());

        simulation.addSimulationResult(simulationResult);

        SimulationRegister listaSimu = new SimulationRegister();
        listaSimu.addSimulation(simulation);
        proj.setSimulationList(listaSimu);
        proj.setActiveSimulation(simulation);
        proj.addSimulation(simulation);
        preg.addProject(proj);
        preg.setActiveProject(proj);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class SimulationResult.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        SimulationResult instance = new SimulationResult();
        instance.setName("Run");
        boolean expResult = true;
        boolean result = instance.equals(simulationResult);
        assertEquals(expResult, result);
    }

    /**
     * Test of runTrafficAnalysis method, of class SimulationResult.
     */
    @Test
    public void testRunTrafficAnalysis() {
        System.out.println("runTrafficAnalysis");
        StaticAnalysis analise = new StaticAnalysis();
        analise.setAlgorithm(new FastestPathAlgorithm());
        List<Traffic> traffic = new ArrayList<>();
        Graph<Junction, List<Section>> roadNetwork = simulation.getAssociatedRoadNetwork().getGraph();
        SimulationResult instance = simulationResult;
        instance.runTrafficAnalysis(analise, traffic, roadNetwork);
        for (Traffic t : traffic) {
            assertTrue("The list of traffic path is Empty", !t.getListPathTraffic().isEmpty());
        }

    }

    /**
     * Test of runSimulation method, of class SimulationResult.
     */
    @Test
    public void testRunSimulation() throws Exception {
        System.out.println("runSimulation");
        DataIO data = new DataXML();
        List<Traffic> t = new ArrayList<>();
        data.readFile(new File("TestSet04_Simulation_Test.xml"), t, proj);
        RoadNetwork roadNetwork = simulation.getAssociatedRoadNetwork();
        SimulationResult instance = new SimulationResult();
        instance.setBestPathMethod(new FastestPathAlgorithm());
        instance.setName("Run");
        instance.setDuration(10);
        instance.setTimeStep(1);
        instance.runSimulation(t, roadNetwork);
        for (VehicleResult vehicle : instance.getCreatedList()) {
            System.out.println("Vehicle: " + vehicle.getVehicleName() + " End Time:" + vehicle.getEndTime());
        }

    }

    /**
     * Test of toString method, of class SimulationResult.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SimulationResult instance = simulationResult;
        String expResult = "Result: Run - Duration: 30,00, Step: 1,00, Method: Fastest Path";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of generateAvEnergyForEachTrafficPattern method, of class - *
     * ShowResultSimulationController. -
     */
    @Test
    public void testGenerateAvEnergyForEachTrafficPattern() {
        System.out.println("\ngenerateAvEnergyForEachTrafficPattern\n");
        SimulationResult instance = simulationResult;
        System.out.println("Resultado que interessa:");
        for (String a : instance.generateAvEnergyForEachTrafficPattern(traffic)) {
            System.out.println(a);
        }

    }

    /**
     * Test of generateAvEnergyForEachTrafficPattenAllSegments method, of class
     * ShowResultSimulationController.
     */
    @Test
    public void testGenerateAvEnergyForEachTrafficPattenAllSegments() {
        System.out.println("\ntestGenerateAvEnergyForEachTrafficPattenAllSegments\n");

        SimulationResult instance = simulationResult;

        System.out.println("Resultado que interessa:");
        for (String a : instance.generateAvEnergyForEachTrafficPattenAllSegments(this.proj.getRoadNetwork(),traffic)) {
            System.out.println(a);
        }
    }

    /**
     * Test of generateAvEnergyForSpecificTrafficPatternAllSegments method, of
     * class ShowResultSimulationController.
     */
    @Test
    public void testGenerateAvEnergyForSpecificTrafficPatternAllSegments() {
        System.out.println("\ntestGenerateAvEnergyForSpecificTrafficPatternAllSegments\n");
        SimulationResult instance = simulationResult;

        for (String a : instance.generateAvEnergyForSpecificTrafficPatternAllSegments(this.proj.getRoadNetwork(),traffic, 0)) {
            System.out.println(a);
        }
    }

}
