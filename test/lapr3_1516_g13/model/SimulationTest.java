/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataIO;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 11410
 */
public class SimulationTest {

    public SimulationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Simulation.
     */
    @Test
    public void testSetandGetName() {
        System.out.println("setandgetName");
        Simulation instance = new Simulation();
        instance.setName("Test 1");
        String expResult = "Test 1";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescription method, of class Simulation.
     */
    @Test
    public void testSetandGetDescription() {
        System.out.println("setandgetDescription");
        Simulation instance = new Simulation();
        instance.setDescription("Description Test");
        String expResult = "Description Test";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAssociatedRoadNetwork method, of class Simulation.
     */
    @Test
    public void testSetandGetAssociatedRoadNetwork() {
        System.out.println("setandgetAssociatedRoadNetwork");
        Simulation instance = new Simulation();
        RoadNetwork expResult = new RoadNetwork();
        instance.setAssociatedRoadNetwork(expResult);
        RoadNetwork result = instance.getAssociatedRoadNetwork();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSimulationResultList method, of class Simulation.
     */
    @Test
    public void testSetandGetSimulationResultList() {
        System.out.println("setandgetSimulationResultList");
        Simulation instance = new Simulation();
        List<SimulationResult> expResult = new ArrayList<>();
        instance.setSimulationResultList(expResult);
        List<SimulationResult> result = instance.getSimulationResultList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTrafficList method, of class Simulation.
     */
    @Test
    public void testSetandGetTrafficList() {
        System.out.println("getTrafficList");
        Simulation instance = new Simulation();
        Traffic trafficTest0 = new Traffic();
        Traffic trafficTest1 = new Traffic();
        List<Traffic> expResult = new ArrayList<>();
        expResult.add(trafficTest0);
        expResult.add(trafficTest1);
        instance.setTrafficList(expResult);
        List<Traffic> result = instance.getTrafficList();
        assertEquals(expResult, result);
    }

    /**
     * Test of isActive method, of class Simulation.
     */
    @Test
    public void testSetandIsActive() {
        System.out.println("setandisActive");
        Simulation instance = new Simulation();
        instance.setActive(true);
        boolean expResult = true;
        boolean result = instance.isActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSimulationResults method, of class Simulation.
     */
    @Test
    public void testDeleteSimulationResults() {
        System.out.println("deleteSimulationResults");
        Simulation instance = new Simulation();
        List<SimulationResult> listTest = new ArrayList<>();
        SimulationResult simResTest = new SimulationResult();
        listTest.add(simResTest);
        instance.setSimulationResultList(listTest);
        assertEquals(1, instance.getSimulationResultList().size());
        instance.deleteSimulationResults();
        assertEquals(0, instance.getSimulationResultList().size());
    }

    /**
     * Test of validate method, of class Simulation.
     */
    @Test
    public void testValidate() throws Exception {
        System.out.println("validate");
        Simulation instance = new Simulation();
        instance.setName("Simulation 1");
        instance.setDescription("Description 1");
        File testFile = new File("Test_Network.xml");
        DataIO dataio = new DataXML();
        RoadNetwork testRoadNetwork = new RoadNetwork();
        dataio.readFile(testFile, testRoadNetwork);
        instance.setAssociatedRoadNetwork(testRoadNetwork);
        ArrayList<Traffic> testListTraffic = new ArrayList<>();
        Traffic testTraffic = new Traffic();
        testListTraffic.add(testTraffic);
        instance.setTrafficList(testListTraffic);
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);
    }

    /**
     * Test of cloneNoResults method, of class Simulation.
     */
    @Test
    public void testCloneNoResults() throws Exception {
        System.out.println("cloneNoResults");
        Simulation instance = new Simulation();
        File testFile = new File("Test_Network.xml");
        DataIO dataio = new DataXML();
        RoadNetwork testRoadNetwork = new RoadNetwork();
        dataio.readFile(testFile, testRoadNetwork);
        instance.setAssociatedRoadNetwork(testRoadNetwork);
        ArrayList<Traffic> testListTraffic = new ArrayList<>();
        Traffic testTraffic = new Traffic();
        testListTraffic.add(testTraffic);
        instance.setTrafficList(testListTraffic);
        Simulation clone = instance.cloneNoResults();
        assertEquals(instance.getAssociatedRoadNetwork(), clone.getAssociatedRoadNetwork());
        assertEquals(instance.getAssociatedRoadNetwork(), clone.getAssociatedRoadNetwork());
    }

    /**
     * Test of clone method, of class Simulation.
     */
    @Test
    public void testCloneandEquals() {
        System.out.println("clone");
        Simulation instance = new Simulation();
        instance.setName("Simulation 1");
        Simulation clone = instance.clone();
        assertEquals(true, instance.equals(clone));
    }

    /**
     * Test of toString method, of class Simulation.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Simulation instance = new Simulation();
        instance.setName("Simulation 1");
        instance.setDescription("Description 1");
        String expResult = "Simulation Simulation 1 - Description 1";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSelectedResult method, of class Simulation.
     */
    @Test
    public void testGetSelectedResult() {
        System.out.println("getSelectedResult");
        String str = "Result 1";
        SimulationResult testResult = new SimulationResult();
        testResult.setName(str);
        Simulation instance = new Simulation();
        List<SimulationResult> testListResults = new ArrayList<>();
        testListResults.add(testResult);
        instance.setSimulationResultList(testListResults);
        assertEquals(testResult, instance.getSelectedResult(testResult));
    }

    /**
     * Test of remove method, of class Simulation.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        String str = "Result 1";
        Simulation instance = new Simulation();
        SimulationResult testResult = new SimulationResult();
        testResult.setName(str);
        List<SimulationResult> testListResults = new ArrayList<>();
        testListResults.add(testResult);
        instance.setSimulationResultList(testListResults);
        assertEquals(true, instance.remove(str));
        assertEquals(false, instance.remove(str));
    }

}
