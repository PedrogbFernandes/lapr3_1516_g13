/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class SimulatorTest {
    
    public SimulatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getProjectRegister method, of class Simulator.
     */
    @Test
    public void testGetProjectRegister() {
        System.out.println("getProjectRegister");
        Simulator instance = new Simulator();
        ProjectRegister expResult = new ProjectRegister();
        ProjectRegister result = instance.getProjectRegister();
        assertEquals(expResult.getProjectList(), result.getProjectList());

    }

    /**
     * Test of getAlgorithmList method, of class Simulator.
     */
    @Test
    public void testGetAlgorithmList() {
        System.out.println("getAlgorithmList");
        Simulator instance = new Simulator();
        AlgorithmList expResult = new AlgorithmList();
        AlgorithmList result = instance.getAlgorithmList();
        assertEquals(expResult.getListOfAlgorithms().isEmpty(), result.getListOfAlgorithms().isEmpty());

    }
    
}
