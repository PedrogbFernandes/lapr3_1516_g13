/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import PathAlgorithms.FastestPathAlgorithm;
import PathAlgorithms.PathAlgorithms;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lapr3_1516_g13.datalayer.DataXML;
import Graph.Graph;
import PathAlgorithms.MostEffcientPathAlgorithm;
import PathAlgorithms.TheoreticalLowestWorkPathAlgorithm;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class StaticAnalysisTest {

    DataXML data = new DataXML();
    RoadNetwork net = new RoadNetwork();
    Vehicle car = new Vehicle();
    VehicleList v = new VehicleList();
    FastestPathAlgorithm fpa = new FastestPathAlgorithm();
    MostEffcientPathAlgorithm mea = new MostEffcientPathAlgorithm();
    TheoreticalLowestWorkPathAlgorithm lwa = new TheoreticalLowestWorkPathAlgorithm();
    Junction j1 = new Junction();
    Junction j2 = new Junction();
    Junction j3 = new Junction();
    StaticAnalysis staticAnalysis = new StaticAnalysis();

    public StaticAnalysisTest() throws Exception {
        File file = new File("Test_Network.xml");
        File file2 = new File("TestSet03_VehiclesSFCGrande.xml");
        data.readFile(file, net);
        data.readFile(file2, v);
        j1 = net.getGraph().getVertex(0).getElement();
        j2 = net.getGraph().getVertex(2).getElement();
        j3 = net.getGraph().getVertex(1).getElement();
        car = v.getVehicleList().get(0);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of Vehicle method, of class StaticAnalysis.
     */
    @Test
    public void testGetVehicle() {
        System.out.println("Vehicle");
        Vehicle expResult = car;
        staticAnalysis.setVehicle(expResult);
        Vehicle result = staticAnalysis.getVehicle();
        assertEquals(expResult, result);

    }

    /**
     * Test of Start method, of class StaticAnalysis.
     */
    @Test
    public void testStart() {
        System.out.println("Start");
        Junction expResult = j1;
        staticAnalysis.setStart(expResult);
        Junction result = staticAnalysis.getStart();
        assertEquals(expResult, result);
    }

    /**
     * Test of End method, of class StaticAnalysis.
     */
    @Test
    public void testGetEnd() {
        System.out.println("End");
        Junction expResult = j3;
        staticAnalysis.setEnd(expResult);
        Junction result = staticAnalysis.getEnd();
        assertEquals(expResult, result);
    }

    /**
     * Test of Algorithm method, of class StaticAnalysis.
     */
    @Test
    public void testGetAlgorithm() {
        System.out.println("Algorithm");
        PathAlgorithms expResult = fpa;
        staticAnalysis.setAlgorithm(fpa);
        PathAlgorithms result = staticAnalysis.getAlgorithm();
        assertEquals(expResult, result);
    }

    /**
     * Test of Tollcost method, of class StaticAnalysis.
     */
    @Test
    public void testTollcost() {
        System.out.println("Toll cost");
        double expResult = 420;
        staticAnalysis.setTollcost(expResult);
        double result = staticAnalysis.getTollcost();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Time method, of class StaticAnalysis.
     */
    @Test
    public void testTime() {
        System.out.println("Time");
        double expResult = 420;
        staticAnalysis.setTime(expResult);
        double result = staticAnalysis.getTime();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of SectionList method, of class StaticAnalysis.
     */
    @Test
    public void testSectionList() {
        System.out.println("SectionTest");
        List<Section> expResult = new ArrayList<Section>();
        staticAnalysis.setSectionList(expResult);
        List<Section> result = staticAnalysis.getSectionList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getComsumption method, of class StaticAnalysis.
     */
    @Test
    public void testComsumption() {
        System.out.println("Cosumption");
        double expResult = 420;
        staticAnalysis.setComsumption(expResult);
        double result = staticAnalysis.getComsumption();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calculate fastest path algorithm method, of class StaticAnalysis.
     */
    @Test
    public void testCalculateFPA() {
        System.out.println("calculate Fastest Path Algoritm");
        staticAnalysis.setAlgorithm(fpa);
        staticAnalysis.setStart(j1);
        staticAnalysis.setEnd(j3);
        staticAnalysis.setVehicle(car);
        staticAnalysis.calculate(net.getGraph());
        double expResultTime = 750.0;
        double expResultTollCost = 12.0;
        double expResultEnergy = 3.74632737E8;
        String expResultPath = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime = staticAnalysis.getTime();
        double resultTollCost = staticAnalysis.getTollcost();
        double resultEnergy = staticAnalysis.getComsumption();
        String resultPath = staticAnalysis.getSectionList().toString();

        assertEquals(expResultTime, resultTime, 0.1);
        assertEquals(expResultTollCost, resultTollCost, 0.0);
        assertEquals(expResultEnergy, resultEnergy, 1);
        assertEquals(expResultPath, resultPath);

    }

    /**
     * Test of calculate most effiecint path algorithm method, of class
     * StaticAnalysis.
     */
    @Test
    public void testCalculateMEA() {
        System.out.println("calculate Most Efficient Path Algorithm");
        staticAnalysis.setAlgorithm(mea);
        staticAnalysis.setStart(j1);
        staticAnalysis.setEnd(j3);
        staticAnalysis.setVehicle(car);
        staticAnalysis.calculate(net.getGraph());
        double expResultTime = 1410.3;
        double expResultTollCost = 12.0;
        double expResultEnergy = 9.8998045E7;
        String expResultPath = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime = staticAnalysis.getTime();
        double resultTollCost = staticAnalysis.getTollcost();
        double resultEnergy = staticAnalysis.getComsumption();
        String resultPath = staticAnalysis.getSectionList().toString();

        assertEquals(expResultTime, resultTime, 0.1);
        assertEquals(expResultTollCost, resultTollCost, 0.0);
        assertEquals(expResultEnergy, resultEnergy, 1);
        assertEquals(expResultPath, resultPath);

    }

    /**
     * Test of calculate lowest work path metohd, of class StaticAnalysis.
     */
    @Test
    public void testCalculateLWA() {

        System.out.println("calculate Lowest Work Algorithm");
        staticAnalysis.setAlgorithm(lwa);
        staticAnalysis.setStart(j1);
        staticAnalysis.setEnd(j3);
        staticAnalysis.setVehicle(car);
        staticAnalysis.calculate(net.getGraph());
        double expResultTime = 970.0;
        double expResultTollCost = 12.0;
        double expResultEnergy = 1.6744572E8;
        String expResultPath = "[Section - Begin: n0, End: n1, Road: \"A01\"]";

        double resultTime = staticAnalysis.getTime();
        double resultTollCost = staticAnalysis.getTollcost();
        double resultEnergy = staticAnalysis.getComsumption();
        String resultPath = staticAnalysis.getSectionList().toString();

        assertEquals(expResultTime, resultTime, 0.1);
        assertEquals(expResultTollCost, resultTollCost, 0.0);
        assertEquals(expResultEnergy, resultEnergy, 1);
        assertEquals(expResultPath, resultPath);

    }

    /**
     * Test of toString method, of class StaticAnalysis.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        staticAnalysis.setStart(j1);
        staticAnalysis.setEnd(j3);
        staticAnalysis.setAlgorithm(fpa);
        staticAnalysis.setComsumption(420);
        staticAnalysis.setTime(420);
        staticAnalysis.setTollcost(420);
        staticAnalysis.setVehicle(car);
        String expResult = "Static Analysis Information: \n"
                + "Fastest Path\n"
                + "From: Junction: n0\n"
                + "To: Junction: n1\n"
                + "\n"
                + "Vehicle: Dummy01-gasoline\n"
                + "\n"
                + "Path: \n"
                + "\n"
                + "Toll Costs: 420.0 $\n"
                + "Driving Time: 420,0 s\n"
                + "Energy Comsumption: 420,0 W";
        String result = staticAnalysis.toString();
        assertEquals(expResult, result);

    }

}
