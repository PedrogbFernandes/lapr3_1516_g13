/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class TrafficTest {
    
    public TrafficTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEnterNode method, of class Traffic.
     */
    @Test
    public void testGetEnterNode() {
        System.out.println("getEnterNode");
        Traffic instance = new Traffic();
        Junction j = new Junction();
        j.setIndex("1");
        instance.setEnterNode(j);
        Junction expResult = j;
        Junction result = instance.getEnterNode();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getExitNode method, of class Traffic.
     */
    @Test
    public void testGetExitNode() {
        System.out.println("getExitNode");
        Traffic instance = new Traffic();
        Junction j= new Junction();
        Junction expResult = j;
        instance.setExitNode(j);
        Junction result = instance.getExitNode();
        assertEquals(expResult, result);

    }

    /**
     * Test of getVehicle method, of class Traffic.
     */
    @Test
    public void testGetVehicle() {
        System.out.println("getVehicle");
        Traffic instance = new Traffic();
        Vehicle v = new Vehicle();
        instance.setVehicle(v);
        Vehicle expResult = v;
        Vehicle result = instance.getVehicle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getArrivalRate method, of class Traffic.
     */
    @Test
    public void testGetArrivalRate() {
        System.out.println("getArrivalRate");
        Traffic instance = new Traffic();
        instance.setArrivalRate(1);
        int expResult = 1;
        int result = instance.getArrivalRate();
        assertEquals(expResult, result);

    }

    /**
     * Test of getListPathTraffic method, of class Traffic.
     */
    @Test
    public void testGetListPathTraffic() {
        System.out.println("getListPathTraffic");
        Traffic instance = new Traffic();
        Road r = new Road();        
        List<Section> expResult = r.getSectionList();
        instance.setListPathTraffic(expResult);
        List<Section> result = instance.getListPathTraffic();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of getNextTimeEnter method, of class Traffic.
     */
    @Test
    public void testGetNextTimeEnter() {
        System.out.println("getNextTimeEnter");
        Traffic instance = new Traffic();
        instance.setNextTimeEnter(1.0);
        double expResult = 1.0;
        double result = instance.getNextTimeEnter();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setEnterNode method, of class Traffic.
     */
    @Test
    public void testSetEnterNode() {
        System.out.println("setEnterNode");
        Junction enterNode = new Junction();
        Traffic instance = new Traffic();
        instance.setEnterNode(enterNode);
       
    }

    /**
     * Test of setExitNode method, of class Traffic.
     */
    @Test
    public void testSetExitNode() {
        System.out.println("setExitNode");
        Junction exitNode = new Junction();
        Traffic instance = new Traffic();
        instance.setExitNode(exitNode);

    }

    /**
     * Test of setVehicle method, of class Traffic.
     */
    @Test
    public void testSetVehicle() {
        System.out.println("setVehicle");
        Vehicle vehicle = new Vehicle();
        Traffic instance = new Traffic();
        instance.setVehicle(vehicle);

    }

    /**
     * Test of setArrivalRate method, of class Traffic.
     */
    @Test
    public void testSetArrivalRate() {
        System.out.println("setArrivalRate");
        int arrivalRate = 1;
        Traffic instance = new Traffic();
        instance.setArrivalRate(arrivalRate);

    }

    /**
     * Test of setListPathTraffic method, of class Traffic.
     */
    @Test
    public void testSetListPathTraffic() {
        System.out.println("setListPathTraffic");
        Road r = new Road();
        List<Section> listPathTraffic = r.getSectionList();
        Traffic instance = new Traffic();
        instance.setListPathTraffic(listPathTraffic);

    }

    /**
     * Test of setNextTimeEnter method, of class Traffic.
     */
    @Test
    public void testSetNextTimeEnter() {
        System.out.println("setNextTimeEnter");
        double nextTimeEnter = 1.0;
        Traffic instance = new Traffic();
        instance.setNextTimeEnter(nextTimeEnter);

    }

    /**
     * Test of timeInterval method, of class Traffic.
     */
    @Test
    public void testTimeInterval() {
        System.out.println("timeInterval");
        Traffic instance = new Traffic();
        double expResult = 0.0;
        double result = instance.timeInterval();
        assertEquals(expResult, result, 0.0);
        System.out.println("Unable to test the method \"timeInterval\" due to a random distribution value.");
    }
    
}
