/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class VehicleListTest {
    
    public VehicleListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getVehicleList method, of class VehicleList.
     */
    @Test
    public void testGetVehicleList() {
        System.out.println("getVehicleList");
        VehicleList instance = new VehicleList();
        List<Vehicle> expResult = new ArrayList();
        List<Vehicle> result = instance.getVehicleList();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of addVehicle method, of class VehicleList.
     */
    @Test
    public void testAddVehicle() {
        System.out.println("addVehicle");
        Vehicle a = new Vehicle();
        VehicleList instance = new VehicleList();
        instance.addVehicle(a);

    }

    /**
     * Test of getVehicle method, of class VehicleList.
     */
    @Test
    public void testGetVehicle() {
        System.out.println("getVehicle");
        String name = "Carro";
        Vehicle a = new Vehicle();
        a.setName(name);
        VehicleList instance = new VehicleList();
        instance.addVehicle(a);
        Vehicle expResult = a;
        Vehicle result = instance.getVehicle(name);
        assertEquals(expResult, result);

    }

    /**
     * Test of validate method, of class VehicleList.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        VehicleList instance = new VehicleList();
        Vehicle a = new Vehicle();
        instance.addVehicle(a);
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);

    }
    
}
