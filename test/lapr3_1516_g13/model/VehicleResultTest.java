/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import Graph.Graph;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class VehicleResultTest {
    File f = new File("TestSet03_VehiclesSFCGrande.xml");
    File file = new File("TestSet02_Network.xml");
    VehicleList vl = new VehicleList();
    DataXML data = new DataXML();
    RoadNetwork rn = new RoadNetwork();
    Junction n01 = new Junction();
    Junction n02 = new Junction();
    Junction n03 = new Junction();
    Segment s01 = new Segment();
    Segment s02 = new Segment();
    Segment s03 = new Segment();
    Segment s04 = new Segment();
    Section sa = new Section();
    Section sa1 = new Section();
    Traffic t = new Traffic();
    
    public VehicleResultTest() {
         //Junctions 
        n01.setIndex("n01");
        n02.setIndex("n02");
        n03.setIndex("n03");

        //Segment 01
        s01.setHeight(100);
        s01.setId("01");
        s01.setLength(3.2);
        s01.setMaxVelocity(90);
        s01.setMinVelocity(0);
        s01.setNumberVehicles(20);
        s01.setSlope(15);

        //Segment 02
        s02.setHeight(148);
        s02.setId("02");
        s02.setLength(3.2);
        s02.setMaxVelocity(90);
        s02.setMinVelocity(0);
        s02.setNumberVehicles(20);
        s02.setSlope(15);

        //Segment 03
        s03.setHeight(148);
        s03.setId("03");
        s03.setLength(3.2);
        s03.setMaxVelocity(90);
        s03.setMinVelocity(0);
        s03.setNumberVehicles(20);
        s03.setSlope(15);

        //Segment 04
        s04.setHeight(148);
        s04.setId("04");
        s04.setLength(3.2);
        s04.setMaxVelocity(90);
        s04.setMinVelocity(0);
        s04.setNumberVehicles(20);
        s04.setSlope(15);

        ArrayList<Segment> segmentList = new ArrayList<>();
        segmentList.add(s01);
        segmentList.add(s02);
        ArrayList<Segment> segmentList2 = new ArrayList<>();
        segmentList2.add(s03);
        segmentList2.add(s04);

        //Section E01
        sa.setBeginNode(n01);
        sa.setEndNode(n02);
        sa.setDirection("bidirectional");
        Road e01 = new Road();
        e01.setName("E01");
        sa.setRoad(e01);
        sa.setSegmentList(segmentList);
        sa.setToll(0);
        sa.setType("regular road");
        sa.setWindDirection(20.0);
        sa.setWindSpeed(3);

        //Section E02
        sa1.setBeginNode(n02);
        sa1.setEndNode(n03);
        sa1.setDirection("bidirectional");
        Road e02 = new Road();
        e02.setName("E02");
        sa1.setRoad(e02);
        sa1.setSegmentList(segmentList2);
        sa1.setToll(0);
        sa1.setType("regular road");
        sa1.setWindDirection(20.0);
        sa1.setWindSpeed(3);

        // Road Network
        Graph<Junction, List<Section>> a = rn.getGraph();
        a.insertVertex(n01);
        a.insertVertex(n02);
        a.insertVertex(n03);
        List<Section> sectionList = new ArrayList<>();
        sectionList.add(sa);
        sectionList.add(sa1);
        a.insertEdge(n01, n02, sectionList, 10);
        a.insertEdge(n02, n03, sectionList, 10);
        rn.setRoadNetwork(a);
        try {
            data.readFile(f, vl);
            data.readFile(file, rn);
        } catch (Exception ex) {
            System.out.println(ex);
        }
      
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTraffic method, of class VehicleResult.
     */
    @Test
    public void testGetSetTraffic() {
        System.out.println("getSetTraffic");
        
        VehicleResult instance = new VehicleResult(t);
        t.setEnterNode(n01);
        t.setExitNode(n02);
        Traffic expResult =t;
        instance.setTraffic(t);
        Traffic result = instance.getTraffic();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEndTime method, of class VehicleResult.
     */
    @Test
    public void testGetSetEndTime() {
        System.out.println("getSetEndTime");
        VehicleResult instance = new VehicleResult(t);
        instance.setEndTime(10.0);
        double expResult = 10.0;
        double result = instance.getEndTime();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getTrafficId method, of class VehicleResult.
     */
    @Test
    public void testGetSetTrafficId() {
        System.out.println("getSetTrafficId");
        VehicleResult instance = new VehicleResult(t);
        instance.setTrafficId(1);
        int expResult = 1;
        int result = instance.getTrafficId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSegmentsResults method, of class VehicleResult.
     */
    @Test
    public void testGetSetSegmentsResults() {
        System.out.println("getSetSegmentsResults");
        VehicleResult instance = new VehicleResult(t);
        LinkedList<Result> sl = new LinkedList();
        Result r = new Result();
        sl.add(r);
        instance.setSegmentsResults(sl);
        LinkedList<Result> expResult = sl;
        LinkedList<Result> result = instance.getSegmentsResults();
        assertEquals(expResult, result);

    }

    /**
     * Test of getVehicleName method, of class VehicleResult.
     */
    @Test
    public void testGetSetVehicleName() {
        System.out.println("getSetVehicleName");
        VehicleResult instance = new VehicleResult(t);
        instance.setVehicleName("Car");
        String expResult = "Car";
        String result = instance.getVehicleName();
        assertEquals(expResult, result);
    
    }

    
    /**
     * Test of addResult method, of class VehicleResult.
     */
    @Test
    public void testAddResult() {
        System.out.println("addResult");
        Result a = new Result();
        VehicleResult instance =  new VehicleResult(t);
        instance.addResult(a);

    }

    /**
     * Test of compareTo method, of class VehicleResult.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        
        VehicleResult o = new VehicleResult(t);
        VehicleResult instance =  new VehicleResult(t);
        o.setTrafficId(1);
        instance.setTrafficId(1);
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class VehicleResult.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        VehicleResult instance =  new VehicleResult(t);
        instance.setEndTime(10);
         LinkedList<Result> sl = new LinkedList();
        Result r = new Result();
        sl.add(r);
        instance.setSegmentsResults(sl);
        Vehicle car = new Vehicle();
        car.setName("Car");
        t.setVehicle(car);
        t.setEnterNode(n01);
        t.setExitNode(n02);
        instance.setTraffic(t);
        instance.setTrafficId(1);
        instance.setVehicleName("Car");
        
        String expResult = "Vehicle name Car Enter node: n01 Exit node: n02 Instant dropped out: 10.0";
        String result = instance.toString();
        System.out.println(result);
        assertEquals(expResult, result);

    }
    
}
