/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr3_1516_g13.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lapr3_1516_g13.datalayer.DataXML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class VehicleTest {

    VehicleList v = new VehicleList();
    DataXML data = new DataXML();
    File f = new File("TestSet01_Vehicles.xml");
    Vehicle car = new Vehicle();

    public VehicleTest() throws Exception {
        data.readFile(f, v);
        car = v.getVehicleList().get(0);
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Vehicle.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Dummy01 - Dummy teste vehicle";
        String result = car.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of calculateCarFowardForce method, of class Vehicle.
     */
    @Test
    public void testCalculateCarFowardForce() {
        System.out.println("calculateCarFowardForce");
        int gear = 1;
        int rpm = car.getMax_rpm();
        double trotle = Vehicle.MAX_TROTLE;

        double expResult = 6916.0;
        double result = car.calculateCarFowardForce(gear, rpm, trotle);
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of calculateCarRollingForce method, of class Vehicle.
     */
    @Test
    public void testCalculateCarRollingForce() {
        System.out.println("calculateCarRollingForce");
        double angle = 15;
        double expResult = 144.03;
        double result = car.calculateCarRollingForce(angle);
        assertEquals(expResult, result, 0.01);

    }

    /**
     * Test of calculateCarAirDragForce method, of class Vehicle.
     */
    @Test
    public void testCalculateCarAirDragForce() {
        System.out.println("calculateCarAirDragForce");
        int gear = 1;
        int rpm = car.getMax_rpm();
        double windSpeed = 10;
        double windDirection = 20;
        double expResult = 245.43;
        double result = car.calculateCarAirDragForce(gear, rpm, windSpeed, windDirection);
        assertEquals(expResult, result, 0.01);

    }

    /**
     * Test of velocityByGearAndRpm method, of class Vehicle.
     */
    @Test
    public void testVelocityByGearAndRpm() {
        System.out.println("velocityByGearAndRpm");
        int gear = 1;
        int rpm = car.getMax_rpm();
        double expResult = 15.8;
        double result = car.velocityByGearAndRpm(gear, rpm);
        assertEquals(expResult, result, 0.1);

    }

    /**
     * Test of calculateInclinationAppliedForce method, of class Vehicle.
     */
    @Test
    public void testCalculateInclinationAppliedForce() {
        System.out.println("calculateInclinationAppliedForce");
        double angle = 15;
        double expResult = 3859.3;
        double result = car.calculateInclinationAppliedForce(angle);
        assertEquals(expResult, result, 0.1);

    }

    /**
     * Test of calculateTotalForceApplied method, of class Vehicle.
     */
    @Test
    public void testCalculateTotalForceApplied() {
        System.out.println("calculateTotalForceApplied");
        int gear = 1;
        double angle = 15;
        double trotle = Vehicle.MAX_TROTLE;
        int rpm = car.getMax_rpm();
        double windSpeed = 10;
        double windDirection = 20;
        double expResult = 2667.24;
        double result = car.calculateTotalForceApplied(gear, angle, trotle, rpm, windSpeed, windDirection);
        assertEquals(expResult, result, 0.1);
        System.out.println(result);
    }

    /**
     * Test of energyComsumption method, of class Vehicle.
     */
    @Test
    public void testEnergyComsumption() {
        System.out.println("energyComsumption");
        int gear = 0;
        int rpm = 0;
        double trotle = 0.0;
        double time = 0.0;
        double angle = 0.0;
        double distance = 0.0;
        double windSpeed = 0.0;
        double windDirection = 0.0;
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.energyComsumption(gear, rpm, trotle, time, angle, distance, windSpeed, windDirection);
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getTorqueByRpmAndTrotle method, of class Vehicle.
     */
    @Test
    public void testGetTorqueByRpmAndTrotle() {
        System.out.println("getTorqueByRpmAndTrotle");
        int rpm = 2500;
        double trotle = Vehicle.MAX_TROTLE;
        int expResult = 240;
        int result = car.getTorqueByRpmAndTrotle(rpm, trotle);
        assertEquals(expResult, result);

    }
    
    /**
     * Test of fuelOnStandBy method, of class Vehicle.
     */
    @Test
    public void testFuelOnStandBy() {
        System.out.println("fuelOnStandBy");
        double expResult = 0;
        double result = car.fuelOnStandBy((double) 1);
        assertEquals(expResult, result, 0.00);

    }

    /**
     * Test of clone method, of class Vehicle.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Vehicle instance = new Vehicle();
        Vehicle expResult = null;
        Vehicle result = instance.clone();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clone2 method, of class Vehicle.
     */
    @Test
    public void testClone2() {
        System.out.println("clone2");
        double i = 0.0;
        double initialTime = 0.0;
        Vehicle instance = new Vehicle();
        Vehicle expResult = null;
        Vehicle result = instance.clone2(i, initialTime);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculatePower method, of class Vehicle.
     */
    @Test
    public void testCalculatePower() {
        System.out.println("calculatePower");
        int rpm = 2500;
        double trotle = Vehicle.MAX_TROTLE;
        double expResult = 62831.85;
        double result = car.calculatePower(rpm, trotle);
        assertEquals(expResult, result, 0.1);

    }

    /**
     * Test of calculateDissipativeForces method, of class Vehicle.
     */
    @Test
    public void testCalculateDissipativeForces() {
        System.out.println("calculateDissipativeForces");
        double angle = 15;
        int gear = 1;
        int rpm = car.getMax_rpm();
        double windSpeed = 10;
        double windDirection = 20;
        double expResult = 4248.76;
        double result = car.calculateDissipativeForces(angle, gear, rpm, windSpeed, windDirection);
        assertEquals(expResult, result, 0.1);

    }

    /**
     * Test of getTrafficId method, of class Vehicle.
     */
    @Test
    public void testGetTrafficId() {
        System.out.println("getTrafficId");
        Vehicle instance = new Vehicle();
        String expResult = "";
        String result = instance.getTrafficId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTrafficId method, of class Vehicle.
     */
    @Test
    public void testSetTrafficId() {
        System.out.println("setTrafficId");
        String trafficId = "";
        Vehicle instance = new Vehicle();
        instance.setTrafficId(trafficId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class Vehicle.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Vehicle instance = new Vehicle();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInitTimeSegment method, of class Vehicle.
     */
    @Test
    public void testGetInitTimeSegment() {
        System.out.println("getInitTimeSegment");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getInitTimeSegment();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInitTimeSegment method, of class Vehicle.
     */
    @Test
    public void testSetInitTimeSegment() {
        System.out.println("setInitTimeSegment");
        double initTimeSegment = 0.0;
        Vehicle instance = new Vehicle();
        instance.setInitTimeSegment(initTimeSegment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEndTimeSegment method, of class Vehicle.
     */
    @Test
    public void testGetEndTimeSegment() {
        System.out.println("getEndTimeSegment");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getEndTimeSegment();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEndTimeSegment method, of class Vehicle.
     */
    @Test
    public void testSetEndTimeSegment() {
        System.out.println("setEndTimeSegment");
        double endTimeSegment = 0.0;
        Vehicle instance = new Vehicle();
        instance.setEndTimeSegment(endTimeSegment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTimeStep method, of class Vehicle.
     */
    @Test
    public void testGetTimeStep() {
        System.out.println("getTimeStep");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getTimeStep();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTimeStep method, of class Vehicle.
     */
    @Test
    public void testSetTimeStep() {
        System.out.println("setTimeStep");
        double timeStep = 0.0;
        Vehicle instance = new Vehicle();
        instance.setTimeStep(timeStep);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSection method, of class Vehicle.
     */
    @Test
    public void testGetSection() {
        System.out.println("getSection");
        Vehicle instance = new Vehicle();
        int expResult = 0;
        int result = instance.getSection();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSection method, of class Vehicle.
     */
    @Test
    public void testSetSection() {
        System.out.println("setSection");
        int section = 0;
        Vehicle instance = new Vehicle();
        instance.setSection(section);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSegment method, of class Vehicle.
     */
    @Test
    public void testGetSegment() {
        System.out.println("getSegment");
        Vehicle instance = new Vehicle();
        int expResult = 0;
        int result = instance.getSegment();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSegment method, of class Vehicle.
     */
    @Test
    public void testSetSegment() {
        System.out.println("setSegment");
        int segment = 0;
        Vehicle instance = new Vehicle();
        instance.setSegment(segment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of Name method, of class Vehicle.
     */
    @Test
    public void testName() {
        System.out.println("Name");
        String expResult = "Car";
        car.setName(expResult);
        String result = car.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of Description method, of class Vehicle.
     */
    @Test
    public void testDescription() {
        System.out.println("Description");
        String expResult = "Car";
        car.setDescription(expResult);
        String result = car.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of Type method, of class Vehicle.
     */
    @Test
    public void testType() {
        System.out.println("Type");
        String expResult = "Car";
        car.setType(expResult);
        String result = car.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of Motorization method, of class Vehicle.
     */
    @Test
    public void testMotorization() {
        System.out.println("Motorization");
        String expResult = "Car";
        car.setMotorization(expResult);
        String result = car.getMotorization();
        assertEquals(expResult, result);
    }

    /**
     * Test of Fuel method, of class Vehicle.
     */
    @Test
    public void testFuel() {
        System.out.println("Fuel");
        String expResult = "Oil";
        car.setFuel(expResult);
        String result = car.getFuel();
        assertEquals(expResult, result);
    }

    /**
     * Test of Mass method, of class Vehicle.
     */
    @Test
    public void testMass() {
        System.out.println("Mass");
        double expResult = 420;
        car.setMass(expResult);
        double result = car.getMass();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLoad method, of class Vehicle.
     */
    @Test
    public void testGetLoad() {
        System.out.println("Load");
        double expResult = 420;
        car.setLoad(expResult);
        double result = car.getLoad();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of DragCoefficient method, of class Vehicle.
     */
    @Test
    public void testDragCoefficient() {
        System.out.println("Load");
        double expResult = 420;
        car.setLoad(expResult);
        double result = car.getLoad();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Rrc method, of class Vehicle.
     */
    @Test
    public void testRrc() {
        System.out.println("Rrc");
        double expResult = 420;
        car.setRrc(expResult);
        double result = car.getRrc();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Frontal_area method, of class Vehicle.
     */
    @Test
    public void testGetFrontal_area() {
        System.out.println("Frontal Area");
        double expResult = 420;
        car.setFrontal_area(expResult);
        double result = car.getFrontal_area();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Wheel_size method, of class Vehicle.
     */
    @Test
    public void testWheel_size() {
        System.out.println("Wheel_size");
        double expResult = 420;
        car.setFrontal_area(expResult);
        double result = car.getFrontal_area();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Velocity_limit_list method, of class Vehicle.
     */
    @Test
    public void testVelocity_limit_list() {
        System.out.println("Velocity_limit_list");
        Map<String, Integer> expResult = new HashMap<>();
        expResult.put("Highway", 420);
        car.setVelocity_limit_list(expResult);
        Map<String, Integer> result = car.getVelocity_limit_list();
        assertEquals(expResult, result);

    }

    /**
     * Test of Min_rpm method, of class Vehicle.
     */
    @Test
    public void testMin_rpm() {
        System.out.println("Min_Rpm");
        int expResult = 420;
        car.setMin_rpm(expResult);
        int result = car.getMin_rpm();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Max_rpm method, of class Vehicle.
     */
    @Test
    public void testGetMax_rpm() {
        System.out.println("Max_Rpm");
        int expResult = 420;
        car.setMax_rpm(expResult);
        int result = car.getMax_rpm();
        assertEquals(expResult, result);
    }

    /**
     * Test of Final_drive_ratio method, of class Vehicle.
     */
    @Test
    public void testFinal_drive_ratio() {
        System.out.println("Final_drive_ratio");
        double expResult = 420;
        car.setFinal_drive_ratio(expResult);
        double result = car.getFinal_drive_ratio();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of Gear_ratio method, of class Vehicle.
     */
    @Test
    public void testGetGear_ratio() {
        System.out.println("Gear_ratio");
        List<Double> expResult = new ArrayList<>();
        expResult.add((double) 2);
        expResult.add((double) 4);
        expResult.add((double) 8);
        expResult.add((double) 16);
        car.setGear_ratio(expResult);
        List<Double> result = car.getGear_ratio();
        assertEquals(expResult, result);

    }

    /**
     * Test of Throttle_list method, of class Vehicle.
     */
    @Test
    public void testThrottle_list() {
        System.out.println("Throttle_list");
        Map<Double, List<Regime>> expResult = new HashMap<>();
        Regime r = new Regime();
        r.setRpm_high(2000);
        r.setRpm_low(2000);
        r.setSfc(2000);
        r.setTorque(2000);
        List<Regime> list = new ArrayList<>();
        list.add(r);
        expResult.put((double) 1,list);
        car.setThrottle_list(expResult);
        Map<Double, List<Regime>> result = car.getThrottle_list();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getInitialTime method, of class Vehicle.
     */
    @Test
    public void testGetInitialTime() {
        System.out.println("getInitialTime");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getInitialTime();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEndTime method, of class Vehicle.
     */
    @Test
    public void testGetEndTime() {
        System.out.println("getEndTime");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getEndTime();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
